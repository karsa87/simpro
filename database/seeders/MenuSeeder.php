<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = Menu::updateOrCreate([
            'name' => 'Settings',
            'url' => 'settings',
        ], [
            'order' => 0,
            'parent_id' => null,
            'icon' => "fas fa-cog",
            'route' => 'setting',
            'is_parent' => Menu::IS_PARENT
        ]);
        $setting->save();

        $this->createMenuSetting($setting);

        $master = Menu::updateOrCreate([
            'url' => 'master',
            'name' => 'Master'
        ], [
            'order' => 1,
            'parent_id' => null,
            'icon' => "fas fa-box",
            'route' => null,
            'is_parent' => Menu::IS_PARENT
        ]);

        $transaction = Menu::updateOrCreate([
            'name' => 'Transaction',
            'url' => 'transaction',
        ], [
            'order' => 2,
            'parent_id' => null,
            'icon' => "fas fa-shopping-cart",
            'route' => null,
            'is_parent' => Menu::IS_PARENT
        ]);

        $saldo = Menu::updateOrCreate([
            'name' => 'Saldo',
            'url' => 'saldo',
        ], [
            'order' => 8,
            'parent_id' => null,
            'icon' => "fas fa-shopping-cart",
            'route' => 'saldo',
            'is_parent' => Menu::IS_PARENT
        ]);

        $finance = Menu::updateOrCreate([
            'name' => 'Finance',
            'url' => 'finance',
        ], [
            'order' => 4,
            'parent_id' => null,
            'icon' => "fas fa-shopping-cart",
            'route' => 'finance',
            'is_parent' => Menu::IS_PARENT
        ]);

        $project = Menu::updateOrCreate([
            'name' => 'Project',
            'url' => 'project',
        ], [
            'order' => 3,
            'parent_id' => null,
            'icon' => "fas fa-shopping-cart",
            'route' => 'project',
            'is_parent' => Menu::IS_PARENT
        ]);

        $arsip = Menu::updateOrCreate([
            'name' => 'Arsip',
            'url' => 'arsip',
        ], [
            'order' => 5,
            'parent_id' => null,
            'icon' => "fas fa-shopping-cart",
            'route' => 'arsip',
            'is_parent' => Menu::IS_PARENT
        ]);
    }

    private function createMenuSetting($setting)
    {
        $setting_id = $setting->id;
        $menus = [
            [
                'name' => 'Menu',
                'url' => "setting/menu",
                'icon' => "",
                'route' => "setting.menu.index",
                'order' => 1,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $setting_id,
            ], [
                'name' => 'Permission',
                'url' => "setting/permission",
                'icon' => "",
                'route' => "setting.permission.index",
                'order' => 2,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $setting_id,
            ], [
                'name' => 'Role',
                'url' => "setting/role",
                'icon' => "",
                'route' => "setting.role.index",
                'order' => 3,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $setting_id,
            ], [
                'name' => 'User',
                'url' => "setting/user",
                'icon' => "",
                'route' => "setting.user.index",
                'order' => 4,
                'is_parent' => Menu::IS_NOT_PARENT,
                'parent_id' => $setting_id,
            ],
        ];

        foreach ($menus as $menu_insert) {
            Menu::updateOrCreate([
                'route' => $menu_insert['route']
            ], $menu_insert);
        }
    }
}

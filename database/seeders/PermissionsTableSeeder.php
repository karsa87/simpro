<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Permission Types
         *
         */
        $Permissionitems = [
            [
                'Dashboard',
                'dashboard',
                ''
            ], [
                'Login',
                'login',
                ''
            ], [
                'Logout',
                'logout',
                ''
            ], [
                'Permission index',
                'setting.permission.index',
                'Can view index permission'
            ], [
                'Permission add',
                'setting.permission.create',
                'Can create permission'
            ], [
                'Permission edit',
                'setting.permission.edit',
                'can edit permission'
            ], [
                'Permission delete',
                'setting.permission.destroy',
                'can delete permission'
            ], [
                'Menu index',
                'setting.menu.index',
                'can add menu'
            ], [
                'Menu add',
                'setting.menu.create',
                'can add menu'
            ], [
                'Menu edit',
                'setting.menu.edit',
                'can edit menu'
            ], [
                'Menu delete',
                'setting.menu.destroy',
                'can delete menu'
            ], [
                'Role index',
                'setting.role.index',
                'can view role index'
            ], [
                'Role add',
                'setting.role.create',
                'can add role'
            ], [
                'Role edit',
                'setting.role.edit',
                'can edit role'
            ], [
                'Role delete',
                'setting.role.destroy',
                'can delete destroy'
            ], [
                'Role access',
                'setting.role.access',
                'Can change access role'
            ], [
                'User index',
                'setting.user.index',
                'can view user index'
            ], [
                'User add',
                'setting.user.create',
                'can add user'
            ], [
                'User edit',
                'setting.user.edit',
                'can edit user'
            ], [
                'User delete',
                'setting.user.destroy',
                'can delete user'
            ], [
                'User show detail',
                'setting.user.show',
                'Can show detail user'
            ], [
                'User change status',
                'setting.user.change.status',
                'Can show change status user'
            ],
        ];

        /*
         * Add Permission Items
         *
         */
        foreach ($Permissionitems as $Permissionitem) {
            $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
            if ($newPermissionitem === null) {
                $newPermissionitem = config('roles.models.permission')::create([
                    'name' => $Permissionitem[0],
                    'slug' => $Permissionitem[1],
                    'description' => $Permissionitem[2],
                    'model' => '',
                ]);
            }
        }
    }
}

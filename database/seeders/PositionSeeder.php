<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\Menu;
use DB;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::all();

        $positions = [
            [
                'name' => 'Direktur',
            ],
            [
                'name' => 'Karyawan',
            ],
        ];

        $input = [];
        foreach ($companies as $company) {
            $position = $positions[array_rand($positions)];
            $position['company_id'] = $company->id;

            $query = DB::table('positions')->updateOrInsert([
                'name' =>  $position['name'],
                'company_id' => $position['company_id']
            ], [
                'name' =>  $position['name'],
            ]);
        }

        $this->addMenu();
    }

    private function addMenu()
    {
        /*
         * Add Menu
         *
         */
        $menu_master = Menu::where('url', 'master')->first();
        if ($menu_master) {
            $menu = Menu::updateOrCreate([
                'route' => 'master.position.index',
            ], [
                'name' => 'Position',
                'order' => 2,
                'parent_id' => $menu_master->id,
                'url' => 'master/position',
                'icon' => "",
                'is_parent' => Menu::IS_NOT_PARENT
            ]);

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Position Index',
                    'master.position.index',
                    'Can view index position'
                ], [
                    'Position add',
                    'master.position.create',
                    'can add position'
                ], [
                    'Position edit',
                    'master.position.edit',
                    'can edit position'
                ], [
                    'Position delete',
                    'master.position.destroy',
                    'can delete position',
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::firstOrNew([
                        'slug' => $Permissionitem[1],
                    ], [
                        'name' => $Permissionitem[0],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                    $newPermissionitem->save();
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */
            $menu->permissions()->syncWithoutDetaching($permissions);

            $roles_slug = [
                'developer',
                'superuser',
                'administrator',
            ];
            foreach ($roles_slug as $slug) {
                $role = config('roles.models.role')::where('slug', '=', $slug)->first();
                if($role) {
                    DB::table('permission_role')->whereIn('permission_id', array_keys($permissions))->delete();
                    $role->permissions()->syncWithoutDetaching(array_values($permissions));

                    DB::table('menu_role')->whereIn('menu_id', [$menu->id])->delete();
                    $role->menus()->syncWithoutDetaching([$menu->id]);
                }
            }
        }
    }
}

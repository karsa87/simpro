<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\Menu;
use DB;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addMenu();
    }

    private function addMenu()
    {
        /*
         * Add Menu
         *
         */
        $menu_project = Menu::where('url', 'project')->first();
        if ($menu_project) {
            $menu = Menu::updateOrCreate([
                'route' => 'project.project.index',
            ], [
                'name' => 'Project',
                'order' => 1,
                'parent_id' => $menu_project->id,
                'url' => 'project/project',
                'icon' => "",
                'is_parent' => Menu::IS_NOT_PARENT
            ]);

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Project Active Index',
                    'project.project.index',
                    'Can view index project'
                ], [
                    'Project Active add',
                    'project.project.create',
                    'can add project shop'
                ], [
                    'Project Active detail',
                    'project.project.show',
                    'can view detail project shop'
                ], [
                    'Project Active edit',
                    'project.project.edit',
                    'can edit project shop'
                ], [
                    'Project Active delete',
                    'project.project.destroy',
                    'can delete project shop',
                ], [
                    'Project change worker',
                    'project.project.change.worker',
                    'can change project worker',
                ], [
                    'Project Index Done',
                    'project.project.index.done',
                    'can view index project done',
                ], [
                    'Project Change Done',
                    'project.project.change.status.done',
                    'can change status done',
                ], [
                    'Project Change Close',
                    'project.project.change.status.close',
                    'can change status close',
                ], [
                    'Project Change Cancel',
                    'project.project.change.status.cancel',
                    'can change status cancel',
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::firstOrNew([
                        'slug' => $Permissionitem[1],
                    ], [
                        'name' => $Permissionitem[0],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                    $newPermissionitem->save();
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */
            $menu->permissions()->syncWithoutDetaching($permissions);

            $roles_slug = [
                'developer',
                'superuser',
                'administrator',
            ];
            foreach ($roles_slug as $slug) {
                $role = config('roles.models.role')::where('slug', '=', $slug)->first();
                if($role) {
                    DB::table('permission_role')->whereIn('permission_id', array_keys($permissions))->delete();
                    $role->permissions()->syncWithoutDetaching(array_values($permissions));

                    DB::table('menu_role')->whereIn('menu_id', [$menu->id])->delete();
                    $role->menus()->syncWithoutDetaching([$menu->id]);
                }
            }
        }
    }
}

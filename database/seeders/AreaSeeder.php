<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\Menu;
use DB;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::all();

        $areas = [
            [
                'code' => 'MLG',
                'name' => 'Malang',
            ],
            [
                'code' => 'SBY',
                'name' => 'Surabaya',
            ],
            [
                'code' => 'JKT',
                'name' => 'Jakarta',
            ],
            [
                'code' => 'BDG',
                'name' => 'Bandung',
            ],
        ];

        $input = [];
        foreach ($companies as $company) {
            $area = $areas[array_rand($areas)];
            $area['company_id'] = $company->id;

            $query = DB::table('areas')->updateOrInsert([
                'code' =>  $area['code'],
                'company_id' => $area['company_id']
            ], [
                'code' =>  $area['code'],
                'name' =>  $area['name'],
            ]);
        }

        $this->addMenu();
    }

    private function addMenu()
    {
        /*
         * Add Menu
         *
         */
        $menu_master = Menu::where('url', 'master')->first();
        if ($menu_master) {
            $menu = Menu::updateOrCreate([
                'route' => 'master.area.index',
            ], [
                'name' => 'Area',
                'order' => 1,
                'parent_id' => $menu_master->id,
                'url' => 'master/area',
                'icon' => "",
                'is_parent' => Menu::IS_NOT_PARENT
            ]);

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Area Index',
                    'master.area.index',
                    'Can view index area'
                ], [
                    'Area add',
                    'master.area.create',
                    'can add area'
                ], [
                    'Area edit',
                    'master.area.edit',
                    'can edit area'
                ], [
                    'Area delete',
                    'master.area.destroy',
                    'can delete area',
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::firstOrNew([
                        'slug' => $Permissionitem[1],
                    ], [
                        'name' => $Permissionitem[0],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                    $newPermissionitem->save();
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */
            $menu->permissions()->syncWithoutDetaching($permissions);

            $roles_slug = [
                'developer',
                'superuser',
                'administrator',
            ];
            foreach ($roles_slug as $slug) {
                $role = config('roles.models.role')::where('slug', '=', $slug)->first();
                if($role) {
                    DB::table('permission_role')->whereIn('permission_id', array_keys($permissions))->delete();
                    $role->permissions()->syncWithoutDetaching(array_values($permissions));

                    DB::table('menu_role')->whereIn('menu_id', [$menu->id])->delete();
                    $role->menus()->syncWithoutDetaching([$menu->id]);
                }
            }
        }
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Role Types
         *
         */
        $RoleItems = [
            [
                'name'        => 'Developer',
                'slug'        => 'developer',
                'description' => 'Developer Role',
                'level'       => config('roles.models.role')::LEVEL_DEVELOPER,
            ],
            [
                'name'        => 'Superuser',
                'slug'        => 'superuser',
                'description' => 'Superuser Role',
                'level'       => config('roles.models.role')::LEVEL_ADMINISTRATOR_WIKA,
            ],
            [
                'name'        => 'Administrator',
                'slug'        => 'administrator',
                'description' => 'Administrator Role',
                'level'       => config('roles.models.role')::LEVEL_ADMINISTRATOR,
            ],
            [
                'name'        => 'User',
                'slug'        => 'user',
                'description' => 'User Role',
                'level'       => config('roles.models.role')::LEVEL_USER,
            ],
            [
                'name'        => 'Unverified',
                'slug'        => 'unverified',
                'description' => 'Unverified Role',
                'level'       => config('roles.models.role')::LEVEL_EMPLOYEE,
            ],
        ];

        /*
         * Add Role Items
         *
         */
        foreach ($RoleItems as $RoleItem) {
            $newRoleItem = config('roles.models.role')::where('slug', '=', $RoleItem['slug'])->first();
            if ($newRoleItem === null) {
                $newRoleItem = config('roles.models.role')::firstOrNew([
                    'slug'          => $RoleItem['slug'],
                ],[
                    'name'          => $RoleItem['name'],
                    'description'   => $RoleItem['description'],
                    'level'         => $RoleItem['level'],
                ]);

                $newRoleItem->save();
            }
        }
    }
}

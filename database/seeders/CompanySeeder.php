<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\Menu;
use DB;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = [
            [
                'name' => 'Company 1',
            ],
            [
                'name' => 'Company 2'
            ]
        ];

        foreach ($companies as $input) {
            $company = Company::firstOrNew(
                $input,
                $input
            );
            $company->save();
        }

        $this->addMenu();
    }

    private function addMenu()
    {
        /*
         * Add Menu
         *
         */
        $menu_master = Menu::where('url', 'master')->first();
        if ($menu_master) {
            $menu = Menu::updateOrCreate([
                'route' => 'accounting.chart_of_account.index',
            ], [
                'name' => 'Company',
                'order' => 0,
                'parent_id' => $menu_master->id,
                'url' => 'master/company',
                'icon' => "",
                'route' => 'master.company.index',
                'is_parent' => Menu::IS_NOT_PARENT
            ]);

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Company Index',
                    'master.company.index',
                    'Can view index company'
                ], [
                    'Company add',
                    'master.company.create',
                    'can add company'
                ], [
                    'Company edit',
                    'master.company.edit',
                    'can edit company'
                ], [
                    'Company delete',
                    'master.company.destroy',
                    'can delete company',
                ], [
                    'Company detail',
                    'master.company.show',
                    'can show detail company',
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::firstOrNew([
                        'slug' => $Permissionitem[1],
                    ], [
                        'name' => $Permissionitem[0],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                    $newPermissionitem->save();
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */
            $menu->permissions()->syncWithoutDetaching($permissions);

            $roles_slug = [
                'developer',
                'superuser',
                'administrator',
            ];
            foreach ($roles_slug as $slug) {
                $role = config('roles.models.role')::where('slug', '=', $slug)->first();
                if($role) {
                    DB::table('permission_role')->whereIn('permission_id', array_keys($permissions))->delete();
                    $role->permissions()->syncWithoutDetaching(array_values($permissions));

                    DB::table('menu_role')->whereIn('menu_id', [$menu->id])->delete();
                    $role->menus()->syncWithoutDetaching([$menu->id]);
                }
            }
        }
    }
}

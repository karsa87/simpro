<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\Menu;
use DB;

class CategoryExpendSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addMenu();
    }

    private function addMenu()
    {
        /*
         * Add Menu
         *
         */
        $menu_finance = Menu::where('url', 'finance')->first();
        if ($menu_finance) {
            $menu = Menu::updateOrCreate([
                'route' => 'finance.category_expend.index',
            ], [
                'name' => 'Category Expend',
                'order' => 0,
                'parent_id' => $menu_finance->id,
                'url' => 'finance/category-expend',
                'icon' => "",
                'is_parent' => Menu::IS_NOT_PARENT
            ]);

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Category Expend Index',
                    'finance.category_expend.index',
                    'Can view index category_expend'
                ], [
                    'Category Expend add',
                    'finance.category_expend.create',
                    'can add category_expend'
                ], [
                    'Category Expend detail',
                    'finance.category_expend.show',
                    'can view detail category_expend'
                ], [
                    'Category Expend edit',
                    'finance.category_expend.edit',
                    'can edit category_expend'
                ], [
                    'Category Expend delete',
                    'finance.category_expend.destroy',
                    'can delete category_expend',
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::firstOrNew([
                        'slug' => $Permissionitem[1],
                    ], [
                        'name' => $Permissionitem[0],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                    $newPermissionitem->save();
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */
            $menu->permissions()->syncWithoutDetaching($permissions);

            $roles_slug = [
                'developer',
                'superuser',
                'administrator',
            ];
            foreach ($roles_slug as $slug) {
                $role = config('roles.models.role')::where('slug', '=', $slug)->first();
                if($role) {
                    DB::table('permission_role')->whereIn('permission_id', array_keys($permissions))->delete();
                    $role->permissions()->syncWithoutDetaching(array_values($permissions));

                    DB::table('menu_role')->whereIn('menu_id', [$menu->id])->delete();
                    $role->menus()->syncWithoutDetaching([$menu->id]);
                }
            }
        }
    }
}

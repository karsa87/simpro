<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\Menu;
use DB;

class ProjectImplementationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addMenu();
    }

    private function addMenu()
    {
        /*
         * Add Menu
         *
         */
        $menu_project = Menu::where('route', 'project')->first();
        if ($menu_project) {
            $menu = Menu::updateOrCreate([
                'route' => 'project.project_implementation.index',
            ], [
                'name' => 'Project Implementation',
                'order' => 2,
                'parent_id' => $menu_project->id,
                'url' => 'project/project-implementation',
                'icon' => "",
                'is_parent' => Menu::IS_NOT_PARENT
            ]);

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Project Implementation Index',
                    'project.project_implementation.index',
                    'Can view index project implementation'
                ], [
                    'Project Implementation add',
                    'project.project_implementation.create',
                    'can add project implementation'
                ], [
                    'Project Implementation detail',
                    'project.project_implementation.show',
                    'can view detail project implementation'
                ], [
                    'Project Implementation detail implementation',
                    'project.project_implementation.show_implementation',
                    'can view detail project implementation'
                ], [
                    'Project Implementation edit',
                    'project.project_implementation.edit',
                    'can edit project implementation'
                ], [
                    'Project Implementation delete',
                    'project.project_implementation.destroy',
                    'can delete project implementation',
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::firstOrNew([
                        'slug' => $Permissionitem[1],
                    ], [
                        'name' => $Permissionitem[0],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                    $newPermissionitem->save();
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */
            $menu->permissions()->syncWithoutDetaching($permissions);

            $roles_slug = [
                'developer',
                'superuser',
                'administrator',
            ];
            foreach ($roles_slug as $slug) {
                $role = config('roles.models.role')::where('slug', '=', $slug)->first();
                if($role) {
                    DB::table('permission_role')->whereIn('permission_id', array_keys($permissions))->delete();
                    $role->permissions()->syncWithoutDetaching(array_values($permissions));

                    DB::table('menu_role')->whereIn('menu_id', [$menu->id])->delete();
                    $role->menus()->syncWithoutDetaching([$menu->id]);
                }
            }
        }
    }
}

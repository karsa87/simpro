<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use DB;

class FormulaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addMenu();
    }

    private function addMenu()
    {
        /*
         * Add Menu
         *
         */
        $menu_master = Menu::where('url', 'master')->first();
        if ($menu_master) {
            $menu = Menu::updateOrCreate([
                'route' => 'master.formula.index',
            ], [
                'name' => 'Formula',
                'order' => 8,
                'parent_id' => $menu_master->id,
                'url' => 'master/formula',
                'icon' => "",
                'is_parent' => Menu::IS_NOT_PARENT
            ]);

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Formula Index',
                    'master.formula.index',
                    'Can view index formula'
                ], [
                    'Formula add',
                    'master.formula.create',
                    'can add formula'
                ], [
                    'Formula detail',
                    'master.formula.show',
                    'can view detail formula'
                ], [
                    'Formula edit',
                    'master.formula.edit',
                    'can edit formula'
                ], [
                    'Formula delete',
                    'master.formula.destroy',
                    'can delete formula',
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::firstOrNew([
                        'slug' => $Permissionitem[1],
                    ], [
                        'name' => $Permissionitem[0],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                    $newPermissionitem->save();
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */
            $menu->permissions()->syncWithoutDetaching($permissions);

            $roles_slug = [
                'developer',
                'superuser',
                'administrator',
            ];
            foreach ($roles_slug as $slug) {
                $role = config('roles.models.role')::where('slug', '=', $slug)->first();
                if($role) {
                    DB::table('permission_role')->whereIn('permission_id', array_keys($permissions))->delete();
                    $role->permissions()->syncWithoutDetaching(array_values($permissions));

                    DB::table('menu_role')->whereIn('menu_id', [$menu->id])->delete();
                    $role->menus()->syncWithoutDetaching([$menu->id]);
                }
            }
        }
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MenuSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ConnectRelationshipsSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AreaSeeder::class);
        $this->call(PositionSeeder::class);
        $this->call(EmployeeSeeder::class);
        $this->call(SaldoEmployeeSeeder::class);
        $this->call(CategoryExpendSeeder::class);
        $this->call(IncentiveSeeder::class);
        $this->call(VendorSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(ExpendSeeder::class);
        $this->call(ProjectSeeder::class);
        $this->call(ProjectPaymentSeeder::class);
        $this->call(ProjectShopSeeder::class);
        $this->call(ProjectImplementationSeeder::class);
        $this->call(ArsipSeeder::class);
        $this->call(ProjectDebtPaymentSeeder::class);
        $this->call(ProjectTypeSeeder::class);
        $this->call(UnitSeeder::class);
        $this->call(MaterialSeeder::class);
        $this->call(FormulaSeeder::class);
        $this->call(ProjectSimulationSeeder::class);
        $this->call(LockDataSeeder::class);
        $this->call(FormatNumberSeeder::class);
        $this->call(ProjectVendorSeeder::class);
        $this->call(ReportSeeder::class);
        $this->call(DebtPaymentPoSeeder::class);
    }
}

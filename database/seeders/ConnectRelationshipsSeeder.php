<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use App\Models\Role;

class ConnectRelationshipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add Users Developer
         *
         */
        $permissions = config('roles.models.permission')::all()->pluck('id')->toArray();
        $role = config('roles.models.role')::where('slug', '=', 'developer')->first();
        $role->permissions()->sync($permissions);

        /*
         * Add Users Superuser
         *
         */
        $permissions = config('roles.models.permission')::whereNotIn('slug', [
            'setting.permission.create',
            'setting.permission.destroy',
            'setting.permission.edit',
            'setting.permission.index'
        ])->get()->pluck('id')->toArray();
        $role = config('roles.models.role')::where('slug', '=', 'superuser')->first();
        $role->permissions()->sync($permissions);

        /*
         * Add Users Administrator
         *
         */
        $permissions = config('roles.models.permission')::whereNotLike('slug', 'setting.')->get()->pluck('id')->toArray();
        $role = config('roles.models.role')::where('slug', '=', 'administrator')->first();
        $role->permissions()->sync($permissions);

        /*
         * Add Users User
         *
         */
        $permissions = config('roles.models.permission')::whereIn('slug', [
            'dashboard',
            'login',
            'logout'
        ])->get()->pluck('id')->toArray();
        $role = config('roles.models.role')::where('slug', '=', 'user')->first();
        $role->permissions()->sync($permissions);

        // relations menu permission
        $menus = Menu::child()->where(function($q){
            $q->whereNotIn('route', ['dashboard','login','logout']);
        })->get();

        foreach ($menus as $menu) {
            $slug = str_replace('index','',$menu->route);

            $permissions = config('roles.models.permission')::query();
            if (config('database.default') == 'mysql') {
                $permissions->where('slug', 'like', "%$slug%");
            } else {
                $permissions->where('slug', 'ilike', "%$slug%");
            }
            $permissions = $permissions->get()->pluck('id')->toArray();

            $menu->permissions()->sync($permissions);
        }

        /*
         * Add Users Developer
         *
         */
        $menus = Menu::all()->pluck('id')->toArray();
        $role = Role::where('slug', '=', 'developer')->first();
        $role->menus()->sync($menus);

        /*
         * Add Users Superuser
         *
         */
        $menus = Menu::whereNotIn('name',['Permission'])->get()->pluck('id')->toArray();
        $role = Role::where('slug', '=', 'superuser')->first();
        $role->menus()->sync($menus);

        /*
         * Add Users Superuser
         *
         */
        $menus = Menu::whereNotIn('name',['Permission', 'Setting', 'Menu', 'Role', 'User', 'Lock Data', 'Format Number'])->get()->pluck('id')->toArray();
        $role = Role::where('slug', '=', 'superuser')->first();
        $role->menus()->sync($menus);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\Menu;
use DB;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::all();

        $units = [
            [
                'name' => 'Kg',
            ],
            [
                'name' => 'Pcs',
            ],
        ];

        $input = [];
        foreach ($companies as $company) {
            $unit = $units[array_rand($units)];
            $unit['company_id'] = $company->id;

            $query = DB::table('units')->updateOrInsert([
                'name' =>  $unit['name'],
                'company_id' => $unit['company_id']
            ], [
                'name' =>  $unit['name'],
            ]);
        }

        $this->addMenu();
    }

    private function addMenu()
    {
        /*
         * Add Menu
         *
         */
        $menu_master = Menu::where('url', 'master')->first();
        if ($menu_master) {
            $menu = Menu::updateOrCreate([
                'route' => 'master.unit.index',
            ], [
                'name' => 'Unit',
                'order' => 1,
                'parent_id' => $menu_master->id,
                'url' => 'master/unit',
                'icon' => "",
                'is_parent' => Menu::IS_NOT_PARENT
            ]);

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Unit Index',
                    'master.unit.index',
                    'Can view index unit'
                ], [
                    'Unit add',
                    'master.unit.create',
                    'can add unit'
                ], [
                    'Unit edit',
                    'master.unit.edit',
                    'can edit unit'
                ], [
                    'Unit delete',
                    'master.unit.destroy',
                    'can delete unit',
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::firstOrNew([
                        'slug' => $Permissionitem[1],
                    ], [
                        'name' => $Permissionitem[0],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                    $newPermissionitem->save();
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */
            $menu->permissions()->syncWithoutDetaching($permissions);

            $roles_slug = [
                'developer',
                'superuser',
                'administrator',
            ];
            foreach ($roles_slug as $slug) {
                $role = config('roles.models.role')::where('slug', '=', $slug)->first();
                if($role) {
                    DB::table('permission_role')->whereIn('permission_id', array_keys($permissions))->delete();
                    $role->permissions()->syncWithoutDetaching(array_values($permissions));

                    DB::table('menu_role')->whereIn('menu_id', [$menu->id])->delete();
                    $role->menus()->syncWithoutDetaching([$menu->id]);
                }
            }
        }
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use DB;

class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addMenu();
    }

    private function addMenu()
    {
        /*
         * Add Menu
         *
         */
        $menu_master = Menu::where('url', 'master')->first();
        if ($menu_master) {
            $menu = Menu::updateOrCreate([
                'route' => 'master.material.index',
            ], [
                'name' => 'Material',
                'order' => 7,
                'parent_id' => $menu_master->id,
                'url' => 'master/material',
                'icon' => "",
                'is_parent' => Menu::IS_NOT_PARENT
            ]);

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Material Index',
                    'master.material.index',
                    'Can view index material'
                ], [
                    'Material add',
                    'master.material.create',
                    'can add material'
                ], [
                    'Material detail',
                    'master.material.show',
                    'can view detail material'
                ], [
                    'Material edit',
                    'master.material.edit',
                    'can edit material'
                ], [
                    'Material delete',
                    'master.material.destroy',
                    'can delete material',
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::firstOrNew([
                        'slug' => $Permissionitem[1],
                    ], [
                        'name' => $Permissionitem[0],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                    $newPermissionitem->save();
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */
            $menu->permissions()->syncWithoutDetaching($permissions);

            $roles_slug = [
                'developer',
                'superuser',
                'administrator',
            ];
            foreach ($roles_slug as $slug) {
                $role = config('roles.models.role')::where('slug', '=', $slug)->first();
                if($role) {
                    DB::table('permission_role')->whereIn('permission_id', array_keys($permissions))->delete();
                    $role->permissions()->syncWithoutDetaching(array_values($permissions));

                    DB::table('menu_role')->whereIn('menu_id', [$menu->id])->delete();
                    $role->menus()->syncWithoutDetaching([$menu->id]);
                }
            }
        }
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use DB;

class ReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add Menu
         *
         */
        $menu_report = Menu::where('url', 'report')->first();
        $menu_report_id = null;
        if ($menu_report) {
            $menu_report_id = $menu_report->id;
        } else {
            $menu_report_id = DB::table('menu')->insertGetId(
                [
                    'name' => 'Report',
                    'order' => 6,
                    'parent_id' => null,
                    'url' => 'report',
                    'icon' => "fas fa-chart-line",
                    'route' => null,
                    'is_parent' => Menu::IS_PARENT
                ]
            );
        }

        if ($menu_report_id) {
            $this->reportProject($menu_report_id);
            $this->reportProjectExpend($menu_report_id);
            $this->reportLossProfit($menu_report_id);
            $this->reportExpend($menu_report_id);
        }
    }

    private function reportProject($menu_report_id)
    {
        $menu = Menu::updateOrCreate([
            'route' => 'report.project.index',
        ], [
            'name' => 'Report Project',
            'order' => 1,
            'parent_id' => $menu_report_id,
            'url' => 'report/project',
            'icon' => "",
            'route' => 'report.project.index',
            'is_parent' => Menu::IS_NOT_PARENT
        ]);

        /*
         * Add Permission
         *
         */
        $Permissionitems = [
            [
                'Report Project Index',
                'report.project.index',
                'Can view index project'
            ], [
                'Report Project export excel',
                'report.project.export',
                'can export report project'
            ], [
                'Report Project detail',
                'report.project.show',
                'can show detail project',
            ]
        ];

        $permissions = [];
        foreach ($Permissionitems as $Permissionitem) {
            $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
            if ($newPermissionitem === null) {
                $newPermissionitem = config('roles.models.permission')::firstOrNew([
                    'slug' => $Permissionitem[1],
                ], [
                    'name' => $Permissionitem[0],
                    'description' => $Permissionitem[2],
                    'model' => '',
                ]);
                $newPermissionitem->save();
            }

            $permissions[] = $newPermissionitem->id;
        }

        /**
         * Add sync to menu
         * */
        $menu->permissions()->syncWithoutDetaching($permissions);

        $role_slug = [
            'developer',
            'administrator',
        ];

        foreach ($role_slug as $slug) {
            $role = config('roles.models.role')::where('slug', '=', $slug)->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
                $role->menus()->syncWithoutDetaching([$menu->id]);
            }
        }
    }

    private function reportProjectExpend($menu_report_id)
    {
        $menu = Menu::updateOrCreate([
            'route' => 'report.project_expend.index',
        ], [
            'name' => 'Report Project Expend',
            'order' => 2,
            'parent_id' => $menu_report_id,
            'url' => 'report/project-expend',
            'icon' => "",
            'route' => 'report.project_expend.index',
            'is_parent' => Menu::IS_NOT_PARENT
        ]);

        /*
         * Add Permission
         *
         */
        $Permissionitems = [
            [
                'Report Project Expend Index',
                'report.project_expend.index',
                'Can view index project'
            ], [
                'Report Project Expend export excel',
                'report.project_expend.export',
                'can export report project'
            ], [
                'Report Project Expend detail',
                'report.project_expend.show',
                'can show detail project',
            ]
        ];

        $permissions = [];
        foreach ($Permissionitems as $Permissionitem) {
            $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
            if ($newPermissionitem === null) {
                $newPermissionitem = config('roles.models.permission')::firstOrNew([
                    'slug' => $Permissionitem[1],
                ], [
                    'name' => $Permissionitem[0],
                    'description' => $Permissionitem[2],
                    'model' => '',
                ]);
                $newPermissionitem->save();
            }

            $permissions[] = $newPermissionitem->id;
        }

        /**
         * Add sync to menu
         * */
        $menu->permissions()->syncWithoutDetaching($permissions);

        $role_slug = [
            'developer',
            'administrator',
        ];

        foreach ($role_slug as $slug) {
            $role = config('roles.models.role')::where('slug', '=', $slug)->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
                $role->menus()->syncWithoutDetaching([$menu->id]);
            }
        }
    }

    private function reportLossProfit($menu_report_id)
    {
        $menu = Menu::updateOrCreate([
            'route' => 'report.loss_profit.index',
        ], [
            'name' => 'Report Loss Profit',
            'order' => 3,
            'parent_id' => $menu_report_id,
            'url' => 'report/loss-profit',
            'icon' => "",
            'route' => 'report.loss_profit.index',
            'is_parent' => Menu::IS_NOT_PARENT
        ]);

        /*
         * Add Permission
         *
         */
        $Permissionitems = [
            [
                'Report Loss Profit Index',
                'report.loss_profit.index',
                'Can view index loss profit'
            ], [
                'Report Loss Profit export excel',
                'report.loss_profit.export',
                'can export report loss profit'
            ], [
                'Report Loss Profit detail',
                'report.loss_profit.show',
                'can show detail loss profit',
            ]
        ];

        $permissions = [];
        foreach ($Permissionitems as $Permissionitem) {
            $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
            if ($newPermissionitem === null) {
                $newPermissionitem = config('roles.models.permission')::firstOrNew([
                    'slug' => $Permissionitem[1],
                ], [
                    'name' => $Permissionitem[0],
                    'description' => $Permissionitem[2],
                    'model' => '',
                ]);
                $newPermissionitem->save();
            }

            $permissions[] = $newPermissionitem->id;
        }

        /**
         * Add sync to menu
         * */
        $menu->permissions()->syncWithoutDetaching($permissions);

        $role_slug = [
            'developer',
            'administrator',
        ];

        foreach ($role_slug as $slug) {
            $role = config('roles.models.role')::where('slug', '=', $slug)->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
                $role->menus()->syncWithoutDetaching([$menu->id]);
            }
        }
    }

    private function reportExpend($menu_report_id)
    {
        $menu = Menu::updateOrCreate([
            'route' => 'report.loss_profit.index',
        ], [
            'name' => 'Report Loss Profit',
            'order' => 3,
            'parent_id' => $menu_report_id,
            'url' => 'report/loss-profit',
            'icon' => "",
            'route' => 'report.loss_profit.index',
            'is_parent' => Menu::IS_NOT_PARENT
        ]);

        /*
         * Add Permission
         *
         */
        $Permissionitems = [
            [
                'Report Expend Index',
                'report.expend.index',
                'Can view index expend'
            ], [
                'Report Expend export excel',
                'report.expend.export',
                'can export report expend'
            ]
        ];

        $permissions = [];
        foreach ($Permissionitems as $Permissionitem) {
            $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
            if ($newPermissionitem === null) {
                $newPermissionitem = config('roles.models.permission')::firstOrNew([
                    'slug' => $Permissionitem[1],
                ], [
                    'name' => $Permissionitem[0],
                    'description' => $Permissionitem[2],
                    'model' => '',
                ]);
                $newPermissionitem->save();
            }

            $permissions[] = $newPermissionitem->id;
        }

        /**
         * Add sync to menu
         * */
        $menu->permissions()->syncWithoutDetaching($permissions);

        $role_slug = [
            'developer',
            'administrator',
        ];

        foreach ($role_slug as $slug) {
            $role = config('roles.models.role')::where('slug', '=', $slug)->first();
            if($role) {
                $role->permissions()->syncWithoutDetaching($permissions);
                $role->menus()->syncWithoutDetaching([$menu->id]);
            }
        }
    }
}

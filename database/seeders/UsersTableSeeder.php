<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRole = config('roles.models.role')::where('slug', '=', 'user')->first();
        $adminRole = config('roles.models.role')::where('slug', '=', 'superuser')->first();
        $developerRole = config('roles.models.role')::where('slug', '=', 'developer')->first();
        $permissions = config('roles.models.permission')::all();

        /*
         * Add Users
         *
         */
        $company_ids = Company::all()->pluck('id')->toArray();
        $users = [
            [
                'name'     => 'Developer',
                'email'    => 'karsaaif87@gmail.com',
                'password' => 'karsadeveloper',
            ],
            [
                'name'     => 'Admin',
                'email'    => 'admin@admin.com',
                'password' => 'admin@12345',
            ]
        ];
        foreach ($users as $user) {
            $newUser = config('roles.models.defaultUser')::where('email', '=', $user['email'])->first();
            if ($newUser === null) {
                $newUser = config('roles.models.defaultUser')::create([
                    'name'     => $user['name'],
                    'email'    => $user['email'],
                    'password' => $user['passwod'],
                ]);
            }

            $newUser->attachRole($developerRole);
            foreach ($permissions as $permission) {
                $newUser->attachPermission($permission);
            }
            $newUser->company()->sync($company_ids);
        }
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\Menu;
use DB;

class ArsipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addMenuCreateArsip();
        $this->addMenuIndexArsip();
    }

    private function addMenuCreateArsip()
    {
        /*
         * Add Menu
         *
         */
        $menu_arsip = Menu::where('url', 'arsip')->first();
        if ($menu_arsip) {
            $menu = Menu::updateOrCreate([
                'route' => 'arsip.arsip.create',
            ], [
                'name' => 'Create Arsip',
                'order' => 0,
                'parent_id' => $menu_arsip->id,
                'url' => 'arsip/arsip/create',
                'icon' => "",
                'is_parent' => Menu::IS_NOT_PARENT
            ]);

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Create Arsip add',
                    'arsip.arsip.create',
                    'can add arsip'
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::firstOrNew([
                        'slug' => $Permissionitem[1],
                    ], [
                        'name' => $Permissionitem[0],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                    $newPermissionitem->save();
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */
            $menu->permissions()->syncWithoutDetaching($permissions);

            $roles_slug = [
                'developer',
                'superuser',
                'administrator',
            ];
            foreach ($roles_slug as $slug) {
                $role = config('roles.models.role')::where('slug', '=', $slug)->first();
                if($role) {
                    DB::table('permission_role')->whereIn('permission_id', array_keys($permissions))->delete();
                    $role->permissions()->syncWithoutDetaching(array_values($permissions));

                    DB::table('menu_role')->whereIn('menu_id', [$menu->id])->delete();
                    $role->menus()->syncWithoutDetaching([$menu->id]);
                }
            }
        }
    }

    private function addMenuIndexArsip()
    {
        /*
         * Add Menu
         *
         */
        $menu_arsip = Menu::where('url', 'arsip')->first();
        if ($menu_arsip) {
            $menu = Menu::updateOrCreate([
                'route' => 'arsip.arsip.index',
            ], [
                'name' => 'Data Arsip',
                'order' => 1,
                'parent_id' => $menu_arsip->id,
                'url' => 'arsip/arsip',
                'icon' => "",
                'is_parent' => Menu::IS_NOT_PARENT
            ]);

            /*
             * Add Permission
             *
             */
            $Permissionitems = [
                [
                    'Data Arsip Index',
                    'arsip.arsip.index',
                    'Can view index arsip'
                ], [
                    'Data Arsip detail',
                    'arsip.arsip.show',
                    'can view detail arsip'
                ], [
                    'Data Arsip edit',
                    'arsip.arsip.edit',
                    'can edit arsip'
                ], [
                    'Data Arsip delete',
                    'arsip.arsip.destroy',
                    'can delete arsip',
                ]
            ];

            $permissions = [];
            foreach ($Permissionitems as $Permissionitem) {
                $newPermissionitem = config('roles.models.permission')::where('slug', '=', $Permissionitem[1])->first();
                if ($newPermissionitem === null) {
                    $newPermissionitem = config('roles.models.permission')::firstOrNew([
                        'slug' => $Permissionitem[1],
                    ], [
                        'name' => $Permissionitem[0],
                        'description' => $Permissionitem[2],
                        'model' => '',
                    ]);
                    $newPermissionitem->save();
                }

                $permissions[] = $newPermissionitem->id;
            }

            /**
             * Add sync to menu
             * */
            $menu->permissions()->syncWithoutDetaching($permissions);

            $roles_slug = [
                'developer',
                'superuser',
                'administrator',
            ];
            foreach ($roles_slug as $slug) {
                $role = config('roles.models.role')::where('slug', '=', $slug)->first();
                if($role) {
                    DB::table('permission_role')->whereIn('permission_id', array_keys($permissions))->delete();
                    $role->permissions()->syncWithoutDetaching(array_values($permissions));

                    DB::table('menu_role')->whereIn('menu_id', [$menu->id])->delete();
                    $role->menus()->syncWithoutDetaching([$menu->id]);
                }
            }
        }
    }
}

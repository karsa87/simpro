<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_role', function (Blueprint $table) {
            $table->bigInteger('menu_id');
            $table->bigInteger('role_id');

            $table->primary(['menu_id', 'role_id']);
        });

        Schema::table('menu', function (Blueprint $table) {
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_role');

        Schema::table('menu', function (Blueprint $table) {
            $table->dropColumn(['description']);
        });
    }
}

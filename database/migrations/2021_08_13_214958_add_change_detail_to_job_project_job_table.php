<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddChangeDetailToJobProjectJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_jobs', function (Blueprint $table) {
            $table->renameColumn('detail_jobs', 'job');
            $table->text('detail')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_jobs', function (Blueprint $table) {
            $table->dropColumn(['detail']);
            $table->renameColumn('job', 'detail_job');
        });
    }
}

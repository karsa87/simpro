<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('no_project');
            $table->string('no_spk');
            $table->string('projectname');
            $table->bigInteger('customer_id')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('customer_address')->nullable();
            $table->string('customer_phone')->nullable();
            $table->text('information')->nullable();
            $table->integer('term_of_work')->default(30);
            $table->dateTime('start_date')->nullable();
            $table->dateTime('due_date')->nullable();
            $table->double('total_amount', 12, 0)->nullable();
            $table->integer('progress')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('status_payment')->default(0);

            $table->bigInteger('company_id')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}

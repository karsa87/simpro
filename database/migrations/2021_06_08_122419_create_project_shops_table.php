<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_shops', function (Blueprint $table) {
            $table->id();
            $table->string('store_name');
            $table->bigInteger('project_id');
            $table->bigInteger('employee_id');
            $table->dateTime('date');
            $table->text('information')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->double('total_amount', 12, 0)->default(0);
            $table->double('paid', 12, 0)->default(0);

            $table->bigInteger('company_id')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_shops');
    }
}

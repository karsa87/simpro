<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeProgressDoubleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->float('progress', 12, 0)->default(0)->change();
        });
        Schema::table('project_jobs', function (Blueprint $table) {
            $table->float('progress', 12, 0)->default(0)->change();
        });
        Schema::table('project_implementations', function (Blueprint $table) {
            $table->float('progress', 12, 0)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->integer('progress')->default(0)->change();
        });
        Schema::table('project_jobs', function (Blueprint $table) {
            $table->integer('progress')->default(0)->change();
        });
        Schema::table('project_implementations', function (Blueprint $table) {
            $table->integer('progress')->default(0)->change();
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectDebtPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_debt_payments', function (Blueprint $table) {
            $table->id();
            $table->string('no_transaction');
            $table->bigInteger('project_id');
            $table->bigInteger('project_shop_id');
            $table->dateTime('date');
            $table->text('information')->nullable();
            $table->double('amount', 12, 0)->nullable();

            $table->bigInteger('company_id')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_debt_payments');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormatNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('format_number', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('menu_id');
            $table->string('prefix', 10)->nullable();
            $table->string('middle', 10)->nullable();
            $table->tinyInteger('year_type')->nullable();
            $table->tinyInteger('month_type')->nullable();
            $table->tinyInteger('increment_type')->default(0);
            $table->string('delimiter', 10)->nullable('/');

            $table->bigInteger('company_id')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('format_number');
    }
}

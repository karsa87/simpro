<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormulaItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formula_items', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('formula_id');
            $table->bigInteger('material_id')->nullable();
            $table->bigInteger('unit_id')->nullable();
            $table->double('coefficient',12,3)->default(0);
            $table->double('amount', 12, 2)->default(0);

            $table->bigInteger('company_id')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formula_items');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectShopDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_shop_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('project_shop_id');
            $table->string('product_name');
            $table->text('information')->nullable();
            $table->integer('qty')->default(0);
            $table->double('price', 12, 0)->default(0);
            $table->double('total_price', 12, 0)->default(0);

            $table->bigInteger('company_id')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_shop_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectVendorJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_vendor_jobs', function (Blueprint $table) {
            $table->bigInteger('project_vendor_id');
            $table->bigInteger('project_job_id');

            $table->primary(['project_vendor_id', 'project_job_id'], "pk_project_vendor_jobs");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_vendor_jobs');
    }
}

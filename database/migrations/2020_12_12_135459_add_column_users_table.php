<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('area_id')->nullable()->after('npwp');
            $table->bigInteger('position_id')->nullable()->after('area_id');
            $table->dateTime('lastlogin')->nullable()->after('position_id');
            $table->string('lastloginip', 32)->nullable()->after('lastlogin');
            $table->text('photo_profile')->nullable()->after('lastloginip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['area_id', 'position_id', 'lastlogin', 'lastloginip', 'photo_profile']);
        });
    }
}

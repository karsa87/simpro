<?php

return [
    'name' => env('APP_NAME', ''),
    'long_name' => env('APP_LONG_NAME', ''),
    'logo_url' => env('APP_LOGO_URL', ''),
    'session' => [
        'active_company' => 'active_company',
        'list_company' => 'list_company',
        'list_lock' => 'list_lock_data',
        'list_format_number' => 'list_format_number',
        'active_language' => 'active_language',
    ],
    'cache' => [
        'notification' => 'notification-not-read-%s',
    ]
];

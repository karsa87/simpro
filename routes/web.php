<?php

use App\Models\Project;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Master\FileController;
use App\Http\Controllers\Master\AreaController;
use App\Http\Controllers\Arsip\ArsipController;
use App\Http\Controllers\Setting\MenuController;
use App\Http\Controllers\Setting\RoleController;
use App\Http\Controllers\Setting\UserController;
use App\Http\Controllers\Master\VendorController;
use App\Http\Controllers\Finance\ExpendController;
use App\Http\Controllers\Log\LogProjectController;
use App\Http\Controllers\Master\CompanyController;
use App\Http\Controllers\Master\CustomerController;
use App\Http\Controllers\Master\PositionController;
use App\Http\Controllers\Master\EmployeeController;
use App\Http\Controllers\Log\LogActivityController;
use App\Http\Controllers\Project\ProjectController;
use App\Http\Controllers\Setting\LockDataController;
use App\Http\Controllers\Finance\IncentiveController;
use App\Http\Controllers\Setting\PermissionController;
use App\Http\Controllers\Project\ProjectTypeController;
use App\Http\Controllers\Project\ProjectShopController;
use App\Http\Controllers\Report\ReportExpendController;
use App\Http\Controllers\Saldo\EmployeeSaldoController;
use App\Http\Controllers\Report\ReportProjectController;
use App\Http\Controllers\Setting\FormatNumberController;
use App\Http\Controllers\Finance\DebtPaymentPOController;
use App\Http\Controllers\Project\ProjectVendorController;
use App\Http\Controllers\Project\ProjectPaymentController;
use App\Http\Controllers\Finance\CategoryExpendController;
use App\Http\Controllers\Master\FormulaController;
use App\Http\Controllers\Master\MaterialController;
use App\Http\Controllers\Master\UnitController;
use App\Http\Controllers\Report\ReportLossProfitController;
use App\Http\Controllers\Report\ReportProjectExpendController;
use App\Http\Controllers\Project\ProjectImplementationController;
use App\Http\Controllers\Project\SimulationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['get', 'post'], 'login', [AuthController::class, 'index'])->name('login');
Route::match(['get', 'post'], 'logout', [AuthController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('change-language/{locale}', [DashboardController::class, 'change_language'])->name('change.language');

    /**
    * Setting
    * */
    Route::name('setting.')->prefix('setting')->group(function () {
        Route::name('role.')->prefix('role')->group(function(){
            Route::resource('', RoleController::class, ['parameters' => [
                '' => 'id',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ]);

            Route::post('access', [RoleController::class, 'access'])->name('access');
        });

        Route::resource('permission', PermissionController::class)->only([
            'index', 'edit', 'create', 'store', 'update', 'destroy'
        ])->names('permission');

        Route::name('user.')->prefix('user')->group(function(){
            Route::resource('', UserController::class, ['parameters' => [
                '' => 'id',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);

            Route::get('{id}/change-status/{status}', [UserController::class, 'change_status'])->name('change.status');
        });

        Route::resource('menu', MenuController::class)->only([
            'index', 'edit', 'create', 'store', 'update', 'destroy'
        ])->names('menu');

        Route::resource('lock-data', LockDataController::class)->only([
            'index', 'edit', 'create', 'store', 'update', 'destroy'
        ])->names('lock_data');

        Route::resource('format-number', FormatNumberController::class)->only([
            'index', 'edit', 'create', 'store', 'update', 'destroy'
        ])->names('format_number');
    });

    /**
    * Master
    * */
    Route::name('master.')->prefix('master')->group(function () {
        Route::name('company.')->prefix('company')->group(function () {
            Route::resource('', CompanyController::class, ['parameters' => [
                '' => 'id',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });
        Route::name('position.')->prefix('position')->group(function () {
            Route::resource('', PositionController::class, ['parameters' => [
                '' => 'id',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ]);
        });
        Route::name('area.')->prefix('area')->group(function () {
            Route::resource('', AreaController::class, ['parameters' => [
                '' => 'id',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ]);
        });
        Route::name('unit.')->prefix('unit')->group(function () {
            Route::resource('', UnitController::class, ['parameters' => [
                '' => 'id',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ]);
        });
        Route::name('employee.')->prefix('employee')->group(function () {
            Route::resource('', EmployeeController::class, ['parameters' => [
                '' => 'employee',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);

            Route::get('{id}/change-status/{status}', [EmployeeController::class, 'change_status'])->name('change.status');
        });
        Route::name('customer.')->prefix('customer')->group(function () {
            Route::resource('', CustomerController::class, ['parameters' => [
                '' => 'customer',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });
        Route::name('material.')->prefix('material')->group(function () {
            Route::resource('', MaterialController::class, ['parameters' => [
                '' => 'material',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });
        Route::name('vendor.')->prefix('vendor')->group(function () {
            Route::resource('', VendorController::class, ['parameters' => [
                '' => 'vendor',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });
        Route::name('file.')->prefix('file')->group(function () {
            // Route::resource('', FileController::class, ['parameters' => [
            //     '' => 'file',
            // ]])->only([
            //     'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            // ]);
        });
        Route::name('formula.')->prefix('formula')->group(function () {
            Route::resource('', FormulaController::class, ['parameters' => [
                '' => 'formula',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });
    });

    /**
    * Log
    * */
    Route::name('log.')->prefix('log')->group(function () {
        Route::resource('activity', LogActivityController::class)->only([
            'index'
        ])->names('activity');

        Route::get('/project', [LogProjectController::class, 'index'])->name('project.index');
    });

    Route::prefix('arsip')->name('arsip.')->group(function() {
        Route::name('arsip.')->prefix('arsip')->group(function(){
            Route::resource('', ArsipController::class,['parameters' => [
                '' => 'arsip',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });
    });

    Route::prefix('project')->name('project.')->group(function() {
        Route::name('project.')->prefix('project')->group(function(){
            Route::get('worker/{id}',[ProjectController::class, 'change_worker'])
                ->name('change.worker');
            Route::post('worker/{id}/update',[ProjectController::class, 'change_worker_update'])
                ->name('change.worker.update');

            Route::get('done',[ProjectController::class, 'index'])
                ->defaults('status', Project::STATUS_DONE)
                ->name('index.done');

            Route::get('change/{id}/done',[ProjectController::class, 'change_status'])
                ->defaults('status', Project::STATUS_DONE)
                ->name('change.status.done');
            Route::get('change/{id}/close',[ProjectController::class, 'change_status'])
                ->defaults('status', Project::STATUS_CLOSE)
                ->name('change.status.close');
            Route::get('change/{id}/cancel',[ProjectController::class, 'change_status'])
                ->defaults('status', Project::STATUS_CANCEL)
                ->name('change.status.cancel');

            Route::resource('', ProjectController::class,['parameters' => [
                '' => 'project',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });

        Route::name('project_vendor.')->prefix('project-vendor')->group(function(){
            Route::get('create/{project_id}',[ProjectVendorController::class, 'create'])->name('create');

            Route::resource('', ProjectVendorController::class,['parameters' => [
                '' => 'projectVendor',
            ]])->only([
                'index', 'edit', 'store', 'update', 'destroy', 'show'
            ]);
        });

        Route::name('project_payment.')->prefix('project-payment')->group(function(){
            Route::get('create/{project_id?}',[ProjectPaymentController::class, 'create'])->name('create');

            Route::resource('', ProjectPaymentController::class,['parameters' => [
                '' => 'projectPayment',
            ]])->only([
                'index', 'edit', 'store', 'update', 'destroy', 'show'
            ]);
        });

        Route::name('project_shop.')->prefix('project-shop')->group(function(){
            Route::get('create/{project_id?}',[ProjectShopController::class, 'create'])->name('create');

            Route::resource('', ProjectShopController::class,['parameters' => [
                '' => 'projectShop',
            ]])->only([
                'index', 'edit', 'store', 'update', 'destroy', 'show'
            ]);
        });

        Route::name('project_implementation.')->prefix('project-implementation')->group(function(){
            Route::get('implementation/{id}',[ProjectImplementationController::class, 'show_implementation'])->name('show_implementation');

            Route::resource('', ProjectImplementationController::class,['parameters' => [
                '' => 'projectImplementation',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });
        Route::name('project_type.')->prefix('project-type')->group(function () {
            Route::resource('', ProjectTypeController::class, ['parameters' => [
                '' => 'projectType',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy'
            ]);
        });
        Route::name('simulation.')->prefix('simulation')->group(function () {
            Route::resource('', SimulationController::class, ['parameters' => [
                '' => 'simulation',
            ]])->only([
                'index'
            ]);
        });
    });

    Route::prefix('saldo')->name('saldo.')->group(function() {
        Route::name('employee.')->prefix('employee')->group(function(){
            Route::resource('', EmployeeSaldoController::class,['parameters' => [
                '' => 'saldoEmployee',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });
    });

    Route::prefix('finance')->name('finance.')->group(function() {
        Route::name('category_expend.')->prefix('category-expend')->group(function(){
            Route::resource('', CategoryExpendController::class,['parameters' => [
                '' => 'categoryExpend',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });

        Route::name('debt_payment_po.')->prefix('debt-payment-po')->group(function(){
            Route::get('/index/all', [DebtPaymentPOController::class, 'index_debt'])->name('index.all');
            Route::get('create/{project_id?}',[DebtPaymentPOController::class, 'create'])->name('create');
            Route::resource('', DebtPaymentPOController::class,['parameters' => [
                '' => 'debtPayment',
            ]])->only([
                'index', 'edit', 'store', 'update', 'destroy', 'show'
            ]);
        });

        Route::name('expend.')->prefix('expend')->group(function(){
            Route::resource('', ExpendController::class,['parameters' => [
                '' => 'expend',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });

        Route::name('incentive.')->prefix('incentive')->group(function(){
            Route::resource('', IncentiveController::class,['parameters' => [
                '' => 'incentive',
            ]])->only([
                'index', 'edit', 'create', 'store', 'update', 'destroy', 'show'
            ]);
        });
    });

    Route::prefix('report')->name('report.')->group(function() {
        Route::name('project.')->prefix('project')->group(function(){
            Route::match(['get', 'post'], 'export', [ReportProjectController::class, 'export'])->name('export');
            Route::get('/', [ReportProjectController::class, 'index'])->name('index');
            Route::get('{id}', [ReportProjectController::class, 'show'])->name('show');
        });

        Route::name('project_expend.')->prefix('project-expend')->group(function(){
            Route::match(['get', 'post'], 'export', [ReportProjectExpendController::class, 'export'])->name('export');
            Route::get('/', [ReportProjectExpendController::class, 'index'])->name('index');
        });

        Route::name('expend.')->prefix('expend')->group(function(){
            Route::match(['get', 'post'], 'export', [ReportExpendController::class, 'export'])->name('export');
            Route::get('/', [ReportExpendController::class, 'index'])->name('index');
        });

        Route::name('loss_profit.')->prefix('loss-profit')->group(function(){
            Route::match(['get', 'post'], 'export', [ReportLossProfitController::class, 'export'])->name('export');
            Route::get('/', [ReportLossProfitController::class, 'index'])->name('index');
        });
    });

    /**
    * Setting Notification
    * */
    // Route::name('notification.')->prefix('notification')->group(function () {
    //     Route::resource('', NotificationController::class, ['parameters' => [
    //         '' => 'id',
    //     ]])->only([
    //         'index'
    //     ]);
    //     Route::match(['post', 'get'], 'read', [NotificationController::class, 'read'])->name('read');
    // });

    Route::name('setting.user.')->prefix('setting/')->group(function () {
        Route::any('profile', [UserController::class, 'profile'])->name('profile');
        Route::match(['post', 'delete'], 'upload', [UserController::class, 'upload'])->name('upload');
    });

    Route::get('master/company/change-active/{id}', [CompanyController::class, 'change_active_company'])->name('master.company.change.active');


    Route::name('ajax.')->prefix('ajax')->group(function(){
        Route::get('user', [UserController::class, 'ajaxUser'])->name('user');
        Route::get('user-employee', [UserController::class, 'ajaxUserEmployee'])->name('user_employee');
        Route::get('user-sales', [UserController::class, 'ajaxUserSales'])->name('user_sales');
        Route::get('user-warehouse', [UserController::class, 'ajaxUserWarehouse'])->name('user_warehouse');
        Route::get('company', [CompanyController::class, 'ajaxCompany'])->name('company');
        Route::get('position', [PositionController::class, 'ajaxPosition'])->name('position');
        Route::get('area', [AreaController::class, 'ajaxArea'])->name('area');
        Route::get('unit', [UnitController::class, 'ajaxUnit'])->name('unit');
        Route::get('vendor', [VendorController::class, 'ajaxVendor'])->name('vendor');
        Route::get('category-expend', [CategoryExpendController::class, 'ajaxCategory'])->name('category_expend');
        Route::get('project', [ProjectController::class, 'ajaxProject'])->name('project');
        Route::get('project-shop/debt', [ProjectShopController::class, 'ajaxProjectHasDebt'])->name('project_shop.debt');
        Route::get('project-job', [ProjectController::class, 'ajaxProjectJob'])->name('project.jobs');
        Route::get('project-type', [ProjectTypeController::class, 'ajaxProjectType'])->name('project_type');
    });

    Route::prefix('upload')->name('upload.')->group(function() {
        Route::post('company-logo', [CompanyController::class, 'uploadLogo'])->name('company.logo');
        Route::post('user-photo-profile', [UserController::class, 'upload'])->name('user.photo_profile');
        Route::post('employee-photo-profile', [EmployeeController::class, 'upload'])->name('employee.photo_profile');
        Route::post('file', [FileController::class, 'upload'])->name('file');
    });

    Route::get('master/file/download/{file}', [FileController::class, 'download'])->name('master.file.download');
});

/**
* Localization
* */
Route::get('/js/lang.js', function () {
    if (config('app.env') == 'local' || config('app.env') == 'development') {
        $lang = \Session::get(config('system.session.active_language'));

        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }
    } else {
        // $strings = Cache::rememberForever('lang.js', function () {
            $lang = \Session::get(config('system.session.active_language'));

            $files   = glob(resource_path('lang/' . $lang . '/*.php'));
            $strings = [];

            foreach ($files as $file) {
                $name           = basename($file, '.php');
                $strings[$name] = require $file;
            }

            // return $strings;
        // });
    }

    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');

const mix = require('laravel-mix');
const version = '/1.0.1';
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.copyDirectory('resources/assets/admin/js/application', 'public/js/admin');

mix.js('resources/js/app.js', 'public/assets/js')
    .postCss('resources/css/app.css', 'public/assets/css', [
        require('postcss-import'),
        require('tailwindcss'),
    ])
    .postCss('resources/css/login.css', 'public/assets/css', [
        require('postcss-import'),
        require('tailwindcss'),
    ])
    .js('resources/js/login.js', 'public/assets/js')
    .copyDirectory('resources/assets/img', 'public/assets/images')
    .copy('resources/js/plugin.js', 'public/assets/js/plugin.js')

    .styles('resources/assets/admin/css', 'public/assets/css/metronic.css')
    .styles('resources/assets/admin/plugins', 'public/assets/css/vendor.css')
    .styles('resources/assets/admin/css/application', 'public/assets/css/embed.css')

    .scripts([
        'resources/assets/admin/js/plugins.bundle.js',
        'resources/assets/admin/js/prismjs.bundle.js',
        'resources/assets/admin/js/scripts.bundle.js',
    ], 'public/assets/js/metronic.js')

    .copyDirectory('resources/assets/admin/fonts', 'public/assets/css/fonts')
    .copyDirectory('resources/assets/admin/media', 'public/assets/media')
    .copyDirectory('resources/css/components', 'public/assets/css/components')
    .options({
        processCssUrls: false
    });

// if (mix.inProduction()) {
//     mix.version();
//     mix.then(() => {
//         const convertToFileHash = require("laravel-mix-make-file-hash");
//         convertToFileHash({
//             publicPath: "public",
//             manifestFilePath: "public/mix-manifest.json",
//             blacklist: ["manifest.json", "assets/css/embed.css", "assets/js/embed.js", "assets/js/login.js", "assets/js/ssosdk.js", "assets/js/picturefirst_widget.js", "assets/js"+version+"/global.js"],
//             keepBlacklistedEntries: true,
//         });
//     });
// }

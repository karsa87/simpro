<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\ProjectShop;
use App\Models\ProjectDebtPayment;

class RecalculateProjectShopPaidJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $project_shop_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($project_shop_id)
    {
        $this->project_shop_id = $project_shop_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $projectShop = ProjectShop::find($this->project_shop_id);
        if (empty($projectShop)) {
            return;
        }

        $total_paid = ProjectDebtPayment::where('project_shop_id', $projectShop->id)->sum('amount');
        $projectShop->paid = $total_paid;
        if ($projectShop->paid >= $projectShop->total_amount) {
            $projectShop->status = ProjectShop::STATUS_PAID;
        } else {
            $projectShop->status = ProjectShop::STATUS_UNPAID;
        }

        $projectShop->save();
    }
}

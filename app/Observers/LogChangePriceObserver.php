<?php

namespace App\Observers;

use App\Models\LogChangePrice;

class LogChangePriceObserver
{
    /**
     * Handle the Models "created" event.
     * set created_by with ID from user login
     * 
     * @param  App\Models  $object
     * @return void
     */
    // public function created($object)
    // {
    //     $table = $object->getTable();
    //     $object_name = "";
    //     $old_price = 0;
    //     $new_price = 0;
    //     $product_id = null;

    //     switch ($table) {
    //         case 'product':
    //             $old_price = 0;
    //             $new_price = $object->price;
    //             $object_name = 'Product';
    //             $product_id = $object->id;
                
    //             break;

    //         case 'price':
    //             $old_price = 0;
    //             $new_price = $object->price;
    //             $object_name = 'Price';
    //             $product_id = $object->product_id;

    //             break;

    //         default:
    //             # code...
    //             break;
    //     }

    //     if ($old_price != $new_price) {
    //         $this->log_history([
    //             'product_id' => $product_id,
    //             'record_id' => $object->id,
    //             'old_price' => $old_price,
    //             'new_price' => $new_price,
    //             'information' => sprintf('Updated from %s', $object_name),
    //             'record_type' => get_class($object),
    //             'created_at' => date('Y-m-d H:i:s'),
    //         ]);
    //     }
    // }

    /**
     * Handle the Models "updated" event.
     * set updated_by with ID from user login
     * @param  App\Models  $object
     * @return void
     */
    public function updating($object)
    {
        $table = $object->getTable();
        $object_name = "";
        $old_price = 0;
        $new_price = 0;
        $product_id = null;

        switch ($table) {
            case 'product':
                $old_price = $object->getOriginal('price');
                $new_price = $object->price;
                $object_name = 'Product';
                $product_id = $object->id;
                
                break;

            case 'price':
                $old_price = $object->getOriginal('price');
                $new_price = $object->price;
                $object_name = 'Price';
                $product_id = $object->product_id;

                break;

            default:
                # code...
                break;
        }

        if ($old_price != $new_price) {
            $this->log_history([
                'product_id' => $product_id,
                'record_id' => $object->id,
                'old_price' => $old_price,
                'new_price' => $new_price,
                'information' => sprintf('Updated from %s', $object_name),
                'record_type' => get_class($object),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }

    protected function log_history($input = [])
    {
        $input['date'] = date('Y-m-d H:i:s');
        $input['user_id'] = auth()->user()->id;

        return LogChangePrice::create($input);
    }
}

<?php

namespace App\Observers;

use App\Models\LogHistory;

class CompanyObserver
{
    /**
     * Handle the Models "created" event.
     * set created_by with ID from user login
     * 
     * @param  App\Models  $object
     * @return void
     */
    public function creating($object)
    {
        if ($user = auth()->user()) { 
            $object->company_id = $object->company_id ?? \Session::get(config('system.session.active_company'))->id;
        }
    }

    /**
     * Handle the Models "updated" event.
     * set updated_by with ID from user login
     * @param  App\Models  $object
     * @return void
     */
    public function updating($object)
    {
        if ($user = auth()->user()) { 
            $object->company_id = $object->company_id ?? \Session::get(config('system.session.active_company'))->id;
        }
    }
}

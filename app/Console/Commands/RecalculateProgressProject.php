<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Project;

class RecalculateProgressProject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recalculate:progress-project {--project_id= : Project ID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate progress job project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $project_id = $this->option('project_id', null);

        $query = Project::with([
            'jobs' => function ($q) {
                $q->with('projectImlementations');
            }
        ]);
        if ($project_id) {
            $query->whereId($project_id);
        }

        $projects = $query->get();
        $bar = $this->output->createProgressBar(count($projects));
        foreach ($projects as $project) {
            $jobs = $project->jobs;
            $progress = [];
            foreach ($jobs as $job) {
                $_progress = 0;
                if ($job->projectImlementations->count() > 0) {
                    $projectImlementation = $job->projectImlementations->sortByDesc('id')->first();
                    $_progress = $projectImlementation ? $projectImlementation->progress : 0 ;

                    $job->progress = $_progress;
                    $job->save();
                }

                $progress[] = $_progress;
            }

            $project->progress = round(array_sum($progress) / count($progress), 2);
            $project->save();
        }
        $bar->finish();
    }
}

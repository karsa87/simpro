<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use Kyslik\ColumnSortable\Sortable;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;

class File extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike, Sortable;

    protected $guarded = [];

    /**
     * Get the transaction record associated with the table transaction.
     */
    public function record()
    {
        return $this->morphTo();
    }

    public function canDownload()
    {
        return !empty($this->path);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use Kyslik\ColumnSortable\Sortable;
use App\Traits\LogActivityTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;
use App\Traits\FileTrait;

class ProjectDebtPayment extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike, Sortable, FileTrait, LogActivityTrait;

    protected $guarded = [];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function projectShop()
    {
        return $this->belongsTo(ProjectShop::class, 'project_shop_id');
    }

    public function getNoTransactionAttribute($value)
    {
        return $this->exists ? $value : self::nextNoTransaction();
    }

    public static function nextNoTransaction()
    {
        try {
            $record = self::orderBy('id', 'DESC')->first();
            if ($record) {
                preg_match('/[0-9]*$/', $record->no_transaction, $last_no);

                // reset every year
                if ($record->created_at->format('Y') !== date('Y')) {
                    $last_no = [0];
                }

                $last_no = count($last_no) > 0 ? $last_no[0] : 0;

                $next_code = $record->trashed() ? $last_no : (((integer) $last_no) + 1);
            } else {
                $next_code = 1;
            }
        } catch (\Throwable $th) {
            $next_code = 1;
        }

        $next_no = null;
        $formatNumber = FormatNumber::getFormatNumber(['finance.debt_payment_po.index']);
        if ($formatNumber) {
            $next_no = $formatNumber->format($next_code);
        } else {
            $roman_month = \App\Util\Helpers\Util::numberToRoman(date('n'));
            $next_no = sprintf("SIMPRO-DSO/%s/%s/%04d", date('y'), $roman_month, $next_code);
        }

        return $next_no;
    }
}

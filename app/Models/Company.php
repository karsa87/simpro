<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Traits\LogCreateUpdateTrait;
use App\Traits\ScopeLike;

class Company extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, ScopeLike;

    protected $table = 'company';
    protected $fillable = ['logo', 'name', 'address', 'city', 'postal_code', 'npwp', 'license_number', 'phone', 'fax', 'email', 'director_id', 'pic_payment', 'pic_purchase', 'pic_warehouse', 'format_date'];

    public function picWarehouse()
    {
        return $this->belongsTo(User::class, 'pic_warehouse')->withTrashed();
    }

    public function picPayment()
    {
        return $this->belongsTo(User::class, 'pic_payment')->withTrashed();
    }

    public function picPurchase()
    {
        return $this->belongsTo(User::class, 'pic_purchase')->withTrashed();
    }

    public function director()
    {
        return $this->belongsTo(User::class, 'director_id')->withTrashed();
    }

    public function userCompany()
    {
        return $this->belongsToMany(User::class, 'user_company', 'company_id', 'user_id');
    }

    public function getLogoUrlAttribute()
    {
        $image = $this->defaultLogoUrl();

        if (!empty($this->logo)) {
            $this->logo = is_array($this->logo) ? $this->logo : json_decode($this->logo, TRUE);
            if (count($this->logo) > 0 && Storage::exists($this->logo[0])) {
                $image = url(Storage::url($this->logo[0]));
            }
        }

        return $image;
    }

    /**
     * Get the default logo URL if no logo has been uploaded.
     *
     * @return string
     */
    protected function defaultLogoUrl()
    {
        return 'https://ui-avatars.com/api/?name='.urlencode($this->name).'&color=FFF&background=fb682f';
    }

    public function getFullAddressAttribute()
    {
        $full_address = $this->address;

        if ($this->city) {
            $full_address = sprintf('%s, %s', $full_address, $this->city);
        }

        if ($this->postal_code) {
            $full_address = sprintf('%s, %s', $full_address, $this->postal_code);
        }

        return $full_address;
    }
}

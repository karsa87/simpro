<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use Kyslik\ColumnSortable\Sortable;
use App\Traits\LogActivityTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;

class ProjectVendor extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike, Sortable, LogActivityTrait;

    protected $guarded = [];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id');
    }

    public function jobs()
    {
        return $this->belongsToMany(
            ProjectJob::class,
            'project_vendor_jobs',
            'project_vendor_id',
            'project_job_id'
        );
    }
}

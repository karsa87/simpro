<?php

namespace App\Models;

use jeremykenedy\LaravelRoles\Models\Permission as JeremyPermission;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ScopeLike;

class Permission extends JeremyPermission
{
    use ScopeLike;

    /*************
    * RELATIONSHIP
    ***************/
    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'menu_permission', 'permission_id', 'menu_id');
    }
    /**
     * Set slug attribute.
     *
     * @param string $value
     *
     * @return void
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = preg_replace('/[^a-z0-9_.]/', '_', strtolower($value));
    }
}

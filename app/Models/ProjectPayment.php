<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use Kyslik\ColumnSortable\Sortable;
use App\Traits\LogActivityTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;
use App\Traits\FileTrait;

class ProjectPayment extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike, Sortable, FileTrait, LogActivityTrait;

    protected $guarded = [];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function employee()
    {
        return $this->belongsTo(User::class, 'employee_id');
    }

    public function getTitleAttribute($value)
    {
        if (!$this->exists) {
            $count = $this->project->payments->count();

            $value = sprintf('Termin %s', $count+1);
        }

        return $value;
    }
}

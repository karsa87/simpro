<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;

class LockData extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, ScopeLike;

    protected $table = 'lock_data';
    protected $fillable = ['menu_id', 'delete_day', 'edit_day'];

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    public static function editable($date, $day = 0)
    {
        if ($day) {
            $diff = \App\Util\Helpers\Util::diff_date($date, date('Y-m-d H:i'));

            return $diff['day'] < $day;
        }

        return true;
    }

    public static function deleteable($date, $day = 0)
    {
        if ($day) {
            $diff = \App\Util\Helpers\Util::diff_date($date, date('Y-m-d H:i'));

            return $diff['day'] < $day;
        }

        return true;
    }
}

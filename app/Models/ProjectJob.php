<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use Kyslik\ColumnSortable\Sortable;
use App\Traits\LogActivityTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;

class ProjectJob extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike, Sortable, LogActivityTrait;

    const STATUS_WAITING = 0;
    const STATUS_ON_PROGRESS = 1;
    const STATUS_DONE = 2;
    const STATUS_CANCEL = 9;

    protected $guarded = [];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function projectImlementation()
    {
        return $this->hasOne(ProjectImplementation::class)->orderBy('project_implementations.created_at', 'DESC')->take(1);
    }

    public function projectImlementations()
    {
        return $this->hasMany(ProjectImplementation::class)->orderBy('project_implementations.created_at', 'DESC');
    }

    public function vendors()
    {
        return $this->belongsToMany(
            ProjectVendor::class,
            'project_vendor_jobs',
            'project_job_id',
            'project_vendor_id',
        );
    }

    public function scopeWaiting($query)
    {
        return $query->whereStatus(self::STATUS_WAITING);
    }

    public function scopeOnProgress($query)
    {
        return $query->whereStatus(self::STATUS_ON_PROGRESS);
    }

    public function scopeDone($query)
    {
        return $query->whereStatus(self::STATUS_DONE);
    }

    public function scopeCancel($query)
    {
        return $query->whereStatus(self::STATUS_CANCEL);
    }

    public function isWaiting()
    {
        return $this->status === self::STATUS_WAITING;
    }

    public function isOnProgress()
    {
        return $this->status === self::STATUS_ON_PROGRESS;
    }

    public function isDone()
    {
        return $this->status === self::STATUS_DONE;
    }

    public function isCancel()
    {
        return $this->status === self::STATUS_CANCEL;
    }
}

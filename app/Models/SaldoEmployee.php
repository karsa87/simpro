<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use Kyslik\ColumnSortable\Sortable;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;

class SaldoEmployee extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike, Sortable;

    protected $table = 'saldo_employees';
    protected $guarded = [];
    public $sortable = ['id', 'employee_id', 'date', 'saldo', 'created_at', 'updated_at'];

    public function employee()
    {
        return $this->belongsTo(User::class, 'employee_id');
    }
}

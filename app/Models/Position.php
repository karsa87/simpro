<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;

class Position extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike;

    protected $guarded = [];
}

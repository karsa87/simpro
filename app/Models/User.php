<?php

namespace App\Models;

use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Traits\ScopeLike;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, HasRoleAndPermission, ScopeLike;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nik',
        'name',
        'email',
        'password',
        'code',
        'address',
        'phone',
        'npwp',
        'area_id',
        'position_id',
        'lastlogin',
        'lastloginip',
        'photo_profile',
        'status',
        'saldo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Check if the user has all permissions.
     *
     * @param int|string|array $permission
     *
     * @return bool
     */
    public function hasOrPermissions($permission)
    {
        $result = FALSE;
        foreach ($this->getArrayFrom($permission) as $permission) {
            if ($this->checkPermission($permission)) {
                $result = TRUE;
            }
        }

        return $result;
    }

    public function has($permissions)
    {
        $has = FALSE;
        $has_permissions = \Session::get('permission');
        $has_permissions = $has_permissions->pluck('slug','id')->toArray();

        if( is_array($permissions) )
        {
            foreach ($permissions as $p)
            {
                if( is_numeric(array_search($p, $has_permissions)) )
                {
                    $has = TRUE;
                }
            }
        }
        elseif( is_numeric(array_search($permissions, $has_permissions)) )
        {
            $has = TRUE;
        }

        return $has;
    }

    /**********
    * RELATION
    *********/
    public function company()
    {
        return $this->belongsToMany(Company::class, 'user_company', 'user_id', 'company_id')->withTrashed();
    }

    public function area()
    {
        return $this->belongsTo(Area::class)->withTrashed();
    }

    public function position()
    {
        return $this->belongsTo(Position::class)->withTrashed();
    }

    public function notifications()
    {
        return $this->belongsToMany(
            Notification::class,
            'notification_users',
            'user_id',
            'notification_id'
        )->withPivot('is_read');
    }

    /**********
    * SCOPE
    *********/
    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    public function scopeDeactive($query)
    {
        return $query->where('status',0);
    }

    public function scopeEmployee($query)
    {
        return $query->whereHas('roles', function($q) {
            $q->where('level', Role::LEVEL_EMPLOYEE);
        });
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function save(array $options = [])
    {
        if ($this->password && $this->password != $this->getOriginal('password')) {
            $this->password = \Hash::make($this->password);
        } else {
            unset($this->password);
        }

        return parent::save($options);
    }

    public function getPhotoUrlAttribute()
    {
        $image = $this->defaultPhotoUrl();

        if (!empty($this->photo_profile)) {
            $this->photo_profile = is_array($this->photo_profile) ? $this->photo_profile : json_decode($this->photo_profile, TRUE);
            if (count($this->photo_profile) > 0 && \Storage::exists($this->photo_profile[0])) {
                $image = url(\Storage::url($this->photo_profile[0]));
            }
        }

        return $image;
    }

    /**
     * Get the default photo profile URL if no photo profile has been uploaded.
     *
     * @return string
     */
    protected function defaultPhotoUrl()
    {
        return 'https://ui-avatars.com/api/?name='.urlencode($this->name).'&color=FFF&background=fb682f';
    }

    public function underAdministrator()
    {
        return $this->level() < Role::LEVEL_ADMINISTRATOR;
    }

    public function isDeveloper()
    {
        return $this->level() == Role::LEVEL_DEVELOPER;
    }

    public function isSuperuser()
    {
        return $this->level() == Role::LEVEL_ADMINISTRATOR_WIKA;
    }

    public function isAdministrator()
    {
        return $this->level() == Role::LEVEL_ADMINISTRATOR;
    }

    /**
     *
     * @return bool
     */
    public function canBeImpersonated()
    {
        // For example
        return $this->can_be_impersonated == 1;
    }

    /**
     * @return bool
     */
    public function canImpersonate()
    {
        // For example
        return $this->is_admin == 1;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;

class Unit extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike;

    protected $guarded = [];

    public function materials()
    {
        return $this->belongsToMany(
            Material::class,
            'material_units',
            'unit_id',
            'material_id',
        )->withPivot('price');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use Kyslik\ColumnSortable\Sortable;
use App\Traits\LogActivityTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;
use App\Traits\FileTrait;

class Project extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike, Sortable, FileTrait, LogActivityTrait;

    const STATUS_WAITING = 0;
    const STATUS_ON_PROGRESS = 1;
    const STATUS_DONE = 2;
    const STATUS_CLOSE = 3;
    const STATUS_CANCEL = 9;

    const PAYMENT_NOT_YET_PAID = 0;
    const PAYMENT_PAID = 1;

    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function jobs()
    {
        return $this->hasMany(ProjectJob::class);
    }

    public function vendors()
    {
        return $this->hasMany(ProjectVendor::class);
    }

    public function payments()
    {
        return $this->hasMany(ProjectPayment::class);
    }

    public function shops()
    {
        return $this->hasMany(ProjectShop::class);
    }

    public function expends()
    {
        return $this->hasMany(Expend::class);
    }

    public function projectImplementations()
    {
        return $this->hasMany(ProjectImplementation::class);
    }

    public function projectType()
    {
        return $this->belongsTo(ProjectType::class, 'project_type_id');
    }

    public function workers()
    {
        return $this->belongsToMany(
            User::class,
            'project_workers',
            'project_id',
            'employee_id'
        )->withPivot('position_id');
    }

    public function scopeWaiting($query)
    {
        return $query->whereStatus(self::STATUS_WAITING);
    }

    public function scopeOnProgress($query)
    {
        return $query->whereStatus(self::STATUS_ON_PROGRESS);
    }

    public function scopeDone($query)
    {
        return $query->whereStatus(self::STATUS_DONE);
    }

    public function scopeClose($query)
    {
        return $query->whereStatus(self::STATUS_CLOSE);
    }

    public function scopeCancel($query)
    {
        return $query->whereStatus(self::STATUS_CANCEL);
    }

    public function isWaiting()
    {
        return $this->status === self::STATUS_WAITING;
    }

    public function isOnProgress()
    {
        return $this->status === self::STATUS_ON_PROGRESS;
    }

    public function isDone()
    {
        return $this->status === self::STATUS_DONE;
    }

    public function isClose()
    {
        return $this->status === self::STATUS_CLOSE;
    }

    public function isCancel()
    {
        return $this->status === self::STATUS_CANCEL;
    }

    public function getCustomerNameAttribute($value)
    {
        return $this->customer ? $this->customer->name : $value;
    }

    public function getCustomerPhoneAttribute($value)
    {
        return $this->customer ? $this->customer->phone : $value;
    }

    public function getCustomerAddressAttribute($value)
    {
        return $this->customer ? $this->customer->address : $value;
    }

    public function getNoProjectCompanyAttribute($value)
    {
        $text = $this->no_project;
        if ($this->customer_company) {
            $text = "$this->customer_company - $text";
        }
        return $text;
    }

    public function getNoProjectAttribute($value)
    {
        return $this->exists ? $value : self::nextNoProject();
    }

    public static function nextNoProject()
    {
        try {
            $record = self::orderBy('id', 'DESC')->first();
            if ($record) {
                preg_match('/[0-9]*$/', $record->no_project, $last_no);

                // reset every year
                if ($record->created_at->format('Y') !== date('Y')) {
                    $last_no = [0];
                }

                $last_no = count($last_no) > 0 ? $last_no[0] : 0;

                $next_code = $record->trashed() ? $last_no : (((integer) $last_no) + 1);
            } else {
                $next_code = 1;
            }
        } catch (\Throwable $th) {
            $next_code = 1;
        }

        $next_no = null;
        $formatNumber = FormatNumber::getFormatNumber(['project.project.index']);
        if ($formatNumber) {
            $next_no = $formatNumber->format($next_code);
        } else {
            $roman_month = \App\Util\Helpers\Util::numberToRoman(date('n'));
            $next_no = sprintf("SIM-PO/%s/%s/%04d", date('y'), $roman_month, $next_code);
        }

        return $next_no;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;
use App\Util\Helpers\Util;
use Kyslik\ColumnSortable\Sortable;

class Formula extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike, Sortable;

    protected $guarded = [];

    public function formula_items()
    {
        return $this->hasMany(FormulaItem::class, 'formula_id');
    }

    public function getTotalRoundAttribute()
    {
        return Util::roundedThousand($this->total_amount);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use Kyslik\ColumnSortable\Sortable;
use App\Traits\LogActivityTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;
use App\Traits\FileTrait;

class ProjectShop extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike, Sortable, FileTrait, LogActivityTrait;

    const STATUS_UNPAID = 0;
    const STATUS_PAID = 1;

    protected $guarded = [];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function employee()
    {
        return $this->belongsTo(User::class, 'employee_id');
    }

    public function projectShopDetails()
    {
        return $this->hasMany(ProjectShopDetail::class);
    }

    public function scopePaid($query)
    {
        return $query->whereStatus(self::STATUS_PAID);
    }

    public function scopeUnpaid($query)
    {
        return $query->whereStatus(self::STATUS_UNPAID);
    }

    public function isPaid($query)
    {
        return $this->status === self::STATUS_PAID;
    }

    public function isUnpaid($query)
    {
        return $this->status === self::STATUS_UNPAID;
    }
}

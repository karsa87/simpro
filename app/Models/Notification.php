<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;

class Notification extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike;

    const TYPE_CREATE = 0;
    const TYPE_UPDATE = 1;
    const TYPE_DELETE = 2;

    const STATUS_UNREAD = 0;
    const STATUS_READ = 1;

    protected $table = 'notification';
    protected $fillable = ['message', 'link', 'route', 'parameter', 'type', 'company_id'];

    public function users()
    {
        return $this->belongsToMany(
            User::class, 
            'notification_users', 
            'notification_id', 
            'user_id'
        )->withPivot('is_read');
    }

    /**********
    * SCOPE
    *********/
    public function scopeCreated($query)
    {
        return $query->where('type', self::TYPE_CREATE);
    }

    public function scopeUpdated($query)
    {
        return $query->where('type', self::TYPE_UPDATE);
    }

    public function scopeDeleted($query)
    {
        return $query->where('type', self::TYPE_DELETE);
    }
}

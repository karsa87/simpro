<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use Kyslik\ColumnSortable\Sortable;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;

class CategoryExpend extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike, Sortable;

    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;

    protected $guarded = [];
    // public $sortable = ['id', 'name', 'status', 'created_at', 'updated_at'];

    public function scopeActive($query)
    {
        return $query->whereStatus(self::STATUS_ACTIVE);
    }

    public function scopeDeactive($query)
    {
        return $query->whereStatus(self::STATUS_DEACTIVE);
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeactive()
    {
        return $this->status == self::STATUS_DEACTIVE;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use App\Traits\CompanyTrait;
use App\Util\Base\Layout;
use App\Traits\ScopeLike;

class FormatNumber extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, ScopeLike, CompanyTrait;

    const MONTH_TYPE_ROMAWI = 0;
    const MONTH_TYPE_DECIMAL = 1;

    const YEAR_TYPE_2 = 0;
    const YEAR_TYPE_4 = 1;

    const INCREMENT_TYPE_1 = 0;
    const INCREMENT_TYPE_4 = 1;
    const INCREMENT_TYPE_2 = 2;
    const INCREMENT_TYPE_3 = 3;

    protected $table = 'format_number';
    protected $fillable = ['menu_id', 'prefix', 'middle', 'year_type', 'month_type', 'increment_type', 'delimiter', 'company_id'];

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id')->withTrashed();
    }

    public function getYearFormatAttribute()
    {
        return $this->year_type ? date('Y') : date('y');
    }

    public function getMonthFormatAttribute()
    {
        return $this->month_type ? date('m') : \App\Util\Helpers\Util::numberToRoman(date('n'));
    }

    public function getPrefixFormatAttribute()
    {
        $format = '';

        if (!empty($this->prefix)) {
            $format .= $this->prefix;
        }

        if (!empty($this->middle)) {
            $format .= $this->middle;
        }

        if (is_numeric($this->year_type)) {
            $format .= $this->delimiter . $this->yearFormat;
        }

        if (is_numeric($this->month_type)) {
            $format .= $this->delimiter . $this->monthFormat;
        }

        return $format;
    }

    public function format(int $number)
    {
        $format = $this->prefixFormat;

        if ($this->increment_type) {
            $format .= $this->delimiter . sprintf(trans('format_number.list.increment_sprintf.' . $this->increment_type), $number);
        } else {
            $format .= $this->delimiter . $number;
        }

        return $format;
    }

    public static function getFormatNumber($arr = [], $company_id = null)
    {
        $formatNumber = null;
        $routes = Layout::buildRowMenu()['route_id'];
        if (count($routes) <= 0) {
            $menu = Menu::whereIn('route', $arr)->first();
            if ($menu) {
                $query = self::where('menu_id', $menu->id);
                if (!empty($company_id)) {
                    $query->where('company_id', $company_id);
                }
                $formatNumber = $query->first();
            }
        } else {
            foreach ($arr as $route) {
                if (array_key_exists($route, $routes)) {
                    $menu_id = $routes[$route];
                    $list_format_number = \Cache::get(config('system.session.list_format_number'));
                    if ($list_format_number) {
                        $formatNumber = $list_format_number->where('menu_id', $menu_id)->first();
                    }

                    break;
                }
            }
        }

        return $formatNumber;
    }
}

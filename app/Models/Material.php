<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\LogCreateUpdateTrait;
use App\Traits\CompanyTrait;
use App\Traits\ScopeLike;
use Kyslik\ColumnSortable\Sortable;

class Material extends Model
{
    use SoftDeletes, LogCreateUpdateTrait, CompanyTrait, ScopeLike, Sortable;

    protected $guarded = [];

    public function units()
    {
        return $this->belongsToMany(
            Unit::class,
            'material_units',
            'material_id',
            'unit_id'
        )->withPivot('price', 'qty');
    }
}

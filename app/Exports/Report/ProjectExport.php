<?php

namespace App\Exports\Report;

use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use App\Util\Helpers\Util;

class ProjectExport extends DefaultValueBinder implements FromCollection, WithHeadings, WithMapping, WithEvents, WithCustomValueBinder
{
    use Exportable;

    protected $projects;
    protected $no = 1;

    public function __construct($projects)
    {
        $this->projects = $projects;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->projects;
    }

    /**
    * @var Array $article
    */
    public function map($project): array
    {
        return [
            'no' => $this->no++,
            'no_project' => $project->no_project,
            'no_spk' => $project->no_spk,
            'projectname' => $project->projectname,
            'customer_name' => $project->customer_name,
            'customer_phone' => $project->customer_phone,
            'start_date' => Util::formatDate($project->start_date, 'Y-m-d'),
            'due_date' => Util::formatDate($project->due_date, 'Y-m-d'),
            'total_amount' => $project->total_amount,
            'paid' => $project->payments->sum('amount'),
        ];
    }

    public function headings(): array
    {
        return [
            trans('global.no'),
            trans('project.label.no_project'),
            trans('project.label.no_spk'),
            trans('project.label.projectname'),
            trans('project.label.customer_name'),
            trans('project.label.customer_phone'),
            trans('project.label.start_date'),
            trans('project.label.due_date'),
            trans('project.label.total_amount'),
            trans('project_shop.label.paid'),
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                // All headers - set font size to 14
                $sheet = $event->sheet->getDelegate();
                $cellRange = 'A1:W1';
                $sheet->getStyle($cellRange)->getFont()->setSize(12);
                $sheet->getStyle($cellRange)->getFont()->setBold(true);

                $sheet->getColumnDimension('A')->setWidth(5);
                $sheet->getColumnDimension('B')->setWidth(20);
                $sheet->getColumnDimension('C')->setWidth(15);
                $sheet->getColumnDimension('D')->setWidth(35);
                $sheet->getColumnDimension('E')->setWidth(35);
                $sheet->getColumnDimension('F')->setWidth(20);
                $sheet->getColumnDimension('G')->setWidth(15);
                $sheet->getColumnDimension('H')->setWidth(15);
                $sheet->getColumnDimension('I')->setWidth(20);
                $sheet->getColumnDimension('J')->setWidth(20);
                $sheet->getDefaultRowDimension()->setRowHeight(-1);

                // Set wrap text in cells
                $sheet->getStyle('D2:E' . $sheet->getHighestRow())->getAlignment()->setWrapText(true);
                $sheet->getStyle('A1:J' . $sheet->getHighestRow())->getAlignment()->setHorizontal('center');

                $sheet->getStyle('D2:E' . $sheet->getHighestRow())->getAlignment()->setHorizontal('left');
                $sheet->getStyle('I1:J' . $sheet->getHighestRow())->getAlignment()->setHorizontal('right');

                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];

                $sheet->getStyle('A1:J' . $sheet->getHighestRow())->applyFromArray($styleArray);

                $sheet->getStyle('I2:J' . $sheet->getHighestRow())->getNumberFormat()->setFormatCode('###,###,###');
            },
        ];
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value) && $value == 0) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}

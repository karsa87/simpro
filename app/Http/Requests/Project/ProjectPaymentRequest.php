<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;
use App\Util\Helpers\Util;

class ProjectPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'project_id' => 'required|numeric',
            'employee_id' => 'required|numeric',
            'information' => 'nullable|string',
            'date' => 'required|date_format:"Y-m-d H:i"',
            'amount' => 'numeric|min:0',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'date' => Util::formatDate($this->date, 'Y-m-d H:i'),
            'amount' => Util::remove_format_currency($this->amount),
        ]);
    }
}

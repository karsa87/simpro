<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\ProjectType;
use App\Util\Helpers\Util;

class ProjectTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();

        return [
            'name' => 'required|unique:'.ProjectType::class.',name,'.($input['id'] ?: 'NULL') .',id',
            'description' => 'nullable|string',
            'price' => 'required|numeric|min:0',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'price' => Util::remove_format_currency($this->price),
        ]);
    }
}

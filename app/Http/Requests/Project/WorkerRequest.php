<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class WorkerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $workers = $this->workers;
        $validator->after(function ($validator) use ($workers) {
            $employee_id = [];
            foreach ($workers as $worker) {
                $attributes = [
                    'employee_id' => $worker['employee_id'],
                    // 'position_id' => $worker['position_id'],
                    'position_id' => null,
                ];
                $validator_new = Validator::make($attributes, [
                    'employee_id' => 'required',
                    'position_id' => 'required',
                ]);

                if ($validator_new->fails()) {
                    foreach ($validator_new->errors() as $error) {
                        dd($error);
                    }
                } else {
                    $employee_id[] = $worker['employee_id'];
                }
            }

            $duplicates = array_diff_assoc($employee_id, array_unique($employee_id));

            if ($duplicates) {
                $validator->errors()->add('employee_id', 'Duplicate employee!');
            }
        });
    }
}

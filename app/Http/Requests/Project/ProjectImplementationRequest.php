<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;
use App\Util\Helpers\Util;

class ProjectImplementationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required|numeric',
            'project_job_id' => 'required|numeric',
            'information' => 'nullable|string',
            'date' => 'required|date_format:"Y-m-d H:i"',
            'progress' => 'numeric|min:1|max:100',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'date' => Util::formatDate($this->date, 'Y-m-d H:i'),
            'progress' => Util::remove_format_currency($this->progress),
        ]);
    }
}

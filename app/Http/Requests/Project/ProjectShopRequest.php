<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Util\Helpers\Util;

class ProjectShopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->toArray();

        return [
            'store_name' => 'required',
            'project_id' => 'required|numeric',
            'employee_id' => 'required|numeric',
            'information' => 'nullable|string',
            'date' => 'required|date_format:"Y-m-d H:i"',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $total_amount = 0;
        $details = [];
        foreach ($this->get('details', []) as $detail) {
            $detail['qty'] = Util::remove_format_currency($detail['qty'] ?? 0);
            $detail['price'] = Util::remove_format_currency($detail['price'] ?? 0);
            $detail['total_price'] = $detail['qty'] * $detail['price'];

            $details[] = $detail;
            $total_amount += $detail['total_price'];
        }

        $this->merge([
            'details' => $details,
            'total_amount' => $total_amount,
        ]);
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $details = $this->details;
        $validator->after(function ($validator) use ($details) {
            foreach ($details as $detail) {
                $validator_new = Validator::make($detail, [
                    'product_name' => 'required|string',
                    'information' => 'nullable|string',
                    'qty' => 'required|numeric|min:1',
                    'price' => 'required',
                    'total_price' => 'numeric',
                ]);

                if ($validator_new->fails()) {
                    foreach ($validator_new->errors()->all() as $message) {
                        $validator->errors()->add('details', $message);
                    }
                }
            }
        });
    }
}

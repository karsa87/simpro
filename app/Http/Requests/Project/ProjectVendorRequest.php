<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;

class ProjectVendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required',
            'vendor_id' => 'required',
            'jobs' => 'required|array|min:1',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $jobs = [];
        foreach ($this->get('jobs', []) as $job) {
            $jobs[] = $job['project_job_id'];
        }

        $this->merge([
            'jobs' => $jobs,
        ]);
    }
}

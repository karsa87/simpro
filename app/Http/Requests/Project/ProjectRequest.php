<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\ProjectType;
use App\Util\Helpers\Util;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();

        return [
            'no_project' => [
                'required',
                Rule::unique('projects')->where(function ($query) use($input) {
                    $query->where('no_project', $input['no_project'])
                            ->where('company_id', session(config('system.session.active_company'))->id)
                            ->whereNull('deleted_at');

                    if (isset($input['id'])) {
                        $query->where('id', '!=', $input['id']);
                    }

                    return $query;
                })
            ],
            'no_spk' => 'required',
            'projectname' => 'required',
            'customer_name' => 'required',
            'customer_phone' => 'required',
            'customer_address' => 'required|string',
            'customer_company' => 'nullable|string',
            'project_type_id' => 'nullable|exists:'.ProjectType::class.',id',
            'information' => 'string|nullable',
            'term_of_work' => 'required|numeric|min:1',
            'start_date' => 'required|date_format:"Y-m-d"',
            'due_date' => 'required|date_format:"Y-m-d"',
            'total_amount' => 'required|numeric|min:0',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'start_date' => date('Y-m-d', strtotime($this->start_date)),
            'due_date' => date('Y-m-d', strtotime($this->due_date)),
            'total_amount' => Util::remove_format_currency($this->total_amount),
        ]);
    }
}

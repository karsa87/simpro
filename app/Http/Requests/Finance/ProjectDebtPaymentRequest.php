<?php

namespace App\Http\Requests\Finance;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\ProjectShop;
use App\Util\Helpers\Util;

class ProjectDebtPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->toArray();

        return [
            'no_transaction' => [
                'required',
                Rule::unique('project_debt_payments')->where(function ($query) use($input) {
                    $query->where('no_transaction', $input['no_transaction'])
                            ->where('company_id', session(config('system.session.active_company'))->id)
                            ->whereNull('deleted_at');

                    if (isset($input['id'])) {
                        $query->where('id', '!=', $input['id']);
                    }

                    return $query;
                })
            ],
            'project_id' => 'required|numeric',
            'project_shop_id' => 'required|numeric',
            'information' => 'nullable|string',
            'date' => 'required|date_format:"Y-m-d H:i"',
            'amount' => 'required|numeric|min:0',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $attributes = [
            'amount' => Util::remove_format_currency($this->amount),
            'date' => date('Y-m-d H:i', strtotime($this->date)),
            'project_shop_id' => null,
            'project_id' => null,
        ];

        if ($this->project_shop_id && ($projectShop = ProjectShop::find($this->project_shop_id))) {
            $attributes['project_shop_id'] = $projectShop->id;
            $attributes['project_id'] = $projectShop->project_id;
        }

        $this->merge($attributes);
    }
}

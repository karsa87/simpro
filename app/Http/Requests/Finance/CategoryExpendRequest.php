<?php

namespace App\Http\Requests\Finance;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\CategoryExpend;

class CategoryExpendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'status' => [
                'required',
                Rule::in([
                    CategoryExpend::STATUS_DEACTIVE,
                    CategoryExpend::STATUS_ACTIVE,
                ]),
            ],
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $status = ($this->status == 'on' || $this->status == CategoryExpend::STATUS_ACTIVE) ? CategoryExpend::STATUS_ACTIVE : CategoryExpend::STATUS_DEACTIVE;

        $this->merge([
            'status' => $status,
        ]);
    }
}

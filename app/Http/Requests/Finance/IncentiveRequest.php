<?php

namespace App\Http\Requests\Finance;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Util\Helpers\Util;

class IncentiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->toArray();

        return [
            'no_transaction' => [
                'required',
                Rule::unique('incentives')->where(function ($query) use($input) {
                    $query->where('no_transaction', $input['no_transaction'])
                            ->where('company_id', session(config('system.session.active_company'))->id)
                            ->whereNull('deleted_at');

                    if (isset($input['id'])) {
                        $query->where('id', '!=', $input['id']);
                    }

                    return $query;
                })
            ],
            'investor' => 'required|string',
            'information' => 'string',
            'date' => 'required|date_format:"Y-m-d H:i"',
            'amount' => 'required|numeric|min:0',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'amount' => Util::remove_format_currency($this->amount),
            'date' => date('Y-m-d H:i', strtotime($this->date)),
        ]);
    }
}

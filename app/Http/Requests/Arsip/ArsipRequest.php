<?php

namespace App\Http\Requests\Arsip;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use App\Util\Helpers\Util;

class ArsipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();

        return [
            'name' => [
                'required',
                Rule::unique('arsips')->where(function ($query) use($input) {
                    $query->where('name', $input['name'])
                            ->where('company_id', session(config('system.session.active_company'))->id)
                            ->whereNull('deleted_at');

                    if (isset($input['id'])) {
                        $query->where('id', '!=', $input['id']);
                    }

                    return $query;
                })
            ],
            'description' => 'nullable|string'
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'name' => Str::upper($this->name),
        ]);
    }
}

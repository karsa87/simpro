<?php

namespace App\Http\Requests\Master;

use App\Models\Unit;
use App\Util\Helpers\Util;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $company = \Session::get(config('system.session.active_company'));

        return [
            'name' => 'required|string',
            'description' => 'nullable|string',
            'units' => 'required|array',
            'units.*.unit_id' => [
                'required',
                'numeric',
                Rule::exists((new Unit())->getTable(), 'id')->where('company_id', $company->id)
            ],
            'units.*.price' => [
                'required',
                'numeric',
                'min:0'
            ],
            'units.*.qty' => [
                'required',
                'numeric',
                'min:1'
            ],
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $units = [];
        foreach ($this->get('units', []) as $key => $unit) {
            $units[] = [
                'unit_id' => $unit['unit_id'],
                'price' => Util::remove_format_currency($unit['price']),
                'qty' => Util::remove_format_currency($unit['qty']),
            ];
        };

        $this->merge([
            'units' => $units,
        ]);
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $duplicates = collect($this->get('units', []))->pluck('unit_id')->duplicates();
            if ($duplicates->count() > 0) {
                $units = Unit::whereIn('id', $duplicates)->get();
                foreach ($units as $unit) {
                    $validator->errors()->add('units', trans('material.message.duplicate_unit', ['unit'=>$unit->name]));
                }
            }
        });
    }
}

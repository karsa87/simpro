<?php

namespace App\Http\Requests\Master;

use App\Models\Formula;
use App\Models\Unit;
use App\Util\Helpers\Util;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FormulaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $company = \Session::get(config('system.session.active_company'));
        $input = $this->all();

        return [
            'code' => [
                'nullable',
                'string',
                Rule::unique((new Formula())->getTable())->where(function ($query) use($input, $company) {
                    if (! empty($input['code'])) {
                        $query->where('name', $input['code'])
                                ->where('company_id', $company->id)
                                ->whereNull('deleted_at');

                        if (isset($input['id'])) {
                            $query->where('id', '!=', $input['id']);
                        }
                    }

                    return $query;
                })
            ],
            'name' => 'required|string',
            'description' => 'nullable|string',

            'items' => 'required|array',
            'items.*.material_id' => [
                'required',
                'numeric',
            ],
            'items.*.unit_id' => [
                'nullable',
                'numeric',
            ],
            'items.*.coefficient' => [
                'required',
                'numeric',
                'min:0'
            ],
            'items.*.amount' => [
                'required',
                'numeric',
                'min:0'
            ],
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $items = [];
        foreach ($this->get('items', []) as $key => $unit) {
            $coefficient = Util::remove_format_currency($unit['coefficient']);
            $amount = Util::remove_format_currency($unit['amount']);

            $items[] = [
                'id' => $unit['id'],
                'material_id' => $unit['material_id'],
                'unit_id' => $unit['unit_id'],
                'coefficient' => $coefficient,
                'amount' => $amount,
            ];
        };

        $this->merge([
            'items' => $items,
        ]);
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $duplicates = collect($this->get('items', []))->pluck('unit_id')->duplicates();
            if ($duplicates->count() > 0) {
                $items = Unit::whereIn('id', $duplicates)->get();
                foreach ($items as $unit) {
                    $validator->errors()->add('items', trans('material.message.duplicate_unit', ['unit'=>$unit->name]));
                }
            }
        });
    }
}

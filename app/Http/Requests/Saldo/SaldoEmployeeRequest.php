<?php

namespace App\Http\Requests\Saldo;

use Illuminate\Foundation\Http\FormRequest;
use App\Util\Helpers\Util;

class SaldoEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => 'required',
            'date' => 'required|date_format:"Y-m-d H:i"',
            'saldo' => 'required|numeric',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'saldo' => Util::remove_format_currency($this->saldo),
            'date' => date('Y-m-d H:i', strtotime($this->date)),
        ]);
    }
}

<?php

namespace App\Http\Livewire\Table;

use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Column;
use App\Models\ProjectShop;
use App\Util\Helpers\Util;

class ProjectShopTable extends LivewireDatatable
{
    public $projectId = null;

    public function builder()
    {
        return ProjectShop::where('project_id', $this->projectId);
    }

    public function columns()
    {
        return [
            Column::name('store_name')
                ->label(trans('project_shop.label.store_name'))
                ->searchable(),

            Column::callback('date', 'formatDate')
                ->label(trans('project_shop.label.date')),

            Column::name('employee.name')
                ->label(trans('project_shop.label.employee_id'))
                ->searchable(),

            NumberColumn::callback('total_amount', 'formatPrice')
                ->label(trans('project_shop.label.total_amount')),

            Column::callback(['status'], function ($status) {
                return "<span class='label font-weight-bold label-lg label-light-". ($status ? 'success' : 'danger') ." label-inline'>". trans("project_shop.list.status.$status") ."</span>";
            })
                ->label(trans('project_shop.label.status'))
                ->searchable(),

            Column::callback(['id'], function ($id) {
                return view('table.project-shop.action', [
                    'id' => $id
                ]);
            })
                ->label(trans('global.action')),
        ];
    }

    public function formatDate($date)
    {
        if (!$date) {
            return;
        }

        return Util::formatDate($date, 'Y-m-d H:i');
    }

    public function formatPrice($price)
    {
        if (!$price) {
            return 0;
        }

        return Util::format_currency($price);
    }
}

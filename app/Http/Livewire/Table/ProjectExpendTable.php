<?php

namespace App\Http\Livewire\Table;

use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Column;
use App\Util\Helpers\Util;
use App\Models\Expend;

class ProjectExpendTable extends LivewireDatatable
{
    public $projectId = null;

    public function builder()
    {
        return Expend::where('project_id', $this->projectId);
    }

    public function columns()
    {
        return [
            Column::name('no_transaction')
                ->label(trans('expend.label.no_transaction'))
                ->searchable(),

            Column::name('categoryExpend.name')
                ->label(trans('expend.label.category_id'))
                ->searchable(),

            Column::name('information')
                ->label(trans('expend.label.category_id'))
                ->searchable(),

            Column::name('employee.name')
                ->label(trans('expend.label.employee_id'))
                ->searchable(),

            Column::callback('date', 'formatDate')
                ->label(trans('expend.label.date')),

            NumberColumn::callback('amount', 'formatPrice')
                ->label(trans('sales_order.label.total')),

            Column::callback(['id'], function ($id) {
                return view('table.project-expend.action', [
                    'id' => $id
                ]);
            })
                ->label(trans('global.action')),
        ];
    }

    public function formatDate($date)
    {
        if (!$date) {
            return;
        }

        return Util::formatDate($date, 'Y-m-d H:i');
    }

    public function formatPrice($price)
    {
        if (!$price) {
            return 0;
        }

        return Util::format_currency($price);
    }
}

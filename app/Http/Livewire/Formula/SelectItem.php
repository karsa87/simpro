<?php

namespace App\Http\Livewire\Formula;

use App\Models\Material;
use App\Models\Unit;
use App\Util\Helpers\Util;
use Livewire\Component;

class SelectItem extends Component
{
    public $items = [];
    public $listMaterials = [];
    public $listUnits = [];
    public $show_modal = [
        'material' => false,
    ];
    public $search_material = [
        'keyword' => '',
        'unit_id' => '',
    ];
    public $search_unit_id = null;
    public $subtotal = 0;
    public $subtotal_tax = 0;
    public $total = 0;
    public $total_round = 0;
    protected $listeners = ['setCoefficientValue', 'searchMaterial', 'chooseMaterial'];

    public function mount()
    {
        if (!empty($this->items)) {
            $olds = collect($this->items);
            $materials = Material::with('units')->whereIn('id', $olds->pluck('material_id'))->get()->keyBy('id');
            $this->items = [];
            foreach ($olds as $old) {
                $material = $materials[$old['material_id']];
                $unit = $material->units->firstWhere('id', $old['unit_id']);

                $this->items[] = [
                    'id' => $old['id'] ?? null,
                    'material_id' => $material->id,
                    'material_name' => $material->name,
                    'unit_id' => $unit->id,
                    'unit_name' => $unit->name,
                    'coefficient' => $old['coefficient'] ?? $unit->pivot->qty,
                    'amount' => $old['amount'] ?? $unit->pivot->price,
                    'unit_qty' => $unit->pivot->qty,
                    'unit_price' => $unit->pivot->price,
                ];
            }

            $this->calculateSubtotal();
        }

        $this->listUnits = Unit::orderBy('name', 'ASC')->get()->pluck('name', 'id')->toArray();
    }

    public function removeItem($index)
    {
        unset($this->items[$index]);
        $this->calculateSubtotal();
    }

    public function openModal($modal_id, $variable_key)
    {
        $this->show_modal[$variable_key] = true;

        if ($variable_key == 'material') {
            $this->listMaterials = Material::with('units')->limit(20)->get();
        }

        $this->dispatchBrowserEvent('openModalJs', [
            'modal_id' => $modal_id
        ]);
    }

    public function searchMaterial()
    {
        $query = Material::with([
            'units' => function ($qUnit) {
                if ($this->search_material['unit_id']) {
                    $qUnit->where('id', $this->search_material['unit_id']);
                }
            }
        ]);

        if ($this->search_material['keyword']) {
            $keyword = $this->search_material['keyword'];
            $query->where(function ($q) use ($keyword) {
                $q->whereLike('name', $keyword);
            });
        }

        if ($this->search_material['unit_id']) {
            $query->whereHas('units', function ($q)  {
                $q->where('id', $this->search_material['unit_id']);
            });
        }

        $this->listMaterials = [];
        $this->listMaterials = $query->limit(20)->get();
    }

    public function chooseMaterial($material_id, $unit_id)
    {
        $material = Material::with([
            'units' => function ($q) use ($unit_id) {
                $q->where('id', $unit_id);
            }
        ])->whereId($material_id)->first();

        $unit = $material->units->first();
        $this->items[] = [
            'id' => null,
            'material_id' => $material->id,
            'material_name' => $material->name,
            'unit_id' => $unit->id,
            'unit_name' => $unit->name,
            'coefficient' => $unit->pivot->qty,
            'amount' => $unit->pivot->price,
            'unit_qty' => $unit->pivot->qty,
            'unit_price' => $unit->pivot->price,
        ];

        $this->calculateSubtotal();

        $this->show_modal['material'] = false;
        $this->dispatchBrowserEvent('closeModalJs', [
            'modal_id' => 'modal-add-material'
        ]);
    }

    private function calculateSubtotal()
    {
        $items = collect($this->items);
        $this->subtotal = $items->sum('amount');
        $this->subtotal_tax = $this->subtotal * 15 / 100;
        $this->total = $this->subtotal + $this->subtotal_tax;
        $this->total_round = Util::roundedThousand($this->total);
    }

    public function setCoefficientValue($index, $value)
    {
        $coefficient = Util::remove_format_currency($value);

        $this->items[$index]['coefficient'] = $coefficient;

        $this->calculateAmount($index);
    }

    public function calculateAmount($index)
    {
        $item = $this->items[$index];
        $coefficient = Util::remove_format_currency($item['coefficient']);
        $item['amount'] = ($item['unit_price'] * $coefficient) / $item['unit_qty'];

        $this->items[$index] = $item;
        $this->calculateSubtotal();
    }

    public function render()
    {
        return view('livewire.formula.select-item');
    }
}

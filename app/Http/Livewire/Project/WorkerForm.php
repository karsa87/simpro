<?php

namespace App\Http\Livewire\Project;

use Illuminate\Support\Facades\Validator;
use App\Models\Position;
use Livewire\Component;
use App\Models\User;

class WorkerForm extends Component
{
    public $workers = [];
    public $users = [];
    public $positions = [];
    public $search_employee_id = null;
    public $search_position_id = null;

    public function mount()
    {
        $this->users = User::employee()->active()->get()->pluck('name', 'id')->toArray();
        $this->positions = Position::all()->pluck('name', 'id')->toArray();
    }

    public function render()
    {
        return view('livewire.project.worker-form');
    }

    public function addWorker()
    {
        $this->workers[] = [
            'employee_id' => $this->search_employee_id,
            'position_id' => $this->search_position_id,
        ];
    }

    public function removeWorker($index)
    {
        unset($this->workers[$index]);
    }
}

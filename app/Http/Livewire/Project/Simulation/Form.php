<?php

namespace App\Http\Livewire\Project\Simulation;

use App\Models\Formula;
use App\Util\Helpers\Util;
use Illuminate\Support\Collection;
use Livewire\Component;

class Form extends Component
{
    public $show_modal = [
        'formula' => false,
    ];
    public $search_formula = [
        'keyword' => '',
    ];
    public $listFormulas = [];

    /**
     * @var Illuminate\Support\Collection $items
     * **/
    public $items;

    public $subtotal = 0;
    protected $listeners = ['setCoefficientValue', 'searchFormula', 'chooseFormula'];

    public function mount()
    {
        $this->items = collect();
    }

    public function openModal($modal_id)
    {
        $this->show_modal['formula'] = true;

        $this->listFormulas = Formula::limit(10)->orderBy('name', 'ASC')->get();

        $this->dispatchBrowserEvent('openModalJs', [
            'modal_id' => $modal_id
        ]);
    }

    public function searchFormula()
    {
        $query = Formula::query();

        if ($this->search_formula['keyword']) {
            $keyword = $this->search_formula['keyword'];
            $query->where(function ($q) use ($keyword) {
                $q->whereLike('name', $keyword);
                $q->orWhereLike('code', $keyword);
            });
        }

        $this->listFormulas = [];
        $this->listFormulas = $query->limit(20)->orderBy('name', 'ASC')->get();
    }

    public function chooseFormula($formula_id)
    {
        $formula = Formula::with('formula_items.material', 'formula_items.unit')->whereId($formula_id)->first();

        $items = collect();
        foreach ($formula->formula_items as $item) {
            $items->push([
                'coefficient' => $item->coefficient,
                'material' => optional($item->material)->name,
                'unit' => optional($item->unit)->name,
            ]);
        }

        $this->items->push([
            'formula_id' => $formula->id,
            'coefficient' => 1,
            'code' => $formula->code,
            'name' => $formula->name,
            'price' => $formula->total_amount,
            'price_round' => $formula->total_amount,
            'total_amount' => $formula->total_amount,
            'total_round' => $formula->total_round,
            'materials' => $items
        ]);

        $this->calculateSubtotal();

        $this->show_modal['formula'] = false;
        $this->dispatchBrowserEvent('closeModalJs', [
            'modal_id' => 'modal-add-formula'
        ]);
    }

    public function removeItem($index)
    {
        $this->items->forget($index);
        $this->calculateSubtotal();
    }

    public function setCoefficientValue($index, $value)
    {
        $coefficient = Util::remove_format_currency($value);


        $item = $this->items[$index];
        $item['coefficient'] = $coefficient;
        $this->items[$index] = $item;

        $this->calculateAmount($index);
    }

    public function calculateAmount($index)
    {
        $item = $this->items[$index];
        $coefficient = Util::remove_format_currency($item['coefficient']);
        $item['total_amount'] = $item['price'] * $coefficient;
        $item['total_round'] = Util::roundedThousand($item['total_amount']);

        $this->items[$index] = $item;
        $this->calculateSubtotal();
    }

    public function calculateSubtotal()
    {
        $this->subtotal = $this->items->sum('total_round');
    }

    public function render()
    {
        return view('livewire.project.simulation.form');
    }
}

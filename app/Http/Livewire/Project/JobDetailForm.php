<?php

namespace App\Http\Livewire\Project;

use Livewire\Component;

class JobDetailForm extends Component
{
    public $jobs = [];

    public function render()
    {
        return view('livewire.project.job-detail-form');
    }

    public function addJob()
    {
        $this->jobs[] = [
            'id' => '',
            'job' => '',
            'detail' => '',
        ];
    }

    public function removeJob($index)
    {
        unset($this->jobs[$index]);
    }
}

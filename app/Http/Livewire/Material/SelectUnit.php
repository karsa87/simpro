<?php

namespace App\Http\Livewire\Material;

use App\Models\Unit;
use App\Util\Helpers\Util;
use Livewire\Component;

class SelectUnit extends Component
{
    public $units = [];
    public $listUnits = [];
    public $search_unit_id = null;
    protected $listeners = ['calculateTotalPrice', 'addUnit'];

    public function mount()
    {
        $this->listUnits = Unit::all()->pluck('name', 'id')->toArray();
    }

    public function addUnit()
    {
        $this->units[] = [
            'unit_id' => collect($this->listUnits)->keys()->first(),
            'price' => 0,
            'qty' => 1,
        ];
    }

    public function removeUnit($index)
    {
        unset($this->units[$index]);
    }

    public function calculateTotalPrice($index, $column, $value)
    {
        $price = $column == 'price' ? $value : $this->units[$index]['price'];
        $price = Util::remove_format_currency($price);

        $this->units[$index][$column] = $value;
    }

    public function render()
    {
        return view('livewire.material.select-unit');
    }
}

<?php

namespace App\Http\Livewire\File;

use Livewire\Component;

class FileForm extends Component
{
    public $files = [];
    public $file = [
        'index' => null,
        'id' => null,
        'name' => null,
        'url' => null,
        'information' => null,
        'path' => null,
    ];
    protected $listeners = ['changePath'];

    public function render()
    {
        return view('livewire.file.file-form');
    }

    public function changePath($index, $path)
    {
        $this->files[$index]['path'] = $path;
    }

    public function addFile()
    {
        $this->files[] = [
            'id' => '',
            'name' => '',
            'path' => '',
            'url' => '',
            'information' => '',
        ];

        $this->editFile(array_key_last($this->files));
    }

    public function removeFile($index)
    {
        unset($this->files[$index]);
    }

    public function removeNewFile($index)
    {
        if (is_numeric($this->file['index']) && empty($this->file['name'])) {
            unset($this->files[$index]);
        }
    }

    public function editFile($index)
    {
        if (array_key_exists($index, $this->files)) {
            $this->file = $this->files[$index];
            $this->file['index'] = $index;

            $this->dispatchBrowserEvent('showModalEdit', []);
        } else {
            $this->dispatchBrowserEvent('msgFileNotFound', [
                'message' => trans('file.message.not_found'),
            ]);
        }
    }

    public function submitForm($data)
    {
        $data = json_decode($data, true);
        $index = $data['index'] ?? null;
        if (array_key_exists($index, $this->files)) {
            unset($data['index']);
            $this->files[$index] =$data;

            $this->file = [
                'index' => null,
                'id' => null,
                'name' => null,
                'url' => null,
                'information' => null,
                'path' => null,
            ];

            $this->dispatchBrowserEvent('hideModalEdit', []);
        } else {
            $this->dispatchBrowserEvent('msgFileNotFound', [
                'message' => trans('file.message.not_found'),
            ]);
        }
    }
}

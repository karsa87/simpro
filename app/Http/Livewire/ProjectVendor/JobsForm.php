<?php

namespace App\Http\Livewire\ProjectVendor;

use App\Models\ProjectJob;
use Livewire\Component;

class JobsForm extends Component
{
    public $projectVendor = null;
    public $jobs = [];
    public $vendor_id = null;
    public $projectJobs = [];
    protected $listeners = ['changeVendor'];

    public function mount()
    {
        $vendor_id = $this->vendor_id;
        $this->projectJobs = ProjectJob::whereProjectId($this->projectVendor->project_id)
        ->where(function($q) use ($vendor_id) {
            $q->doesntHave('vendors')
                ->orWhereHas('vendors', function ($q1) use ($vendor_id) {
                    $q1->where('vendor_id', $vendor_id);
                });
        })
        ->get()->pluck('job', 'id')->toArray();
    }

    public function render()
    {
        return view('livewire.project-vendor.jobs-form');
    }

    public function addJob()
    {
        $this->jobs[] = [
            'id' => '',
            'project_job_id' => null,
        ];
    }

    public function removeJob($index)
    {
        unset($this->jobs[$index]);
    }

    public function changeVendor($vendor_id)
    {
        $this->vendor_id = $vendor_id;
    }
}

<?php

namespace App\Http\Livewire\ProjectShop;

use App\Util\Helpers\Util;
use Livewire\Component;

class ProductForm extends Component
{
    public $details = [];
    protected $listeners = ['calculateTotalPrice'];

    public function render()
    {
        return view('livewire.project-shop.product-form');
    }

    public function addProduct()
    {
        $this->details[] = [
            'id' => null,
            'product_name' => '',
            'information' => '',
            'qty' => 1,
            'price' => 0,
            'total_price' => 0,
        ];
    }

    public function removeProduct($index)
    {
        unset($this->details[$index]);
    }

    public function calculateTotalPrice($index, $column, $value)
    {
        $price = $column == 'price' ? $value : $this->details[$index]['price'];
        $price = Util::remove_format_currency($price);
        $qty = $column == 'qty' ? $value : $this->details[$index]['qty'];
        $qty = Util::remove_format_currency($qty);

        $this->details[$index][$column] = $value;
        $this->details[$index]['total_price'] = $price * $qty;
    }
}

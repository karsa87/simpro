<?php

namespace App\Http\Controllers;

use jeremykenedy\LaravelUsers\App\Http\Controllers\LaravelUsersController;
use Illuminate\Support\Facades\Cache;
use App\Util\Base\CoreController;
use App\Models\Notification;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use DB;

class NotificationController extends CoreController
{
    public function index()
    {
        $notifications = Notification::with([
            'users' => function($q) {
                $q->where('id', auth()->user()->id);
            }
        ])
        ->whereHas('users', function($q) {
            $q->where('id', auth()->user()->id);
        })
        ->orderBy('created_at', 'DESC')
        ->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        
        DB::table('notification_users')->where('user_id', auth()->user()->id)->update([
            'is_read' => Notification::STATUS_READ
        ]);

        Cache::forget(sprintf(config('system.cache.notification'), auth()->user()->id));
        Cache::remember(sprintf(config('system.cache.notification'), auth()->user()->id), 60 * 60 * 1, function() {
            return auth()->user()->notifications()->wherePivot('is_read', 0)->orderBy('created_at', 'DESC')->get();
        });

        return Layout::render('notification.index', [
            "notifications" => $notifications,
        ]);
    }

    public function read()
    {        
        DB::table('notification_users')->where('user_id', auth()->user()->id)->update([
            'is_read' => Notification::STATUS_READ
        ]);

        Cache::forget(sprintf(config('system.cache.notification'), auth()->user()->id));
        Cache::remember(sprintf(config('system.cache.notification'), auth()->user()->id), 60 * 60 * 1, function() {
            return auth()->user()->notifications()->wherePivot('is_read', 0)->orderBy('created_at', 'DESC')->get();
        });

        if (request()->isMethod('post')) {
            return response()->json([
                'data' => 'Success read all'
            ]);
        } else {
            return redirect()->back()->with('success', 'Success read all');
        }
    }
}

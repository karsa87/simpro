<?php

namespace App\Http\Controllers\Project;

use App\Http\Requests\Project\ProjectShopRequest;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Models\ProjectShopDetail;
use App\Models\ProjectShop;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use Exception;

class ProjectShopController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status = null)
    {
        $query = ProjectShop::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('store_name', Util::get('_k'))
                    ->orWhereHas('employee', function($q) {
                        $q->whereLike('name', Util::get('_k'));
                    })
                    ->orWhereHas('project', function($q) {
                        $q->whereLike('store_name', Util::get('_k'));
                    });
            });
        }

        $projectShops = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                                ->appends(request()->except(["page"]));

        return Layout::render('project.project_shop.index', [
            'projectShops' => $projectShops
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($project_id = null)
    {
        $model = new ProjectShop();
        $model->project_id = $project_id;

        return Layout::render('project.project_shop.create', [
            'model' => $model,
            'files' => [],
            'details' => [],
            'employees' => $model->project->workers->pluck('name', 'id')->toArray()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Finance\ProjectShopRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectShopRequest $request)
    {
        $redirect = redirect()->route('project.project_shop.create');
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'store_name',
            'project_id',
            'employee_id',
            'date',
            'information',
            'total_amount',
            'status',
        ]);

        try {
            if ($input['status'] == ProjectShop::STATUS_PAID) {
                $input['paid'] = $input['total_amount'];
            }
            $model = new ProjectShop();
            $model->fill($input);
            $model->save();

            $detail_ids = [];
            if ($details = $request->get('details', [])) {
                foreach ($details as $detail) {
                    $projectShopDetail = isset($detail['id']) ? ProjectShopDetail::find($detail['id']) : new ProjectShopDetail();
                    unset($detail['id']);
                    $projectShopDetail->fill($detail);
                    $projectShopDetail->project_shop_id = $model->id;

                    $projectShopDetail->save();
                    $detail_ids[] = $projectShopDetail->id;
                }
            }
            $model->projectShopDetails()->whereNotIn('id', $detail_ids)->delete();

            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $model->saveFile($file);
                }
            }
            \DB::commit();

            $redirect = redirect()->route('project.project_shop.show', $model->id)->with('success', trans('global.success_save'));
        } catch (Exception $e) {
            \DB::rollback();
            if ($project_id = $request->get('project_id')) {
                $redirect = redirect()->route('project.project_shop.create', ['project_id' => $project_id]);
            }

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectShop $projectShop)
    {
        return Layout::render('project.project_shop.show', [
            'model' => $projectShop,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectShop $projectShop)
    {
        $files = $projectShop->files()->select('id','name','information','url','path')->get()->toArray();
        $details = $projectShop->projectShopDetails()->select('id','product_name','information','qty','price','total_price')->get()->toArray();

        return Layout::render('project.project_shop.edit', [
            'model' => $projectShop,
            'files' => $files,
            'details' => $details,
            'employees' => $projectShop->project->workers->pluck('name', 'id')->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Finance\ProjectShopRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectShopRequest $request, ProjectShop $projectShop)
    {
        $redirect = redirect()->route('project.project_shop.edit', $projectShop->id);
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'store_name',
            'project_id',
            'employee_id',
            'date',
            'information',
            'total_amount',
            'status',
        ]);

        try {
            if ($input['status'] == ProjectShop::STATUS_PAID) {
                $input['paid'] = $input['total_amount'];
            }
            $projectShop->fill($input);
            $projectShop->save();

            $detail_ids = [];
            if ($details = $request->get('details', [])) {
                foreach ($details as $detail) {
                    $projectShopDetail = isset($detail['id']) ? ProjectShopDetail::find($detail['id']) : new ProjectShopDetail();
                    unset($detail['id']);
                    $projectShopDetail->fill($detail);
                    $projectShopDetail->project_shop_id = $projectShop->id;

                    $projectShopDetail->save();
                    $detail_ids[] = $projectShopDetail->id;
                }
            }
            $projectShop->projectShopDetails()->whereNotIn('id', $detail_ids)->delete();

            $file_ids = [];
            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $file = $projectShop->saveFile($file);
                    $file_ids[] = $file->id;
                }
            }
            $projectShop->files()->whereNotIn('id', $file_ids)->delete();

            \DB::commit();

            $redirect = redirect()->route('project.project_shop.show', $projectShop->id)->with('success', trans('global.success_save'));
        } catch (Exception $e) {
            \DB::rollback();

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectShop $projectShop)
    {
        \DB::beginTransaction();
        $name = $projectShop->store_name;

        $result = $this->delete($projectShop, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("project_shop.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("project_shop.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $result["status"]] );
    }

    public function ajaxProjectHasDebt()
    {
        $params = request()->all();

        $query = ProjectShop::with('project')->whereRaw('total_amount > paid');

        if ($projet_id = request('pid', null)) {
            $query->where('project_id', $projet_id);
        }

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $query->where(function($q1) use($q) {
                $q1->whereLike('store_name', $q);
                $q1->orWhereHas('project', function($q2) use($q) {
                    $q2->whereLike("no_project", $q);
                });
            });
        }

        $list = [];
        foreach ($query->get() as $projectShop) {
            $list[] = [
                'id' => $projectShop->id,
                'text' => sprintf('%s - %s', $projectShop->project->no_project, $projectShop->store),
                'html' => sprintf('%s - %s<br><small>Sisa: %s</small>',
                    $projectShop->project->no_project,
                    $projectShop->store_name,
                    Util::format_currency($projectShop->total_amount - $projectShop->paid),
                ),
            ];
        }

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}

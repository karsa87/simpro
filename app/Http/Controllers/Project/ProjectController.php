<?php

namespace App\Http\Controllers\Project;

use App\Http\Requests\Project\ProjectRequest;
use App\Http\Requests\Project\WorkerRequest;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Models\ProjectJob;
use App\Util\Base\Layout;
use App\Models\Position;
use App\Models\Project;
use Exception;

class ProjectController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status = null)
    {
        $query = Project::query();

        if (empty(request('sort', ''))) {
            $query->orderBy('no_project', 'desc');
        }

        if (empty($status)) {
            $query->whereIn('status', [
                Project::STATUS_WAITING,
                Project::STATUS_ON_PROGRESS,
                Project::STATUS_DONE,
            ]);
        } else {
            $query->where('status', $status);
        }

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('no_project', Util::get('_k'))
                    ->orWhereLike('customer_name', Util::get('_k'));
            });
        }

        $projects = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                                ->appends(request()->except(["page"]));

        return Layout::render('project.project.index', [
            'projects' => $projects
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Layout::render('project.project.create', [
            'model' => new Project(),
            'files' => [],
            'jobs' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Finance\ProjectRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        $redirect = redirect()->route('project.project.create');
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'no_project',
            'no_spk',
            'projectname',
            'customer_name',
            'customer_phone',
            'customer_address',
            'information',
            'term_of_work',
            'start_date',
            'due_date',
            'total_amount',
            'customer_company',
            'project_type_id',
        ]);

        try {
            $model = new Project();
            $model->fill($input);
            $model->save();

            if ($jobs = $request->get('jobs', [])) {
                foreach ($jobs as $job) {
                    $projectJob = isset($job['id']) ? ProjectJob::find($job['id']) : new ProjectJob();
                    $projectJob->project_id = $model->id;
                    $projectJob->job = $job['job'];
                    $projectJob->detail = htmlspecialchars($job['detail']);

                    if (!$projectJob->exists) {
                        $projectJob->status = ProjectJob::STATUS_WAITING;
                    }

                    $projectJob->save();
                }
            }

            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $model->saveFile($file);
                }
            }
            \DB::commit();

            $redirect = redirect()->route('project.project.show', $model->id)->with('success', trans('global.success_save'));
        } catch (Exception $e) {
            \DB::rollback();

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $position_ids = $project->workers->pluck('pivot.position_id')->toArray();;

        $positions = [];
        if ($position_ids) {
            $positions = Position::withTrashed()->whereIn('id', $position_ids)->get()->keyBy('id');
        }

        return Layout::render('project.project.show', [
            'model' => $project,
            'positions' => $positions,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $files = $project->files()->select('id','name','information','url','path')->get()->toArray();
        $jobs = $project->jobs()->select('id','job', 'detail')->get()->toArray();

        return Layout::render('project.project.edit', [
            'model' => $project,
            'files' => $files,
            'jobs' => $jobs,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Finance\ProjectRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, Project $project)
    {
        $redirect = redirect()->route('project.project.edit', $project->id);
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'no_project',
            'no_spk',
            'projectname',
            'customer_name',
            'customer_phone',
            'customer_address',
            'information',
            'term_of_work',
            'start_date',
            'due_date',
            'total_amount',
            'customer_company',
            'project_type_id',
        ]);

        try {
            $project->fill($input);
            $project->save();

            $job_ids = [];
            if ($jobs = $request->get('jobs', [])) {
                foreach ($jobs as $job) {
                    $projectJob = isset($job['id']) ? ProjectJob::find($job['id']) : new ProjectJob();
                    $projectJob->project_id = $project->id;
                    $projectJob->job = $job['job'];
                    $projectJob->detail = htmlspecialchars($job['detail']);

                    if (!$projectJob->exists) {
                        $projectJob->status = ProjectJob::STATUS_WAITING;
                    }

                    $projectJob->save();
                    $job_ids[] = $projectJob->id;
                }
            }
            $project->jobs()->whereNotIn('id', $job_ids)->delete();

            $file_ids = [];
            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $file = $project->saveFile($file);
                    $file_ids[] = $file->id;
                }
            }
            $project->files()->whereNotIn('id', $file_ids)->delete();

            \DB::commit();

            $redirect = redirect()->route('project.project.show', $project->id)->with('success', trans('global.success_save'));
        } catch (Exception $e) {
            \DB::rollback();

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        \DB::beginTransaction();
        $name = $project->no_project;

        $result = $this->delete($project, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("project.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("project.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $result["status"]] );
    }

    /**
     * change status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_status($id, $status)
    {
        return redirect()->back()->with('success', "sukses update status");
    }

    /**
     * change worker the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_worker($id)
    {
        $model = Project::find($id);

        $workers = [];
        foreach ($model->workers as $worker) {
            $workers[] = [
                'employee_id' => $worker->pivot->employee_id,
                'position_id' => $worker->pivot->position_id
            ];
        }
        return Layout::render('project.project.form_worker', [
            'model' => $model,
            'workers' => $workers,
        ]);
    }

    /**
     * change worker the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_worker_update(WorkerRequest $request, $id)
    {
        $project = Project::find($id);
        $project->workers()->sync($request->workers);

        return redirect()->route('project.project.show', $project->id)->with('success', trans('global.success_save'));
    }

    public function ajaxProject()
    {
        $params = request()->all();

        $query = Project::query();

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $query->where(function ($q1) use ($q) {
                $q1->whereLike("no_project", $q)
                    ->orWhereLike("customer_company", $q);
            });
        }

        $list = [];
        foreach ($query->get() as $project) {
            $list[] = [
                'id' => $project->id,
                'text' => $project->no_project_company
            ];
        }

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }

    public function ajaxProjectJob()
    {
        $params = request()->all();

        $query = ProjectJob::select("id","job as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $query->whereLike("job", $q);
        }

        if ($project_id = request('pid', '')) {
            $query->where("project_id", $project_id );
        }

        $list = $query->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}

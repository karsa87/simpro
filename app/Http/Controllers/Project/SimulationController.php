<?php

namespace App\Http\Controllers\Project;

use App\Util\Base\CoreController;
use App\Util\Base\Layout;

class SimulationController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status = null)
    {
        return Layout::render('project.simulation.index');
    }
}

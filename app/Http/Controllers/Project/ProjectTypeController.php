<?php

namespace App\Http\Controllers\Project;

use App\Http\Requests\Project\ProjectTypeRequest;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Models\ProjectType;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;

class ProjectTypeController extends CoreController
{
    public function index()
    {
        $query = ProjectType::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('name', Util::get('_k'));
            });
        }

        $projectTypes = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))->appends(request()->except(["page"]));

        return Layout::render('project.project_type.index', [
            "projectTypes" => $projectTypes,
        ]);
    }

    public function edit($id)
    {
        $projectType = ProjectType::find($id);
        $projectType = $projectType ? $projectType->toArray() : [];

        return response()->json([
            'url_edit' => route('project.project_type.update', $id),
            'data' => $projectType
        ]);
    }

    public function store(ProjectTypeRequest $request)
    {
        return $this->store_update($request, new ProjectType());
    }

    public function update(ProjectTypeRequest $request, ProjectType $projectType)
    {
        return $this->store_update($request, $projectType);
    }

    private function store_update(ProjectTypeRequest $request, ProjectType $projectType)
    {
        $redirect = redirect()->route('project.project_type.index');
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'name',
            'description',
            'price',
        ]);

        try {
            $model = $projectType;
            $model->fill($input);
            $model->save();
            \DB::commit();

            $redirect->with('success', trans('global.success_save'));
        } catch (Exception $e) {
            \DB::rollback();

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy(ProjectType $projectType)
    {
        \DB::beginTransaction();
        $name = $projectType->name;

        $result = $this->delete($projectType, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("project_type.title"), $name));
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("project_type.title"), $name));
            \DB::rollback();
        }

        return response()->json(["status" => $result["status"]]);
    }

    public function ajaxProjectType()
    {
        $params = request()->all();

        $query = ProjectType::query();

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $query->whereLike("name", $q)
                    ->orWhereLike('price', $q);
        }

        $query->limit(20);

        $list = [];
        foreach ($query->get() as $projectType) {
            $list[] = [
                'id' => $projectType->id,
                'text' => sprintf('%s - %s', $projectType->name, Util::format_currency($projectType->price)),
            ];
        }

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}

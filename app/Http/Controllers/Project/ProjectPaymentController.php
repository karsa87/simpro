<?php

namespace App\Http\Controllers\Project;

use App\Http\Requests\Project\ProjectPaymentRequest;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Models\ProjectPayment;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Project;
use Exception;

class ProjectPaymentController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = ProjectPayment::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('title', Util::get('_k'));
            });
        }

        $projects = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                                ->appends(request()->except(["page"]));

        return Layout::render('project.project_payment.index', [
            'projects' => $projects
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($project_id = null)
    {
        $model = new ProjectPayment();
        $model->project_id = $project_id;

        return Layout::render('project.project_payment.create', [
            'model' => $model,
            'files' => [],
            'employees' => $model->project->workers->pluck('name', 'id')->toArray()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Project\ProjectPaymentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectPaymentRequest $request)
    {
        $redirect = redirect()->route('project.project_payment.create');
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'project_id',
            'title',
            'employee_id',
            'date',
            'information',
            'amount',
        ]);

        try {
            $model = new ProjectPayment();
            $model->fill($input);
            $model->save();

            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $model->saveFile($file);
                }
            }
            \DB::commit();

            // $redirect = redirect()->route('project.project_payment.show', $model->id)->with('success', trans('global.success_save'));
            $redirect = redirect()->route('project.project.show', $model->project_id)->with('success', trans('global.success_save'));
        } catch (Exception $e) {
            \DB::rollback();
            if ($project_id = $request->get('project_id')) {
                $redirect = redirect()->route('project.project_payment.create', ['project_id' => $project_id]);
            }

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectPayment $projectPayment)
    {
        return Layout::render('project.project_payment.show', [
            'model' => $projectPayment,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectPayment $projectPayment)
    {
        $files = $project->files()->select('id','name','information','url','path')->get()->toArray();

        return Layout::render('project.project_payment.edit', [
            'model' => $projectPayment,
            'files' => $files,
            'employees' => $model->project->workers->pluck('name', 'id')->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Project\ProjectPaymentRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectPaymentRequest $request, ProjectPayment $projectPayment)
    {
        $redirect = redirect()->route('project.project_payment.edit', $projectPayment->id);
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'project_id',
            'title',
            'employee_id',
            'date',
            'information',
            'amount',
        ]);

        try {
            $projectPayment->fill($input);
            $projectPayment->save();

            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $projectPayment->saveFile($file);
                }
            }
            \DB::commit();

            // $redirect = redirect()->route('project.project_payment.show', $projectPayment->id)->with('success', trans('global.success_update'));
            $redirect = redirect()->route('project.project.show', $projectPayment->project_id)->with('success', trans('global.success_update'));
        } catch (Exception $e) {
            \DB::rollback();

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectPayment $projectPayment)
    {
        \DB::beginTransaction();
        $name = $projectPayment->project->no_project;

        $result = $this->delete($projectPayment, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("project.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("project.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $result["status"]] );
    }
}

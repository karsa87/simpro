<?php

namespace App\Http\Controllers\Project;

use App\Http\Requests\Project\ProjectVendorRequest;
use App\Http\Controllers\Controller;
use App\Models\ProjectVendor;
use Illuminate\Http\Request;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Project;

class ProjectVendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = ProjectVendor::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereHas('project', function($q1) {
                    $q1->whereLike('no_projet', Util::get('_k'))
                        ->orWhereLike('no_spk', Util::get('_k'));
                })->orWhereHas('vendor', function($q1) {
                    $q1->whereLike('name', Util::get('_k'));
                });
            });
        }

        $projectVendors = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))->appends(request()->except(["page"]));

        return Layout::render('master.file.index', [
            'projectVendors' => $projectVendors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($project_id)
    {
        $model = new ProjectVendor();
        $model->project_id = $project_id;

        return Layout::render('project.project_vendor.create', [
            'model' => $model,
            'jobs' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Project\ProjectVendorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectVendorRequest $request)
    {
        $input = $request->only([
            'project_id',
            'vendor_id'
        ]);

        $model = new ProjectVendor();
        $model->fill($input);
        $model->save();

        $model->jobs()->sync($request->jobs);

        return redirect()->route('project.project.show', $model->project_id)->with('success', trans('global.success_save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectVendor $projectVendor)
    {
        return Layout::render('project.project_vendor.show', [
            'model' => $projectVendor
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectVendor $projectVendor)
    {
        return Layout::render('project.project_vendor.edit', [
            'model' => $projectVendor,
            'jobs' => $projectVendor->jobs()->select('id as project_job_id')->get()->toArray(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Project\ProjectVendorRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectVendorRequest $request, ProjectVendor $projectVendor)
    {
        $input = $request->only([
            'project_id',
            'vendor_id'
        ]);

        $projectVendor->fill($input);
        $projectVendor->save();

        $projectVendor->jobs()->sync($request->jobs);

        return redirect()->route('project.project.show', $projectVendor->project_id)->with('success', trans('global.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $name = sprintf('%s - %s', $projectVendor->project->no_project, $projectVendor->vendor->name);

        $status = $this->delete($projectVendor, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("project_vendor.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("project_vendor.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $status["status"]] );
    }
}

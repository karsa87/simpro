<?php

namespace App\Http\Controllers\Project;

use App\Http\Requests\Project\ProjectImplementationRequest;
use App\Models\ProjectImplementation;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Project;
use Exception;

class ProjectImplementationController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Project::query();

        if (empty(request('sort', ''))) {
            $query->orderBy('no_project', 'desc');
        }

        $query->whereIn('status', [
            Project::STATUS_WAITING,
            Project::STATUS_ON_PROGRESS,
        ]);

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('no_project', Util::get('_k'))
                    ->orWhereLike('customer_name', Util::get('_k'));
            });
        }

        $projects = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                                ->appends(request()->except(["page"]));

        return Layout::render('project.project_implementation.index', [
            'projects' => $projects
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new ProjectImplementation();
        if ($project_id = request('_pid')) {
            $model->project_id = $project_id;
        }

        return Layout::render('project.project_implementation.create', [
            'model' => $model,
            'files' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Project\ProjectImplementationRequest;  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectImplementationRequest $request)
    {
        $redirect = redirect()->route('project.project_implementation.create');
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'project_id',
            'project_job_id',
            'progress',
            'date',
            'information',
        ]);

        try {
            $model = new ProjectImplementation();
            $model->fill($input);
            $model->save();

            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $model->saveFile($file);
                }
            }
            \DB::commit();
            \App\Jobs\RecalculateProgressProjectJob::dispatch($model->project_id);

            $redirect = redirect()->route('project.project_implementation.show', $model->id)->with('success', trans('global.success_save'));
        } catch (Exception $e) {
            \DB::rollback();

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $projectImplementation)
    {
        $project = $projectImplementation;

        $query = $project->projectImplementations();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereHas('project', function ($q1) {
                    $q1->whereLike('no_project', Util::get('_k'));
                })
                    ->orWhereHas('projectJob', function ($q1) {
                        $q1->whereLike('job', Util::get('_k'));
                    });
            });
        }

        $projectImplementations = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                                ->appends(request()->except(["page"]));

        return Layout::render('project.project_implementation.show', [
            'model' => $project,
            'projectImplementations' => $projectImplementations,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_implementation($id)
    {
        $projectImplementation = ProjectImplementation::find($id);

        return Layout::render('project.project_implementation.show_implementation', [
            'model' => $projectImplementation,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectImplementation $projectImplementation)
    {
        $files = $projectImplementation->files()->select('id','name','information','url','path')->get()->toArray();

        return Layout::render('project.project_implementation.edit', [
            'model' => $projectImplementation,
            'files' => $files,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Project\ProjectImplementationRequest;  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectImplementationRequest $request, ProjectImplementation $projectImplementation)
    {
        $redirect = redirect()->route('project.project_implementation.edit', $projectImplementation->id);
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'project_id',
            'project_job_id',
            'progress',
            'date',
            'information',
        ]);

        try {
            $projectImplementation->fill($input);
            $projectImplementation->save();

            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $projectImplementation->saveFile($file);
                }
            }
            \DB::commit();
            \App\Jobs\RecalculateProgressProjectJob::dispatch($projectImplementation->project_id);

            $redirect = redirect()->route('project.project_implementation.show', $projectImplementation->id)->with('success', trans('global.success_update'));
        } catch (Exception $e) {
            \DB::rollback();

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_update")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectImplementation $projectImplementation)
    {
        \DB::beginTransaction();
        $name = $projectImplementation->project->no_project;
        $project_id = $projectImplementation->project_id;

        $result = $this->delete($projectImplementation, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("project.title"), $name) );
            \DB::commit();
            \App\Jobs\RecalculateProgressProjectJob::dispatch($project_id);
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("project.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $result["status"]] );
    }
}

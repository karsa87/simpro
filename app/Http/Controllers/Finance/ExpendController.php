<?php

namespace App\Http\Controllers\Finance;

use App\Http\Requests\Finance\ExpendRequest;
use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Models\Expend;
use App\Util\Base\Layout;

class ExpendController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Expend::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('no_transaction', Util::get('_k'))
                    ->orWhereLike('investor', Util::get('_k'));
            });
        }

        $expends = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                                ->appends(request()->except(["page"]));

        return Layout::render('finance.expend.index', [
            'expends' => $expends
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Expend();
        if ($project_id = request('_pid')) {
            $model->project_id = $project_id;
        }

        return Layout::render('finance.expend.create', [
            'model' => $model,
            'files' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Finance\ExpendRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpendRequest $request)
    {
        $input = $request->only([
            'no_transaction',
            'project_id',
            'category_expend_id',
            'employee_id',
            'information',
            'date',
            'amount',
        ]);

        $model = new Expend();
        $model->fill($input);
        $model->save();

        if ($files = $request->get('files', [])) {
            foreach ($files as $file) {
                $model->saveFile($file);
            }
        }

        return redirect()->route('finance.expend.show', $model->id)->with('success', trans('global.success_save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Expend $expend)
    {
        return Layout::render('finance.expend.show', [
            'model' => $expend
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Expend $expend)
    {
        $files = $expend->files()->select('id','name','information','url','path')->get()->toArray();

        return Layout::render('finance.expend.edit', [
            'model' => $expend,
            'files' => $files,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Finance\ExpendRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ExpendRequest $request, Expend $expend)
    {
        $input = $request->only([
            'no_transaction',
            'project_id',
            'category_expend_id',
            'employee_id',
            'information',
            'date',
            'amount',
        ]);

        $expend->fill($input);
        $expend->save();

        if ($files = $request->get('files', [])) {
            foreach ($files as $file) {
                $expend->saveFile($file);
            }
        }

        return redirect()->route('finance.expend.show', $expend->id)->with('success', trans('global.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expend $expend)
    {
        \DB::beginTransaction();
        $name = $expend->no_transaction;

        $result = $this->delete($expend, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("expend.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("expend.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $result["status"]] );
    }
}

<?php

namespace App\Http\Controllers\Finance;

use App\Http\Requests\Finance\CategoryExpendRequest;
use App\Util\Base\CoreController;
use App\Models\CategoryExpend;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;

class CategoryExpendController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = CategoryExpend::query();

        if (request('_k')) {
            $query->whereLike('name', Util::get('_k'));
        }

        $categoryExpends = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                                ->appends(request()->except(["page"]));

        return Layout::render('finance.category_expend.index', [
            'categoryExpends' => $categoryExpends
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Layout::render('finance.category_expend.create', [
            'model' => new CategoryExpend()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Finance\CategoryExpendRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryExpendRequest $request)
    {
        $input = $request->only([
            'name',
            'status',
        ]);

        $model = new CategoryExpend();
        $model->fill($input);
        $model->save();

        return redirect()->route('finance.category_expend.show', $model->id)->with('success', trans('global.success_save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryExpend $categoryExpend)
    {
        return Layout::render('finance.category_expend.show', [
            'model' => $categoryExpend
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryExpend $categoryExpend)
    {
        return Layout::render('finance.category_expend.edit', [
            'model' => $categoryExpend
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Finance\CategoryExpendRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryExpendRequest $request, CategoryExpend $categoryExpend)
    {
        $input = $request->only([
            'name',
            'status',
        ]);

        $categoryExpend->fill($input);
        $categoryExpend->save();

        return redirect()->route('finance.category_expend.show', $categoryExpend->id)->with('success', trans('global.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryExpend $categoryExpend)
    {
        \DB::beginTransaction();
        $name = $categoryExpend->name;

        $result = $this->delete($categoryExpend, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("category_expend.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("category_expend.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $result["status"]] );
    }

    public function ajaxCategory()
    {
        $params = request()->all();

        $query = CategoryExpend::select("id","name as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $query->whereLike("name", $q);
        }

        $list = $query->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}

<?php

namespace App\Http\Controllers\Finance;

use App\Http\Requests\Finance\IncentiveRequest;
use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Models\Incentive;
use App\Util\Base\Layout;

class IncentiveController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Incentive::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('no_transaction', Util::get('_k'))
                    ->orWhereLike('investor', Util::get('_k'));
            });
        }

        $incentives = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                                ->appends(request()->except(["page"]));

        return Layout::render('finance.incentive.index', [
            'incentives' => $incentives
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Layout::render('finance.incentive.create', [
            'model' => new Incentive()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Finance\IncentiveRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IncentiveRequest $request)
    {
        $input = $request->only([
            'no_transaction',
            'investor',
            'information',
            'date',
            'amount',
        ]);

        $model = new Incentive();
        $model->fill($input);
        $model->save();

        return redirect()->route('finance.incentive.show', $model->id)->with('success', trans('global.success_save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Incentive $incentive)
    {
        return Layout::render('finance.incentive.show', [
            'model' => $incentive
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Incentive $incentive)
    {
        return Layout::render('finance.incentive.edit', [
            'model' => $incentive
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Finance\IncentiveRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IncentiveRequest $request, Incentive $incentive)
    {
        $input = $request->only([
            'no_transaction',
            'investor',
            'information',
            'date',
            'amount',
        ]);

        $incentive->fill($input);
        $incentive->save();

        return redirect()->route('finance.incentive.show', $incentive->id)->with('success', trans('global.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Incentive $incentive)
    {
        \DB::beginTransaction();
        $name = $incentive->no_transaction;

        $result = $this->delete($incentive, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("incentive.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("incentive.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $result["status"]] );
    }
}

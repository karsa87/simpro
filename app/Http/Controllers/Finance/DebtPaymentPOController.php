<?php

namespace App\Http\Controllers\Finance;

use App\Http\Requests\Finance\ProjectDebtPaymentRequest;
use App\Models\ProjectDebtPayment;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Project;

class DebtPaymentPOController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Project::with('shops')->whereHas('shops', function($q) {
            $q->whereRaw('total_amount > paid');
        });

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('no_project', Util::get('_k'));
            });
        }

        $projects = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                                ->appends(request()->except(["page"]));

        return Layout::render('finance.debt_payment_po.index', [
            'projects' => $projects
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_debt()
    {
        $query = ProjectDebtPayment::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('no_transaction', Util::get('_k'));
            });
        }

        $debtPayments = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                                ->appends(request()->except(["page"]));

        return Layout::render('finance.debt_payment_po.index_debt', [
            'debtPayments' => $debtPayments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($project_id = null)
    {
        $model = new ProjectDebtPayment();
        $model->project_id = $project_id;

        return Layout::render('finance.debt_payment_po.create', [
            'model' => $model,
            'files' => []
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Finance\ProjectDebtPaymentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectDebtPaymentRequest $request)
    {
        $redirect = $request->get('project_id', null) ? redirect()->route('finance.debt_payment_po.create') : redirect()->route('finance.debt_payment_po.create', ['project_id'=>$request->get('project_id', null)]);
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'no_transaction',
            'project_id',
            'project_shop_id',
            'store',
            'date',
            'information',
            'amount',
        ]);

        try {
            $model = new ProjectDebtPayment();
            $model->fill($input);
            $model->save();

            $file_ids = [];
            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $file = $model->saveFile($file);
                    $file_ids[] = $file->id;
                }
            }
            $model->files()->whereNotIn('id', $file_ids)->delete();

            \DB::commit();
            \App\Jobs\RecalculateProjectShopPaidJob::dispatch($model->project_shop_id);

            $redirect = redirect()->route('finance.debt_payment_po.show', $model->id)->with('success', trans('global.success_save'));
        } catch (Exception $e) {
            \DB::rollback();

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectDebtPayment $debtPayment)
    {
        return Layout::render('finance.debt_payment_po.show', [
            'model' => $debtPayment,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectDebtPayment $debtPayment)
    {
        $files = $debtPayment->files()->select('id','name','information','url','path')->get()->toArray();

        return Layout::render('finance.debt_payment_po.edit', [
            'model' => $debtPayment,
            'files' => $files
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Finance\ProjectDebtPaymentRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectDebtPaymentRequest $request, ProjectDebtPayment $debtPayment)
    {
        $redirect = redirect()->route('finance.debt_payment_po.edit', $debtPayment->id);
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'no_transaction',
            'project_id',
            'project_shop_id',
            'store',
            'date',
            'information',
            'amount',
        ]);

        try {
            $debtPayment->fill($input);
            $debtPayment->save();

            $file_ids = [];
            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $file = $debtPayment->saveFile($file);
                    $file_ids[] = $file->id;
                }
            }
            $debtPayment->files()->whereNotIn('id', $file_ids)->delete();

            \DB::commit();
            \App\Jobs\RecalculateProjectShopPaidJob::dispatch($debtPayment->project_shop_id);

            $redirect = redirect()->route('finance.debt_payment_po.show', $debtPayment->id)->with('success', trans('global.success_update'));
        } catch (Exception $e) {
            \DB::rollback();

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_update")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectDebtPayment $debtPayment)
    {
        \DB::beginTransaction();
        $name = $debtPayment->no_transaction;
        $project_shop_id = $debtPayment->project_shop_id;

        $result = $this->delete($debtPayment, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("project_debt_payment.title"), $name));
            \DB::commit();
            \App\Jobs\RecalculateProjectShopPaidJob::dispatch($project_shop_id);
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("project_debt_payment.title"), $name));
            \DB::rollback();
        }

        return response()->json(["status" => $result["status"]]);
    }
}

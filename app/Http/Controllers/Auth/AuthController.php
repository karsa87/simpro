<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Util\Events\UserLogin;
use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            return $this->login();
        }

        return view('auth.login');
    }

    public function login()
    {
        request()->validate([
            'email' => 'required|email',
            'password' => 'required|min:5',
        ]);

        $input = request()->all();
        $user = User::where('email', $input['email'])->first();

        if (empty($user)) {
            return redirect('login')
                        ->withErrors([
                            'message' => trans('auth.message.email_not_found', [
                                'value' => $input['email']
                            ])
                        ])
                        ->withInput();
        } elseif (!\Hash::check(request('password'), $user->password)) {
            return redirect('login')
                        ->withErrors([
                            'message' => trans('auth.message.password_not_correct')
                        ])
                        ->withInput();
        } elseif (!$user->isActive()) {
            return redirect('login')
                        ->withErrors([
                            'message' => trans('auth.message.email_deactived', [
                                'value' => $input['email']
                            ])
                        ])
                        ->withInput();
        } else {
            \Auth::login($user, false);

            event(new UserLogin($user));

            return redirect()->intended('/')->with('message', 'Success login');
        }
    }

    public function logout()
    {
        \Session::flush();
        \Auth::logout();
        return redirect('login');
    }

    public function impersonate($id)
    {
        $user_name = '';
        try {
            $user_impersonate = User::find($id);
            $manager = app('impersonate');
            $manager->take(auth()->user(), $user_impersonate);

            $user_name = $user_impersonate->name;
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', trans("auth.message.failed_impersonate"));
        }

        event(new UserLogin($user_impersonate));
        return redirect()->route('dashboard')
                ->with('success', trans("auth.message.success_impersonate", [
                    'user' => $user_name
                ]));
    }

    public function impersonate_leave()
    {
        $user_name = '';
        try {
            $manager = app('impersonate');
            if (!$manager->isImpersonating()) {
                return redirect()->back()->with('error', trans("auth.message.not_impersonate"));
            }
            $user_impersonate = auth()->user();
            $manager->leave();
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', trans("auth.message.failed_leave_impersonate"));
        }

        event(new UserLogin(auth()->user()));
        return redirect()->route('dashboard')
                ->with('success', trans("auth.message.success_leave_impersonate", [
                    'user' => $user_name
                ]));
    }
}

<?php

namespace App\Http\Controllers\Setting;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\LockData;
use App\Models\Menu;

class LockDataController extends CoreController
{
    public function index()
    {
        $query = LockData::with('menu')->orderBy('updated_at','DESC');

        if (request('_m')) {
            $query->where('menu_id', Util::get('_m'));
        }

        if (request('_k')) {
            $query->whereHas('menu', function($q) {
                $q->whereLike('name', Util::get('_k'));
            });
        }

        $lockDatas = $query->paginate(Layout::ROW_PER_PAGE)->appends(request()->except(["page"]));

        $menus = Menu::where(function($q) {
            $q->whereLike('route', 'transaction.purchase_order.index')
                ->orWhereLike('route', 'transaction.purchase_order')
                ->orWhereLike('route', 'transaction.stock_adjust')
                ->orWhereLike('route', 'transaction.sales_order')
                ->orWhereLike('route', 'transaction.sales_invoice')
                ->orWhereLike('route', 'transaction.sales_return')
                ->orWhereLike('route', 'warehouse.stock_opname')
                ->orWhereLike('route', 'warehouse.stock_in')
                ->orWhereLike('route', 'accounting.credit_note');
        })->child()->orderBy('name', 'ASC')->get();
        
        return Layout::render('setting.lock_data.index', [
            "lockDatas" => $lockDatas,
            "menus" => $menus,
        ]);
    }

    public function edit($id)
    {
        $lockData = LockData::find($id);
        $lockData = $lockData ? $lockData->toArray() : [];

        return response()->json([
            'url_edit' => route('setting.lock_data.update', $id),
            'data' => $lockData
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        
        $redirect = redirect()->back();
        $query = LockData::query();
        $result = $this->save($query, $input,[
            "rules" => [
                "menu_id" => 'required|unique:lock_data,menu_id,'.($input['id'] ?: 'NULL') .',id',
                "edit_day" => 'required|numeric|min:1',
                "delete_day" => 'required|numeric|min:1',
            ],
        ]);

        if ($result['status']) {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            \Session::put(config('system.session.list_lock'), LockData::all()->keyBy('menu_id'));
        } else {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = LockData::find($id);
        $name = $row->menu->name;

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("lock_data.title"), $name) );
            \DB::commit();
            \Session::put(config('system.session.list_lock'), LockData::all()->keyBy('menu_id'));
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("lock_data.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }
}

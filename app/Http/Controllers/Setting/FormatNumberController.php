<?php

namespace App\Http\Controllers\Setting;

use App\Util\Base\CoreController;
use Illuminate\Validation\Rule;
use App\Models\FormatNumber;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Menu;

class FormatNumberController extends CoreController
{
    public function index()
    {
        $query = FormatNumber::with('menu')->orderBy('updated_at','DESC');

        if (request('_m')) {
            $query->where('menu_id', Util::get('_m'));
        }

        if (request('_k')) {
            $query->whereHas('menu', function($q) {
                $q->whereLike('name', Util::get('_k'));
            });
        }

        $formatNumbers = $query->paginate(Layout::ROW_PER_PAGE)->appends(request()->except(["page"]));

        $menus = Menu::where(function($q) {
            $q->whereLike('route', 'finance.incentive.index')
                ->orWhereLike('route', 'finance.expend.index')
                ->orWhereLike('route', 'project.project.index');
        })->child()->orderBy('name', 'ASC')->get();

        return Layout::render('setting.format_number.index', [
            "formatNumbers" => $formatNumbers,
            "menus" => $menus,
        ]);
    }

    public function edit($id)
    {
        $formatNumber = FormatNumber::find($id);
        $formatNumber = $formatNumber ? $formatNumber->toArray() : [];

        return response()->json([
            'url_edit' => route('setting.format_number.update', $id),
            'data' => $formatNumber
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }

    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null)
    {
        $input = request()->except(["_token"]);
        $input['delimiter'] = htmlspecialchars(request('delimiter', ''));

        $redirect = redirect()->back();
        $query = FormatNumber::query();
        $result = $this->save($query, $input, [
            "rules" => [
                "menu_id" => [
                    'required',
                    Rule::unique('format_number')->where(function ($query) use($input) {
                        $query->where('menu_id', $input['menu_id'])
                                ->where('company_id', session(config('system.session.active_company'))->id)
                                ->whereNull('deleted_at');
                        if ($input['id']) {
                            $query->where('id', '!=', $input['id']);
                        }

                        return $query;
                    }),
                ],
                'delimiter' => 'regex:/[^a-zA-Z0-9]/i',
            ],
        ]);

        if ($result['status']) {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
            \Cache::put(config('system.session.list_format_number'), FormatNumber::all()->keyBy('menu_id'));
        } else {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = FormatNumber::find($id);
        $name = $row->menu->name;

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("format_number.title"), $name) );
            \DB::commit();
            \Cache::put(config('system.session.list_format_number'), FormatNumber::all()->keyBy('menu_id'));
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("format_number.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $status["status"]] );
    }
}

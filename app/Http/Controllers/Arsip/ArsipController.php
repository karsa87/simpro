<?php

namespace App\Http\Controllers\Arsip;

use App\Http\Requests\Arsip\ArsipRequest;
use Illuminate\Support\MessageBag;
use App\Util\Base\CoreController;
use Illuminate\Validation\Rule;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Arsip;

class ArsipController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Arsip::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('name', Util::get('_k'));
            });
        }

        $arsips = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))->appends(request()->except(["page"]));

        return Layout::render('arsip.arsip.index', [
            'arsips' => $arsips
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Layout::render('arsip.arsip.create', [
            'model' => new Arsip(),
            'files' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Arsip\ArsipRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArsipRequest $request)
    {
        $redirect = redirect()->route('arsip.arsip.create');
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'name',
        ]);

        try {
            $model = new Arsip();
            $model->fill($input);
            $model->save();

            $file_ids = [];
            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $file = $model->saveFile($file);
                    $file_ids[] = $file->id;
                }
            }
            $model->files()->whereNotIn('id', $file_ids)->delete();

            \DB::commit();

            $redirect = redirect()->route('arsip.arsip.show', $model->id)->with('success', trans('global.success_save'));
        } catch (Exception $e) {
            \DB::rollback();

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Arsip $arsip)
    {
        return Layout::render('arsip.arsip.show', [
            'model' => $arsip,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Arsip $arsip)
    {
        $files = $arsip->files()->select('id','name','information','url','path')->get()->toArray();

        return Layout::render('arsip.arsip.edit', [
            'model' => $arsip,
            'files' => $files,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Arsip\ArsipRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArsipRequest $request, Arsip $arsip)
    {
        $redirect = redirect()->route('arsip.arsip.edit', $arsip->id);
        $result = [
            'status' => true,
            'error' => new MessageBag
        ];

        \DB::beginTransaction();
        $input = $request->only([
            'name',
        ]);

        try {
            $arsip->fill($input);
            $arsip->save();

            $file_ids = [];
            if ($files = $request->get('files', [])) {
                foreach ($files as $file) {
                    $file = $arsip->saveFile($file);
                    $file_ids[] = $file->id;
                }
            }
            $arsip->files()->whereNotIn('id', $file_ids)->delete();

            \DB::commit();

            $redirect = redirect()->route('arsip.arsip.show', $arsip->id)->with('success', trans('global.success_update'));
        } catch (Exception $e) {
            \DB::rollback();

            if (!empty($e->getMessage())) {
                $result['error']->add('throw', $e->getMessage());
            }

            $redirect->withInput( $request->except(['token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_update")) );
        }

        return $redirect;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Arsip $arsip)
    {
        \DB::beginTransaction();
        $name = $arsip->name;

        $result = $this->delete($arsip, 'soft');
        if($result["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("arsip.title"), $name));
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("arsip.title"), $name));
            \DB::rollback();
        }

        return response()->json(["status" => $result["status"]]);
    }
}

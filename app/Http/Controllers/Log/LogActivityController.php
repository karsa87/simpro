<?php

namespace App\Http\Controllers\Log;

use App\Util\Base\CoreController;
use App\Models\LogActivity;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\user;

class LogActivityController extends CoreController
{
    public function index()
    {
        $query = LogActivity::with('user', 'record')->orderBy('id','DESC');

        if( request('search_datetime') )
        {
            $search_datetime = Util::get('search_datetime');
            $search_datetime = explode(" - ", $search_datetime);

            $query->whereBetween('log_datetime', $search_datetime);
        }

        if( request('search_table') )
        {
            $query->where('table', Util::get('search_table'));
        }

        if( request('search_user_id') )
        {
            $query->where('user_id', Util::get('search_user_id'));
        }

        if( request('_ri') )
        {
            $query->where('record_id', Util::get('_ri'));
        }

        if( is_numeric(request('search_transaction_type')) )
        {
            $query->transactionType(Util::get('search_transaction_type'));
        }

        $logs = $query->paginate(request('showing', Layout::ROW_PER_PAGE))->appends( request()->except(["page"]) );
        $tables = LogActivity::select('table')->groupBy('table')->get()->pluck('table')->unique();
        $users = User::active()->get()->pluck('name','id');

        return Layout::render('log.log_activity.index', [
            "logs" => $logs,
            "tables" => $tables,
            "users" => $users,
        ]);
    }
}

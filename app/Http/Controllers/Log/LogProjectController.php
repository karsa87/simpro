<?php

namespace App\Http\Controllers\Log;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LogActivity;
use App\Util\Helpers\Util;
use App\Models\Project;

class LogProjectController extends Controller
{
    public function index(Request $request)
    {
        $query = LogActivity::whereIn('record_type', [
            Project::class,
            ProjectDebtPayment::class,
            ProjectImplementation::class,
            ProjectJob::class,
            ProjectPayment::class,
            ProjectShop::class,
            ProjectVendor::class,
            ProjectShopDetail::class,
        ]);

        $date = [
            date('Y-m-d 00:00', strtotime('-7days')),
            date('Y-m-d 23:59'),
        ];

        if (request('_dt')) {
            $date = Util::get('_dt');
            $date = explode(" - ", $date);
        }
        $query->whereBetween('log_datetime', $date);

        $query->selectRaw("log_activity.*, log_datetime::DATE as date");
        $logs = $query->get();

        return view('log.project.index', [
            'logs' => $logs,
            'date' => implode(' - ', $date),
        ]);
    }
}

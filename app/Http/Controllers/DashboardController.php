<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ProjectDebtPayment;
use App\Models\ProjectPayment;
use Illuminate\Http\Request;
use App\Models\ProjectShop;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Project;
use App\Models\Expend;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Support\Str;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
//         $routeCollection = \Illuminate\Support\Facades\Route::getRoutes();

//         $routeNames = collect();
//         foreach ($routeCollection as $value) {
//             $name = $value->getName();
//             $contain = Str::contains($name, [
//                 'debugbar.',
//                 'ignition.',
//                 'livewire.',
//                 'change.language',
//                 'ajax.',
//                 'upload.',
//                 'assets.',
//                 'master.company.change.active',
//                 '.update',
//                 '.store',
//             ]);

//             if (! empty($name) && ! $contain) {
//                 $exists = \App\Models\Permission::whereSlug($name)->exists();

//                 if (!$exists) {
//                     $routeNames->push($name);
//                 }
//             }
//         }
// dd($routeNames->toArray());
        $date = [
            date('Y-m-01'),
            date('Y-m-t')
        ];

        $projects_run = Project::whereIn('status', [
            Project::STATUS_WAITING,
            Project::STATUS_ON_PROGRESS,
        ])->count();

        $projects_done = Project::whereIn('status', [
            Project::STATUS_DONE,
            Project::STATUS_CLOSE,
        ])->count();

        //income
        $income_all = ProjectPayment::selectRaw('EXTRACT(MONTH from date) as month, SUM(amount) as amount')
                            ->whereBetween('date', [
                                date('Y-01-01 00:00:00'),
                                date('Y-12-31 23:59:59'),
                            ])
                            ->groupBy(\DB::raw('EXTRACT(MONTH from date)'))
                            ->get()->keyBy('month');

        $incomes = [];
        for ($i=1; $i <= 12; $i++) {
            if ($income_all->has($i)) {
                $incomes[$i] = $income_all[$i]->amount;
            } else {
                $incomes[$i] = 0;
            }
        }

        //expends
        $projectShops = ProjectShop::selectRaw("EXTRACT(MONTH from date) as month, SUM(total_amount) as amount")
                            ->whereBetween('date', [
                                date('Y-01-01 00:00:00'),
                                date('Y-12-31 23:59:59'),
                            ])
                            ->groupBy(\DB::raw('EXTRACT(MONTH from date)'));
        $expends = Expend::selectRaw("EXTRACT(MONTH from date) as month, SUM(amount) as amount")
                            ->whereBetween('date', [
                                date('Y-01-01 00:00:00'),
                                date('Y-12-31 23:59:59'),
                            ])
                            ->groupBy(\DB::raw('EXTRACT(MONTH from date)'));

        $expend_all = $projectShops->unionAll($expends)->get()->groupBy('month');
        $expends = [];
        for ($i=1; $i <= 12; $i++) {
            if ($expend_all->has($i)) {
                $expends[$i] = $expend_all[$i]->sum('amount');
            } else {
                $expends[$i] = 0;
            }
        }

        $omzet = [];
        for ($i=1; $i <= 12; $i++) {
            $omzet[$i] = $incomes[$i] - $expends[$i];
        }

        return Layout::render('dashboard.index', [
            'projects' => [
                'run' => $projects_run,
                'done' => $projects_done,
            ],
            'expends' => $expends,
            'incomes' => $incomes,
            'omzet' => $omzet,
        ]);
    }

    public function change_language($locale = 'en')
    {
        \Session::put(config('system.session.active_language'), $locale);

        return redirect()->back()->with('success', trans('dashboard.message.success_change_language'));
    }
}

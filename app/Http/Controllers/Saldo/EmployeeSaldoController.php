<?php

namespace App\Http\Controllers\Saldo;

use App\Http\Requests\Saldo\SaldoEmployeeRequest;
use App\Util\Base\CoreController;
use App\Models\SaldoEmployee;
use Illuminate\Http\Request;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;

class EmployeeSaldoController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = SaldoEmployee::query();

        if (request('_em')) {
            $query->whereEmployeeId(Util::get('_em'));
        }

        if (request('_k')) {
            $query->whereHas('employee', function($q) {
                $q->whereLike('name', Util::get('_k'))
                    ->orWhereLike('code', Util::get('_k'))
                    ->orWhereLike('nik', Util::get('_k'));
            });
        }

        $saldoEmployees = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))->appends(request()->except(["page"]));

        return Layout::render('saldo.employee.index', [
            'saldoEmployees' => $saldoEmployees
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Layout::render('saldo.employee.create', [
            'model' => new SaldoEmployee()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaldoEmployeeRequest $request)
    {
        $input = $request->only([
            'employee_id',
            'saldo',
            'date',
        ]);
        $model = new SaldoEmployee();
        $model->fill($input);
        $model->save();

        $employee = $model->employee;
        $employee->saldo += $model->saldo;
        $employee->save();

        return redirect()->route('saldo.employee.show', $model->id)->with('success', trans('global.success_save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SaldoEmployee $saldoEmployee)
    {
        return Layout::render('saldo.employee.show', [
            'model' => $saldoEmployee
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SaldoEmployee $saldoEmployee)
    {
        return Layout::render('saldo.employee.edit', [
            'model' => $saldoEmployee
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaldoEmployeeRequest $request, SaldoEmployee $saldoEmployee)
    {
        $employee = $saldoEmployee->employee;
        $employee->saldo -= $saldoEmployee->saldo;

        $input = $request->only([
            'employee_id',
            'saldo',
            'date',
        ]);

        $saldoEmployee->fill($input);
        $saldoEmployee->save();
        $employee->saldo += $saldoEmployee->saldo;
        $employee->save();

        return redirect()->route('saldo.employee.show', $saldoEmployee->id)->with('success', trans('global.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SaldoEmployee $saldoEmployee)
    {
        \DB::beginTransaction();
        $row = $saldoEmployee;
        $employee = $row->employee;
        $employee->saldo -= $row->saldo;
        $name = $employee->name;

        $result = $this->delete($row, 'soft');
        if($result["status"]){
            $employee->save();
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("saldo_employee.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("saldo_employee.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $result["status"]] );
    }
}

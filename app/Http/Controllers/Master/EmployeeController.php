<?php

namespace App\Http\Controllers\Master;

use jeremykenedy\LaravelUsers\App\Http\Controllers\LaravelUsersController;
use App\Util\Base\CoreController;
use App\Util\Helpers\Upload;
use App\Util\Helpers\Util;
use App\Models\Permission;
use App\Util\Base\Layout;
use App\Models\User;
use App\Models\Role;

class EmployeeController extends CoreController
{
    public function index()
    {
        $query = User::whereHas('roles', function($q) {
            $q->where('level', '<=', Role::LEVEL_EMPLOYEE);
        });

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy)) {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if( request('_nm') )
        {
            $query->whereLike('name', Util::get('_nm'));
        }

        if( request('_e') )
        {
            $query->whereLike('email', Util::get('_e'));
        }

        if( request('_nk') )
        {
            $query->whereLike('nik', Util::get('_nk'));
        }

        if( request('_cd') )
        {
            $query->whereLike('code', Util::get('_cd'));
        }

        if( request('_ad') )
        {
            $query->whereLike('address', Util::get('_ad'));
        }

        if( request('_rl') )
        {
            $role_id = Util::get('_rl');
            $query->whereHas('roles', function($q) use($role_id) {
                $q->where('role_id', $role_id);
            });
        }

        if( is_numeric(request('_sts')) )
        {
            $query->where('status', Util::get('_sts'));
        }

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('name', Util::get('_k'))
                    ->orWhereLike('code', Util::get('_k'))
                    ->orWhereLike('nik', Util::get('_k'));
            });
        }

        $permissions = Permission::all()->pluck('name','id')->toArray();
        $roles = Role::whereIn('level', array_keys(Role::getListRole()))->pluck('name','id')->toArray();

        $users = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );
        return Layout::render('master.employee.index', [
            "users" => $users,
            'roles' => $roles,
            "permissions" => $permissions,
        ]);
    }

    public function show(User $employee)
    {
        return Layout::render('master.employee.show', [
            'model' => $employee,
        ]);
    }

    public function create()
    {
        $model = new User();

        $permissions = Permission::all()->pluck('name','id')->toArray();
        $roles = Role::whereIn('level', array_keys(Role::getListRole()))->pluck('name','id')->toArray();

        return Layout::render('master.employee.form', [
            'model' => $model,
            'roles' => $roles,
            'permissions' => $permissions,
        ]);
    }

    public function edit(User $employee)
    {
        $permissions = Permission::all()->pluck('name','id')->toArray();
        $roles = Role::whereIn('level', array_keys(Role::getListRole()))->pluck('name','id')->toArray();

        return Layout::render('master.employee.form', [
            'model' => $employee,
            'roles' => $roles,
            'permissions' => $permissions,
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }

    public function update(User $employee)
    {
        return $this->store_update($employee->id);
    }

    private function store_update($id = null, $isprofile = false)
    {
        $input = request()->except(["_token"]);
        $query = User::query();

        $user = null;
        if ($input["id"]) {
            $user = User::find($input["id"]);
        }

        $valid_password = 'required|min:5';
        if ($input['id']) {
            $valid_password = isset($input['password']) ? 'min:5' : '';
        }

        $options = [
            "rules" => [
                "nik" => 'nullable|numeric',
                "name" => 'required|max:255',
                "email" => 'required|max:255|unique:users,email,'.($input['id'] ?: 'NULL') .',id',
                "phone" => [
                    'nullable',
                    'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/',
                    'not_regex:/[a-zA-Z]/',
                    'max:16'
                ],
                "password" => $valid_password,
                // 'user_role' => 'required',
                // 'user_company' => 'required',
            ],
            'remove' => ['user_role', 'user_company']
        ];

        if ($isprofile) {
            unset($options['rules']['user_role']);
            unset($options['rules']['user_company']);
        }

        $result = $this->save($query, $input, $options);

        $redirect = redirect()->back();
        if ($result['status']) {
            $user = $result['object'];

            if (($user->photo_profile = json_decode($user->photo_profile, TRUE))
                    && count($user->photo_profile) > 0) {
                $user->photo_profile = json_encode(Upload::moveFiles($user->photo_profile, sprintf('%s/%s',(new User)->getTable(), $user->id)));
            }
            $user->save();

            $role = Role::employee()->first();
            $user->syncRoles([$role->id]);

            if (array_key_exists('user_company', $input) && $input['user_company']) {
                $companies = $input['user_company'];
                $user->company()->sync($companies);
            } else {
                $user->company()->sync([
                    \Session::get(config('system.session.active_company'))->id
                ]);
            }

            if ($isprofile) {
                $redirect = redirect()->route('master.employee.profile');
            } else {
                $redirect = redirect()->route('master.employee.show', [$user->id]);
            }

            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        } else {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy(User $employee)
    {
        \DB::beginTransaction();
        $employee->detachAllRoles();
        $employee->detachAllPermissions();
        $name = $employee->user_realname;

        $status = $this->delete($employee, 'soft');

        if ($status["status"]) {
            session()->flash('success', sprintf("%s %s %s", trans("user.title"), trans("global.success_delete"), $name) );
            \DB::commit();
        } else {
            session()->flash('error', sprintf("%s %s %s", trans("user.title"), trans("global.failed_delete"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $status["status"]] );
    }

    public function upload()
    {
        $path = Upload::uploadTmp(request()->file('file'));

        return [
            'path' => $path
        ];
    }

    public function profile()
    {
        $user = auth()->user();
        if (request()->isMethod('post')) {
            return $this->store_update($user->id, true);
        } else {
            return Layout::render('master.employee.profile', [
                "model" => $user,
            ]);
        }
    }

    public function ajaxUser()
    {
        $params = request()->all();

        $list = User::select("id","name as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $list->whereLike("name", $q);
        }

        $list = $list->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }

    public function ajaxUserEmployee()
    {
        $params = request()->all();

        $list = User::select("id","name as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $list->whereLike("name", $q);
        }

        $list = $list->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }

    public function ajaxUserSales()
    {
        $params = request()->all();

        $list = User::select("id","name as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $list->whereLike("name", $q);
        }

        $list = $list->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }

    public function ajaxUserWarehouse()
    {
        $params = request()->all();

        $list = User::select("id","name as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $list->whereLike("name", $q);
        }

        $list = $list->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}

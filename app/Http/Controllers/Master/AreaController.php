<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use Illuminate\Validation\Rule;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Area;

class AreaController extends CoreController
{
    public function index()
    {
        $query = Area::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if (request('_nm')) {
            $query->whereLike('name', Util::get('_nm'));
        }

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('name', Util::get('_k'))
                    ->orWhereLike('code', Util::get('_k'));
            });
        }

        $areas = $query->paginate(Layout::ROW_PER_PAGE)->appends(request()->except(["page"]));

        return Layout::render('master.area.index', [
            "areas" => $areas,
        ]);
    }

    public function edit($id)
    {
        $area = Area::find($id);
        $area = $area ? $area->toArray() : [];

        return response()->json([
            'url_edit' => route('master.area.update', $id),
            'data' => $area
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }

    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null)
    {
        $input = request()->except(["_token"]);

        $redirect = redirect()->back();
        $query = Area::query();
        $result = $this->save($query, $input,[
            "rules" => [
                'code' => [
                    'required',
                    Rule::unique('areas')->where(function ($query) use($input) {
                        $query->where('code', $input['code'])
                                ->where('company_id', session(config('system.session.active_company'))->id)
                                ->whereNull('deleted_at');
                        if ($input['id']) {
                            $query->where('id', '!=', $input['id']);
                        }

                        return $query;
                    }),
                ],
                "name" => [
                    'required',
                    'max:255',
                    'not_regex:/["]/',
                ],
            ],
        ]);

        if ($result['status']) {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        } else {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Area::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("area.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("area.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxArea()
    {
        $params = request()->all();

        $list = Area::select("id","name as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $list->whereLike("name", $q)
                    ->orWhereLike('code', $q);
        }

        $list = $list->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}

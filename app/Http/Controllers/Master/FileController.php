<?php

namespace App\Http\Controllers\Master;

use App\Http\Requests\Master\FileRequest;
use Illuminate\Support\Facades\Storage;
use App\Util\Base\CoreController;
use Illuminate\Validation\Rule;
use App\Util\Helpers\Upload;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\File;

class FileController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = File::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('name', Util::get('_k'));
            });
        }

        $files = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))->appends(request()->except(["page"]));

        return Layout::render('master.file.index', [
            'files' => $files
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Layout::render('master.file.create', [
            'model' => new File()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Master\FileRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FileRequest $request)
    {
        $input = $request->only([
            'name',
            'url',
            'information',
        ]);
        $model = new File();
        $model->fill($input);
        $model->save();

        return redirect()->route('master.file.show', $model->id)->with('success', trans('global.success_save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {
        return Layout::render('master.file.show', [
            'model' => $file
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(File $file)
    {
        return Layout::render('master.file.edit', [
            'model' => $file
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Master\FileRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FileRequest $request, File $file)
    {
        $input = $request->only([
            'name',
            'url',
            'information',
        ]);

        $file->fill($input);
        $file->save();

        return redirect()->route('master.file.show', $file->id)->with('success', trans('global.success_update'));
    }

    public function destroy(File $file)
    {
        \DB::beginTransaction();
        $name = $file->name;

        $status = $this->delete($file, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("file.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("file.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $status["status"]] );
    }

    public function upload()
    {
        $path = Upload::uploadTmp(request()->file('file'));

        return [
            'path' => $path
        ];
    }

    public function download(File $file)
    {
        return Storage::download($file->path);
    }
}

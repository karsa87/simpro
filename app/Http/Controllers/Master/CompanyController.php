<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use App\Util\Helpers\Upload;
use App\Models\FormatNumber;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\LockData;
use App\Models\Company;

class CompanyController extends CoreController
{
    public function index()
    {
        $query = Company::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $orderBy = explode('|', $orderBy);
            foreach ($orderBy as $column) {
                $query->orderBy($column, trans("global.array.sorting.$sorting"));
            }
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if (request('_nm')) {
            $query->whereLike('name', Util::get('_nm'));
        }

        if (request('_k')) {
            $query->whereLike('name', Util::get('_k'));
        }

        $companies = $query->paginate(Layout::ROW_PER_PAGE)->appends( request()->except(["page"]) );

        return Layout::render('master.company.index', [
            "companies" => $companies,
        ]);
    }

    public function show($id)
    {
        $model = Company::whereId($id)->first();

        return Layout::render('master.company.show', [
            'model' => $model,
        ]);
    }

    public function create()
    {
        $model = new Company();

        return Layout::render('master.company.form', [
            'model' => $model,
        ]);
    }

    public function edit($id)
    {
        $model = Company::find($id);

        if ($model) {
            return Layout::render('master.company.form', [
                'model' => $model,
            ]);
        } else {
            return redirect()->back()->with('error', trans('company.message.not_found'));
        }
    }

    public function store()
    {
        return $this->store_update();
    }
    
    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null) 
    {
        $input = request()->except(["_token"]);
        $redirect = redirect()->back();
        $query = Company::query();

        $result = $this->save($query, $input,[
            "rules" => [
                'name' => 'required|max:255',
                "phone" => [
                    'nullable',
                    'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/',
                    'not_regex:/[a-zA-Z]/',
                    'max:16'
                ],
                "fax" => [
                    'nullable',
                    'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/',
                    'not_regex:/[a-zA-Z]/',
                    'max:16'
                ],
                'postal_code' => 'numeric|nullable'
            ],
        ]);

        if( $result['status'] )
        {
            $company = $result['object'];

            if (($company->logo = json_decode($company->logo, TRUE)) 
                    && count($company->logo) > 0) {
                $company->logo = json_encode(Upload::moveFiles($company->logo, sprintf('%s/%s',(new Company)->getTable(), $company->id)));
            }
            $company->save();
            
            $redirect = redirect()->route('master.company.show', [$company->id]);
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        }
        else
        {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Company::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("company.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("company.title"), $name) );
            \DB::rollback();
        }
        
        return response()->json( ["status" => $status["status"]] );
    }

    public function change_active_company($id)
    {
        $company = Company::find($id);

        if ($company) {
            \Session::forget(config('system.session.active_company'));
            \Session::put(config('system.session.active_company'), $company);
            \Session::put(config('system.session.list_lock'), LockData::all()->keyBy('menu_id'));
            \Cache::put(config('system.session.list_format_number'), FormatNumber::all()->keyBy('menu_id'));
            
            return redirect()->back()->with('success', trans('company.message.success_change_active'));
        } else {
            return redirect()->back()->with('error', trans('company.message.not_found'));
        }
    }

    public function uploadLogo()
    {
        $path = Upload::uploadTmp(request()->file('file'));
        
        return [
            'path' => $path
        ];
    }

    public function ajaxCompany()
    {
        $params = request()->all();

        $list = Company::select("id","name as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $list->whereLike("name", $q);
        }

        $list = $list->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}

<?php

namespace App\Http\Controllers\Master;

use App\Http\Requests\Master\VendorRequest;
use App\Util\Base\CoreController;
use Illuminate\Validation\Rule;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Vendor;

class VendorController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Vendor::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('name', Util::get('_k'))
                    ->orWhereLike('phone', Util::get('_k'))
                    ->orWhereLike('email', Util::get('_k'));
            });
        }

        $vendors = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))->appends(request()->except(["page"]));

        return Layout::render('master.vendor.index', [
            'vendors' => $vendors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Layout::render('master.vendor.create', [
            'model' => new Vendor()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Master\VendorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorRequest $request)
    {
        $input = $request->only([
            'name',
            'email',
            'phone',
            'fax',
            'address',
        ]);
        $model = new Vendor();
        $model->fill($input);
        $model->save();

        return redirect()->route('master.vendor.show', $model->id)->with('success', trans('global.success_save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        return Layout::render('master.vendor.show', [
            'model' => $vendor
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Vendor $vendor)
    {
        return Layout::render('master.vendor.edit', [
            'model' => $vendor
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Master\VendorRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VendorRequest $request, Vendor $vendor)
    {
        $input = $request->only([
            'name',
            'email',
            'phone',
            'fax',
            'address',
        ]);

        $vendor->fill($input);
        $vendor->save();

        return redirect()->route('master.vendor.show', $vendor->id)->with('success', trans('global.success_update'));
    }

    public function destroy(Vendor $vendor)
    {
        \DB::beginTransaction();
        $name = $vendor->name;

        $status = $this->delete($vendor, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("vendor.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("vendor.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxVendor()
    {
        $params = request()->all();

        $query = Vendor::select("id","name as text");

        $k = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($k) {
            $query->where(function($q) use ($k) {
                $q->whereLike('name', $k)
                    ->orWhereLike('phone', $k)
                    ->orWhereLike('email', $k);
            });
        }

        $list = $query->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}

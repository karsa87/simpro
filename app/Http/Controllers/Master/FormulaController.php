<?php

namespace App\Http\Controllers\Master;

use App\Http\Requests\Master\FormulaRequest;
use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Formula;
use App\Models\FormulaItem;

class FormulaController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Formula::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('name', Util::get('_k'))
                    ->orWhereLike('code', Util::get('_k'));
            });
        }

        $formulas = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))->appends(request()->except(["page"]));

        return Layout::render('master.formula.index', [
            'formulas' => $formulas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Layout::render('master.formula.create', [
            'model' => new Formula(),
            'formulaItems' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Master\FormulaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormulaRequest $request)
    {
        $input = $request->only([
            'name',
            'code',
            'description',
        ]);
        $model = new Formula();
        $model->fill($input);

        $items = collect($request->get('items', []));
        $model->subtotal = $items->sum('amount');
        $model->tax_percentage = 15;
        $model->tax_amount =  ($model->subtotal * 15) / 100;
        $model->total_amount = $model->subtotal + $model->tax_amount;
        $model->save();

        if ($request->get('items', [])) {
            foreach ($request->get('items', []) as $item) {
                unset($item['id']);
                $formulaItem = new FormulaItem();
                $formulaItem->fill($item);
                $formulaItem->formula_id = $model->id;
                $formulaItem->save();
            }
        }

        return redirect()->route('master.formula.show', $model->id)->with('success', trans('global.success_save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Formula $formula)
    {
        $formula->loadMissing('formula_items');

        return Layout::render('master.formula.show', [
            'model' => $formula
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Formula $formula)
    {
        $formula->loadMissing('formula_items');

        $formulaItems = [];
        foreach ($formula->formula_items as $item) {
            $formulaItems[] = [
                'id' => $item->id,
                'material_id' => $item->material_id,
                'unit_id' => $item->unit_id,
                'coefficient' => $item->coefficient,
                'amount' => $item->amount,
            ];
        }

        return Layout::render('master.formula.edit', [
            'model' => $formula,
            'formulaItems' => $formulaItems,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Master\FormulaRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormulaRequest $request, Formula $formula)
    {
        $input = $request->only([
            'code',
            'name',
            'description',
        ]);

        $formula->fill($input);

        $items = collect($request->get('items', []));
        $formula->subtotal = $items->sum('amount');
        $formula->tax_percentage = 15;
        $formula->tax_amount =  ($formula->subtotal * 15) / 100;
        $formula->total_amount = $formula->subtotal + $formula->tax_amount;
        $formula->save();

        $formulaItemInsert = [];
        if ($request->get('items', [])) {
            foreach ($request->get('items', []) as $item) {
                $formulaItem = !empty($item['id']) ? FormulaItem::find($item['id']) : new FormulaItem();
                unset($item['id']);
                $formulaItem->fill($item);
                $formulaItem->formula_id = $formula->id;
                $formulaItem->save();

                $formulaItemInsert[] = $formulaItem->id;
            }
        }

        if ($formulaItemInsert) {
            $formula->formula_items()->whereNotIn('id', $formulaItemInsert)->delete();
        }

        return redirect()->route('master.formula.show', $formula->id)->with('success', trans('global.success_update'));
    }

    public function destroy(Formula $formula)
    {
        \DB::beginTransaction();
        $name = $formula->name;

        $formula->formula_items()->delete();

        $status = $this->delete($formula, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("formula.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("formula.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $status["status"]] );
    }
}

<?php

namespace App\Http\Controllers\Master;

use App\Http\Requests\Master\CustomerRequest;
use App\Util\Base\CoreController;
use Illuminate\Validation\Rule;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Customer;

class CustomerController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Customer::query();

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('name', Util::get('_k'))
                    ->orWhereLike('phone', Util::get('_k'))
                    ->orWhereLike('email', Util::get('_k'));
            });
        }

        $customers = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))->appends(request()->except(["page"]));

        return Layout::render('master.customer.index', [
            'customers' => $customers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Layout::render('master.customer.create', [
            'model' => new Customer()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Master\CustomerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $input = $request->only([
            'name',
            'email',
            'phone',
            'fax',
            'address',
        ]);
        $model = new Customer();
        $model->fill($input);
        $model->save();

        return redirect()->route('master.customer.show', $model->id)->with('success', trans('global.success_save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        return Layout::render('master.customer.show', [
            'model' => $customer
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return Layout::render('master.customer.edit', [
            'model' => $customer
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Master\CustomerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, Customer $customer)
    {
        $input = $request->only([
            'name',
            'email',
            'phone',
            'fax',
            'address',
        ]);

        $customer->fill($input);
        $customer->save();

        return redirect()->route('master.customer.show', $customer->id)->with('success', trans('global.success_update'));
    }

    public function destroy(Customer $customer)
    {
        \DB::beginTransaction();
        $name = $customer->name;

        $status = $this->delete($customer, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("customer.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("customer.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxCustomer()
    {
        $params = request()->all();

        $query = Customer::select("id","name as text");

        $k = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($k) {
            $query->where(function($q) use ($k) {
                $q->whereLike('name', $k)
                    ->orWhereLike('phone', $k)
                    ->orWhereLike('email', $k);
            });
        }

        $list = $query->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}

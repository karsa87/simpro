<?php

namespace App\Http\Controllers\Master;

use App\Http\Requests\Master\MaterialRequest;
use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Material;

class MaterialController extends CoreController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Material::with('units');

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('name', Util::get('_k'));
            });
        }

        $materials = $query->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))->appends(request()->except(["page"]));

        return Layout::render('master.material.index', [
            'materials' => $materials
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Layout::render('master.material.create', [
            'model' => new Material(),
            'units' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Master\MaterialRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MaterialRequest $request)
    {
        $input = $request->only([
            'name',
            'description',
        ]);
        $model = new Material();
        $model->fill($input);
        $model->save();

        if ($request->get('units', [])) {
            $model->units()->sync($request->get('units', []));
        }

        return redirect()->route('master.material.show', $model->id)->with('success', trans('global.success_save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Material $material)
    {
        return Layout::render('master.material.show', [
            'model' => $material
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Material $material)
    {
        $material->loadMissing('units');

        $units = [];
        foreach ($material->units as $unit) {
            $units[] = [
                'unit_id' => $unit->id,
                'price' => $unit->pivot->price,
                'qty' => $unit->pivot->qty,
            ];
        }

        return Layout::render('master.material.edit', [
            'model' => $material,
            'units' => $units,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Master\MaterialRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MaterialRequest $request, Material $material)
    {
        $input = $request->only([
            'name',
            'description',
        ]);

        $material->fill($input);
        $material->save();

        if ($request->get('units', [])) {
            $material->units()->detach();
            $material->units()->sync($request->get('units', []));
        }

        return redirect()->route('master.material.show', $material->id)->with('success', trans('global.success_update'));
    }

    public function destroy(Material $material)
    {
        \DB::beginTransaction();
        $name = $material->name;

        $material->units()->detach();

        $status = $this->delete($material, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("material.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("material.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $status["status"]] );
    }
}

<?php

namespace App\Http\Controllers\Master;

use App\Util\Base\CoreController;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Position;

class PositionController extends CoreController
{
    public function index()
    {
        $query = Position::query();

        $orderBy = Util::get('_s_ob');
        $sorting = Util::get('_s_s');
        $sorting = empty($sorting) ? 0 : $sorting;
        if (!empty($orderBy) && $orderBy != 'stock') {
            $query->orderBy($orderBy, trans("global.array.sorting.$sorting"));
        } else {
            $query->orderBy('updated_at','DESC');
        }

        if (request('_nm')) {
            $query->whereLike('name', Util::get('_nm'));
        }

        if (request('_k')) {
            $query->whereLike('name', Util::get('_k'));
        }

        $positions = $query->paginate(Layout::ROW_PER_PAGE)->appends(request()->except(["page"]));

        return Layout::render('master.position.index', [
            "positions" => $positions,
        ]);
    }

    public function edit($id)
    {
        $position = Position::find($id);
        $position = $position ? $position->toArray() : [];

        return response()->json([
            'url_edit' => route('master.position.update', $id),
            'data' => $position
        ]);
    }

    public function store()
    {
        return $this->store_update();
    }

    public function update($id)
    {
        return $this->store_update($id);
    }

    private function store_update($id = null)
    {
        $input = request()->except(["_token"]);

        $redirect = redirect()->back();
        $query = Position::query();
        $result = $this->save($query, $input,[
            "rules" => [
                "name" => [
                    'required',
                    'max:255',
                    'not_regex:/["]/',
                ],
            ],
        ]);

        if ($result['status']) {
            $redirect->with('success', sprintf('%s', trans("global.success_save")) );
        } else {
            $redirect->withInput( request()->except(['_token']) )
                        ->withErrors($result['error'])
                        ->with('error', sprintf('%s', trans("global.failed_save")) );
        }

        return $redirect;
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $row = Position::find($id);
        $name = $row->name;

        $status = $this->delete($row, 'soft');
        if($status["status"]){
            session()->flash('success', sprintf("%s %s : %s", trans("global.success_delete"), trans("position.title"), $name) );
            \DB::commit();
        }else{
            session()->flash('error', sprintf("%s %s : %s", trans("global.failed_delete"), trans("position.title"), $name) );
            \DB::rollback();
        }

        return response()->json( ["status" => $status["status"]] );
    }

    public function ajaxPosition()
    {
        $params = request()->all();

        $list = Position::select("id","name as text");

        $q = array_key_exists('query', $params) ? $params['query'] : (array_key_exists('q', $params) ? $params['q'] : '');
        if ($q) {
            $list->whereLike("name", $q);
        }

        $list = $list->get()->toArray();

        return response()->json([
            "items" => $list,
            "count" => count($list)
        ]);
    }
}

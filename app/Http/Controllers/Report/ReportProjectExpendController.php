<?php

namespace App\Http\Controllers\Report;

use App\Exports\Report\ProjectExpendExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProjectShop;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Project;
use App\Models\Expend;
use Excel;

class ReportProjectExpendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this_month = date('n');
        $last_month = date("n", strtotime("first day of previous month"));

        $projectShops = ProjectShop::selectRaw('EXTRACT(MONTH from date) as month, SUM(total_amount) as amount')
                            ->whereBetween('date', [
                                date('Y-01-01 00:00:00'),
                                date('Y-12-31 23:59:59'),
                            ])
                            ->groupBy(\DB::raw('EXTRACT(MONTH from date)'))
                            ->get()->groupBy('month');
        $expends = Expend::selectRaw('EXTRACT(MONTH from date) as month, SUM(amount) as amount')
                            ->whereNotNull('project_id')
                            ->whereBetween('date', [
                                date('Y-01-01 00:00:00'),
                                date('Y-12-31 23:59:59'),
                            ])
                            ->groupBy(\DB::raw('EXTRACT(MONTH from date)'))
                            ->get()->groupBy('month');

        $projectShops_chart = [];
        $expends_chart = [];
        for ($i=1; $i <= 12; $i++) {
            if ($projectShops->has($i)) {
                $projectShops_chart[$i] = $projectShops[$i]->sum('amount');
            } else {
                $projectShops_chart[$i] = 0;
            }

            if ($expends->has($i)) {
                $expends_chart[$i] = $expends[$i]->sum('amount');
            } else {
                $expends_chart[$i] = 0;
            }
        }

        $projects = $this->get_query()->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                        ->appends(request()->except(["page"]));

        $customers = Project::select('customer_name')->groupBy('customer_name')->get()->pluck('customer_name')->toArray();
        return view('report.project_expend.index', [
            'projects' => $projects,
            'customers' => $customers,
            'expends' => [
                'this_month' => $expends_chart[$this_month],
                'last_month' => $expends_chart[$last_month],
                'chart' => $expends_chart,
            ],
            'projectShops' => [
                'this_month' => $projectShops_chart[$this_month],
                'last_month' => $projectShops_chart[$last_month],
                'chart' => $projectShops_chart,
            ],
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        $query = $this->get_query();
        $projects = $query->get();
        $filename = sprintf('%s %s & %s - %s.xlsx', trans('project.title'), trans('project.label.expend'), trans('project_shop.title'), date('Y-m-d H:i:s'));
        return Excel::download(new ProjectExpendExport($projects), $filename);
    }

    private function get_query()
    {
        $date = [
            date('Y-m-01'),
            date('Y-m-t')
        ];

        $query = Project::query();

        if (empty($status)) {
            $query->whereIn('status', [
                Project::STATUS_WAITING,
                Project::STATUS_ON_PROGRESS,
                Project::STATUS_DONE,
            ]);
        } else {
            $query->where('status', $status);
        }

        if (request('_c')) {
            $query->where('customer_name', Util::get('_c'));
        }

        if (is_numeric(request('_sts'))) {
            $query->where('status', Util::get('_sts'));
        }

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('no_project', Util::get('_k'))
                    ->orWhereLike('projectname', Util::get('_k'))
                    ->orWhereLike('customer_name', Util::get('_k'));
            });
        }

        return $query;
    }
}

<?php

namespace App\Http\Controllers\Report;

use App\Exports\Report\ProjectExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Project;
use Excel;

class ReportProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects_run = Project::whereIn('status', [
            Project::STATUS_WAITING,
            Project::STATUS_ON_PROGRESS,
        ])->count();

        $projects_done = Project::whereIn('status', [
            Project::STATUS_DONE,
            Project::STATUS_CLOSE,
        ])->count();

        $projects = $this->get_query()->sortable()->paginate(request('showing', Layout::ROW_PER_PAGE))
                                ->appends(request()->except(["page"]));

        $customers = Project::select('customer_name')->groupBy('customer_name')->get()->pluck('customer_name')->toArray();
        return view('report.project.index',[
            'project' => [
                'run' => $projects_run,
                'done' => $projects_done,
            ],
            'projects' => $projects,
            'customers' => $customers
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        $query = $this->get_query();
        $projects = $query->get();
        $filename = sprintf('%s - %s.xlsx', trans('project.title'), date('Y-m-d H:i:s'));
        return Excel::download(new ProjectExport($projects), $filename);
    }

    private function get_query()
    {
        $date = [
            date('Y-m-01'),
            date('Y-m-t')
        ];

        $query = Project::query();

        if (empty($status)) {
            $query->whereIn('status', [
                Project::STATUS_WAITING,
                Project::STATUS_ON_PROGRESS,
                Project::STATUS_DONE,
            ]);
        } else {
            $query->where('status', $status);
        }

        if (request('_c')) {
            $query->where('customer_name', Util::get('_c'));
        }

        if (is_numeric(request('_sts'))) {
            $query->where('status', Util::get('_sts'));
        }

        if (request('_k')) {
            $query->where(function($q) {
                $q->whereLike('no_project', Util::get('_k'))
                    ->orWhereLike('projectname', Util::get('_k'))
                    ->orWhereLike('customer_name', Util::get('_k'));
            });
        }

        return $query;
    }
}

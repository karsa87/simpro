<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\ProjectDebtPayment;
use App\Models\ProjectPayment;
use Illuminate\Http\Request;
use App\Models\ProjectShop;
use App\Util\Helpers\Util;
use App\Util\Base\Layout;
use App\Models\Project;
use App\Models\Expend;
use App\Models\User;

class ReportLossProfitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $month = sprintf("%02d", request('_m', date('n')));
        $year = request('_y', date('Y'));
        $date = [
            sprintf('%s-%s-01 00:00', $year, $month),
            sprintf('%s-%s-%s 23:59', $year, $month, date('t', strtotime(sprintf('%s-%s-01 00:00', $year, $month)))),
        ];

        //income
        $income_all = ProjectPayment::selectRaw('EXTRACT(DAY from date) as day, SUM(amount) as amount')
                        ->whereBetween('date', $date)
                        ->groupBy(\DB::raw('EXTRACT(DAY from date)'))
                        ->get()->keyBy('day');

        $incomes = [];
        for ($i=1; $i <= date('t', strtotime($date[1])); $i++) {
            if ($income_all->has($i)) {
                $incomes[$i] = $income_all[$i]->amount;
            } else {
                $incomes[$i] = 0;
            }
        }

        //expends

        $projectShops = ProjectShop::selectRaw("EXTRACT(DAY from date) as day, SUM(total_amount) as amount")
                            ->whereBetween('date', $date)
                            ->groupBy(\DB::raw('EXTRACT(DAY from date)'));
        $expends = Expend::selectRaw("EXTRACT(DAY from date) as day, SUM(amount) as amount")
                            ->whereBetween('date', $date)
                            ->groupBy(\DB::raw('EXTRACT(DAY from date)'));

        $clone_projectShops = clone $projectShops;
        $clone_expends = clone $expends;
        $expend_all = $projectShops->unionAll($expends)->get()->groupBy('day');
        $project_shops = $clone_projectShops->sum('total_amount');
        $project_expends = $clone_expends->sum('amount');

        $expends = [];
        for ($i=1; $i <= date('t', strtotime($date[1])); $i++) {
            if ($expend_all->has($i)) {
                $expends[$i] = $expend_all[$i]->sum('amount');
            } else {
                $expends[$i] = 0;
            }
        }

        $omzet = [];
        for ($i=1; $i <= date('t', strtotime($date[1])); $i++) {
            $omzet[$i] = $incomes[$i] - $expends[$i];
        }

        return view('report.loss_profit.index', [
            'expends' => $expends,
            'incomes' => $incomes,
            'omzet' => $omzet,
            'project_shops' => $project_shops,
            'project_expends' => $project_expends,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        return redirect()->route('report.loss_profit.index')->with('success', 'sukses export laporan buku besar');
    }
}

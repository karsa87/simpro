<?php

namespace App\Util\Listeners;

use App\Util\Events\UserLogin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;

class UpdateLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLogin  $event
     * @return void
     */
    public function handle(UserLogin $event)
    {
        // $manager = app('impersonate');
        // if (!$manager->isImpersonating()) {
            $user = $event->user;
            $user->fill([
                'nowlogin'     => date('Y-m-d H:i:s'),
                'loginip'      => request()->ip(),
                'lastlogin'    => date('Y-m-d H:i:s'),
                'lastloginip'  => request()->ip(),
                // 'photo'        => $event->userGoogle->picture,
            ]);
    
            $user->save();
        // }
    }
}

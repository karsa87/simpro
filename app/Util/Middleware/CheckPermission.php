<?php

namespace App\Util\Middleware;

use Closure;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = \Request::route()->getName();
        $user = \Auth::user();

        $arr_route = explode('.',$route);
        $index = count($arr_route) - 1;
        $last_route = $arr_route[$index];

        if ($last_route == 'update') {
            $arr_route[$index] = 'edit';
            $route = implode('.', $arr_route);
        } elseif ($last_route == 'store') {
            $arr_route[$index] = 'create';
            $route = implode('.', $arr_route);
        }

        if( !$user->hasRole(['developer', 'administrator']) )
        {
            $this->has_access = $user->has($route);
            if(!$this->has_access)
            {
                abort(404);
            }
        }
        
        $parameters_1 = request()->route()->parameters;
        $parameters_2 = request()->all();

        return $next($request);
    }
}

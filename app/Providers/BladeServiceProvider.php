<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \Blade::directive('classInpError', function ($column) {
            $command = '$errors->first(' . $column . ') ? "is-invalid" : ""';
            return "<?= {$command} ?>";
        });

        \Blade::if('hasPermission', function ($permission) {
            $user = auth()->user();
            $has_access = false;
            $permission = str_replace("'",'',$permission);
            if ($user) {
                $has_access = $user->has($permission);
            }
            
            return $has_access;
        });

        Blade::include('components.error', 'inpSpanError');
        Blade::include('components.alert', 'alert');
    }
}

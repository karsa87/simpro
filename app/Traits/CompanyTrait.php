<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use App\Observers\CompanyObserver;
use App\Models\Company;

trait CompanyTrait
{
    public static function bootCompanyTrait()
    {
        static::observe(new CompanyObserver);

        static::addGlobalScope('company', function(Builder $builder) {
            $table = with(new static)->getTable();
            $company = \Session::get(config('system.session.active_company'));
            if ($company) {
                $builder->where("$table.company_id", $company->id);
            }
        });
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
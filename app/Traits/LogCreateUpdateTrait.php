<?php

namespace App\Traits;

use App\Observers\LogCreateUpdateObserver;
use App\Models\User;

trait LogCreateUpdateTrait
{
    public static function bootLogCreateUpdateTrait()
    {
        static::observe(new LogCreateUpdateObserver);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by')->withTrashed();
    }
}
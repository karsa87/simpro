<?php

namespace App\Traits;

use App\Models\LogActivity;
use App\Observers\LogActivityObserver;

trait LogActivityTrait
{
    private $skip_log = false;

    public static function bootLogActivityTrait()
    {
        static::observe(new LogActivityObserver);
    }

    public function defaultName()
    {
        $name = $this->name;
        $name = $name ?: $this->no_transaction;
        $name = $name ?: sprintf('%s with id: %s', $this->getTable(), $this->id);

        return $name;
    }

    public function skipLog()
    {
        $this->skip_log = true;

        return $this;
    }

    public function getSkipLog()
    {
        return $this->skip_log;
    }

    /**
     * Get all of the owning commentable models.
     */
    public function logActivity()
    {
        return $this->morphMany(LogActivity::class, 'record');
    }
}
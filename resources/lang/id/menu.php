<?php

$lang = [
    'title' => 'Menu',
    
    'label' => [
        'name' => 'Nama',
        'url' => 'URL',
        'icon' => 'Ikon',
        'route' => 'Route',
        'order' => 'Urutan',
        'is_parent' => 'Is Parent',
        'parent_id' => 'Dibawah Menu',
    ],
    
    'placeholder' => [
        'name' => 'ex : Menu',
        'url' => 'ex : menu',
        'icon' => 'fas fa-gear',
        'route' => 'ex : setting.menu',
        'order' => 'ex : 1',
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>menu</code>',
        'description_list' => 'Daftar <code>menu</code>',
    ],

];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'url.required' => sprintf('%s harap diisi', $lang["label"]["url"]),
    'route.required' => sprintf('%s harap diisi', $lang["label"]["route"]),
];

return $lang;
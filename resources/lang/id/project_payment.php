<?php

return [
    'title' => 'Pembayaran Proyek',

    'label' => [
        'title' => 'Pembayaran',
        'project_id' => 'Proyek',
        'employee_id' => 'Petugas',
        'date' => 'Tanggal',
        'information' => 'Informasi',
        'amount' => 'Biaya',
        'file' => 'File',
        'company_id' => 'Perusahaan',
        'pay' => 'Bayar',
    ],

    'placeholder' => [
        'title' => 'ex: Pembayaran Termin 1',
        'information' => 'ex: Pembayaran Proyek',
        'date' => 'ex: YYYY-mm-dd HH:ii',
        'amount' => 'ex: 1,000,000.00',
    ],

    'message' => [
        'sure' => 'Apakah anda yakin ?',
    ],

    'list' => [],
];

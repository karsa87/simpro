<?php

return [
    'title' => 'Pelunasan Proyek',

    'label' => [
        'no_transaction' => 'No Transaksi',
        'project_id' => 'Proyek',
        'project_shop_id' => 'Belanja Proyek',
        'store' => 'Toko',
        'date' => 'Tanggal',
        'information' => 'Informasi',
        'amount' => 'Bayar',
        'company_id' => 'Perusahaan',
        'debt' => 'Total Hutang',
    ],

    'placeholder' => [
        'no_transaction' => 'ex: SIMPRO-DSO/2021/VI/0001',
        'date' => 'ex: YYYY-mm-dd H:i',
        'information' => 'ex: pembayaran belanja bulanan di Toko A',
        'amount' => 'ex: 1.000.000',
    ],

    'message' => [
        'sure' => 'Apakah anda yakin ?',
        'change_project' => 'Mengganti no proyek',
    ],

    'list' => [],
];

<?php

return [
    'title' => 'Arsip',

    'label' => [
        'name' => 'Nama',
        'description' => 'Deskripsi',
        'company_id' => 'Perusahaan',
        'total_file' =>  'Total File Arsip',
    ],

    'placeholder' => [
        'name' => 'ex: BERKAS PROYEK A',
        'description' => 'ex: Kumpulan berkas dari proyek A',
    ],

    'message' => [],

    'list' => [],
];

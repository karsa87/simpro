<?php

return [
    'title' => 'Kategory Pengeluaran',

    'label' => [
        'name' => 'Nama',
        'status' => 'Status',
        'company_id' => 'Perusahaan'
    ],

    'placeholder' => [
        'name' => 'ex: Operasional',
    ],

    'message' => [
        'please_name' => 'Tolong isi nama kategori',
    ],

    'list' => [
        'status' => [
            1 => 'Aktif',
            0 => 'Non Aktif',
        ]
    ],
];

<?php

return [
    'title' => 'Pelaksanaan Proyek',

    'label' => [
        'project_id' => 'Proyek',
        'project_job_id' => 'Pekerjaan',
        'date' => 'Tanggal',
        'information' => 'Informasi',
        'progress' => 'Persentase',
        'company_id' => 'Perusahaan',
    ],

    'placeholder' => [
        'date' => 'ex: YYYY-mm-dd',
        'porgress' => 'ex: 60',
    ],

    'message' => [
        'sute' => 'Are you sure ?',
        'confirm_change_project' => 'Change project',
        'desc_progress' => 'Percentage of project work (%)'
    ],

    'list' => [],
];

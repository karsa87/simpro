<?php

$lang = [
    'title' => 'Dashboard',
    
    'label' => [
        'welcome' => 'Selamat Datang',
        'create' => 'Buat',
        'more' => 'Detail',
        'sales_order' => 'Penjualan',
        'purchase_order' => 'Pembelian',
        'stock_product' => 'Stok <= 1 Produk',
        'expend' => 'Uang Keluar',
        'income' => 'Uang Masuk',
        'chart' => 'Grafik',
        'date' => 'Tanggal',
        'member' => 'Member Aktif',
        'sales_order_paid' => 'Penjualan Terbayar',
        'sales_order_not_paid' => 'Penjualan Belum Terbayar',
    ],
    
    'placeholder' => [
    ],

    'message' => [
        'welcome' => 'Selamat datang di sistem POS (Point of Sale)',
        'success_change_language' => 'Sukses ganti bahasa'
    ],

    'list' => [
    ]
];

return $lang;
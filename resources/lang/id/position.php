<?php

return [
    'title' => 'Jabatan',

    'label' => [
        'name' => 'Nama', 
        'description' => 'Deskripsi', 
        'company_id' => 'Perusahaan'
    ],

    'placeholder' => [
        'name' => 'ex: Direktur', 
        'description' => 'ex: Direktur Pusat', 
    ],

    'message' => [],
    
    'list' => [],
];

<?php

return [
    'title' => 'Tipe Proyek',

    'label' => [
        'name' => 'Nama',
        'description' => 'Deskripsi',
        'price' => 'Kisaran Harga',
        'company_id' => 'Company',
    ],

    'placeholder' => [
        'name' => 'ex: Proyek Outdoor',
        'description' => 'ex: Proyek outdoor pembangunan jalan raya, gorong-gorong dll.',
        'price' => 'ex: 1,000,000',
    ],

    'message' => [
    ],

    'list' => [],
];

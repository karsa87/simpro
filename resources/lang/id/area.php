<?php

return [
    'title' => 'Area',

    'label' => [
        'code' => 'Kode', 
        'name' => 'Nama', 
        'description' => 'Deskripsi', 
        'company_id' => 'Perusahaan'
    ],

    'placeholder' => [
        'code' => 'ex: MLG', 
        'name' => 'ex: Malang', 
        'description' => 'ex: Area Malang', 
    ],

    'message' => [],
    
    'list' => [],
];

<?php

return [
    'title' => 'File',

    'label' => [
        'name' => 'Nama',
        'information' => 'Informasi',
        'path' => 'Path',
        'url' => 'URL',
        'company_id' => 'Perusahaan',
    ],

    'placeholder' => [
        'name' => 'ex: Berkas SPK',
        'information' => 'Description of file',
    ],

    'message' => [
        'sure' => 'Apakah anda yakin ?',
        'deleted_file' => 'Mengahpus berkas',
        'not_found' => 'Berkas tidak ditemukan!!!',
        'close_form' => 'Menutup form',
    ],

    'list' => [],
];

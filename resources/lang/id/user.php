<?php

$lang = [
    'title' => "User",

    'placeholder' => [
        'name' => 'ex: Agus',
        'nik' => 'ex : 333314779124979xxxx',
        'email' => 'agus@gmail.com',
        'lastlogin' => 'Last Login',
        'lastloginip' => 'Last Login IP',
        'address' => 'ex: Malang',
        'phone' => 'ex: 08982423987xx',
        'code' => 'ex: AG',
    ],
    'list' => [
        'status' => ['Tidak Aktif', 'Aktif']
    ],
    'message' => [
        'description_find' => 'Form <code>search</code> for get <code>user</code>',
        'description_list' => 'Daftar <code>user</code>',
        'failed_status' => 'Gagal <code>%s</code>',
        'success_status' => 'Berhasil <code>%s</code>',
        'not_found' => 'User <code>%s</code> tidak ditemukan',
    ],
    'label' => [
        'id' => 'ID',
        'name' => 'Nama',
        'nik' => 'NIK',
        'email' => 'Email',
        'photo_profile' => 'Foto',
        'lastlogin' => 'Login Terakhir',
        'lastloginip' => 'IP Login Terakhir',
        'role' => 'Role',
        'permission' => 'Permission',
        'password' => 'Kata Kunci',
        'profile' => 'Profil',
        'code' => 'Kode',
        'address' => 'Alamat',
        'phone' => 'Telepon',
        'npwp' => 'NPWP',
        'area_id' => 'Area',
        'position_id' => 'Jabatan',
        'company_id' => 'Perusahaan',
        'status' => 'Status',
    ]
];

return $lang;
<?php

return [
    'title' => 'Format Number',

    'label' => [
        'menu_id' => 'Menu', 
        'prefix' => 'Awalan', 
        'middle' => 'Jenis', 
        'year_type' => 'Tahun', 
        'month_type' => 'Bulan', 
        'increment_type' => 'Urutan', 
        'delimiter' => 'Pemisah', 
        'company_id' => 'Perusahaan',
        'format' => 'Format',
    ],

    'placeholder' => [
        'prefix' => 'ex: RSP', 
        'middle' => 'ex: PO', 
        'year_type' => 'ex: 21', 
        'month_type' => 'ex: II', 
        'increment_type' => 'ex: 0001', 
        'delimiter' => 'ex: /', 
    ],

    'message' => [],
    
    'list' => [
        'year_type' => [
            '21',
            '2021',
        ],
        'month_type' => [
            'IV',
            'April',
        ],
        'increment_type' => [
            '1',
            '0001',
        ],
        'increment_sprintf' => [
            '%d',
            '%04d',
        ],
    ],
];

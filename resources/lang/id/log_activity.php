<?php

$lang = [
    'title' => 'Riwayat',

    'label' => [
        'log_datetime' => 'Tanggal Riwayat',
        'user_id' => 'User',
        'transaction_type' => 'Tipe Transaksi',
        'information' => 'Keterangan',
        'record_id' => 'ID',
        'data_before' => 'Data Sebelumnya',
        'data_after' => 'Data Sesudahnya',
        'data_change' => 'Perubahan Data',
        'record_type' => 'Kelas',
        'date' => 'Tanggal',
        'table' => 'Tabel'
    ],

    'message' => [
        'description_find' => 'Form <code>Cari</code> untuk <code>riwayat</code>',
        'description_list' => 'Daftar <code>riwayat</code>',
    ],

    'list' => [
        'transaction_type' => [
            1 => 'Buat',
            2 => 'Edit',
            3 => 'Hapus',
        ]
    ]
];

return $lang;

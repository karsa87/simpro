<?php

return [
    'title' => 'Insentif',

    'label' => [
        'no_transaction' => 'No Transaksi',
        'investor' => 'Investor',
        'information' => 'Informasi',
        'date' => 'Tanggal',
        'amount' => 'Jumlah',
        'company_id' => 'Perusahaan'
    ],

    'placeholder' => [
        'no_transaction' => 'ex: INC/2021/II/0005',
        'investor' => 'ex: Ayu',
        'information' => 'ex: Penambahan Modal',
        'date' => 'ex: YYYY-mm-dd HH:ii',
        'amount' => 'ex: 1,000,000.00',
    ],

    'message' => [],

    'list' => [],
];

<?php

return [
    'title' => 'Lock Data',

    'label' => [
        'menu_id' => 'Menu', 
        'delete_day' => 'Delete Day', 
        'edit_day' => 'Edit Day'
    ],

    'placeholder' => [
        'day' => 'ex: 10', 
    ],

    'message' => [],
    
    'list' => [],
];

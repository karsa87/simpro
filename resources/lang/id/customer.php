<?php

return [
    'title' => 'Pelanggan',

    'label' => [
        'name' => 'Nama',
        'email' => 'Email',
        'phone' => 'Telepon',
        'fax' => 'Fax',
        'address' => 'Alamat',
        'company_id' => 'Perusahaan'
    ],

    'placeholder' => [
        'name' => 'ex: Linda',
        'email' => 'ex: linda@gmail.com',
        'phone' => 'ex: 087821932xxxxx',
        'fax' => 'ex: (0341) 7777xxxx',
        'address' => 'ex: Jl. Margonda',
    ],

    'message' => [],

    'list' => [],
];

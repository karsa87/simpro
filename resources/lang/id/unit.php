<?php

return [
    'title' => 'Unit',

    'label' => [
        'name' => 'Nama',
        'description' => 'Deskripsi',
        'company_id' => 'Perusahaan'
    ],

    'placeholder' => [
        'name' => 'ex: Kg',
        'description' => 'ex: Satuan Kilogram',
    ],

    'message' => [],

    'list' => [],
];

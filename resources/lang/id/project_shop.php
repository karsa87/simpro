<?php

return [
    'title' => 'Belanja Proyek',

    'label' => [
        'store_name' => 'Nama Toko',
        'project_id' => 'Proyek',
        'employee_id' => 'Petugas',
        'information' => 'Informasi',
        'date' => 'Tanggal',
        'total_amount' => 'Jumlah Total',
        'total' => 'Total',
        'file' => 'File',
        'status' => 'Status',
        'company_id' => 'Perusahaan',

        'project_shop_id' => 'Belanja Proyek',
        'product_name' => 'Produk',
        'qty' => 'Jumlah',
        'price' => 'Harga',
        'total_price' => 'Total Harga',
        'product_materials' => 'Barang / Bahan Baku',
        'paid' => 'Bayar',
        'unpaid' => 'Belum Dibayar',

        'this_month' => 'Bulan Sekarang',
        'last_month' => 'Bulan Kemarin',
    ],

    'placeholder' => [
        'store_name' => 'ex: TOKO JAYA ABADI',
        'information' => 'ex: Pembayaran Proyek',
        'date' => 'ex: YYYY-mm-dd HH:ii',
        'total_amount' => 'ex: 1,000,000.00',
    ],

    'message' => [
        'sure' => 'Apakah anda yakin ?',
        'change_project' => 'Mengganti no proyek ?',
        'delete_product_materials' => 'Menghapus barang / bahan baku ?',
    ],

    'list' => [
        'status' => [
            0 => 'Belum Lunas',
            1 => 'Lunas',
        ]
    ],
];

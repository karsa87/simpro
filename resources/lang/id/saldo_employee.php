<?php

return [
    'title' => 'Saldo Karyawan',

    'label' => [
        'employee_id' => 'Karyawan',
        'date' => 'Tanggal',
        'saldo' => 'Deposit',
        'company_id' => 'Perusahaan'
    ],

    'placeholder' => [
        'date' => 'ex: YYYY-mm-dd HH:ii',
        'saldo' => 'ex: 10.000',
    ],

    'message' => [
        'please_employe_id' => 'Tolong pilih karyawan'
    ],

    'list' => [],
];

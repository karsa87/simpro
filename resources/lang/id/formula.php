<?php

return [
    'title' => 'Formula',

    'label' => [
        'code' => 'Kode',
        'name' => 'Nama',
        'description' => 'Deskripsi',
        'coefficient' => 'Koefisien',
        'subtotal' => 'Subtotal',
        'tax' => 'Pajak',
        'tax_amount' => 'Jumlah Pajak',
        'total_amount' => 'Total',

        'material_id' => 'Uraian',
        'unit_id' => 'Unit',
        'qty' => 'Qty',
        'amount' => 'Harga',
        'unit_price' => 'Harga Satuan',
        'total_price' => 'Total Harga',
        'item' => 'Item',
        'subtotal' => 'Jumlah',
        'subtotal_tax' => 'Jumlah x 15%',
        'total' => 'Total',
        'rounding' => 'Pembulatan',

        'company_id' => 'Company'
    ],

    'placeholder' => [
        'code' => 'ex: SNI-2835-2008-6.1',
        'name' => 'ex: 1 M3 Galian tanah biasa sedalam 1 m',
        'desciption' => 'ex: 1 M3 Galian tanah biasa sedalam 1 m',
        'amount' => 'ex: 100.000',
        'qty' => 'ex: 100',
    ],

    'message' => [
        'duplicate_material_unit' => 'Terdeteksi duplikat item material :material unit :unit'
    ],

    'list' => [],
];

<?php

return [
    'title' => 'Bahan',

    'label' => [
        'name' => 'Nama',
        'description' => 'Deskripsi',
        'unit' => 'Unit',
        'price' => 'Harga',
        'qty' => 'Quantity',
        'company_id' => 'Perusahaan'
    ],

    'placeholder' => [
        'name' => 'ex: Pasir Merah',
        'desciption' => 'ex: Bahan ini berupa pasir merah tanah',
        'price' => 'ex: 100.000',
        'qty' => 'ex: 100',
    ],

    'message' => [
        'duplicate_unit' => 'Unit terdeteksi lebih dari 1 :unit'
    ],

    'list' => [],
];

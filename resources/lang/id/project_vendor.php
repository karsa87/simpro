<?php

return [
    'title' => 'Vendor Proyek',

    'label' => [
        'project_id' => 'Proyek',
        'vendor_id' => 'Vendor',
        'company_id' => 'Perusahaan',
    ],

    'placeholder' => [
    ],

    'message' => [
        'list_data' => 'List data vendor',
        'sute' => 'Apakah anda yakin ?',
        'confirm_change_vendor' => 'Merubah vendor',
    ],

    'list' => [],
];

<?php

return [
    'title' => 'Pengeluaran',

    'label' => [
        'no_transaction' => 'No Transaksi',
        'project_id' => 'Proyek',
        'employee_id' => 'Petugas',
        'category_expend_id' => 'Kategori Pengeluaran',
        'information' => 'Informasi',
        'date' => 'Tanggal',
        'amount' => 'Biaya',
        'file' => 'File',
        'company_id' => 'Perusahaan'
    ],

    'placeholder' => [
        'no_transaction' => 'ex: INC/2021/II/0005',
        'information' => 'ex: Pembayaran Proyek',
        'date' => 'ex: YYYY-mm-dd HH:ii',
        'amount' => 'ex: 1,000,000.00',
    ],

    'message' => [],

    'list' => [],
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Masuk untuk memulai sesi Anda',
    'failed' => 'Kredensial ini tidak cocok dengan catatan kami.',
    'throttle' => 'Terlalu banyak upaya masuk. Silakan coba lagi dalam :seconds detik.',

    'message' => [
        'password_not_correct' => 'Kata sandi salah',
        'email_not_found' => 'Email :value tidak terdaftar',
        'email_deactived' => 'User dengan email :value non aktif',
        'success_impersonate' => 'Berhasil impersonate user &nbsp;<span class="text-primary">:user</span>',
        'failed_impersonate' => 'Gagal impersonate user',
        'success_leave_impersonate' => 'Berhasil meninggalkan impersonate user &nbsp;<span class="text-primary">:user</span>',
        'failed_leave_impersonate' => 'Gagal meninggalkan impersonate user',
        'not_impersonate' => 'Saat ini tidak impersonate user',
    ],
    
    'validation' => [
        'email' => [
            'required' => 'Silahkan masukkan kata sandi'
        ],
        'password' => [
            'required' => 'Harap berikan kata sandi'
        ],
    ],

    'meta' => [
        'title' => 'Login',
        'description' => 'Global POS is system retail management',
        'keyword' => 'pos, point of sales, point, of, sales, mochamad karsa syaifudin jupri, mochamad, karsa, syaifudin, jupri, wikacell, system, retail, management, system retail management'
    ]
];

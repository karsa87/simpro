<?php

return [
    'title' => 'Notifikasi',

    'label' => [
        'message' => 'Pesan', 
        'link' => 'Link', 
        'route' => 'Route', 
        'parameter' => 'Parameter', 
        'is_read' => 'Dibaca', 
        'created_by' => 'Dibuat Oleh', 
        'updated_by' => 'Diedit Oleh', 
        'by' => 'Oleh', 
        'time' => 'Waktu', 
        'company_id' => 'Perusahan',
        'year' => 'tahun',
        'month' => 'bulan',
        'week' => 'minggu',
        'day' => 'hari',
        'hour' => 'jam',
        'minute' => 'menit',
        'second' => 'detik',
        'ago' => 'lalu',
        'now' => 'baru saja',
        'see_all' => 'Lihat Semua Pemberitahuan',
    ],

    'placeholder' => [
    ],

    'message' => [],
    
    'list' => [
        'is_read' => ['Belum Dibaca', 'Sudah Dibaca'],
        'type' => ['Buat', 'Edit', 'Hapus'],
    ],
];

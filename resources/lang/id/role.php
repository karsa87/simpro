<?php

$lang = [
    'title' => "Role",

    'placeholder' => [
        'id' => 'ID',
        'name' => 'ex: Administrator',
        'slug' => 'ex: administrator',
        'description' => 'ex: Role for user administrator',
        'level' => 'ex: administrator',
    ],
    'list' => [
        'level' => [
            100 => 'Developer',
            95 => 'Administrator WIKA',
            90 => 'Administrator',
            80 => 'Employee',
        ]
    ],
    'message' => [
        'description_find' => 'Form <code>Cari</code> for get <code>role</code>',
        'description_list' => 'Daftar <code>role</code>',
    ],
    'label' => [
        'menu_access' => "Menu Access",
        'id' => 'ID',
        'name' => 'Name',
        'slug' => 'Slug',
        'description' => 'Description',
        'level' => 'Level',
        'permission' => 'Permission',
    ]
];

$lang['errors'] = [
    'name.required' => sprintf('%s harap diisi', $lang["label"]["name"]),
    'slug.required' => sprintf('%s harap diisi', $lang["label"]["slug"]),
    'level.required' => sprintf('%s harap diisi', $lang["label"]["level"]),
    'level.numeric' => sprintf('%s format nomor salah', $lang["label"]["level"]),
];

return $lang;
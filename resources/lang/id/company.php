<?php

return [
    'title' => 'Perusahaan',

    'label' => [
        'logo' => 'Logo', 
        'name' => 'Nama', 
        'address' => 'Alamat', 
        'city' => 'Kota', 
        'postal_code' => 'Kode POS', 
        'npwp' => 'NPWP', 
        'license_number' => 'Nomor Lisensi', 
        'phone' => 'Telepon', 
        'fax' => 'Fax', 
        'email' => 'Email', 
        'director_id' => 'Nama Direktur', 
        'pic_payment' => 'PIC Pembayaran', 
        'pic_purchase' => 'PIC Pembelian', 
        'pic_warehouse' => 'PIC Gudang',
        'format_date' => 'Format Tanggal',
        'pdf' => [
            'name' => 'Nama', 
            'address' => 'Alamat', 
            'npwp' => 'NPWP', 
            'phone' => 'Telepon', 
            'fax' => 'Fax', 
            'email' => 'Email', 
        ]
    ],

    'placeholder' => [
        'name' => 'ex: Perusahaan Malang', 
        'address' => 'ex: LA. Sucipto', 
        'city' => 'ex: Malang', 
        'postal_code' => 'ex: 6512xx', 
        'npwp' => 'ex: 09.254.294.3-407.xxx', 
        'license_number' => 'ex: 097204390xxxx', 
        'phone' => 'ex: 0898127xxxxx', 
        'fax' => 'ex: (0341) 78778xxxx', 
        'email' => 'ex: company@gmail.com', 
        'director_name' => 'ex: Agus', 
        'pic_payment' => 'ex: Agus', 
        'pic_purchase' => 'ex: Agus', 
        'pic_warehouse' => 'ex: Agus', 
    ],

    'message' => [
        'not_found' => 'Perusahaan <code>%s</code> tidak ditemukan!!!',
        'success_change_active' => 'Berhasil mengubah perusahaan yang aktif',
    ],
    
    'list' => [
        'format_date' => [
            1 => '18-02-2021',
            4 => '2021-02-18',
            8 => '18/02/2021',
            9 => '2021/02/18',
            2 => '18 Februari 2021',
            6 => '18 Februari 2021 20:25',
            3 => 'Kamis, 18 Februari 2021',
            5 => 'Kamis, 18 Februari 2021. 20:23',
        ],
    ],
];

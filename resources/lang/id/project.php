<?php

return [
    'title' => 'Proyek',

    'label' => [
        'no_project' => 'No Proyek',
        'no_spk' => 'No SPK',
        'projectname' => 'Nama Proyek',
        'customer_id' => 'Pelanggan',
        'customer_name' => 'Nama Pelanggan',
        'customer_address' => 'Alamat Pelanggan',
        'customer_phone' => 'Telepon Pelanggan',
        'customer_company' => 'Nama Perusahaan',
        'project_type_id' => 'Tipe Proyek',
        'information' => 'Keterangan Jenis Pekerjaan',
        'term_of_work' => 'Jangka Waktu Pengerjaan',
        'start_date' => 'Mulai Tanggal',
        'due_date' => 'Batas Tanggal Terakhir',
        'total_amount' => 'Total Kontrak Disepakati',
        'progress' => 'Progress',
        'status' => 'Status',
        'status_payment' => 'Status Pembayaran',
        'company_id' => 'Perusahaan',

        'files' => 'Files',
        'project_detail' => 'Proyek Detail',
        'job' => 'Pekerjaan',
        'detail_jobs' => 'Detail Pekerjaan',
        'review_submit' => 'Tinjau dan Simpan',
        'jobs' => 'Jobs',
        'job' => 'Job',
        'workers' => 'Pekerja',
        'employee_name' => 'Nama Pekerja',
        'employee_position' => 'Jabatan',

        'shop' => 'Belanja',
        'expend' => 'Pengeluaran',
        'income' => 'Pemasukkan',

        'project_run' => 'Proyek Berjalan',
        'project_done' => 'Proyek Selesai',
        'omzet' => 'Omzet',

        'loss_profit' => 'Loss Profit',
    ],

    'placeholder' => [
        'no_project' => 'ex: PO/2021/II/0005',
        'no_spk' => 'ex: 0022/SPK/VII/2021',
        'projectname' => 'ex: Proyek Hambalang',
        'customer_id' => 'ex: Andi Mallarangeng',
        'customer_name' => 'ex: Andi Mallarangeng',
        'customer_address' => 'ex: Jakarta',
        'customer_phone' => 'ex: 089124874xxx',
        'information' => 'Keterangan Jenis Pekerjaan',
        'term_of_work' => 'ex: 11',
        'start_date' => 'ex: 05-06-2021',
        'due_date' => 'ex: 05-06-2021',
        'total_amount' => 'ex: 1,000,000.00',
        'customer_company' => 'ex: PT. XXX',
    ],

    'message' => [
        'desc_customer_name' => 'Isi nama pelanggan',
        'desc_customer_phone' => 'Kami tidak akan pernah membagikan nomor telepon Anda kepada orang lain.',
        'desc_term_of_work' => 'Hari',
        'desc_due_date' => 'Perkiraan Tanggal Selesai',
        'desc_start_date' => 'Tanggal Mulai',
        'desc_total_amount' => 'Perkiraan Total Biaya proyek ini',
        'desc_review_submit' => 'Tinjau Detail Anda dan Kirim',
        'sure' => 'Apakah anda yakin ?',
        'deleted_job' => 'Mengahpus detail pekerjaan',
        'list_workers' => 'List data pekerja',
        'desc_loss_profit' => 'Pilih bulan dan tahun untuk mendapatkan laporan buku besar sesuai dengan kebutuhan anda',
    ],

    'list' => [
        'status' => [
            0 => 'Menunggu',
            1 => 'Dalam Proses',
            2 => 'Selesai',
            3 => 'Tutup',
            9 => 'Batal',
        ],
        'status_class' => [
            0 => 'warning',
            1 => 'primary',
            2 => 'success',
            3 => 'danger',
            9 => 'secondary',
        ],
        'status_payment' => [
            0 => 'Belum Lunas',
            1 => 'Lunas',
        ]
    ],
];

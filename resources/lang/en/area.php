<?php

return [
    'title' => 'Area',

    'label' => [
        'code' => 'Code', 
        'name' => 'Name', 
        'description' => 'Description', 
        'company_id' => 'Company'
    ],

    'placeholder' => [
        'code' => 'ex: MLG', 
        'name' => 'ex: Malang', 
        'description' => 'ex: Area Malang', 
    ],

    'message' => [],
    
    'list' => [],
];

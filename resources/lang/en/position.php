<?php

return [
    'title' => 'Position',

    'label' => [
        'name' => 'Name', 
        'description' => 'Description', 
        'company_id' => 'Company'
    ],

    'placeholder' => [
        'name' => 'ex: Direktur', 
        'description' => 'ex: Direktur Pusat', 
    ],

    'message' => [],
    
    'list' => [],
];

<?php

$lang = [
    'title' => 'Log Activity',

    'label' => [
        'log_datetime' => 'Log Date',
        'user_id' => 'User',
        'transaction_type' => 'Type',
        'information' => 'Information',
        'record_id' => 'Record ID',
        'data_before' => 'Data Before',
        'data_after' => 'Data After',
        'data_change' => 'Data Change',
        'record_type' => 'Class',
        'date' => 'Date',
        'table' => 'Table'
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>log activity</code>',
        'description_list' => 'List data <code>log activity</code>',
    ],

    'list' => [
        'transaction_type' => [
            1 => 'CREATE',
            2 => 'UPDATE',
            3 => 'DELETE',
        ]
    ]
];

return $lang;

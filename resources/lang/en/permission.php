<?php

$lang = [
    'title' => "Permission",
    'label' => [
        'id' => 'ID',
        'name' => 'Name',
        'slug' => 'Route Name',
        'description' => 'Description',
        'model' => 'Model',
        'level' => 'Level',
        'permission' => 'Permission',
        'menu' => "Menu",
    ],
    'placeholder' => [
        'id' => 'ID',
        'name' => 'ex: Add Permission',
        'slug' => 'setting.permission.store',
        'description' => 'Can add permission',
        'level' => 'Level',
        'model' => 'Model',
    ],
    'list' => [],
    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>permission</code>',
        'description_list' => 'List data <code>permission</code>',
        'route_not_found' => 'Route not found',
    ],
];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'slug.required' => sprintf('A %s is required', $lang["label"]["slug"]),
    // 'level.required' => sprintf('A %s is required', $lang["label"]["level"]),
    // 'level.numeric' => sprintf('A %s is wrong format number', $lang["label"]["level"]),
];

return $lang;
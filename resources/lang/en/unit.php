<?php

return [
    'title' => 'Unit',

    'label' => [
        'name' => 'Name',
        'description' => 'Description',
        'company_id' => 'Company'
    ],

    'placeholder' => [
        'name' => 'ex: Kg',
        'description' => 'ex: Satuan Kilogram',
    ],

    'message' => [],

    'list' => [],
];

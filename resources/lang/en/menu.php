<?php

$lang = [
    'title' => 'Menu',
    
    'label' => [
        'name' => 'Name',
        'url' => 'URL',
        'icon' => 'Icon',
        'route' => 'Route',
        'order' => 'Order',
        'is_parent' => 'Is Parent',
        'parent_id' => 'Under Menu',
    ],
    
    'placeholder' => [
        'name' => 'ex : Menu',
        'url' => 'ex : menu',
        'icon' => 'fas fa-gear',
        'route' => 'ex : setting.menu',
        'order' => 'ex : 1',
    ],

    'message' => [
        'description_find' => 'Form <code>Search</code> for get <code>menu</code>',
        'description_list' => 'List data <code>menu</code>',
    ],

];

$lang['errors'] = [
    'name.required' => sprintf('A %s is required', $lang["label"]["name"]),
    'url.required' => sprintf('A %s is required', $lang["label"]["url"]),
    'route.required' => sprintf('A %s is required', $lang["label"]["route"]),
];

return $lang;
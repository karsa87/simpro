<?php

return [
    'title' => 'File',

    'label' => [
        'name' => 'Name',
        'information' => 'Information',
        'path' => 'Path',
        'url' => 'URL',
        'company_id' => 'Company',
    ],

    'placeholder' => [
        'name' => 'ex: Berkas SPK',
        'url' => 'ex: https://www.simpro.com/asset/image.png',
        'information' => 'Description of file',
    ],

    'message' => [
        'sure' => 'Are you sure ?',
        'deleted_file' => 'Deleted file',
        'not_found' => 'File not found!!!',
        'close_form' => 'Close form',
    ],

    'list' => [],
];

<?php

return [
    'title' => 'Project Payment',

    'label' => [
        'title' => 'Payment',
        'project_id' => 'Project',
        'employee_id' => 'Officer',
        'date' => 'Date',
        'information' => 'Information',
        'amount' => 'Amount',
        'file' => 'File',
        'company_id' => 'Company',
        'pay' => 'Pay',
    ],

    'placeholder' => [
        'title' => 'ex: Pembayaran Termin 1',
        'information' => 'ex: Pembayaran Proyek',
        'date' => 'ex: YYYY-mm-dd HH:ii',
        'amount' => 'ex: 1,000,000.00',
    ],

    'message' => [
        'sure' => 'Are you sure ?',
    ],

    'list' => [],
];

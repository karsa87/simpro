<?php

return [
    'title' => 'Notification',

    'label' => [
        'message' => 'Message', 
        'link' => 'Link', 
        'route' => 'Route', 
        'parameter' => 'Parameter', 
        'is_read' => 'Read', 
        'created_by' => 'Created By', 
        'updated_by' => 'Updated By', 
        'by' => 'By', 
        'time' => 'Time', 
        'company_id' => 'Company',
        'year' => 'year',
        'month' => 'month',
        'week' => 'week',
        'day' => 'day',
        'hour' => 'hour',
        'minute' => 'minute',
        'second' => 'second',
        'ago' => 'ago',
        'now' => 'just now',
        'see_all' => 'See All Notifications',
    ],

    'placeholder' => [
    ],

    'message' => [],
    
    'list' => [
        'is_read' => ['Unread', 'Read'],
        'type' => ['Create', 'Update', 'Delete'],
    ],
];

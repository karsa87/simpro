<?php

return [
    'title' => 'Project',

    'label' => [
        'no_project' => 'No Project',
        'no_spk' => 'No SPK',
        'projectname' => 'Project Name',
        'customer_id' => 'Customer',
        'customer_name' => 'Customer Name',
        'customer_address' => 'Customer Address',
        'customer_phone' => 'Customer Phone',
        'customer_company' => 'Company Name',
        'project_type_id' => 'Project Type',
        'information' => 'Description of Job Type',
        'term_of_work' => 'Term of Work',
        'start_date' => 'Start Date',
        'due_date' => 'Due Date',
        'total_amount' => 'Total Contract Agreed',
        'progress' => 'Progress',
        'status' => 'Status',
        'status_payment' => 'Payment Status',
        'company_id' => 'Company',

        'files' => 'Files',
        'project_detail' => 'Project Details',
        'job' => 'Job',
        'detail_jobs' => 'Detail Jobs',
        'review_submit' => 'Review and Submit',
        'jobs' => 'Jobs',
        'job' => 'Job',
        'workers' => 'Workers',
        'employee_name' => 'Name of Worker',
        'employee_position' => 'Position',

        'shop' => 'Shop',
        'expend' => 'Expend',
        'income' => 'Income',

        'project_run' => 'Project Run',
        'project_done' => 'Project Done',
        'omzet' => 'Omzet',

        'loss_profit' => 'Loss Profit',
    ],

    'placeholder' => [
        'no_project' => 'ex: PO/2021/II/0005',
        'no_spk' => 'ex: 0022/SPK/VII/2021',
        'projectname' => 'ex: Proyek Hambalang',
        'customer_id' => 'ex: Andi Mallarangeng',
        'customer_name' => 'ex: Andi Mallarangeng',
        'customer_address' => 'ex: Jakarta',
        'customer_phone' => 'ex: 089124874xxx',
        'information' => 'Description of Job Type',
        'term_of_work' => 'ex: 11',
        'start_date' => 'ex: 05-06-2021',
        'due_date' => 'ex: 05-06-2021',
        'total_amount' => 'ex: 1,000,000.00',
        'customer_company' => 'ex: PT. XXX',
    ],

    'message' => [
        'desc_customer_name' => 'Fill name of customer',
        'desc_customer_phone' => 'We will never share your phone number with anyone else.',
        'desc_term_of_work' => 'Days',
        'desc_due_date' => 'Estimated Finish Date',
        'desc_start_date' => 'Start Date',
        'desc_total_amount' => 'Estimated Total Cost of this project',
        'desc_review_submit' => 'Review your Details and Submit',
        'sure' => 'Are you sure ?',
        'deleted_job' => 'Deleted detail job',
        'list_workers' => 'List of workers',
        'desc_loss_profit' => 'Select the month and year to get a general ledger report according to your needs',
    ],

    'list' => [
        'status' => [
            0 => 'Waiting',
            1 => 'On Progress',
            2 => 'Done',
            3 => 'Close',
            9 => 'Cancel',
        ],
        'status_class' => [
            0 => 'warning',
            1 => 'primary',
            2 => 'success',
            3 => 'danger',
            9 => 'secondary',
        ],
        'status_payment' => [
            0 => 'Not Yet Paid Off',
            1 => 'Paid Off',
        ]
    ],
];

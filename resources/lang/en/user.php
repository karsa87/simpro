<?php

$lang = [
    'title' => "User",

    'placeholder' => [
        'name' => 'ex: Agus',
        'nik' => 'ex : 333314779124979xxxx',
        'email' => 'agus@gmail.com',
        'lastlogin' => 'Last Login',
        'lastloginip' => 'Last Login IP',
        'address' => 'ex: Malang',
        'phone' => 'ex: 08982423987xx',
        'code' => 'ex: AG',
    ],
    'list' => [
        'status' => ['Deactive', 'Active']
    ],
    'message' => [
        'description_find' => 'Form <code>search</code> for get <code>user</code>',
        'description_list' => 'List data <code>user</code>',
        'failed_status' => 'Failed <code>%s</code>',
        'success_status' => 'Successfully <code>%s</code>',
        'not_found' => 'User <code>%s</code> not found',
    ],
    'label' => [
        'id' => 'ID',
        'name' => 'Name',
        'nik' => 'NIK',
        'email' => 'Email',
        'photo_profile' => 'Photo',
        'lastlogin' => 'Last Login',
        'lastloginip' => 'Last Login IP',
        'role' => 'Role',
        'permission' => 'Permission',
        'password' => 'Password',
        'profile' => 'Profile',
        'code' => 'Code',
        'address' => 'Address',
        'phone' => 'Phone',
        'npwp' => 'NPWP',
        'area_id' => 'Area',
        'position_id' => 'Position',
        'company_id' => 'Company',
        'status' => 'Status',
    ]
];

return $lang;
<?php

return [
    'title' => 'Project Shop',

    'label' => [
        'store_name' => 'Store Name',
        'project_id' => 'Project',
        'employee_id' => 'Officer',
        'information' => 'Information',
        'date' => 'Date',
        'total_amount' => 'Total Amount',
        'total' => 'Total',
        'file' => 'File',
        'status' => 'Status',
        'company_id' => 'Company',

        'project_shop_id' => 'Project Shop',
        'product_name' => 'Product',
        'qty' => 'Quantity',
        'price' => 'Price',
        'total_price' => 'Total Price',
        'product_materials' => 'Goods / Raw Materials',
        'paid' => 'Paid',
        'unpaid' => 'Unpaid',

        'this_month' => 'This Month',
        'last_month' => 'Last Month',
    ],

    'placeholder' => [
        'store_name' => 'ex: TOKO JAYA ABADI',
        'information' => 'ex: Pembayaran Proyek',
        'date' => 'ex: YYYY-mm-dd HH:ii',
        'total_amount' => 'ex: 1,000,000.00',
    ],

    'message' => [
        'sure' => 'Are you sure ?',
        'change_project' => 'Change no project ?',
        'delete_product_materials' => 'Delete Goods / Raw Materials ?',
    ],

    'list' => [
        'status' => [
            0 => 'Not Yet Paid Off',
            1 => 'Paid Off',
        ]
    ],
];

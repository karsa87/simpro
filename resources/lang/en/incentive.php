<?php

return [
    'title' => 'Incentive',

    'label' => [
        'no_transaction' => 'No Transaction',
        'investor' => 'Investor',
        'information' => 'Information',
        'date' => 'Date',
        'amount' => 'Amount',
        'company_id' => 'Company'
    ],

    'placeholder' => [
        'no_transaction' => 'ex: INC/2021/II/0005',
        'investor' => 'ex: Ayu',
        'information' => 'ex: Penambahan Modal',
        'date' => 'ex: YYYY-mm-dd HH:ii',
        'amount' => 'ex: 1,000,000.00',
    ],

    'message' => [],

    'list' => [],
];

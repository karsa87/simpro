<?php

return [
    'title' => 'Company',

    'label' => [
        'logo' => 'Logo', 
        'name' => 'Name', 
        'address' => 'Address', 
        'city' => 'City', 
        'postal_code' => 'Postal Code', 
        'npwp' => 'NPWP', 
        'license_number' => 'License Number', 
        'phone' => 'Phone', 
        'fax' => 'Fax', 
        'email' => 'Email', 
        'director_id' => 'Director Name', 
        'pic_payment' => 'PIC Payment', 
        'pic_purchase' => 'PIC Purchase', 
        'pic_warehouse' => 'PIC Warehouse',
        'format_date' => 'Format Date',
        'pdf' => [
            'name' => 'Nama', 
            'address' => 'Alamat', 
            'npwp' => 'NPWP', 
            'phone' => 'Telepon', 
            'fax' => 'Fax', 
            'email' => 'Email', 
        ]
    ],

    'placeholder' => [
        'name' => 'ex: Perusahaan Malang', 
        'address' => 'ex: LA. Sucipto', 
        'city' => 'ex: Malang', 
        'postal_code' => 'ex: 6512xx', 
        'npwp' => 'ex: 09.254.294.3-407.xxx', 
        'license_number' => 'ex: 097204390xxxx', 
        'phone' => 'ex: 0898127xxxxx', 
        'fax' => 'ex: (0341) 78778xxxx', 
        'email' => 'ex: company@gmail.com', 
        'director_name' => 'ex: Agus', 
        'pic_payment' => 'ex: Agus', 
        'pic_purchase' => 'ex: Agus', 
        'pic_warehouse' => 'ex: Agus', 
    ],

    'message' => [
        'not_found' => 'Company <code>%s</code> not found!!!',
        'success_change_active' => 'Successfully changed the active company',
    ],
    
    'list' => [
        'format_date' => [
            1 => '18-02-2021',
            4 => '2021-02-18',
            8 => '18/02/2021',
            9 => '2021/02/18',
            2 => '18 Februari 2021',
            6 => '18 Februari 2021 20:25',
            3 => 'Kamis, 18 Februari 2021',
            5 => 'Kamis, 18 Februari 2021. 20:23',
        ],
    ],
];

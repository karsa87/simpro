<?php

return [
    'title' => 'Saldo Employee',

    'label' => [
        'employee_id' => 'Employee',
        'date' => 'Date',
        'saldo' => 'Deposit',
        'company_id' => 'Company'
    ],

    'placeholder' => [
        'date' => 'ex: YYYY-mm-dd HH:ii',
        'saldo' => 'ex: 10.000',
    ],

    'message' => [
        'please_employe_id' => 'Please select employee'
    ],

    'list' => [],
];

<?php

$lang = [
    'title' => 'Dashboard',
    
    'label' => [
        'welcome' => 'Welcome',
        'create' => 'Create',
        'more' => 'More',
        'sales_order' => 'Sales Order',
        'purchase_order' => 'Purchase Order',
        'stock_product' => 'Stock less than equal to 1 Product',
        'expend' => 'Cash Out',
        'income' => 'Cash In',
        'chart' => 'Chart',
        'date' => 'Date',
        'member' => 'Active Member',
        'sales_order_paid' => 'Sales Order Paid',
        'sales_order_not_paid' => 'Sales Order Not Paid',
    ],
    
    'placeholder' => [
    ],

    'message' => [
        'welcome' => 'welcome to the POS (Point of Sale) system',
        'success_change_language' => 'Sukses ganti bahasa'
    ],

    'list' => [
    ]
];

return $lang;
<?php

return [
    'title' => 'Expend',

    'label' => [
        'no_transaction' => 'No Transaction',
        'project_id' => 'Project',
        'employee_id' => 'Officer',
        'category_expend_id' => 'Category Expend',
        'information' => 'Information',
        'date' => 'Date',
        'amount' => 'Cost',
        'file' => 'File',
        'company_id' => 'Company'
    ],

    'placeholder' => [
        'no_transaction' => 'ex: INC/2021/II/0005',
        'information' => 'ex: Pembayaran Proyek',
        'date' => 'ex: YYYY-mm-dd HH:ii',
        'amount' => 'ex: 1,000,000.00',
    ],

    'message' => [],

    'list' => [],
];

<?php

return [
    'title' => 'Project Vendor',

    'label' => [
        'project_id' => 'Project',
        'vendor_id' => 'Vendor',
        'company_id' => 'Company',
    ],

    'placeholder' => [
    ],

    'message' => [
        'list_data' => 'List data vendor',
        'sute' => 'Are you sure ?',
        'confirm_change_vendor' => 'Change vendor',
    ],

    'list' => [],
];

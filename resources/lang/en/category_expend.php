<?php

return [
    'title' => 'Category Expend',

    'label' => [
        'name' => 'Name',
        'status' => 'Status',
        'company_id' => 'Company'
    ],

    'placeholder' => [
        'name' => 'ex: Operasional',
    ],

    'message' => [
        'please_name' => 'Please enter category name',
    ],

    'list' => [
        'status' => [
            1 => 'Active',
            0 => 'Deactive',
        ]
    ],
];

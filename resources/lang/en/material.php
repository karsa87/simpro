<?php

return [
    'title' => 'Material',

    'label' => [
        'name' => 'Name',
        'description' => 'Description',
        'unit' => 'Unit',
        'price' => 'Price',
        'qty' => 'Qty',
        'company_id' => 'Company'
    ],

    'placeholder' => [
        'name' => 'ex: Pasir Merah',
        'desciption' => 'ex: Bahan ini berupa pasir merah tanah',
        'price' => 'ex: 100.000',
        'qty' => 'ex: 100',
    ],

    'message' => [
        'duplicate_unit' => 'Unit detected duplicate :unit'
    ],

    'list' => [],
];

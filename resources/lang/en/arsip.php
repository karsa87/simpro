<?php

return [
    'title' => 'Arsip',

    'label' => [
        'name' => 'Name',
        'description' => 'Description',
        'company_id' => 'Company',
        'total_file' =>  'Total File Arsip',
    ],

    'placeholder' => [
        'name' => 'ex: BERKAS PROYEK A',
        'description' => 'ex: Kumpulan berkas dari proyek A',
    ],

    'message' => [],

    'list' => [],
];

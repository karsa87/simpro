<?php

return [
    'title' => 'Customer',

    'label' => [
        'name' => 'name',
        'email' => 'Email',
        'phone' => 'Phone',
        'fax' => 'Fax',
        'address' => 'Address',
        'company_id' => 'Company'
    ],

    'placeholder' => [
        'name' => 'ex: Linda',
        'email' => 'ex: linda@gmail.com',
        'phone' => 'ex: 087821932xxxxx',
        'fax' => 'ex: (0341) 7777xxxx',
        'address' => 'ex: Jl. Margonda',
    ],

    'message' => [],

    'list' => [],
];

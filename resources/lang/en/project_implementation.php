<?php

return [
    'title' => 'Project Implementation',

    'label' => [
        'project_id' => 'Project',
        'project_job_id' => 'Job',
        'date' => 'Date',
        'information' => 'Information',
        'progress' => 'Progress',
        'company_id' => 'Company',
    ],

    'placeholder' => [
        'date' => 'ex: YYYY-mm-dd',
        'porgress' => 'ex: 60',
    ],

    'message' => [
        'sute' => 'Are you sure ?',
        'confirm_change_project' => 'Change project',
        'desc_progress' => 'Percentage of project work (%)'
    ],

    'list' => [],
];

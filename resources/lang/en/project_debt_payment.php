<?php

return [
    'title' => 'Project Debt Payment',

    'label' => [
        'no_transaction' => 'No Transaction',
        'project_id' => 'Project',
        'project_shop_id' => 'Project Shop',
        'store' => 'Store',
        'date' => 'Date',
        'information' => 'Information',
        'amount' => 'Amount',
        'company_id' => 'Company',
        'debt' => 'Total Debt',
    ],

    'placeholder' => [
        'no_transaction' => 'ex: SIMPRO-DSO/2021/VI/0001',
        'date' => 'ex: YYYY-mm-dd H:i',
        'information' => 'ex: pembayaran belanja bulanan di Toko A',
        'amount' => 'ex: 1.000.000',
    ],

    'message' => [
        'sure' => 'Are you sure ?',
        'change_project' => 'Change no project',
    ],

    'list' => [],
];

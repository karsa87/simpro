<?php

return [
    'title' => 'Formula',

    'label' => [
        'code' => 'Code',
        'name' => 'Name',
        'description' => 'Description',
        'coefficient' => 'Coefficient',
        'subtotal' => 'Subtotal',
        'tax' => 'Tax',
        'tax_amount' => 'Tax Amount',
        'total_amount' => 'Total Amount',

        'material_id' => 'Component',
        'unit_id' => 'Unit',
        'qty' => 'Qty',
        'amount' => 'Amount',
        'unit_price' => 'Unit Price',
        'total_price' => 'Total Price',
        'item' => 'Item',
        'subtotal' => 'Subtotal',
        'subtotal_tax' => 'Subtotal x 15%',
        'total' => 'Total',
        'rounding' => 'Rounding',

        'company_id' => 'Company'
    ],

    'placeholder' => [
        'code' => 'ex: SNI-2835-2008-6.1',
        'name' => 'ex: 1 M3 Galian tanah biasa sedalam 1 m',
        'desciption' => 'ex: 1 M3 Galian tanah biasa sedalam 1 m',
        'amount' => 'ex: 100.000',
        'qty' => 'ex: 100',
    ],

    'message' => [
        'duplicate_material_unit' => 'Detected duplicate item material :material unit :unit'
    ],

    'list' => [],
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'login' => 'Sign in to start your session',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'message' => [
        'password_not_correct' => 'Password not correct',
        'email_not_found' => 'Email :value not registered',
        'email_deactived' => 'User with email :value deactivated',
        'success_impersonate' => 'Success impersonate user &nbsp;<span class="text-primary">:user</span>',
        'failed_impersonate' => 'Failed impersonate user',
        'success_leave_impersonate' => 'Success leave impersonate user &nbsp;<span class="text-primary">:user</span>',
        'failed_leave_impersonate' => 'Failed leave impersonate user',
        'not_impersonate' => 'Currently do not impersonate the user',
    ],
    
    'validation' => [
        'email' => [
            'required' => 'Please enter a email address'
        ],
        'password' => [
            'required' => 'Please provide a password'
        ],
    ],

    'meta' => [
        'title' => 'Login',
        'description' => 'Global POS is system retail management',
        'keyword' => 'pos, point of sales, point, of, sales, mochamad karsa syaifudin jupri, mochamad, karsa, syaifudin, jupri, wikacell, system, retail, management, system retail management'
    ]
];

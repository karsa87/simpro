<?php

return [
    'title' => 'Format Number',

    'label' => [
        'menu_id' => 'Menu',
        'prefix' => 'Prefix',
        'middle' => 'Type',
        'year_type' => 'Year',
        'month_type' => 'Month',
        'increment_type' => 'Increment',
        'delimiter' => 'Delimiter',
        'company_id' => 'Company',
        'format' => 'Format',
    ],

    'placeholder' => [
        'prefix' => 'ex: RSP',
        'middle' => 'ex: PO',
        'year_type' => 'ex: 21',
        'month_type' => 'ex: II',
        'increment_type' => 'ex: 0001',
        'delimiter' => '/',
    ],

    'message' => [],

    'list' => [
        'year_type' => [
            '21',
            '2021',
        ],
        'month_type' => [
            'IV',
            '04',
        ],
        'increment_type' => [
            0 => '1',
            2 => '01',
            3 => '001',
            1 => '0001',
        ],
        'increment_sprintf' => [
            0 => '%d',
            2 => '%02d',
            3 => '%03d',
            1 => '%04d',
        ],
    ],
];

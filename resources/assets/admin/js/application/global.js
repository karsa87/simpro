function getCookieLiputan6id(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function setCookieLiputan6id(name,value,days, domain) {
    var expires, domains = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    if (domain) {
        domains = ";domain=." + domain;
    }
    var secure = '';
    if (location.protocol === 'https:') {
        secure = 'SameSite=None; Secure; ';
    }
    document.cookie = name + "=" + (value || "")  + expires + domains + "; "+ secure +"path=/";
}
function getSSODataUser(visitId){


}
(function() {
    
    var url_visitor_liputan6_id = "https://collect.liputan6.id/";
    if(liputan6_id_base_url == 'https://www.staging.liputan6.id')
    {
        url_visitor_liputan6_id = "https://staging.collect.liputan6.id/";
    }
    
    var sso_user_id = (new URLSearchParams(window.location.search)).get('sso-uid');
    if (sso_user_id != '' && sso_user_id != null && sso_user_id != undefined) {
        // setCookieLiputan6id('liputan6_id_u_id', encodeURIComponent(sso_user_id), 2);
        setCookieLiputan6id('liputan6_id_u_id', sso_user_id, 2);
    }
    
    var visit_id_lip6id = getCookieLiputan6id('liputan6_id_visitor_id');
    var user_id_lip6id = encodeURIComponent(getCookieLiputan6id('liputan6_id_u_id'));
    
    // get request
    for (var i = 1; i < 11; i++) {
        if(typeof window.VidioPersonalization !== 'undefined')
        {
            window.VidioPersonalization.getVisitorId(
                function(userId) {
                    if(userId !== '' || typeof userId !== 'undefined')
                    {
                        if(visit_id_lip6id != userId)
                        {
                            document.getElementById('sso-liputan6-id-iframe').src = liputan6_id_base_url+'/html/embed.html?visit_id='+userId;
                            var xhr_sso_lip6id = new XMLHttpRequest();
                            xhr_sso_lip6id.open('GET', url_visitor_liputan6_id+"?visitor_id="+userId);
                            xhr_sso_lip6id.onload = function() {
                                if (xhr_sso_lip6id.status === 200) {
                                    setCookieLiputan6id('liputan6_id_visitor_id', userId, 7);
                                }
                            };
                            xhr_sso_lip6id.send();                          
                        }
                        if(getCookieLiputan6id('liputan6_id_age') == null || getCookieLiputan6id('liputan6_id_gender') == null)
                        {
                            var xhr_sso_lip6id_user = new XMLHttpRequest();
                            xhr_sso_lip6id_user.open('GET', url_visitor_liputan6_id+"user-visitor/get/"+userId);
                            xhr_sso_lip6id_user.onload = function() {
                                response_lip6id = JSON.parse(xhr_sso_lip6id_user.response);
                                if (xhr_sso_lip6id_user.status === 200) {
                                    setCookieLiputan6id('liputan6_id_age', response_lip6id.data.age, 1);
                                    setCookieLiputan6id('liputan6_id_gender', response_lip6id.data.gender, 1);
                                }
                            };
                            xhr_sso_lip6id_user.send();        
                        }
                        
                    } 
                }, 
                function(err) { 
                    
                }
            );
            visit_id_lip6id = getCookieLiputan6id('liputan6_id_visitor_id');
            break;
        }
        else
        {
            setTimeout(function() {
                window.VidioPersonalization.getVisitorId();
            }, 500 * i);
        }
    }

    if (typeof kly !== 'undefined' && typeof kly.visitor !== 'undefined') 
    {
        window.VidioPersonalization.sendData(user_id_lip6id, kly.visitor);
    }
    else if(typeof kmklabs !== 'undefined' && typeof kmklabs.visitor !== 'undefined')
    {
        window.VidioPersonalization.sendData(user_id_lip6id, kmklabs.visitor);
    }
    
    if(typeof liputan6_id_client_id !== 'undefined' && typeof liputan6_id_client_token !== 'undefined')
    {
        var url_json_liputan6_id = liputan6_id_base_url+'/api/trackers/?client_id='+liputan6_id_client_id+'&client_secret='+liputan6_id_client_token;
        var xhr_sso_lip6id = new XMLHttpRequest();
        xhr_sso_lip6id.responseType = 'json';
        xhr_sso_lip6id.open('GET', url_json_liputan6_id);
        xhr_sso_lip6id.onload = function() {
            if (xhr_sso_lip6id.status === 200) {
                var data_lip6id = xhr_sso_lip6id.response;
                data_lip6id.forEach( function(script) {
                    var html_script_lip6id = document.createElement('script'); // is a node
                    html_script_lip6id.innerHTML = script;
                    document.body.appendChild(html_script_lip6id);
                });
            }
        };
        xhr_sso_lip6id.send();
    }
    
    
})();
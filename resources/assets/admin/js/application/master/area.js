$('.btn-edit-area').click(function(e){
    e.preventDefault();

    var btn = $(this);

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="code"]').val(result.data.code);
                $('input[name="name"]').val(result.data.name);

                $('#form-area').attr('action', result.url_edit);
                $('#form-area').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});

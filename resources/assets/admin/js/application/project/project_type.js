$('.btn-edit-project_type').click(function(e){
    e.preventDefault();

    var btn = $(this);

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="price"]').val(result.data.price);
                $('input[name="name"]').val(result.data.name);
                $('textarea[name="description"]').val(result.data.description);

                $('#form-project_type').attr('action', result.url_edit);
                $('#form-project_type').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});

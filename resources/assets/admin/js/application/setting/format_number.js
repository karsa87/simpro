$('.btn-edit-format_number').click(function(e){
    e.preventDefault();

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('#menu-menu_id').val(result.data.menu_id).trigger('change');
                $('input[name="prefix"]').val(result.data.prefix);
                $('input[name="middle"]').val(result.data.middle);
                $('input[name="delimiter"]').val(result.data.delimiter);
                $('#menu-year_type').val(result.data.year_type);
                $('#menu-month_type').val(result.data.month_type);
                $('#menu-increment_type').val(result.data.increment_type);
                
                $('#form-format_number').attr('action', result.url_edit);
                $('#form-format_number').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});

$('#form-search-menu_id').change(function() {
    $('#form-search-format_number').submit();
});
$('#btn-show-password').click(function(){
    if( $(this).hasClass('btn-secondary') ) {
        $(this).addClass('btn-default');
        $(this).removeClass('btn-secondary');

        $('input[name="password"]').attr('type','password');
    }else{
        $(this).addClass('btn-secondary');
        $(this).removeClass('btn-default');
        $('input[name="password"]').attr('type','text');
    }
});
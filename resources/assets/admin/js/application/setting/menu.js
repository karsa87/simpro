$('.btn-edit-menu').click(function(e){
    e.preventDefault();
    var btn = $(this);
    
    // $(".box-title").hide();
    // $(".box-update").show();
    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.menu.id !== undefined)
            {
                $('input[name="id"]').val(result.menu.id);
                $('input[name="name"]').val(result.menu.name);
                $('input[name="url"]').val(result.menu.url);
                $('input[name="route"]').val(result.menu.route);
                $('input[name="icon"]').val(result.menu.icon);
                $('input[name="order"]').val(result.menu.order);
                $('#menu-parent_id').val(result.menu.parent_id).trigger('change');
                $('#menu-is_parent').prop('checked', result.menu.is_parent ? true : false);
            }
            Swal.close()
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close()
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});

var updateOutput = function(e) {
    var list = e.length ? e : $(e.target),
        output = list.data('output');
    if (window.JSON) {
        output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
    } else {
        output.val('JSON browser support required for this demo.');
    }
};

// activate Nestable for list 1
$('#nestable').nestable({
    group: 1
})
.on('change', updateOutput);

// output initial serialised data
updateOutput($('#nestable').data('output', $('#nestable-output')));

$('.dd').on('change', function() {
    $('.div-footer-btn').show();
});

$('.btn-update').click(function(){
    $('#inp-order').val(JSON.stringify($('.dd').nestable('serialize')));
    $('#menu-order-form').submit();
});
$('.btn-edit-role').click(function(e){
    e.preventDefault();

    var btn = $(this);
    
    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.role.id !== undefined)
            {
                $('input[name="id"]').val(result.role.id);
                $('input[name="name"]').val(result.role.name);
                $('input[name="slug"]').val(result.role.slug);
                $('textarea[name="description"]').val(result.role.description);

                $('#form-level').val(result.role.level);
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});

$('.btn-modal-access').click(function(e){
    e.preventDefault();

    var btn = $(this);
    var role_id = $(this).data('id');
    
    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.role.id !== undefined)
            {
                $('.modal-menu-role').modal('show');
                $('#form-access-id').val(role_id);

                $('.check-permission').removeAttr('checked');
                $('.list-group-permission').removeClass('list-group-item-warning');

                $('.check-menu').removeAttr('checked');
                $('.list-group-menu').removeClass('list-group-item-info');
                
                for (var i = 0, _len = result.permission.length; i < _len; i++) {
                    var id = result.permission[i];
                    $('#check-permission-'+id).attr('checked','checked');
                    var li = $('#check-permission-'+id).closest("li");
                    li.addClass('list-group-item-warning');
                }

                for (var i = 0, _len = result.menus.length; i < _len; i++) {
                    var id = result.menus[i];
                    $('#check-menu-'+id).attr('checked','checked');
                    var li = $('#check-menu-'+id).closest("li");
                    li.addClass('list-group-item-info');
                }

                $('#form-role').attr('action', result.url_edit);
                $('#form-role').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});

$('.btn-show-permission').click(function(){
    if ($(this).hasClass('down')) {
        $('#'+$(this).data('id')).slideDown( "slow" );
        $(this).html('<i class="fas fa-arrow-alt-circle-up"></i>');

        $(this).addClass('up');
        $(this).removeClass('down');
    } else {
        $('.li-permission').slideUp( "fast" );
        $(this).html('<i class="fas fa-arrow-alt-circle-down"></i>');

        $(this).addClass('down');
        $(this).removeClass('up');
    }
});

$('.check-menu').change(function(){
    var li = $(this).closest("li");
    
    if($(this).prop('checked'))
    {
        $('#btn-show-permission-'+$(this).data('id')).click();
        $('.check-permission-'+$(this).data('id')).attr('checked','checked');
        li.addClass('list-group-item-info');
    }
    else
    {
        $('.check-permission-'+$(this).data('id')).removeAttr('checked','checked');
        li.removeClass('list-group-item-info');
    }
    
    $('.check-permission-'+$(this).data('id')).change();
});

$('.check-permission').change(function(){
    var li = $(this).closest("li");
    
    if($(this).prop('checked'))
    {
        li.addClass('list-group-item-warning');
    }
    else
    {
        li.removeClass('list-group-item-warning');
    }
});
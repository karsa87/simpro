$('.btn-edit-permission').click(function(e){
    e.preventDefault();

    var btn = $(this);
    
    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('input[name="name"]').val(result.data.name);
                $('input[name="slug"]').val(result.data.slug);
                $('textarea[name="description"]').val(result.data.description);
                // $("#form-model").val(result.model).trigger('change');
                $("#permission-menu").val(result.data.menu).trigger('change');
                
                $('#form-permission').attr('action', result.url_edit);
                $('#form-permission').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});
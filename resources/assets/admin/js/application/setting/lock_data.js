$('.btn-edit-lock_data').click(function(e){
    e.preventDefault();

    $.ajax({
        type: 'GET',
        url: $(this).data("href"),
        dataType: "json",
        success: function success(result) {
            if(result.data.id !== undefined)
            {
                $('input[name="id"]').val(result.data.id);
                $('#menu-menu_id').val(result.data.menu_id).trigger('change');
                $('input[name="delete_day"]').val(result.data.delete_day);
                $('input[name="edit_day"]').val(result.data.edit_day);
                
                $('#form-lock_data').attr('action', result.url_edit);
                $('#form-lock_data').append('<input type="hidden" name="_method" value="PUT">');
            }
            Swal.close();
        },
        error: function(xhr, status, error) {
            var result = xhr.responseJSON;
            showAlert(result.message, "error");
            Swal.close();
        },
        beforeSend: function( xhr ) {
            showProgress();
        }
    });
});

$('#form-search-menu_id').change(function() {
    $('#form-search-lock_data').submit();
});
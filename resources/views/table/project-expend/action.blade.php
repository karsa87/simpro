@hasPermission('finance.expend.edit')
<a title="@lang('global.edit')" href="{{ route('finance.expend.edit', $id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
    <i class="icon-md flaticon2-pen"></i>
</a>
@endhasPermission

@hasPermission('finance.expend.show')
<a title="@lang('global.show_detail')" href="{{ route('finance.expend.show', $id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
    <i class="icon-md flaticon-eye"></i>
</a>
@endhasPermission

@hasPermission('finance.expend.destroy')
<button title="@lang('global.delete')" data-href="{{ route('finance.expend.destroy', $id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
    <i class="icon-md flaticon2-trash"></i>
</button>
@endhasPermission

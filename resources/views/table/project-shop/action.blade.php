@hasPermission('project.project_shop.edit')
<a title="@lang('global.edit')" href="{{ route('project.project_shop.edit', $id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
    <i class="icon-md flaticon2-pen"></i>
</a>
@endhasPermission

@hasPermission('project.project_shop.show')
<a title="@lang('global.show_detail')" href="{{ route('project.project_shop.show', $id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
    <i class="icon-md flaticon-eye"></i>
</a>
@endhasPermission

@hasPermission('project.project_shop.destroy')
<button title="@lang('global.delete')" data-href="{{ route('project.project_shop.destroy', $id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
    <i class="icon-md flaticon2-trash"></i>
</button>
@endhasPermission

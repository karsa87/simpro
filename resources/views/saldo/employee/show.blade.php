@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('saldo_employee.label.saldo') @lang('saldo_employee.title')</span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">@lang('global.detail')</span>
                </h3>
                <div class="card-toolbar">
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">
                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ki ki-bold-more-hor"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover py-5">
                                @hasPermission('saldo.employee.create')
                                <li class="navi-item">
                                    <a href="{{ route('saldo.employee.create') }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-add-1"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.new')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('saldo.employee.edit')
                                <li class="navi-item">
                                    <a href="{{ route('saldo.employee.edit', $model->id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-pen"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.edit')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                <li class="navi-separator my-3"></li>
                                @hasPermission('saldo.employee.destroy')
                                <li class="navi-item">
                                    <a data-href="{{ route('saldo.employee.destroy', $model->id) }}" data-redirect="{{ route('saldo.employee.index') }}" class="navi-link btn-delete">
                                        <span class="navi-icon">
                                            <i class="flaticon2-trash"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.delete')</span>
                                    </a>
                                </li>
                                @endhasPermission
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-4">
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('saldo_employee.label.employee_id'):</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->employee->name }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('saldo_employee.label.date'):</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $util->formatDate($model->date) }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('saldo_employee.label.saldo'):</label>
                    <div class="col-8">
                        <span class="form-control-plaintext">
                        <span class="font-weight-bolder">{{ $util->format_currency($model->saldo) }}</span>&#160;
                    </div>
                </div>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

@push('js')
@endpush

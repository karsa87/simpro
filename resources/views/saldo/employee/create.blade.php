@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('saldo.employee.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id', $model->id) }}" />
    @include('saldo.employee._form')
</form>
<!--end::Form-->
@endsection

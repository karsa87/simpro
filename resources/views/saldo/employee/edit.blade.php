@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('saldo.employee.update', $model->id) }}">
    @csrf
    @method('put')
    <input type="hidden" name="id" value="{{ old('id', $model->id) }}" />
    @include('saldo.employee._form')
</form>
<!--end::Form-->
@endsection

<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b" id="kt_page_sticky_card">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.form') @lang('saldo_employee.label.saldo') @lang('saldo_employee.title')</h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('saldo.employee.index') }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('saldo_employee.label.employee_id')</label>
                    <div class="col-lg-7">
                        <select class="form-control ajax-select2 @classInpError('employee_id')" name="employee_id" data-url="{{ route('ajax.user_employee') }}" data-placeholder="Select...">
                            @if (old('employee_id') || $model->employee_id)
                            @php
                                $model->employee_id = old('employee_id') ?? $model->employee_id;
                            @endphp
                            <option value="{{ $model->employee_id }}" selected>
                                {{ $model->employee->name }}
                            </option>
                            @endif
                        </select>

                        @inpSpanError(['column'=>'employee_id'])
                        <span class="form-text text-muted">@lang('saldo_employee.message.please_employe_id')</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('saldo_employee.label.date')</label>
                    <div class="col-lg-7">
                        <div class="input-group date">
                            <input type="text" class="form-control datetimepicker @classInpError('date')" readonly="readonly" value="{{ date('Y-m-d H:i', strtotime(old('date', $model->date ?? now()))) }}" name="date" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar glyphicon-th"></i>
                                </span>
                            </div>
                            @inpSpanError(['column'=>'date'])
                        </div>
                    </div>

                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">Saldo</label>
                    <div class="col-lg-7">
                        <input class="form-control format-currency @classInpError('saldo')" type="text" value="{{ old('saldo', $model->saldo) }}" name="saldo" />
                        @inpSpanError(['column'=>'saldo'])
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>
</form>

@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Deposit Saldo Karyawan</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('saldo.employee.create')
                    <!--begin::Button-->
                    <a href="{{ route('saldo.employee.create') }}" class="btn btn-primary font-weight-bolder">
                        Add
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <div class="mb-7">
                    <form id="form-search-saldo-employee" role="form" action="{{ route('saldo.employee.index') }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" name="_k" class="form-control" placeholder="Search..." value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->
                <!--begin: Datatable-->
                <table class="table">
                    <thead>
                        <tr>
                            <th class="col-sm-4">@sortablelink('employee.name', trans('saldo_employee.label.employee_id'), request()->except(["token"]),  ['rel' => 'nofollow'])</th>
                            <th class="col-sm-3">@sortablelink('date', trans('saldo_employee.label.date'), request()->except(["token"]),  ['rel' => 'nofollow'])</th>
                            <th class="col-sm-3">@sortablelink('saldo', trans('saldo_employee.label.saldo'), request()->except(["token"]),  ['rel' => 'nofollow'])</th>
                            <th class="col-sm-2">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($saldoEmployees as $saldo)
                        <tr>
                            <td>{{ $saldo->employee->name }}</td>
                            <td>{{ date('Y-m-d H:i', strtotime($saldo->date)) }}</td>
                            <td>{{ number_format($saldo->saldo) }}</td>
                            <td>
                                @hasPermission('saldo.employee.edit')
                                <a title="@lang('global.edit')" href="{{ route('saldo.employee.edit', $saldo->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-pen"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('saldo.employee.show')
                                <a title="@lang('global.show_detail')" href="{{ route('saldo.employee.show', $saldo->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon-eye"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('saldo.employee.show')
                                <button title="@lang('global.delete')" data-href="{{ route('saldo.employee.destroy', $saldo->id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-trash"></i>
                                </button>
                                @endhasPermission
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($saldoEmployees, 'form-search-saldo-employee') !!}
        </div>
        <!--end::Card-->

    </div>
</div>
@endsection

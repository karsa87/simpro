<div class="alert alert-{{ isset($type) ? $type : 'info' }} alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> {{ isset($title) ? $title : 'Alert!' }}</h5>
    {{ isset($message) ? $message : 'info' }}
</div>
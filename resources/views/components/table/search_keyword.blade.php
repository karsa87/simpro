<div class="input-group">
    <input type="text" name="search_kw" class="form-control float-right" placeholder="@lang('global.keyword')" value="{{ request('_k') }}">

    <div class="input-group-append">
        <button type="submit" id="btn-search" class="btn btn-default"><i class="fas fa-search"></i></button>
    </div>
</div>

@push('js')
<script type="text/javascript">
    $(document).ready(function() {
        if ($("input[name='_k']").length <= 0) {
            $('<input>').attr({
                type: 'hidden',
                name: '_k',
                value: '{{ request("_k") }}'
            }).appendTo('#{{ $form_search }}');
        }

        $("input[name='search_kw']").keypress(function(e){
            if (e.keyCode == 13) {
                $("input[name='_k']").val($(this).val());
                $('#{{ $form_search }}').submit();
            }
        });

        $("#btn-search").click(function() {
            $("input[name='_k']").val($("input[name='search_kw']").val());
            $('#{{ $form_search }}').submit();
        });
    });
</script>
@endpush
@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row" id="content-format_number">
    <div class="col-xs-12 col-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header flex-wrap pt-6 pb-0">
                <h3 class="card-title"> @lang('global.list') @lang('format_number.title')</h3>
            </div>
            <div class="card-body table-responsive">
                <!--begin::Search Form-->
                <div class="mb-7">
                    <div class="row align-items-center">
                        <div class="col-lg-12">
                            <form id="form-search-format_number" role="form" action="{{ route('setting.format_number.index') }}">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <label>@lang('format_number.label.menu_id')</label>
                                        <select name="_m" id="form-search-menu_id" class="form-control float-right" data-placeholder="" style="width: 100%;">
                                            <option value=""></option>
                                            @foreach ($menus as $menu)
                                            <option value="{{ $menu->id }}" {{ request('_m') == $menu->id ? "selected"  : ""}}>{{ $menu->parent ? $menu->parent->name . ' -' : '' }} {{ $menu->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--end: Search Form-->
                <table class="table table-hover projects">
                    <thead>
                        <tr>
                            <th width="5%" class="text-center">@lang('global.no')</th>
                            <th width="45%">@lang('format_number.label.menu_id')</th>
                            <th width="35%" class="text-center">@lang('format_number.label.format')</th>
                            <th width="15%" class="text-center">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($formatNumbers) && count($formatNumbers) > 0 )
                        @foreach( $formatNumbers as $i => $r)
                        <tr>
                            <td class="text-center">{{ $formatNumbers->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->menu->name }}
                            </td>
                            <td class="text-wrap text-center">
                                {{ $r->format(1) }}
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    @hasPermission('setting.format_number.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('setting.format_number.edit', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-edit-format_number btn-loader" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('setting.format_number.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('setting.format_number.destroy', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($formatNumbers) && count($formatNumbers) > 0 )
            {!! \App\Util\Base\Layout::paging($formatNumbers) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header flex-wrap pt-6 pb-0">
                <h3 class="card-title">@lang('global.form') @lang('format_number.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'setting.format_number.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/setting/format_number.js') }}?{{ config('app.version') }}"></script>
@endpush
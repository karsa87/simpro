<!-- form start -->
<form id="form-format_number" role="form" method="POST" action="{{ route('setting.format_number.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="menu_id">@lang('format_number.label.menu_id')</label>
            <select name="menu_id"  id="menu-menu_id" class="form-control select2 @classInpError('menu_id')" data-placeholder="Choose" style="width: 100%;">
                <option value=""></option>
                @foreach ($menus as $menu)
                <option value="{{ $menu->id }}" {{ old('menu_id') == $menu->id ? "selected"  : ""}}>{{ $menu->parent ? $menu->parent->name . ' -' : '' }} {{ $menu->name }}</option>
                @endforeach
            </select>
            
            @inpSpanError(['column'=>'menu_id'])
        </div>
        <div class="form-group">
            <label for="prefix">@lang('format_number.label.prefix')</label>
            <input type="text" name="prefix" class="form-control @classInpError('prefix')" placeholder="@lang('format_number.placeholder.prefix')" value="{{ old('prefix') }}">
            
            @inpSpanError(['column'=>'prefix'])
        </div>
        <div class="form-group">
            <label for="middle">@lang('format_number.label.middle')</label>
            <input type="text" name="middle" class="form-control @classInpError('middle')" placeholder="@lang('format_number.placeholder.middle')" value="{{ old('middle') }}">
            
            @inpSpanError(['column'=>'middle'])
        </div>
        <div class="form-group">
            <label for="year_type">@lang('format_number.label.year_type')</label>
            <select name="year_type"  id="menu-year_type" class="form-control @classInpError('year_type')">
                <option value=""></option>
                @foreach (trans('format_number.list.year_type') as $id => $name)
                <option value="{{ $id }}" {{ old('year_type') == $id ? "selected"  : ""}}>{{ $name }}</option>
                @endforeach
            </select>
            
            @inpSpanError(['column'=>'year_type'])
        </div>
        <div class="form-group">
            <label for="month_type">@lang('format_number.label.month_type')</label>
            <select name="month_type"  id="menu-month_type" class="form-control @classInpError('month_type')">
                <option value=""></option>
                @foreach (trans('format_number.list.month_type') as $id => $name)
                <option value="{{ $id }}" {{ old('month_type') == $id ? "selected"  : ""}}>{{ $name }}</option>
                @endforeach
            </select>
            
            @inpSpanError(['column'=>'month_type'])
        </div>
        <div class="form-group">
            <label for="increment_type">@lang('format_number.label.increment_type')</label>
            <select name="increment_type"  id="menu-increment_type" class="form-control @classInpError('increment_type')">
                @foreach (trans('format_number.list.increment_type') as $id => $name)
                <option value="{{ $id }}" {{ old('increment_type') == $id ? "selected"  : ""}}>{{ $name }}</option>
                @endforeach
            </select>
            
            @inpSpanError(['column'=>'increment_type'])
        </div>
        <div class="form-group">
            <label for="delimiter">@lang('format_number.label.delimiter')</label>
            <input type="text" name="delimiter" class="form-control @classInpError('delimiter')" placeholder="@lang('format_number.placeholder.delimiter')" value="{{ old('delimiter') }}">
            
            @inpSpanError(['column'=>'delimiter'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route(request()->route()->getName()) }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>
<!-- form start -->
<form id="form-lock_data" role="form" method="POST" action="{{ route('setting.lock_data.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="menu_id">@lang('lock_data.label.menu_id')</label>
            <select name="menu_id"  id="menu-menu_id" class="form-control select2 @classInpError('menu_id')" data-placeholder="Choose" style="width: 100%;">
                <option value=""></option>
                @foreach ($menus as $menu)
                <option value="{{ $menu->id }}" {{ old('menu_id') == $menu->id ? "selected"  : ""}}>{{ $menu->parent ? $menu->parent->name . ' -' : '' }} {{ $menu->name }}</option>
                @endforeach
            </select>
            
            @inpSpanError(['column'=>'menu_id'])
        </div>
        <div class="form-group">
            <label for="edit_day">@lang('lock_data.label.edit_day')</label>
            <input type="number" name="edit_day" class="form-control @classInpError('edit_day')" placeholder="@lang('lock_data.placeholder.day')" value="{{ old('edit_day') }}">
            
            @inpSpanError(['column'=>'edit_day'])
        </div>
        <div class="form-group">
            <label for="delete_day">@lang('lock_data.label.delete_day')</label>
            <input type="number" name="delete_day" class="form-control @classInpError('delete_day')" placeholder="@lang('lock_data.placeholder.day')" value="{{ old('delete_day') }}">
            
            @inpSpanError(['column'=>'delete_day'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route(request()->route()->getName()) }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>
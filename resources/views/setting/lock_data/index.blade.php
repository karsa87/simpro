@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row" id="content-lock_data">
    <div class="col-xs-12 col-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header flex-wrap pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label"> @lang('global.list') @lang('lock_data.title')</h3>
                </div>
            </div>
            <div class="card-body table-responsive">
                <!--begin::Search Form-->
                <div class="mb-7">
                    <div class="row align-items-center">
                        <div class="col-lg-12">
                            <form id="form-search-lock_data" role="form" action="{{ route('setting.lock_data.index') }}">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <label>@lang('lock_data.label.menu_id')</label>
                                        <select name="_m" id="form-search-menu_id" class="form-control float-right select2" data-placeholder="Choose" style="width: 100%;">
                                            @foreach ($menus as $menu)
                                            <option value="{{ $menu->id }}" {{ request('_m') == $menu->id ? "selected"  : ""}}>{{ $menu->parent ? $menu->parent->name . ' -' : '' }} {{ $menu->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--end: Search Form-->
                <table class="table table-hover projects">
                    <thead>
                        <tr>
                            <th width="5%" class="text-center">@lang('global.no')</th>
                            <th width="35%">@lang('lock_data.label.menu_id')</th>
                            <th width="25%" class="text-center">@lang('lock_data.label.edit_day')</th>
                            <th width="25%" class="text-center">@lang('lock_data.label.delete_day')</th>
                            <th width="10%" class="text-center">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($lockDatas) && count($lockDatas) > 0 )
                        @foreach( $lockDatas as $i => $r)
                        <tr>
                            <td class="text-center">{{ $lockDatas->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->menu->name }}
                            </td>
                            <td class="text-wrap text-center">
                                {{ $util->format_currency($r->edit_day) }}
                            </td>
                            <td class="text-wrap text-center">
                                {{ $util->format_currency($r->delete_day) }}
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('setting.lock_data.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('setting.lock_data.edit', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-edit-lock_data btn-loader" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('setting.lock_data.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('setting.lock_data.destroy', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($lockDatas) && count($lockDatas) > 0 )
            {!! \App\Util\Base\Layout::paging($lockDatas) !!}
            @endif
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header flex-wrap pt-6 pb-0">
                <h3 class="card-title">@lang('global.form') @lang('lock_data.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'setting.lock_data.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/setting/lock_data.js') }}?{{ config('app.version') }}"></script>
@endpush
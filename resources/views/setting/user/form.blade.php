@extends('layouts.admin')

@section('content')
<!-- form start -->
<form id="form-user" role="form" method="POST" action="{{ route('setting.user.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id', $model->id) }}">
    <div class="card card-primary">
        <div class="card-header flex-wrap pt-6 pb-0">
            <h3 class="card-title">@lang('global.form') @lang('user.title')</h3>
        </div>
        <!-- /.card-header -->

        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <input type="hidden" name="photo_profile" id="dir-upload-photo_profile" value="{{ old('photo_profile', $model->photo_profile) }}">
                        <div class="dropzone dropzone-default dropzone-success files-upload" id="files-upload" data-inp-id="dir-upload-photo_profile" data-href="{{ route('upload.user.photo_profile') }}" data-accept-file="image/jpeg,image/png,.png,.jpg,.jpeg">
                            <div class="dropzone-msg dz-message needsclick">
                                <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                <span class="dropzone-msg-desc">Only image, pdf and psd files are allowed for upload</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">@lang('user.label.name')</label>
                        <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('user.placeholder.name')" value="{{ old('name', $model->name) }}">
                        
                        @inpSpanError(['column'=>'name'])
                    </div>
                    <div class="form-group">
                        <label for="nik">@lang('user.label.nik')</label>
                        <input type="text" class="form-control @classInpError('nik')" name="nik" value="{{ old('nik', $model->nik) }}" data-inputmask='"mask": "9999999999999999"' data-mask>
                        
                        @inpSpanError(['column'=>'nik'])
                    </div>
                    <div class="form-group">
                        <label for="phone">@lang('user.label.phone')</label>
                        <input type="text" name="phone" class="form-control @classInpError('phone')" placeholder="@lang('user.placeholder.phone')" value="{{ old('phone', $model->phone) }}">
                        
                        @inpSpanError(['column'=>'phone'])
                    </div>
                    <div class="form-group">
                        <label for="address">@lang('user.label.address')</label>
                        <textarea name="address" class="form-control @classInpError('address')" rows="2" placeholder="@lang('user.placeholder.address')" >{{ old('address', $model->address) }}</textarea>
                        
                        @inpSpanError(['column'=>'address'])
                    </div>
                    <div class="form-group">
                        <label for="npwp">@lang('user.label.npwp')</label>
        
                        <input type="text" class="form-control @classInpError('npwp')" name="npwp" value="{{ old('npwp', $model->npwp) }}" data-inputmask='"mask": "99.999.999.9-999.999"' data-mask>
                        
                        @inpSpanError(['column'=>'npwp'])
                    </div>
                </div>
        
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="email">@lang('user.label.email')</label>
                        <input type="email" name="email" class="form-control @classInpError('email')" placeholder="@lang('user.placeholder.email')" value="{{ old('email', $model->email) }}">
        
                        @inpSpanError(['column'=>'email'])
                    </div>
                    <div class="form-group">
                        <label for="password">@lang('user.label.password')</label>
                        <div class="input-group mb-3">
                            <input type="password" name="password" class="form-control @classInpError('password')" value="{{ old('password') }}">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-default btn-flat" id="btn-show-password">
                                    <i class="fas fa-eye"></i>
                                </button>
                            </div>
        
                            @inpSpanError(['column'=>'password'])
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="form-user_role" class="control-label">@lang('user.label.role')</label>
                        @php
                            $role_id = $model->roles->first() ? $model->roles->first()->id : null;
                        @endphp
                        <select name="user_role" id="form-user_role" class="form-control select2 @classInpError('user_role')" data-placeholder="" style="width: 100%;">
                            @foreach($roles as $id => $name)
                                <option value="{{ $id }}" {{ old('user_role', $role_id) == $id ? "selected"  : ""}} >
                                    {{ $name }}
                                </option>
                            @endforeach
                        </select>
        
                        @inpSpanError(['column'=>'user_role'])
                    </div>
                    <div class="form-group">
                        <label for="form-company_id" class="control-label">
                            @lang('user.label.company_id')
                        </label>
                        <select name="user_company[]" id="form-company_id" class="form-control ajax-select2 @classInpError('user_company')" data-url="{{ route('ajax.company') }}" data-placeholder="" style="width: 100%;" multiple>
                            @if (old('user_company') || $model->company)
                                @php
                                    $model->company = old('user_company') ?? $model->company->pluck('id')->toArray();
        
                                    $companies = \App\Models\Company::whereIn('id', $model->company)->get();
                                @endphp
                                @foreach ($companies as $company)
                                    <option value="{{ $company->id }}" selected>{{ $company->name }}</option>
                                @endforeach
                            @endif
                        </select>
        
                        @inpSpanError(['column'=>'company_id'])
                    </div>
                    <div class="form-group">
                        <label for="status" class="control-label">@lang('user.label.status')</label>
                        <select name="status" id="form-status" class="form-control  @classInpError('status')" data-placeholder="" style="width: 100%;">
                            @foreach(trans('user.list.status') as $id => $name)
                                <option value="{{ $id }}" {{ old('status', $model->status) == $id ? "selected"  : ""}} >
                                    {{ $name }}
                                </option>
                            @endforeach
                        </select>
        
                        @inpSpanError(['column'=>'status'])
                    </div>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <a href="{{ route('setting.user.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
            <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
        </div>
    </div>  
</form>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/setting/user.js') }}?{{ config('app.version') }}"></script>
@endpush
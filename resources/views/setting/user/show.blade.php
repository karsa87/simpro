@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom gutter-b">
            <!--begin::Body-->
            <div class="card-body pt-4">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end">
                    <div class="dropdown dropdown-inline">
                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ki ki-bold-more-hor"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover py-5">
                                <li class="navi-item">
                                    <a href="{{ route('setting.user.create') }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-add-1"></i>
                                        </span>
                                        <span class="navi-text">New User</span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="{{ route('setting.user.edit', $model->id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-pen"></i>
                                        </span>
                                        <span class="navi-text">Edit</span>
                                    </a>
                                </li>
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <a href="{{ route('setting.user.destroy', $model->id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-trash"></i>
                                        </span>
                                        <span class="navi-text">Delete</span>
                                    </a>
                                </li>
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                </div>
                <!--end::Toolbar-->
                <!--begin::User-->
                <div class="d-flex align-items-center">
                    <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                        <div class="symbol-label" style="background-image: url('{{ $model->photoUrl }}');"></div>
                        <i class="symbol-badge bg-success"></i>
                    </div>
                    <div>
                        <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">{{ $model->name }}</a>
                        <div class="text-muted">{{ $model->email }}</div>
                        <span class="badge {{ $model->status ? 'badge-success' : 'badge-danger' }}">@lang('user.list.status.'.$model->status)</span>
                    </div>
                </div>
                <!--end::User-->
                <!--begin::Contact-->
                <div class="pt-8 pb-6">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="d-flex align-items-center justify-content-between mb-2">
                                <span class="font-weight-bold mr-2">@lang('user.label.nik'):</span>
                                <span class="text-muted">{{ $model->nik }}</span>
                            </div>
                            <div class="d-flex align-items-center justify-content-between mb-2">
                                <span class="font-weight-bold mr-2">@lang('user.label.phone'):</span>
                                <span class="text-muted">{{ $model->phone }}</span>
                            </div>
                            <div class="d-flex align-items-center justify-content-between mb-2">
                                <span class="font-weight-bold mr-2">@lang('user.label.npwp'):</span>
                                <span class="text-muted">{{ $model->npwp }}</span>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <span class="font-weight-bold mr-2">@lang('user.label.address'):</span>
                                <span class="text-muted">{{ $model->address }}</span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="d-flex align-items-center justify-content-between mb-2">
                                <span class="font-weight-bold mr-2">@lang('user.label.role'):</span>
                                <span class="text-muted">{{ $model->roles->first() ? $model->roles->first()->name : '-' }}</span>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <span class="font-weight-bold mr-2">@lang('user.label.company_id'):</span>
                                <span class="text-muted">{{ $model->company->pluck('name')->join(', ') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Contact-->
            </div>
            <!--end::Body-->
        </div>
    </div>
</div>
@stop

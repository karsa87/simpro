<!-- form start -->
<form role="form" id="form-search-user" action="{{ route('setting.user.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="name">@lang('user.label.name')</label>
                    <input type="text" name="_nm" class="form-control" placeholder="@lang('user.placeholder.name')" value="{{ request('_nm') }}">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="_e">@lang('user.label.email')</label>
                    <input type="text" name="_e" class="form-control" placeholder="@lang('user.placeholder.email')" value="{{ request('_e') }}">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="_nk">@lang('user.label.nik')</label>
                    <input type="text" name="_nk" class="form-control" placeholder="@lang('user.placeholder.nik')" value="{{ request('_nk') }}">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="_cd">@lang('user.label.code')</label>
                    <input type="text" name="_cd" class="form-control" placeholder="@lang('user.placeholder.code')" value="{{ request('_cd') }}">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="_ad">@lang('user.label.address')</label>
                    <input type="text" name="_ad" class="form-control" placeholder="@lang('user.placeholder.address')" value="{{ request('_ad') }}">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="_rl">@lang('user.label.role')</label>
                    <select name="_rl"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($roles as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_rl')) && request('_rl') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="_sts">@lang('user.label.status')</label>
                    <select name="_sts"  class="form-control select2" data-placeholder="" style="width: 100%;">
                        <option value=""></option>
                        @foreach (trans('user.list.status') as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_sts')) && request('_sts') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('setting.user.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>
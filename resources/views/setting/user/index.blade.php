@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <h3 class="card-title">@lang('global.search') @lang('user.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'setting.user.search')
        </div>
    </div>
</div>
<div class="row mt-5" id="content-user">
    <div class="col-xs-12 col-sm-12 col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-title"> @lang('global.list') @lang('user.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('setting.user.create')
                    <a href="{{ route('setting.user.create') }}" class="btn btn-success float-right">@lang('global.add')</a>
                    @endhasPermission
                </div>
            </div>

            <div class="card-body table-responsive p-0">
                <table class="table projects sorting-table" data-form="form-search-user">
                    <thead>
                        <tr>
                            <th width="10%" class="text-center">@lang('global.no')</th>
                            <th width="30%">@lang('user.label.name')</th>
                            <th width="25%">@lang('user.label.email')</th>
                            <th width="25%">@lang('user.label.lastlogin')</th>
                            <th width="10%">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($users) && count($users) > 0 )
                        @foreach( $users as $i => $r)
                        <tr>
                            <td class="text-center">{{ $users->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->name }}
                                </h6>
                                <p class="m-0">
                                    <span class="users-list-date">{{ $r->nik }}</span>
                                </p>
                                <p class="m-0">
                                    <span class="users-list-date">{{ $r->code }}</span>
                                </p>
                                @if (!auth()->user()->underAdministrator())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->address }}
                                </h6>
                                <p class="m-0">
                                    <span class="users-list-date">{{ $r->email }}</span>
                                </p>
                                <p class="m-0">
                                    <span class="users-list-date">{{ $r->phone }}</span>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    @if( $r->lastlogin )
                                    {{ date('D, d-m-Y H:i', strtotime($r->lastlogin)) }}
                                    @else
                                    -
                                    @endif
                                </h6>
                                <p class="m-0">
                                    <span class="users-list-date">{{ $r->lastloginip }}</span>
                                </p>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @if (auth()->user()->isDeveloper())
                                    {{-- <a title="@lang('global.impersonate')" href="{{ route('impersonate', $r['id']) }}" class="btn btn-sm btn-primary btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-user-clock"></i>
                                    </a> --}}
                                    @endif

                                    @hasPermission('setting.user.edit')
                                    <a title="@lang('global.edit')" href="{{ route('setting.user.edit', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('setting.user.show')
                                    <a title="@lang('global.show_detail')" href="{{ route('setting.user.show', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                        <i class="icon-md flaticon-eye"></i>
                                    </a>
                                    @endhasPermission
                                    
                                    @hasPermission('setting.user.destroy')
                                    @if(!$r->employee)
                                    <button title="@lang('global.delete')" data-href="{{ route('setting.user.destroy', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </button>
                                    @endif
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($users) && count($users) > 0 )
            {!! \App\Util\Base\Layout::paging($users) !!}
            @endif
        </div>
    </div>
</div>
@stop
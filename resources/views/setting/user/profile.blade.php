@extends('layouts.admin')
@inject('util', 'App\Util\Helpers\Util')
@section('content')
<div class="row">
    <div class="col-md-3">
        <!-- Profile Image -->
        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
                <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle" src="{{ $model->photoUrl }}" alt="{{ $model->name }}" />
                </div>

                <h3 class="profile-username text-center">{{ $model->name }}</h3>

                <p class="text-muted text-center">{{ $model->roles->first()->name }}</p>

                <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item"><b>@lang('user.label.lastlogin')</b> <a class="float-right">{{ $util->formatDate($model->lastlogin, session(config('system.session.active_company'))->format_date) }}</a></li>
                    <li class="list-group-item"><b>@lang('user.label.lastloginip')</b> <a class="float-right">{{ $model->lastloginip }}</a></li>
                </ul>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="card">
            <div class="card-header p-2">
                Settings
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form id="form-user" role="form" method="POST" action="{{ route('setting.user.profile') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ old('id') ?? $model->id }}">
                    <input type="hidden" name="branch_id" value="{{ $model->branch_id }}">
                    <input type="hidden" name="user_role" value="{{ $model->roles->first()->id }}">
                    @foreach ($model->company as $company)
                    <input type="hidden" name="user_company[]" value="{{ $company->id }}">
                    @endforeach
                    
                    @if (!empty($model->userPermissions))
                        @foreach ($model->userPermissions as $k => $v)
                        <input type="hidden" name="user_permission[{{$k}}]" value="{{ $v->id }}">
                        @endforeach
                    @else
                    <input type="hidden" name="user_permission" value="">
                    @endif

                    <div class="card-body">
                        <div class="form-group">
                            <label for="photo_profile">@lang('user.label.photo_profile')</label>
                            <input type="hidden" name="photo_profile" id="dir-upload-photo_profile" value="{{ old('photo_profile', json_encode($model->photo_profile ?? [])) }}">
                            <div class="dropzone files-upload" id="files-upload" data-inp-id="dir-upload-photo_profile" data-href="{{ route('upload.user.photo_profile') }}" data-accept-file="image/jpeg,image/png,.png,.jpg,.jpeg"></div>
                        </div>
                        <div class="form-group">
                            <label for="name">@lang('user.label.name')</label>
                            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('user.placeholder.name')" value="{{ old('name') ?? $model->name }}">
                            
                            @inpSpanError(['column'=>'name'])
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('user.label.email')</label>
                            <input type="email" name="email" class="form-control @classInpError('email')" placeholder="@lang('user.placeholder.email')" value="{{ old('email', $model->email) }}">
    
                            @inpSpanError(['column'=>'email'])
                        </div>
                        <div class="form-group">
                            <label for="password">@lang('user.label.password')</label>
                            <div class="input-group mb-3">
                                <input type="password" name="password" class="form-control @classInpError('password')" value="{{ old('password') }}">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-default btn-flat" id="btn-show-password">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                </div>
                
                                @inpSpanError(['column'=>'password'])
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nik">@lang('user.label.nik')</label>
                            <input type="text" class="form-control @classInpError('nik')" name="nik" value="{{ old('nik', $model->nik) }}" data-inputmask='"mask": "9999999999999999"' data-mask>
                            
                            @inpSpanError(['column'=>'nik'])
                        </div>
                        <div class="form-group">
                            <label for="npwp">@lang('user.label.npwp')</label>
        
                            <input type="text" class="form-control @classInpError('npwp')" name="npwp" value="{{ old('npwp', $model->npwp) }}" data-inputmask='"mask": "99.999.999.9-999.999"' data-mask>
                            
                            @inpSpanError(['column'=>'nik'])
                        </div>
                        <div class="form-group">
                            <label for="address">@lang('user.label.address')</label>
                            <textarea name="address" class="form-control @classInpError('address')" rows="2" placeholder="@lang('user.placeholder.address')" >{{ old('address', $model->address) }}</textarea>
                            
                            @inpSpanError(['column'=>'address'])
                        </div>
                        <div class="form-group">
                            <label for="phone">@lang('user.label.phone')</label>
                            <input type="text" name="phone" class="form-control @classInpError('phone')" placeholder="@lang('user.placeholder.phone')" value="{{ old('phone', $model->phone) }}">
                            
                            @inpSpanError(['column'=>'phone'])
                        </div>
                    </div>
                    <!-- /.card-body -->
                
                    <div class="card-footer">
                        <a href="{{ route('setting.user.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/setting/user.js') }}?{{ config('app.version') }}"></script>
@endpush
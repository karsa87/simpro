<ul class="list-group list-group-flush">
    @foreach( $menus as $menu )
    @php
        if( empty($menu->icon) && empty($menu->url) && empty($menu->route) && !$menu->is_parent )
        {
            continue;
        }
    @endphp

    <li class="list-group-item list-group-menu list-group-item-info">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" name="menu[{{ $menu->id }}]" class="custom-control-input check-menu" id="check-menu-{{ $menu->id }}" data-id="{{ $menu->id }}">
            <label class="custom-control-label" for="check-menu-{{ $menu->id }}">{{ $menu->name }}</label>
            @if( $menu->permissions->count() > 0 )
            <a href="javascript:void(0)" data-id='li-permission-{{ $menu->id }}' class="btn-show-permission down" id="btn-show-permission-{{ $menu->id }}"><i class="fas fa-arrow-alt-circle-down"></i></a>
            @endif
        </div>
    </li>
    @if( $menu->permissions->count() > 0 )
    <li class="list-group-item li-permission" id="li-permission-{{ $menu->id }}" style="display: none;">
        <ul class="list-group list-group-flush">
            @php
            $permissions = $menu->permissions->unique('id');
            @endphp
            @foreach( $permissions as $permission )
            <li class="list-group-item list-group-permission list-group-item-warning">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="permission[{{ $permission->id }}]" class="custom-control-input check-permission check-permission-{{ $menu->id }}" id="check-permission-{{ $permission->id }}">
                    <label class="custom-control-label" for="check-permission-{{ $permission->id }}">{{ $permission->name }}</label>
                </div>
            </li>
            @endforeach
        </ul>
    </li>
    @endif
    @if( $menu->submenu->count() > 0 )
    <li class="list-group-item">
        @include($template . '.setting.role.menu',['menus'=>$menu->submenu])
    </li>
    @endif
    @endforeach
</ul>

@extends('layouts.admin')

@inject('role', 'App\Models\Role')
@section('content')
<div class="row mb-5">
    <div class="col-lg-12">
        <div class="card card-secondary">
            <div class="card-header flex-wrap pt-6 pb-0">
                <h3 class="card-title">@lang('global.search') @lang('role.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'setting.role.search')
        </div>
    </div>
</div>

<div class="modal fade modal-menu-role" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <form method="post" id="form-access" action="{{ route('setting.role.access') }}" novalidate="novalidate" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="role_id" id="form-access-id">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title h4" id="myLargeModalLabel">@lang('role.label.menu_access') <code id="modal-name-role"></code></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="container my-4">
                        @include($template . '.setting.role.menu',['menus'=>$menus])
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        @lang('global.cancel')
                    </button>
                    <button type="submit" class="btn btn-success btn-loader">
                        @lang('global.save')
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="row" id="content-role">
    <div class="col-xs-12 col-sm-12 col-lg-8">
        <div class="card card-primary card-outline">
            <div class="card-header flex-wrap pt-6 pb-0">
                <h3 class="card-title"> @lang('global.list') @lang('role.title')</h3>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th>@lang('role.label.name')</th>
                            <th>@lang('role.label.slug')</th>
                            <th class="text-center">@lang('role.label.description')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($roles) && count($roles) > 0 )
                        @foreach( $roles as $i => $r)
                        <tr>
                            <td class="text-center">{{ $roles->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                {{ $r->name }}
                                @if (!auth()->user()->underAdministrator())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">{{ $r->slug }}</td>
                            <td class="text-wrap">
                                {{ $r->description }}
                                <p class="pt-1">
                                    <small>
                                        <strong>@lang('role.label.level'):</strong>
                                        @lang('role.list.level.' . $r->level)
                                    </small>
                                </p>
                            </td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('setting.role.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('setting.role.edit', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-edit-role" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('setting.role.access')
                                    <a title="@lang('global.access')" href="javascript:void(0)" data-href="{{ route('setting.role.edit', $r['id']) }}" data-id="{{ $r['id'] }}" class="btn btn-icon btn-outline-secondary mr-2 btn-modal-access" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-group"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('setting.role.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('setting.role.destroy', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($roles) && count($roles) > 0 )
            {!! \App\Util\Base\Layout::paging($roles) !!}
            @endif
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header flex-wrap pt-6 pb-0">
                <h3 class="card-title">@lang('global.form') @lang('role.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'setting.role.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/setting/role.js') }}?{{ config('app.version') }}"></script>
@endpush

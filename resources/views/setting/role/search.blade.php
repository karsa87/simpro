<!-- form start -->
<form role="form" action="{{ route('setting.role.index') }}">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="name">@lang('role.label.name')</label>
                    <input type="text" name="search_name" class="form-control" placeholder="@lang('role.placeholder.name')" value="{{ request('search_name') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="slug">@lang('role.label.slug')</label>
                    <input type="text" name="search_slug" class="form-control" placeholder="@lang('role.placeholder.slug')" value="{{ request('search_slug') }}">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="_rl">@lang('role.label.level')</label>
                    <select name="_rl"  class="form-control" style="width: 100%;">
                        <option value=""></option>
                        @foreach ($role->getListRole() as $id => $name)
                        <option value="{{ $id }}" {{ is_numeric(request('_rl')) && request('_rl') == $id ? "selected"  : ""}}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('setting.role.index') }}" class="btn btn-info btn-loader">@lang('global.show_all')</a>
        <button type="submit" class="btn btn-success btn-loader">
            @lang('global.search')
        </button>
    </div>
</form>
<ol class="dd-list">
    @php
    $parents = $parents->sortBy('order');
    @endphp
    @foreach( $parents as $parent )
        <li class="dd-item dd3-item" data-id="{{ $parent->id }}">
            <div class="dd-handle dd3-handle"></div>
            <div class="dd3-content">
                <i class="{{ $parent->icon ? $parent->icon : 'mdi mdi-circle-medium' }}"></i> {{ $parent->name }}
                <div class="btn-group float-right m-0">
                    @hasPermission('setting.menu.edit')
                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('setting.menu.edit', $parent['id']) }}" class="btn btn-icon btn-xs btn-outline-warning mr-2 btn-edit-menu btn-loader" data-toggle="tooltip">
                        <i class="icon-md flaticon2-pen"></i>
                    </a>
                    @endhasPermission
                    
                    @hasPermission('setting.menu.destroy')
                    <button title="@lang('global.delete')" data-href="{{ route('setting.menu.destroy', $parent['id']) }}" class="btn btn-icon btn-xs btn-outline-danger mr-2 btn-delete btn-loader" data-toggle="tooltip">
                        <i class="icon-md flaticon2-trash"></i>
                    </button>
                    @endhasPermission
                </div>
            </div>
            @if($parent->submenu->count() > 0)
            @include($template . '.setting.menu.menu',['parents' => $parent->submenu])
            @endif
        </li>
    @endforeach
</ol>
@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::List Widget 9-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header align-items-center border-0 mt-4">
                <h3 class="card-title align-items-start flex-column">
                    <span class="font-weight-bolder text-dark">@lang('log_activity.title') @lang('project.title')</span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">@lang('global.list')</span>
                </h3>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-4">
                <!--begin::Search Form-->
                <div class="mb-7">
                    <form id="form-search-log" role="form" action="{{ route(request()->route()->getName()) }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control datetime-rangepicker" placeholder="@lang('log_activity.label.date')" name="_dt" value="{{ request('_dt', $date) }}" autocomplete="off" id="search_date" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="d-flex align-items-center">
                                            <label class="mr-3 mb-0 d-none d-md-block">@lang('log_activity.label.record_type'):</label>
                                            <select class="form-control" id="search_record_type" name="_rt">
                                                <option value="">@lang('global.all')</option>
                                                @foreach (trans('log_activity.list.transaction_type') as $id => $name)
                                                <option value="{{ $id }}" {{ is_numeric(request('_rt')) && $id == request('_rt') ? 'selected' : '' }}>{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->

                @php
                    $color_type = ["success", "info", "danger"];
                @endphp
                @foreach ($logs->groupBy('date') as $date => $log_days)
                <h3 class="card-title align-items-start flex-column mt-10 mb-5 p-5 bg-light-secondary">
                    <span class="font-weight-bolder text-dark">{{ date('d M Y', strtotime($date)) }}</span>
                </h3>
                <div class="timeline timeline-5 mt-3">
                    @foreach ($log_days->sortBy('log_datetime') as $log)
                        <!--begin::Item-->
                        <div class="timeline-item align-items-start">
                            <!--begin::Label-->
                            <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">
                                {{ date('H:i', strtotime($log->log_datetime)) }}
                            </div>
                            <!--end::Label-->
                            <!--begin::Badge-->
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-{{ $color_type[$log->transaction_type] }} icon-xl"></i>
                            </div>
                            <!--end::Badge-->
                            <!--begin::Text-->
                            <div class="font-weight-mormal font-size-lg timeline-content pl-3">
                                {{ $log->user->name }} <span class="label label-inline label-light-{{ $color_type[$log->transaction_type] }} font-weight-bold">@lang("log_activity.list.transaction_type.$log->transaction_type")</span>
                                <span class="text-muted">
                                    {{ $log->record_type_name }}
                                </span>
                                <span class="font-weight-bolder text-dark-75 font-size-sm">
                                    {{ $log->record->no_project }}
                                </span>
                            </div>
                            <!--end::Text-->
                        </div>
                        <!--end::Item-->
                    @endforeach
                </div>
                <!--end: Items-->
                @endforeach
            </div>
            <!--end: Card Body-->
        </div>
        <!--end: Card-->
        <!--end: List Widget 9-->
    </div>
</div>
@endsection

@push('js')
<script>
    $('#search_record_type').change(function() {
        $('#form-search-log').submit();
    });
    $('#search_date').change(function() {
        console.log('masuk');
        $('#form-search-log').submit();
    });
</script>
@endpush

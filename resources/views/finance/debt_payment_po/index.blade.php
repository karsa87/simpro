@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">@lang('project_debt_payment.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('finance.debt_payment_po.index.all')
                    <!--begin::Button-->
                    <a href="{{ route('finance.debt_payment_po.index.all') }}" class="btn btn-primary font-weight-bolder mr-5">
                        @lang('global.show_all')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                    {{--
                    @hasPermission('finance.debt_payment_po.create')
                    <!--begin::Button-->
                    <a href="{{ route('finance.debt_payment_po.create') }}" class="btn btn-success font-weight-bolder">
                        @lang('global.add')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                     --}}
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <!--begin::Search Form-->
                <div class="mb-7">
                    <form id="form-search-project-debt-payment" role="form" action="{{ route(request()->route()->getName()) }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" name="_k" value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->
                <!--end: Search Form-->
                <!--begin: Datatable-->
                <table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-5">
                                @sortablelink('no_project', trans('project.label.no_project'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-3">@lang('project_debt_payment.label.information')</th>
                            <th class="col-2 text-right">@lang('project_debt_payment.label.debt')</th>
                            <th class="col-2 text-center">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($projects as $project)
                        <tr>
                            <td>{{ $project->no_project_company }}</td>
                            <td class="text-wrap">{{ $project->information }}</td>
                            <td class="text-right">{{ $util->format_currency($project->shops->sum('total_amount') - $project->shops->sum('paid')) }}</td>
                            <td class="text-center">
                                @hasPermission('finance.debt_payment_po.create')
                                <a href="{{ route('finance.debt_payment_po.create', ['project_id'=>$project->id]) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                    <i class="icon-lg flaticon-price-tag"></i>
                                </a>
                                @endhasPermission('finance.debt_payment_po.create')
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($projects, 'form-search-project-debt-payment') !!}
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

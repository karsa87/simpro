@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.show_all') @lang('project_debt_payment.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('finance.debt_payment_po.index')
                    <!--begin::Button-->
                    <a href="{{ route('finance.debt_payment_po.index') }}" class="btn btn-primary font-weight-bolder">
                        @lang('global.back')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <!--begin::Search Form-->
                <div class="mb-7">
                    <form id="form-search-project-debt-payment" role="form" action="{{ route(request()->route()->getName()) }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" name="_k" value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->
                <!--end: Search Form-->
                <!--begin: Datatable-->
                <table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-2">
                                @sortablelink('no_transaction', trans('project_debt_payment.label.no_transaction'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @sortablelink('project.no_project', trans('project.label.no_project'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @sortablelink('projectShop.store_name', trans('project_shop.label.store_name'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @sortablelink('date', trans('project_debt_payment.label.date'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2 text-right">
                                @sortablelink('amount', trans('project_debt_payment.label.amount'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2 text-center">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($debtPayments as $debtPayment)

                        <tr>
                            <td>{{ $debtPayment->no_transaction }}</td>
                            <td>{{ $debtPayment->project->no_project }}</td>
                            <td>{{ $debtPayment->projectShop->store_name }}</td>
                            <td>{{ $util->formatDate($debtPayment->date) }}</td>
                            <td>{{ $util->format_currency($debtPayment->amount) }}</td>
                            <td>
                                @hasPermission('finance.debt_payment_po.edit')
                                <a title="@lang('global.edit')" href="{{ route('finance.debt_payment_po.edit', $debtPayment->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-pen"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('finance.debt_payment_po.show')
                                <a title="@lang('global.show_detail')" href="{{ route('finance.debt_payment_po.show', $debtPayment->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon-eye"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('finance.debt_payment_po.destroy')
                                <button title="@lang('global.delete')" data-href="{{ route('finance.debt_payment_po.destroy', $debtPayment->id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-trash"></i>
                                </button>
                                @endhasPermission
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($debtPayments, 'form-search-project-debt-payment') !!}
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

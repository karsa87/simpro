@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('finance.debt_payment_po.update', $model->id) }}">
    @csrf
    @method('put')
    <input type="hidden" name="id" value="{{ old('id', $model->id) }}" />

    @include('finance.debt_payment_po._form')
</form>
<!--end::Form-->
@endsection

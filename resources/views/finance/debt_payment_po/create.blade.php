@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('finance.debt_payment_po.store') }}">
    @csrf

    @include('finance.debt_payment_po._form')
</form>
<!--end::Form-->
@endsection

<div class="row">
    <div class="col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b" id="kt_page_sticky_card">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.form') @lang('project_debt_payment.title')</h3>
                </div>
                <div class="card-toolbar">
                    @if ($model->project_id)
                    <a href="{{ route('finance.debt_payment_po.index') }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    @else
                    <a href="{{ route('finance.debt_payment_po.index.all') }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    @endif
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_debt_payment.label.no_transaction')</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control @classInpError('no_transaction')" value="{{ old('no_transaction', $model->no_transaction) }}" name="no_transaction" readonly />

                        @inpSpanError(['column'=>'no_transaction'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_debt_payment.label.date')</label>
                    <div class="col-lg-7">
                        <div class="input-group date">
                            <input type="text" class="form-control datetimepicker @classInpError('date')" readonly="readonly" value="{{ date('Y-m-d H:i', strtotime(old('date', $model->date ?? now()))) }}" name="date"/>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar glyphicon-th"></i>
                                </span>
                            </div>

                            @inpSpanError(['column'=>'date'])
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_debt_payment.label.project_shop_id')</label>
                    <div class="col-lg-7">
                        <select name="project_shop_id" id="form-project_shop_id" class="form-control ajax-select2 @classInpError('project_shop_id')" data-url="{{ route('ajax.project_shop.debt', ['pid'=>$model->project_id]) }}" data-placeholder="@lang('global.select')" style="width: 100%;">
                            @if (old('project_shop_id') || $model->project_shop_id)
                                @php
                                    $model->project_shop_id = old('project_shop_id', $model->project_shop_id);
                                @endphp
                                <option value="{{ $model->projectShop->id }}" selected>{{ sprintf('%s - %s', $model->projectShop->project->no_project, $model->projectShop->store_name) }}</option>
                            @endif
                        </select>

                        @inpSpanError(['column'=>'project_shop_id'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_debt_payment.label.information')</label>
                    <div class="col-lg-7">
                        <textarea class="form-control @classInpError('information')" name="information" placeholder="@lang('project_debt_payment.placeholder.information')">{{ old('information', $model->information) }}</textarea>

                        @inpSpanError(['column'=>'information'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_debt_payment.label.amount')</label>
                    <div class="col-lg-7">
                        <input class="form-control format-currency @classInpError('amount')" type="text" name="amount" placeholder="@lang('project_debt_payment.placeholder.amount')" value="{{ old('amount', $model->amount) }}" />

                        @inpSpanError(['column'=>'amount'])
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    <div class="col-lg-6">
        @livewire('file.file-form', [
            'files' => old('files', $files)
        ])
    </div>
</div>

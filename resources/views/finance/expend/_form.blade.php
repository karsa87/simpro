<div class="row">
    <div class="col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.form') @lang('expend.title')</h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('finance.expend.index') }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('expend.label.no_transaction')</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control @classInpError('category_expend_id')" value="{{ old('no_transaction', $model->no_transaction) }}" name="no_transaction" />

                        @inpSpanError(['column'=>'employee_id'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('expend.label.project_id')</label>
                    <div class="col-lg-7">
                        <select name="project_id" id="form-project_id" class="form-control ajax-select2 @classInpError('project_id')" data-url="{{ route('ajax.project') }}" data-placeholder="@lang('global.select')" style="width: 100%;" {{ $model->project_id ? 'disabled' : '' }}>
                            @if (old('project_id') || $model->project_id)
                                @php
                                    $model->project_id = old('project_id', $model->project_id);
                                @endphp
                                <option value="{{ $model->project->id }}" selected>{{ $model->project->no_project_company }}</option>
                            @endif
                        </select>

                        @inpSpanError(['column'=>'project_id'])
                    </div>
                    @if ($model->project_id)
                        <input type="hidden" name="project_id" value="{{ $model->project_id }}" />
                    @endif
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('expend.label.category_expend_id')</label>
                    <div class="col-lg-7">
                        <select name="category_expend_id" id="form-category_expend_id" class="form-control ajax-select2 @classInpError('category_expend_id')" data-url="{{ route('ajax.category_expend') }}" data-placeholder="@lang('global.select')" style="width: 100%;">
                            @if (old('category_expend_id') || $model->category_expend_id)
                                @php
                                    $model->category_expend_id = old('category_expend_id', $model->category_expend_id);
                                @endphp
                                <option value="{{ $model->categoryExpend->id }}" selected>{{ $model->categoryExpend->name }}</option>
                            @endif
                        </select>

                        @inpSpanError(['column'=>'category_expend_id'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('expend.label.employee_id')</label>
                    <div class="col-lg-7">
                        <select name="employee_id" id="form-employee_id" class="form-control ajax-select2 @classInpError('employee_id')" data-url="{{ route('ajax.user_employee') }}" data-placeholder="@lang('global.select')" style="width: 100%;">
                            @if (old('employee_id') || $model->employee_id)
                                @php
                                    $model->employee_id = old('employee_id', $model->employee_id);
                                @endphp
                                <option value="{{ $model->employee->id }}" selected>{{ $model->employee->name }}</option>
                            @endif
                        </select>

                        @inpSpanError(['column'=>'employee_id'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('expend.label.date')</label>
                    <div class="col-lg-7">
                        <div class="input-group date">
                            <input type="text" class="form-control datetimepicker @classInpError('date')" readonly="readonly" value="{{ date('Y-m-d H:i', strtotime(old('date', $model->date ?? now()))) }}" name="date" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar glyphicon-th"></i>
                                </span>
                            </div>

                            @inpSpanError(['column'=>'date'])
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('expend.label.amount')</label>
                    <div class="col-lg-7">
                        <input class="form-control format-currency @classInpError('amount')" type="text" name="amount" value="{{ old('amount', $model->amount) }}" />

                        @inpSpanError(['column'=>'amount'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('expend.label.information')</label>
                    <div class="col-lg-7">
                        <textarea class="form-control @classInpError('information')" name="information" rows="3">{{ old('information', $model->information) }}</textarea>

                        @inpSpanError(['column'=>'information'])
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    <div class="col-lg-6">
        @livewire('file.file-form', [
            'files' => old('files', $files)
        ])
    </div>
</div>

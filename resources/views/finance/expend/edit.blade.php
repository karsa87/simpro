@extends('layouts.admin')

@section('content')
@if ($model->project)
<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <div class="card-body">
                @include('project.project.section.project_detail', ['model' => $model->project])
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>
@endif

<form method="POST" action="{{ route('finance.expend.update', $model->id) }}">
    @csrf
    @method('put')
    <input type="hidden" name="id" value="{{ old('id', $model->id) }}" />

    @include('finance.expend._form')
</form>
<!--end::Form-->
@endsection

@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">@lang('expend.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('finance.expend.create')
                    <!--begin::Button-->
                    <a href="{{ route('finance.expend.create') }}" class="btn btn-primary font-weight-bolder">
                        @lang('global.add')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <!--begin::Search Form-->
                <div class="mb-7">
                    <form id="form-search-expend" role="form" action="{{ route('finance.expend.index') }}">
                    <div class="row align-items-center">
                        <div class="col-lg-12">
                            <div class="row align-items-center">
                                <div class="col-md-4 my-2 my-md-0">
                                    <div class="input-icon">
                                        <input type="text" class="form-control" placeholder="Search..." name="_k" value="{{ request('_k') }}" />
                                        <span>
                                            <i class="flaticon2-search-1 text-muted"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <!--end::Search Form-->
                <!--end: Search Form-->
                <!--begin: Datatable-->
                <table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-2 text-center">
                                @sortablelink('no_transaction', trans('expend.label.no_transaction'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-3">
                                @sortablelink('date', trans('expend.label.date'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-3">@lang('expend.label.information')</th>
                            <th class="col-2 text-right">
                                @sortablelink('amount', trans('expend.label.amount'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2 text-center">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($expends as $expend)
                        <tr>
                            <td>{{ $expend->no_transaction }}</td>
                            <td>{{ $util->formatDate($expend->date) }}</td>
                            <td class="text-wrap">
                                <p class="p-0">
                                    <strong>@lang('expend.label.employee_id'):</strong> {{ $expend->employee->name }}
                                </p>

                                @if ($expend->categoryExpend)
                                <p class="p-0">
                                    <strong>@lang('expend.label.category_expend_id'):</strong> {{ $expend->categoryExpend->name }}
                                </p>
                                @endif

                                @if ($expend->project)
                                <p class="p-0">
                                    <strong>@lang('expend.label.project_id'):</strong> {{ $expend->project->no_project_company }}
                                </p>
                                @endif
                            </td>
                            <td class="text-right">{{ $util->format_currency($expend->amount) }}</td>
                            <td class="text-center">
                                @hasPermission('finance.expend.edit')
                                <a title="@lang('global.edit')" href="{{ route('finance.expend.edit', $expend->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-pen"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('finance.expend.show')
                                <a title="@lang('global.show_detail')" href="{{ route('finance.expend.show', $expend->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon-eye"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('finance.expend.destroy')
                                <button title="@lang('global.delete')" data-href="{{ route('finance.expend.destroy', $expend->id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-trash"></i>
                                </button>
                                @endhasPermission
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($expends, 'form-search-expend') !!}
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

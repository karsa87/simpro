@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('finance.incentive.store') }}">
    @csrf

    @include('finance.incentive._form')
</form>
<!--end::Form-->
@endsection

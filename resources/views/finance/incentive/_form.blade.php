<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.form') @lang('incentive.title')</h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('finance.incentive.index') }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('incentive.label.no_transaction')</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control @classInpError('no_transaction')" value="{{ old('no_transaction', $model->no_transaction) }}" name="no_transaction" />

                        @inpSpanError(['column'=>'no_transaction'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('incentive.label.investor')</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control @classInpError('investor')" value="{{ old('investor', $model->investor) }}" name="investor" />

                        @inpSpanError(['column'=>'investor'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('incentive.label.information')</label>
                    <div class="col-lg-7">
                        <textarea class="form-control @classInpError('information')" name="information" rows="3">{{ old('information', $model->information) }}</textarea>

                        @inpSpanError(['column'=>'information'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('incentive.label.date')</label>
                    <div class="col-lg-7">
                        <div class="input-group date">
                            <input type="text" class="form-control datetimepicker @classInpError('date')" readonly="readonly" value="{{ date('Y-m-d H:i', strtotime(old('date', $model->date ?? now()))) }}" name="date" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar glyphicon-th"></i>
                                </span>
                            </div>

                            @inpSpanError(['column'=>'date'])
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('incentive.label.amount')</label>
                    <div class="col-lg-7">
                        <input class="form-control format-currency @classInpError('amount')" type="text" name="amount" value="{{ old('amount', $model->amount) }}" />

                        @inpSpanError(['column'=>'amount'])
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>

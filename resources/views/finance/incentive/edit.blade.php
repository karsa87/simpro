@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('finance.incentive.update', $model->id) }}">
    @csrf
    @method('put')
    <input type="hidden" name="id" value="{{ old('id', $model->id) }}" />

    @include('finance.incentive._form')
</form>
<!--end::Form-->
@endsection

@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('finance.category_expend.update', $model->id) }}">
    @csrf
    @method('put')
    <input type="hidden" name="id" value="{{ old('id', $model->id) }}" />

    @include('finance.category_expend._form')
</form>
<!--end::Form-->
@endsection

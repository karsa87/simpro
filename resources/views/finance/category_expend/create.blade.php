@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('finance.category_expend.store') }}">
    @csrf

    @include('finance.category_expend._form')
</form>
<!--end::Form-->
@endsection

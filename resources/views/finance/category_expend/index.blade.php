@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Kategori Pengeluaran</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('finance.category_expend.create')
                    <!--begin::Button-->
                    <a href="{{ route('finance.category_expend.create') }}" class="btn btn-primary font-weight-bolder">
                        @lang('global.add')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <div class="mb-7">
                    <form id="form-search-category-expend" role="form" action="{{ route('finance.category_expend.index') }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search.." name="_k" value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->

                <!--begin: Datatable-->
                <table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-6">
                                @sortablelink('name', trans('category_expend.label.name'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-4 text-center">
                                @sortablelink('statsu', trans('category_expend.label.status'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categoryExpends as $category)
                        <tr>
                            <td>{{ $category->name }}</td>
                            <td class="text-center">
                                <span class="label font-weight-bold label-lg {{ $category->isActive() ? 'label-light-success' : 'label-light-danger' }} label-inline">@lang("category_expend.list.status.$category->status")</span>
                            </td>
                            <td>
                                @hasPermission('finance.category_expend.edit')
                                <a title="@lang('global.edit')" href="{{ route('finance.category_expend.edit', $category->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-pen"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('finance.category_expend.show')
                                <a title="@lang('global.show_detail')" href="{{ route('finance.category_expend.show', $category->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon-eye"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('finance.category_expend.show')
                                <button title="@lang('global.delete')" data-href="{{ route('finance.category_expend.destroy', $category->id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-trash"></i>
                                </button>
                                @endhasPermission
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($categoryExpends, 'form-search-category-expend') !!}
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection


<div class="row">
    <div class="col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.form') @lang('category_expend.title')</h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('finance.category_expend.index') }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('category_expend.label.name')</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control @classInpError('name')" placeholder="@lang('category_expend.placeholder.name')" name="name" value="{{ old('name', $model->name) }}" />

                        <span class="form-text text-muted">@lang('category_expend.message.please_name')</span>
                        @inpSpanError(['column'=>'name'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label text-lg-right">@lang('category_expend.label.status')</label>
                    <div class="col-3">
                        <span class="switch switch-outline switch-icon switch-success">
                            <label>
                                <input type="checkbox" name="status" class="@classInpError('status')" {{ in_array(old('status', $model->status ?? 1), [1, 'on']) ? 'checked' : ''  }} />
                                <span></span>
                            </label>
                            @inpSpanError(['column'=>'status'])
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom gutter-b">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">File</h3>
                </div>
                <div class="card-toolbar">
                    <button type="button" class="btn btn-primary font-weight-bolder" wire:click="addFile">
                        Add File
                    </button>
                </div>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th style="width: 5%;" class="text-center">@lang('global.no')</th>
                            <th style="width: 35%;">@lang('file.label.name')</th>
                            <th style="width: 35%;">@lang('file.label.information')</th>
                            <th style="width: 15%;">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($files as $i => $fl)
                        <tr>
                            <td class="text-center">
                                {{ $i+1 }}

                                <input type="hidden" name="files[{{$i}}][id]" wire:model="files.{{$i}}.id">
                                <input type="hidden" name="files[{{$i}}][path]" wire:model="files.{{$i}}.path" data-index="{{$i}}">
                                <input type="hidden" name="files[{{$i}}][url]" wire:model="files.{{$i}}.url" />
                                <input type="hidden" name="files[{{$i}}][name]" wire:model="files.{{$i}}.name" />
                                <input type="hidden" name="files[{{$i}}][information]" wire:model="files.{{$i}}.information" />
                            </td>
                            <td>{{ $fl['name'] }}</td>
                            <td>{{ $fl['information'] }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-icon btn-outline-warning form-control" wire:click="$emit('triggerEditFile',{{ $i }})">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-icon btn-outline-danger form-control" wire:click="$emit('triggerDeleteFile',{{ $i }})">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div id="modal-form-edit-file" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-form-edit-fileTitle"  data-backdrop="static" data-keyboard="false" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-form-edit-fileTitle">@lang('global.form') @lang('file.title')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        @php
                            $i = $file['index'];
                        @endphp
                        <div class="col-sm-12 col-lg-12 col-xl-12">
                            <input type="hidden" id="form-id" name="form_id" value="{{ $file['id'] }}">
                            <input type="hidden" id="form-index" name="form_index" value="{{ $file['index'] }}">
                            <div class="form-group">
                                {{ $file['path'] ?? '' }}
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="dropzone dropzone-default dropzone-success kt_dropzone" data-index="{{$i}}">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                            <span class="dropzone-msg-desc">Only image, pdf and psd files are allowed for upload</span>
                                        </div>
                                    </div>

                                    <input type="hidden" id="form-path" name="form_path" value="{{ $file['path'] }}" data-index="{{$i}}">
                                </div>
                            </div>
                            <h3 class="card-label text-center">@lang('global.or')</h3>
                            <div class="form-group">
                                <label>@lang('file.label.url')</label>
                                <input type="text" class="form-control" placeholder="@lang('file.placeholder.url')" id="form-url" name="form_url" value="{{ $file['url'] }}" />
                            </div>
                            <div class="form-group">
                                <label>@lang('file.label.name')</label>
                                <input type="text" class="form-control" placeholder="@lang('file.placeholder.name')" id="form-name" name="form_name" value="{{ $file['name'] }}" />
                            </div>
                            <div class="form-group">
                                <label>@lang('file.label.information')</label>
                                <textarea class="form-control" placeholder="@lang('file.placeholder.information')" id="form-information" name="form_information" rows="3">{{ $file['information'] }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" wire:click="$emit('cancelForm',{{ $i }})">@lang('global.cancel')</button>
                        <button type="button" class="btn btn-primary" wire:click="$emit('submitForm',{{ $i }})">@lang('global.upload')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    "use strict";
    // Class definition

    var KTDropzoneDemo = function () {
        // Private functions
        var demo1 = function () {
            // file type validation
            var thumbnail = '{{ asset("assets/images/file-manager.jpg") }}'
            $(".kt_dropzone").each(function(i) {
                let dropzoneControl = $(this)[0].dropzone;
                var index = $(this).data('index');
                var inp_upload_id = `#form-path`;
                var img_path = $(inp_upload_id).val();
                if (dropzoneControl) {
                    dropzoneControl.destroy();
                }

                $(this).dropzone({
                    url: "{{ route('upload.file') }}", // Set the url for your upload script location
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 10, // MB
                    addRemoveLinks: true,
                    acceptedFiles: "image/*,application/pdf,.psd",
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    init: function() { /* event listeners for removed files from dropzone*/
                        if(img_path !== "" && img_path !== undefined && img_path !== null){
                            var dropzone = this;
                            var mockFile = { name: CONFIG.storage_url + img_path, size: 32000, src:CONFIG.storage_url + img_path };
                            dropzone.options.addedfile.call(dropzone, mockFile);
                            if(thumbnail !== undefined) {
                                dropzone.options.thumbnail.call(dropzone, mockFile, thumbnail);
                            } else {
                                dropzone.options.thumbnail.call(dropzone, mockFile, mockFile.src);
                            }
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');
                        }
                        this.on('processing', function(file){
                            showAlert("Processing upload file", "info");
                            return true;
                        });
                        this.on('queuecomplete', function(){
                            return true;
                        });
                        this.on('success',function(file, result){
                            // changing src of preview element
                            if(thumbnail !== undefined) {
                                file.previewElement.querySelector("img").src = thumbnail;
                            }

                            $(inp_upload_id).val(result.path);
                            // Livewire.emit('changePath', $(inp_upload_id).data('index'), result.path);

                            Swal.fire({
                                title: "Upload",
                                text: "Complete upload file",
                                icon: 'success',
                            });

                            return true;
                        });

                        this.on("error", function(file, message){
                            Swal.fire({
                                title: "Upload",
                                text: message,
                                icon: 'error',
                            });
                        });

                        this.on('reloadFiles', function() {
                            image_uploaded = [];
                            var img_path = $(inp_upload_id).val();
                            if(img_path !== "" && img_path !== undefined && img_path !== null){
                                var dropzone = this;

                                dropzone.files.forEach(function(file) {
                                    file.previewElement.remove();
                                });
                                dropzone.removeAllFiles();
                                $('div.dz-success').remove();

                                var mockFile = { name: CONFIG.storage_url + img_path, size: 32000, src:CONFIG.storage_url + img_path };
                                dropzone.options.addedfile.call(dropzone, mockFile);
                                if(thumbnail !== undefined) {
                                    dropzone.options.thumbnail.call(dropzone, mockFile, thumbnail);
                                } else {
                                    dropzone.options.thumbnail.call(dropzone, mockFile, mockFile.src);
                                }
                                mockFile.previewElement.classList.add('dz-success');
                                mockFile.previewElement.classList.add('dz-complete');
                            }
                            $(inp_upload_id).val( img_path );
                        });
                    }
                });
            });
        }

        return {
            // public functions
            init: function() {
                demo1();
            }
        };
    }();

    document.addEventListener('DOMContentLoaded', function () {
        Livewire.on('triggerDeleteFile', detailId => {
            const swal_confirm = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success ml-1',
                  cancelButton: 'btn btn-danger mr-1'
                },
                buttonsStyling: false
            })
            swal_confirm.fire({
                title: '{!! trans("file.message.sure") !!}',
                text: '{!! trans("file.message.deleted_file") !!}',
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                    @this.call('removeFile', detailId)

                }
            });
        })

        Livewire.on('cancelForm', detailId => {
            const swal_confirm = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success ml-1',
                  cancelButton: 'btn btn-danger mr-1'
                },
                buttonsStyling: false
            })
            swal_confirm.fire({
                title: '{!! trans("file.message.sure") !!}',
                text: '{!! trans("file.message.close_form") !!}',
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                    @this.call('removeNewFile', detailId)
                    $('#modal-form-edit-file').modal('hide');
                }
            });
        })

        Livewire.on('triggerEditFile', detailId => {
            @this.call('editFile', detailId)
        })

        Livewire.on('submitForm', detailId => {
            var object = {};
            object['index'] = $('#form-index').val();
            object['id'] = $('#form-id').val();
            object['name'] = $('#form-name').val();
            object['url'] = $('#form-url').val();
            object['path'] = $('#form-path').val();
            object['information'] = $('#form-information').val();

            var errors = [];
            if (object.name == '' || object.name == undefined || object.name == null) {
                errors.push("Name required");
            }

            if ((object.path == '' || object.path == undefined || object.path == null)
                && (object.url == '' || object.url == undefined || object.url == null)) {
                errors.push("Upload file / url");
            }

            if (errors.length > 0) {
                showAlert(errors.join('<br> '), "error");
            } else {
                var json = JSON.stringify(object);
                @this.call('submitForm', json)
            }
        })

        KTDropzoneDemo.init();
    });
    document.addEventListener('livewire:load', function (event) {
        Livewire.hook('message.processed', () => {
            KTDropzoneDemo.init();
        });
    });
    window.addEventListener('msgFileNotFound', event => {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: event.detail.message,
        })
    })
    window.addEventListener('showModalEdit', event => {
        $('#modal-form-edit-file').modal('show');
    })
    window.addEventListener('hideModalEdit', event => {
        $('#form-index').val("");
        $('#form-id').val("");
        $('#form-name').val("");
        $('#form-url').val("");
        $('#form-path').val("");
        $('#form-information').val("");
        $('#modal-form-edit-file').modal('hide');
    })
</script>
@endpush

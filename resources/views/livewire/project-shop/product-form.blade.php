@inject('util', 'App\Util\Helpers\Util')

<div class="card card-custom gutter-b">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label">@lang('project_shop.label.product_materials')</h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="card-body">
        <table class="w-100 table table-head-custom table-head-bg table-borderless table-vertical-center">
            <thead>
                <tr>
                    <th class="col-3">@lang('project_shop.label.product_materials')</th>
                    <th class="col-2">@lang('project_shop.label.information')</th>
                    <th class="col-2">@lang('project_shop.label.qty')</th>
                    <th class="col-2">@lang('project_shop.label.price')</th>
                    <th class="col-2">@lang('project_shop.label.total_price')</th>
                    <th class="col-1 text-center">
                        <button type="button" class="btn btn-sm btn-icon btn-primary font-weight-bolder text-sm" wire:click="addProduct">
                            <i class="icon-md flaticon2-plus"></i>
                        </button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($details as $i => $detail)
                <tr>
                    <td>
                        <input type="hidden" wire:model="details.{{ $i }}.id" name="details[{{ $i }}][id]" />
                        <input type="text" class="form-control" wire:model="details.{{ $i }}.product_name" name="details[{{ $i }}][product_name]" />
                    </td>
                    <td>
                        <textarea class="form-control" wire:model="details.{{ $i }}.information" name="details[{{ $i }}][information]"></textarea>
                    </td>
                    <td class="text-center">
                        <input type="number" class="form-control format-number-self" wire:model="details.{{ $i }}.qty" name="details[{{ $i }}][qty]" min="1" data-index="{{ $i }}" data-column="qty" wire:change="calculateTotalPrice({{ $i }}, 'qty', this.value)" />
                    </td>
                    <td>
                        <input class="form-control format-number-self" type="text" wire:model="details.{{ $i }}.price" name="details[{{ $i }}][price]" data-index="{{ $i }}" data-column="price" />
                    </td>
                    <td>
                        <span class="text-dark-75 font-weight-bolder text-hover-primary mt-1 font-size-lg">{{ $util->format_currency($detail['total_price']) }}</span>
                        <input type="hidden" wire:model="details.{{ $i }}.total_price" name="details[{{ $i }}][total_price]" />
                    </td>
                    <td class="text-center">
                        <button type="button" class="btn btn-icon btn-outline-secondary mr-2" wire:click="$emit('triggerDelete',{{ $i }})">
                            <i class="icon-md flaticon2-trash"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@push('js')
<script>
    document.addEventListener('DOMContentLoaded', function () {
        Livewire.on('triggerDelete', detailId => {
            const swal_confirm = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success ml-1',
                  cancelButton: 'btn btn-danger mr-1'
                },
                buttonsStyling: false
            })
            swal_confirm.fire({
                title: '{!! trans("project.message.sure") !!}',
                text: '{!! trans("project.message.deleted_job") !!}',
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                    @this.call('removeProduct', detailId)

                }
            });
        });

        $(".format-number-self").inputmask({ alias : "currency", prefix: '', digits: 2 });
        $('.format-number-self').change(formatNumber);
    });

    document.addEventListener('livewire:load', function (event) {
        Livewire.hook('message.processed', () => {
            $(".format-number-self").inputmask({ alias : "currency", prefix: '', digits: 2 });
            $('.format-number-self').change(formatNumber);
        });
    });

    function formatNumber(){
        if ($(this).val() != undefined && $(this).val() != '') {
            var value = $(this).val();
            Livewire.emit('calculateTotalPrice', $(this).data('index'), $(this).data('column'), value);
        }
    };
</script>
@endpush

<table class="table">
    <thead>
        <tr>
            <th class="col-lg-10">@lang('project.label.jobs')</th>
            <th class="col-lg-2 text-center">
                <button type="button" class="btn btn-sm btn-icon btn-primary font-weight-bolder text-sm" wire:click="addJob" {{ empty($vendor_id) ? 'disabled' : '' }}>
                    <i class="icon-md flaticon2-plus"></i>
                </button>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($jobs as $i => $job)
        <tr>
            <td>
                <select wire:model="jobs.{{ $i }}.project_job_id" name="jobs[{{ $i }}][project_job_id]" class="form-control text-sm col-sm-12 col-lg-6">
                    @foreach ($projectJobs as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </td>
            <td class="text-center">
                <button type="button" class="btn btn-icon btn-outline-secondary mr-2" wire:click="$emit('triggerDelete',{{ $i }})">
                    <i class="icon-md flaticon2-trash"></i>
                </button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@push('js')
<script>
    document.addEventListener('DOMContentLoaded', function () {
        Livewire.on('triggerDelete', detailId => {
            const swal_confirm = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success ml-1',
                  cancelButton: 'btn btn-danger mr-1'
                },
                buttonsStyling: false
            })
            swal_confirm.fire({
                title: '{!! trans("project.message.sure") !!}',
                text: '{!! trans("project.message.deleted_job") !!}',
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                    @this.call('removeJob', detailId)

                }
            });
        })
    });
</script>
@endpush

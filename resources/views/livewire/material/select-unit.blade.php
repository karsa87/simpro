

<div class="card card-custom gutter-b">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label">@lang('material.label.unit')</h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="card-body">
        <table class="w-100 table table-head-custom table-head-bg table-borderless table-vertical-center">
            <thead>
                <tr>
                    <th class="w-20">@lang('material.label.qty')</th>
                    <th class="w-50">@lang('material.label.unit')</th>
                    <th class="w-20">@lang('material.label.price')</th>
                    <th class="w-10 text-center">
                        <button type="button" class="btn btn-sm btn-icon btn-primary font-weight-bolder text-sm" wire:click="addUnit">
                            <i class="icon-md flaticon2-plus"></i>
                        </button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($units as $i => $unit)
                <tr>
                    <td>
                        <input class="form-control format-number-self" type="text" wire:model="units.{{ $i }}.qty" name="units[{{ $i }}][qty]" data-index="{{ $i }}" data-column="qty" readonly />
                    </td>
                    <td>
                        <select wire:model="units.{{ $i }}.unit_id" name="units[{{ $i }}][unit_id]" class="form-control text-sm col-sm-12 col-lg-6">
                            @foreach ($listUnits as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input class="form-control format-number-self" type="text" wire:model="units.{{ $i }}.price" name="units[{{ $i }}][price]" data-index="{{ $i }}" data-column="price" />
                    </td>
                    <td class="text-center">
                        <button type="button" class="btn btn-icon btn-outline-secondary mr-2" wire:click="$emit('triggerDeleteUnit',{{ $i }})">
                            <i class="icon-md flaticon2-trash"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@push('js')
<script>
    document.addEventListener('DOMContentLoaded', function () {
        Livewire.on('triggerDeleteUnit', detailId => {
            const swal_confirm = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success ml-1',
                  cancelButton: 'btn btn-danger mr-1'
                },
                buttonsStyling: false
            })
            swal_confirm.fire({
                title: '{!! trans("file.message.sure") !!}',
                text: '{!! trans("file.message.deleted_file") !!}',
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                    @this.call('removeUnit', detailId)

                }
            });
        });

        $(".format-number-self").inputmask({ alias : "currency", prefix: '', digits: 2 });
        $('.format-number-self').change(formatNumber);
    });

    document.addEventListener('livewire:load', function (event) {
        Livewire.hook('message.processed', () => {
            $(".format-number-self").inputmask({ alias : "currency", prefix: '', digits: 2 });
            $('.format-number-self').change(formatNumber);
        });
    });

    function formatNumber(){
        if ($(this).val() != undefined && $(this).val() != '') {
            var value = $(this).val();
            Livewire.emit('calculateTotalPrice', $(this).data('index'), $(this).data('column'), value);
        }
    };

    window.addEventListener('msgError', event => {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: event.detail.message,
        })
    })
</script>
@endpush

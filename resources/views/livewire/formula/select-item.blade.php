@inject('util', 'App\Util\Helpers\Util')

<div class="card card-custom gutter-b">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label">@lang('formula.label.item')</h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="card-body">
        <table class="w-100 table table-head-custom table-head-bg table-borderless table-vertical-center">
            <thead>
                <tr>
                    <th class="col-sm-2">@lang('formula.label.coefficient')</th>
                    <th class="col-sm-1">@lang('formula.label.unit_id')</th>
                    <th class="col-sm-3">@lang('formula.label.material_id')</th>
                    <th class="col-sm-2">@lang('formula.label.unit_price')</th>
                    <th class="col-sm-2">@lang('formula.label.amount')</th>
                    <th class="col-sm-1 text-center">

                        <button type="button" class="btn btn-sm btn-icon btn-primary font-weight-bolder text-sm" wire:click="openModal('modal-add-material', 'material')"><i class="icon-md flaticon2-plus"></i></button>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($items as $i => $item)
                <tr>
                    <td>
                        <input class="form-control format-number-self-coefficient" type="text" wire:model="items.{{ $i }}.coefficient" name="items[{{ $i }}][coefficient]" data-index="{{ $i }}" data-column="coefficient" />
                    </td>
                    <td>
                        <input class="form-control" type="hidden" wire:model="items.{{ $i }}.id" name="items[{{ $i }}][id]" />
                        <input class="form-control" type="hidden" wire:model="items.{{ $i }}.unit_id" name="items[{{ $i }}][unit_id]" />
                        {{ $item['unit_name'] }}
                    </td>
                    <td>
                        <input class="form-control" type="hidden" wire:model="items.{{ $i }}.material_id" name="items[{{ $i }}][material_id]" />
                        {{ $item['material_name'] }}
                    </td>
                    <td class="text-right">
                        {{ $util->format_currency($item['unit_price']) }}
                    </td>
                    <td class="text-right">
                        <input class="form-control" type="hidden" wire:model="items.{{ $i }}.amount" name="items[{{ $i }}][amount]" />
                        {{ $util->format_currency($item['amount']) }}
                    </td>
                    <td class="text-center">
                        <button type="button" class="btn btn-icon btn-outline-secondary mr-2" wire:click="$emit('triggerDeleteItem',{{ $i }})">
                            <i class="icon-md flaticon2-trash"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th class="text-right" colspan="4">@lang('formula.label.subtotal')</th>
                    <th class="text-right">{{ $util->format_currency($subtotal) }}</th>
                    <th></th>
                </tr>
                <tr>
                    <th class="text-right" colspan="4">@lang('formula.label.tax')</th>
                    <th class="text-right">{{ $util->format_currency($subtotal_tax) }}</th>
                    <th></th>
                </tr>
                <tr>
                    <th class="text-right" colspan="4">@lang('formula.label.total')</th>
                    <th class="text-right">{{ $util->format_currency($total) }}</th>
                    <th></th>
                </tr>
                <tr>
                    <th class="text-right" colspan="4">@lang('formula.label.rounding')</th>
                    <th class="text-right">{{ $util->format_currency($total_round) }}</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="modal fade {{ $show_modal['material'] ? 'show' : '' }}" id="modal-add-material" aria-hidden="true"
        data-keyboard="false" data-backdrop="static"
        style="display: {{ $show_modal['material'] ? 'block' : 'none' }};">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">@lang('material.title')</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-inline">
                        {{-- <a href="{{ route('master.material.create') }}" class="btn btn-primary m-2 rounded-lg" target="_blank">@lang('global.add'){{ trans('formula.label.material_id') }}</a> --}}

                        <div class="input-group">
                            <input type="text" name="search_kw" class="form-control float-right" placeholder="@lang('global.keyword')" wire:model="search_material.keyword">
                        </div>

                        <div class="input-group ml-2">
                            <select wire:model="search_material.unit_id" name="search_unit_id" class="form-control" placeholder="Cari Unit">
                                <option value="">ALL Unit</option>
                                @foreach ($listUnits as $id => $name)
                                    <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="button" class="btn btn-primary m-2 rounded-lg" target="_blank" wire:click="searchMaterial()">@lang('global.search')</button>
                    </div>

                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Qty</th>
                                <th>Unit</th>
                                <th>Material</th>
                                <th>Unit Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listMaterials as $material)
                                @foreach ($material->units as $unit)
                                    <tr style="cursor: pointer;" wire:click="chooseMaterial({{$material->id}},{{ $unit->id }})" wire:key="material-{{$material->id}}-{{$unit->id}}">
                                        <td>{{ optional($unit->pivot)->qty }}</td>
                                        <td>{{ optional($unit)->name }}</td>
                                        <td>{{ $material->name }}</td>
                                        <td>{{ $util->format_currency(optional($unit->pivot)->price) }}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    {{-- <button type="button" class="btn btn-primary" data-dismiss="modal" wire:click="calculateTotalAll()">Save changes</button> --}}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>

@push('js')
<script>
    document.addEventListener('DOMContentLoaded', function () {
        Livewire.on('triggerDeleteItem', detailId => {
            const swal_confirm = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success ml-1',
                  cancelButton: 'btn btn-danger mr-1'
                },
                buttonsStyling: false
            })
            swal_confirm.fire({
                title: '{!! trans("file.message.sure") !!}',
                text: '{!! trans("file.message.deleted_file") !!}',
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                    @this.call('removeItem', detailId)

                }
            });
        });

        $(".format-number-self-coefficient").inputmask({ alias : "currency", prefix: '', digits: 3 });
        $('.format-number-self-coefficient').change(formatNumber);
    });

    document.addEventListener('livewire:load', function (event) {
        Livewire.hook('message.processed', () => {
            $(".format-number-self-coefficient").inputmask({ alias : "currency", prefix: '', digits: 3 });
            $('.format-number-self-coefficient').change(formatNumber);
        });
    });

    function formatNumber(){
        if ($(this).val() != undefined && $(this).val() != '') {
            var value = $(this).val();
            Livewire.emit('setCoefficientValue', $(this).data('index'), value);
        }
    };

    window.addEventListener('msgError', event => {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: event.detail.message,
        })
    })

    window.addEventListener('openModalJs', event => {
        $(`#${event.detail.modal_id}`).modal('show');
    })

    window.addEventListener('closeModalJs', event => {
        $(`#${event.detail.modal_id}`).modal('hide');
    })
</script>
@endpush

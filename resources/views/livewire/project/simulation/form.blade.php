@inject('util', 'App\Util\Helpers\Util')

<div>
    <div class="row">
        <div class="col-lg-12">
            <table class="w-100 table table-head-custom table-head-bg table-borderless table-vertical-center">
                <thead>
                    <tr>
                        <th class="col-sm-2">@lang('formula.label.qty')</th>
                        <th class="col-sm-2">@lang('formula.title') @lang('formula.label.name')</th>
                        <th class="col-sm-3">@lang('formula.label.material_id')</th>
                        <th class="col-sm-2 text-right">@lang('formula.label.amount')</th>
                        <th class="col-sm-1 text-center">
                            <button type="button" class="btn btn-sm btn-icon btn-primary font-weight-bolder text-sm" wire:click="openModal('modal-add-material', 'material')"><i class="icon-md flaticon2-plus"></i></button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($items as $i => $item)
                        <tr wire:key="formula-{{$i}}">
                            <td class="align-top">
                                <input class="form-control format-number-self-coefficient" type="text" wire:model="items.{{ $i }}.coefficient" data-index="{{ $i }}" data-column="coefficient" />
                            </td>
                            <td class="align-top">
                                {{ $item['name'] }}
                                <span class="text-gray-400 fw-semibold d-block fs-7">{{ $item['code'] }}</span>
                            </td>
                            <td class="align-top">
                                <table border="0" width="100%">
                                    <tr>
                                        <th>@lang('formula.label.material_id')</th>
                                        <th>@lang('formula.label.coefficient')</th>
                                        <th>@lang('formula.label.unit_id')</th>
                                    </tr>
                                    @foreach ($item['materials'] as $material)
                                        <tr>
                                            <td>{{ $material['material'] }}</td>
                                            <td>{{ $material['coefficient'] }}</td>
                                            <td>{{ $material['unit'] }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>
                            <td class="text-right align-top">{{ $util->format_currency($item['total_round']) }}</td>
                            <td class="text-center align-top">
                                <button type="button" class="btn btn-icon btn-outline-secondary mr-2" wire:click="$emit('triggerDeleteItem',{{ $i }})">
                                    <i class="icon-md flaticon2-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th class="text-right" colspan="3">@lang('formula.label.subtotal')</th>
                        <th class="text-right">{{ $util->format_currency($subtotal) }}</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="modal fade {{ $show_modal['formula'] ? 'show' : '' }}" id="modal-add-formula" aria-hidden="true"
        data-keyboard="false" data-backdrop="static"
        style="display: {{ $show_modal['formula'] ? 'block' : 'none' }};"
        tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">@lang('formula.title')</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-inline">
                        <div class="input-group">
                            <input type="text" name="search_kw" class="form-control float-right" placeholder="@lang('global.keyword')" wire:model="search_formula.keyword">
                        </div>

                        <button type="button" class="btn btn-primary m-2 rounded-lg" target="_blank" wire:click="searchFormula()">@lang('global.search')</button>
                    </div>

                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th class="col-2 text-center">@lang('formula.label.code')</th>
                                <th class="col-7 text-center">@lang('formula.label.name')</th>
                                <th class="col-3 text-right">@lang('formula.label.total_amount')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listFormulas as $formula)
                            <tr style="cursor: pointer;" wire:click="chooseFormula({{$formula->id}})" wire:key="formula-{{$formula->id}}">
                                <td>{{ $formula->code }}</td>
                                <td>{{ $formula->name }}</td>
                                <td class="text-right">{{ $util->format_currency($formula->total_round) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    {{-- <button type="button" class="btn btn-primary" data-dismiss="modal" wire:click="calculateTotalAll()">Save changes</button> --}}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>


@push('js')
<script>
    document.addEventListener('DOMContentLoaded', function () {
        Livewire.on('triggerDeleteItem', detailId => {
            const swal_confirm = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success ml-1',
                  cancelButton: 'btn btn-danger mr-1'
                },
                buttonsStyling: false
            })
            swal_confirm.fire({
                title: '{!! trans("file.message.sure") !!}',
                text: '{!! trans("file.message.deleted_file") !!}',
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                    @this.call('removeItem', detailId)

                }
            });
        });

        $(".format-number-self-coefficient").inputmask({ alias : "currency", prefix: '', digits: 3 });
        $('.format-number-self-coefficient').change(formatNumber);
    });

    document.addEventListener('livewire:load', function (event) {
        Livewire.hook('message.processed', () => {
            $(".format-number-self-coefficient").inputmask({ alias : "currency", prefix: '', digits: 3 });
            $('.format-number-self-coefficient').change(formatNumber);
        });
    });

    function formatNumber(){
        if ($(this).val() != undefined && $(this).val() != '') {
            var value = $(this).val();
            Livewire.emit('setCoefficientValue', $(this).data('index'), value);
        }
    };

    window.addEventListener('msgError', event => {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: event.detail.message,
        })
    })

    window.addEventListener('openModalJs', event => {
        $(`#${event.detail.modal_id}`).modal('show');
    })

    window.addEventListener('closeModalJs', event => {
        $(`#${event.detail.modal_id}`).modal('hide');
    })
</script>
@endpush

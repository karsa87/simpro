<table class="table">
    <thead>
        <tr>
            <th class="w-50">@lang('saldo_employee.label.employee_id')</th>
            <th class="w-30">@lang('user.label.position_id')</th>
            <th class="w-20 text-center">
                <button type="button" class="btn btn-sm btn-icon btn-primary font-weight-bolder text-sm" wire:click="addWorker">
                    <i class="icon-md flaticon2-plus"></i>
                </button>
            </th>
        </tr>
        {{-- <tr>
            <td>
                <select id="form-employee_id" class="form-control ajax-select2 " data-url="{{ route('ajax.user_employee') }}" wire:model="search_employee_id" data-placeholder="@lang('global.select')">
                </select>
            </td>
            <td>
                <select id="form-position_id" class="form-control ajax-select2 " data-url="{{ route('ajax.position') }}" wire:model="search_position_id" data-placeholder="@lang('global.select')">
                </select>
            </td>
            <td class="text-center">
                <button type="button" class="btn btn-sm btn-icon btn-primary font-weight-bolder text-sm" wire:click="addWorker">
                    <i class="icon-md flaticon2-plus"></i>
                </button>
            </td>
        </tr> --}}
    </thead>
    <tbody>
        @foreach ($workers as $i => $worker)
        <tr>
            <td>
                <select wire:model="workers.{{ $i }}.employee_id" name="workers[{{ $i }}][employee_id]" class="form-control text-sm col-sm-12 col-lg-6">
                    @foreach ($users as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <select wire:model="workers.{{ $i }}.position_id" name="workers[{{ $i }}][position_id]" class="form-control text-sm col-sm-12 col-lg-6">
                    @foreach ($positions as $id => $name)
                        <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </td>
            <td class="text-center">
                <button type="button" class="btn btn-icon btn-outline-secondary mr-2" wire:click="$emit('triggerDeleteWorker',{{ $i }})">
                    <i class="icon-md flaticon2-trash"></i>
                </button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@push('js')
<script>
    document.addEventListener('DOMContentLoaded', function () {
        Livewire.on('triggerDeleteWorker', detailId => {
            const swal_confirm = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success ml-1',
                  cancelButton: 'btn btn-danger mr-1'
                },
                buttonsStyling: false
            })
            swal_confirm.fire({
                title: '{!! trans("file.message.sure") !!}',
                text: '{!! trans("file.message.deleted_file") !!}',
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true,
                showLoaderOnConfirm: true,
            }).then((result) => {
                //if user clicks on delete
                if (result.value) {
                    // calling destroy method to delete
                    @this.call('removeWorker', detailId)

                }
            });
        })
    });

    window.addEventListener('msgError', event => {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: event.detail.message,
        })
    })
</script>
@endpush

<div class="row">
    <div class="col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b" id="kt_page_sticky_card">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.form') @lang('project_payment.title')</h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('project.project.show', $model->project_id) }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_payment.label.project_id')</label>
                    <div class="col-lg-7">
                        @if (isset($model->project_id))
                            <label class="col-lg-12 col-form-label text-lg-right">{{ $model->project->no_project }}</label>
                            <input type="hidden" name="project_id" value="{{ old('project_id', $model->project_id) }}" />
                        @else
                            <select name="project_id" id="form-project_id" class="form-control w-100 ajax-select2 @classInpError('project_id')" data-url="{{ route('ajax.project') }}" data-placeholder="@lang('global.select')" style="width: 100%;">
                                @if (old('project_id') || $model->project_id)
                                    @php
                                        $model->project_id = old('project_id', $model->project_id);
                                    @endphp
                                    <option value="{{ $model->project->id }}" selected>{{ $model->project->no_project }}</option>
                                @endif
                            </select>

                            @inpSpanError(['column'=>'project_id'])
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_payment.label.title')</label>
                    <div class="col-lg-7">
                        <input class="form-control @classInpError('project_id')" type="text" name="title" value="{{ old('title', $model->title) }}" readonly/>

                        @inpSpanError(['column'=>'title'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_payment.label.employee_id')</label>
                    <div class="col-lg-7">
                        <select name="employee_id" id="form-employee_id" class="form-control select2 @classInpError('employee_id')" placeholder="@lang('global.select')" style="width: 100%;">
                            @foreach ($employees as $id => $name)
                            <option value="{{ $id }}" {{ old('employee_id', $model->employee_id) == $id ? 'selected' : '' }}>{{ $name }}</option>
                            @endforeach
                        </select>

                        @inpSpanError(['column'=>'employee_id'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_payment.label.date')</label>
                    <div class="col-lg-7">
                        <div class="input-group date">
                            <input type="text" class="form-control datetimepicker @classInpError('date')" readonly="readonly" value="{{ date('Y-m-d H:i', strtotime(old('date', $model->date ?? now()))) }}" name="date" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar glyphicon-th"></i>
                                </span>
                            </div>

                            @inpSpanError(['column'=>'date'])
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_payment.label.information')</label>
                    <div class="col-lg-7">
                        <textarea class="form-control @classInpError('information')" name="information">{{ old('information', $model->information) }}</textarea>

                        @inpSpanError(['column'=>'information'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_payment.label.amount')</label>
                    <div class="col-lg-7">
                        <input class="form-control format-currency @classInpError('amount')" type="text" name="amount" value="{{ old('amount', $model->amount) }}" />

                        @inpSpanError(['column'=>'amount'])
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    <div class="col-lg-6">
        @livewire('file.file-form', [
            'files' => old('files', $files)
        ])
    </div>
</div>

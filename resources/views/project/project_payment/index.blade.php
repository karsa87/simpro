@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Pelunasan Pembelian</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="{{ route('project.project_payment.create') }}" class="btn btn-primary font-weight-bolder">
                        Add
                    </a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <!--begin::Search Form-->
                <div class="mb-7">
                    <div class="row align-items-center">
                        <div class="col-lg-12">
                            <div class="row align-items-center">
                                <div class="col-md-4 my-2 my-md-0">
                                    <div class="input-icon">
                                        <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" name="_k" value="{{ request('_k') }}" />
                                        <span>
                                            <i class="flaticon2-search-1 text-muted"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Search Form-->
                <!--end: Search Form-->
                <!--begin: Datatable-->
                <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                    <thead>
                        <tr>
                            <th title="Field #1">No Pelunasan</th>
                            <th title="Field #2">Date</th>
                            <th title="Field #4">Informasi</th>
                            <th title="Field #3">Jumlah</th>
                            <th title="Field #5">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i < 20; $i++)
                            <tr>
                                <td>DSO/2021/II/{{ sprintf('%04d', $i+1) }}</td>
                                <td>{{ date('Y-m-d H:i', strtotime("-$i"."days")) }}</td>
                                <td>Tambah Modal Agus</td>
                                <td>{{ number_format(random_int(100000, 1000000000)) }}</td>
                                <td>
                                    <a href="{{ route('project.project_payment.edit', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </a>
                                    <a href="{{ route('project.project_payment.show', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon-eye"></i>
                                    </a>
                                    <a href="{{ route('project.project_payment.destroy', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>DSO/2021/II/{{ sprintf('%04d', $i+1) }}</td>
                                <td>{{ date('Y-m-d H:i', strtotime("-$i"."days")) }}</td>
                                <td>Tambah Modal Ayu</td>
                                <td>{{ number_format(random_int(100000, 1000000000)) }}</td>
                                <td>
                                    <a href="{{ route('project.project_payment.edit', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </a>
                                    <a href="{{ route('project.project_payment.show', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon-eye"></i>
                                    </a>
                                    <a href="{{ route('project.project_payment.destroy', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>DSO/2021/II/{{ sprintf('%04d', $i+1) }}</td>
                                <td>{{ date('Y-m-d H:i', strtotime("-$i"."days")) }}</td>
                                <td>Tambah Modal Novi</td>
                                <td>{{ number_format(random_int(100000, 1000000000)) }}</td>
                                <td>
                                    <a href="{{ route('project.project_payment.edit', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </a>
                                    <a href="{{ route('project.project_payment.show', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon-eye"></i>
                                    </a>
                                    <a href="{{ route('project.project_payment.destroy', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endfor
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->

    </div>
</div>
@endsection

@push('js')
<script>
"use strict";
// Class definition

var KTDatatableHtmlTableDemo = function() {
    // Private functions

    // demo initializer
    var demo = function() {

		var datatable = $('#kt_datatable').KTDatatable({
			data: {
				saveState: {cookie: false},
			},
			search: {
				input: $('#kt_datatable_search_query'),
				key: 'generalSearch'
			},
			columns: [],
		});

    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function() {
	KTDatatableHtmlTableDemo.init();
});
</script>
@endpush

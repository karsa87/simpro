@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row" id="content-project_type">
    <div class="col-xs-12 col-sm-12 col-lg-8">
        <div class="card card-primary card-custom card-outline">
            <div class="card-header flex-wrap pt-6 pb-0">
                <h3 class="card-title"> @lang('global.list') @lang('project_type.title')</h3>

                @hasPermission('project.project_type.create')
                <div class="card-toolbar">
                    <div class="example-tools justify-content-center">
                        <a href="{{ route('project.project_type.create') }}" class="btn btn-success example-toggle">@lang('global.add')</a>
                    </div>
                </div>
                @endhasPermission
            </div>
            <div class="card-body table-responsive">
                <div class="mb-7">
                    <form id="form-search-project_type" role="form" action="{{ route('project.project_type.index') }}">
                        <div class="row align-items-center">
                            <div class="form-group col-lg-4">
                                <div class="input-group">
                                    <input type="text" name="_k" placeholder="@lang('global.keyword')" value="{{ request('_k') }}" class="form-control float-right" />
                                    <div class="input-group-append">
                                        <button type="submit" id="btn-search" class="btn btn-default"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th style="width: 5%;" class="text-center">@lang('global.no')</th>
                            <th style="width: 25%;">
                                @sortablelink('name', trans('project_type.label.name'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th style="width: 25%;">
                                @lang('project_type.label.description')
                            </th>
                            <th style="width: 15%;">
                                @sortablelink('price', trans('project_type.label.price'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="text-center" style="width: 10%;">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($projectTypes) && count($projectTypes) > 0 )
                        @foreach( $projectTypes as $i => $r)
                        <tr>
                            <td class="text-center">{{ $projectTypes->firstItem() + $i }}</td>
                            <td class="text-wrap">{{ $r->name }}</td>
                            <td class="text-wrap">{{ $r->description }}</td>
                            <td class="text-wrap text-right">{{ $util->format_currency($r->price) }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    @hasPermission('project.project_type.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('project.project_type.edit', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-edit-project_type btn-loader" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('project.project_type.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('project.project_type.destroy', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip" data-refresh="0">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($projectTypes) && count($projectTypes) > 0 )
            {!! \App\Util\Base\Layout::paging($projectTypes, 'form-search-project_type') !!}
            @endif
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header flex-wrap pt-6 pb-0">
                <h3 class="card-title">@lang('global.form') @lang('project_type.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'project.project_type.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/project/project_type.js') }}?{{ config('app.version') }}"></script>
@endpush

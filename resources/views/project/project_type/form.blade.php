<!-- form start -->
<form id="form-project_type" role="form" method="POST" action="{{ route('project.project_type.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('project_type.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('project_type.placeholder.name')" value="{{ old('name') }}">

            @inpSpanError(['column'=>'name'])
        </div>
        <div class="form-group">
            <label for="name">@lang('project_type.label.description')</label>
            <textarea class="form-control form-control-lg form-control @classInpError('description')" name="description" rows="3">{{ old('description') }}</textarea>

            @inpSpanError(['column'=>'description'])
        </div>
        <div class="form-group">
            <label for="price">@lang('project_type.label.price')</label>
            <input type="text" name="price" class="form-control format-currency @classInpError('price')" placeholder="@lang('project_type.placeholder.price')" value="{{ old('price') }}">

            @inpSpanError(['column'=>'price'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('project.project_type.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>

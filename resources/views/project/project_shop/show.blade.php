@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <div class="card-body">
                @include('project.project.section.project_detail', ['model' => $model->project])
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('global.detail') @lang('project_shop.title')</span>
                </h3>
                <div class="card-toolbar">
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">
                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ki ki-bold-more-hor"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover py-5">
                                @hasPermission('project.project_shop.create')
                                <li class="navi-item">
                                    <a href="{{ route('project.project_shop.create', ['project_id'=>$model->project_id]) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-add-1"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.new')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('project.project_shop.edit')
                                <li class="navi-item">
                                    <a href="{{ route('project.project_shop.edit', $model->id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-pen"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.edit')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('project.project_shop.destroy')
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <a data-href="{{ route('project.project_shop.destroy', $model->id) }}" data-redirect="{{ route('project.project_shop.index') }}" class="navi-link btn-delete">
                                        <span class="navi-icon">
                                            <i class="flaticon2-trash"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.delete')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('project.project.show')
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <a href="{{ route('project.project.show', $model->project_id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-left-arrow-1"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.back') @lang('global.detail') @lang('project.title')</span>
                                    </a>
                                </li>
                                @endhasPermission
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Header-->
            <div class="card-body">
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('project_shop.label.store_name')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->store_name }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('project_shop.label.project_id')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->project->no_project }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('project_shop.label.date')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $util->formatDate($model->date) }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('project_shop.label.employee_id')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->employee->name }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('project_shop.label.information')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->information }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('project_shop.label.status')</label>
                    <div class="col-8">
                        <span class="label font-weight-bold label-lg label-light-{{ $model->status ? 'success' : 'danger' }} label-inline">@lang("project_shop.list.status.$model->status")</span>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    <div class="col-sm-12 col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('file.title')</span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">@lang('global.list')</span>
                </h3>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-4">
                @include('master.file.component.list_file', ['files' => $model->files ?? []])
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('project_shop.label.product_materials')</span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">@lang('global.list')</span>
                </h3>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-4">
                <table class="w-100 table table-head-custom table-head-bg table-borderless table-vertical-center">
                    <thead>
                        <tr>
                            <th class="w-30">@lang('project_shop.label.product_materials')</th>
                            <th class="w-10 text-center">@lang('project_shop.label.qty')</th>
                            <th class="w-20 text-right">@lang('project_shop.label.price')</th>
                            <th class="w-20 text-right">@lang('project_shop.label.total_price')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($model->projectShopDetails as $detail)
                        <tr>
                            <td>{{ $detail->product_name }}</td>
                            <td class="text-center">{{ $detail->qty }}</td>
                            <td class="text-right">{{ $util->format_currency($detail->price) }}</td>
                            <td class="text-right">
                                <span class="text-dark-75 font-weight-bolder text-hover-primary mt-1 font-size-lg">{{ $util->format_currency($detail->total_price) }}</span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <thead>
                        <tr>
                            <th colspan="3">@lang('project_shop.label.total_amount')</th>
                            <th class="w-20 text-right">
                                <span class="text-dark-75 font-weight-bolder text-hover-primary mt-1 font-size-lg">{{ $util->format_currency($model->total_amount) }}</span></th>
                        </tr>
                        <tr>
                            <th colspan="3">@lang('project_shop.label.paid')</th>
                            <th class="w-20 text-right">
                                <span class="text-dark-75 font-weight-bolder text-hover-primary mt-1 font-size-lg">{{ $util->format_currency($model->paid) }}</span></th>
                        </tr>
                        <tr>
                            <th colspan="3">@lang('project_shop.label.unpaid')</th>
                            <th class="w-20 text-right">
                                <span class="text-dark-75 font-weight-bolder text-hover-primary mt-1 font-size-lg">{{ $util->format_currency($model->total_amount - $model->paid) }}</span></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

@push('js')
@endpush

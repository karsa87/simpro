<div class="row">
    <div class="col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.form') @lang('project_shop.title')</h3>

                    @if (isset($model->project_id))
                        :&nbsp;<h3 class="card-label">{{ $model->project->no_project }}</h3>
                        <input type="hidden" name="project_id" value="{{ old('project_id', $model->project_id) }}" />
                    @else
                        <select name="project_id" id="form-project_id" class="form-control w-100 ajax-select2 @classInpError('project_id')" data-url="{{ route('ajax.project') }}" data-placeholder="@lang('global.select')" style="width: 100%;">
                            @if (old('project_id') || $model->project_id)
                                @php
                                    $model->project_id = old('project_id', $model->project_id);
                                @endphp
                                <option value="{{ $model->project->id }}" selected>{{ $model->project->no_project }}</option>
                            @endif
                        </select>

                        @inpSpanError(['column'=>'project_id'])
                    @endif
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('project.project.show', $model->project_id) }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_shop.label.store_name')</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control @classInpError('store_name')" value="{{ old('store_name', $model->store_name) }}" name="store_name" placeholder="@lang('project_shop.placeholder.store_name')" />

                        @inpSpanError(['column'=>'store_name'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_shop.label.date')</label>
                    <div class="col-lg-7">
                        <div class="input-group date">
                            <input type="text" class="form-control datetimepicker @classInpError('date')" readonly="readonly" value="{{ date('Y-m-d H:i', strtotime(old('date', $model->date ?? now()))) }}" name="date" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar glyphicon-th"></i>
                                </span>
                            </div>

                            @inpSpanError(['column'=>'date'])
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_shop.label.employee_id')</label>
                    <div class="col-lg-7">
                        <select name="employee_id" id="form-employee_id" class="form-control select2 @classInpError('employee_id')" placeholder="@lang('global.select')" style="width: 100%;">
                            @foreach ($employees as $id => $name)
                            <option value="{{ $id }}" {{ old('employee_id', $model->employee_id) == $id ? 'selected' : '' }}>{{ $name }}</option>
                            @endforeach
                        </select>

                        @inpSpanError(['column'=>'employee_id'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_shop.label.status')</label>
                    <div class="col-lg-7">
                        <select name="status" id="form-status" class="form-control select2 @classInpError('employee_id')" placeholder="@lang('global.select')" style="width: 100%;">
                            @foreach (trans('project_shop.list.status') as $id => $name)
                            <option value="{{ $id }}" {{ old('status', $model->status) == $id ? 'selected' : '' }}>{{ $name }}</option>
                            @endforeach
                        </select>

                        @inpSpanError(['column'=>'status'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_shop.label.information')</label>
                    <div class="col-lg-7">
                        <textarea class="form-control" name="information">{{ old('information', $model->information) }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    <div class="col-lg-6">
        @livewire('file.file-form', [
            'files' => old('files', $files)
        ])
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        @livewire('project-shop.product-form', [
            'details' => old('details', $details)
        ])
    </div>
</div>

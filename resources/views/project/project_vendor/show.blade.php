@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('global.detail') @lang('project_vendor.title') : {{ $model->vendor->name }}</span>
                </h3>
                <div class="card-toolbar">
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">
                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ki ki-bold-more-hor"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover py-5">
                                @hasPermission('project.project_vendor.edit')
                                <li class="navi-item">
                                    <a href="{{ route('project.project_vendor.edit', $model->id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-pen"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.edit')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('project.project_vendor.destroy')
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <button data-href="{{ route('project.project_vendor.destroy', $model->id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-trash"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.delete')</span>
                                    </button>
                                </li>
                                @endhasPermission
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Header-->
            <div class="card-body">
                <table class="w-100 table table-head-custom table-head-bg table-borderless table-vertical-center">
                    <thead>
                        <tr>
                            <td class="font-weight-bold col-1 text-muted text-center">@lang('global.no')</td>
                            <td class="font-weight-bold col-11">@lang('project.label.job')</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($model->jobs as $i => $job)
                        <tr>
                            <td class="font-weight-bold text-muted text-center">{{ $i+1 }}</td>
                            <td class="font-weight-bold">{{ $job->job }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

@push('js')
@endpush

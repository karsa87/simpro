{{-- @include('project.project.form') --}}
@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <div class="card-body">
                @include('project.project.section.project_detail', ['model' => $model->project])
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>

<form method="POST" action="{{ route('project.project_vendor.update', $model->id) }}">
    @csrf
    @method('put')

    @include('project.project_vendor._form')
</form>
<!--end::Form-->
@endsection

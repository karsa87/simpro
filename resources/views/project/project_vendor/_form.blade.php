<input type="hidden" name="project_id" value="{{ old('project_id', $model->project_id) }}" />

<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b" id="kt_page_sticky_card">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('project_vendor.label.vendor_id')</h3>

                    <select name="vendor_id" id="form-vendor_id" class="form-control ajax-select2 @classInpError('vendor_id')" data-url="{{ route('ajax.vendor') }}" data-placeholder="@lang('global.select')" style="width: 100%">
                        @if (old('vendor_id') || $model->vendor_id)
                        @php
                            $model->vendor_id = old('vendor_id') ?? $model->vendor_id;
                        @endphp
                        <option value="{{ $model->vendor_id }}" selected>
                            {{ $model->vendor->name }}
                        </option>
                        @endif
                    </select>

                    @inpSpanError(['column'=>'vendor_id'])
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('project.project.show', $model->project_id) }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                @livewire('project-vendor.jobs-form', [
                    'projectVendor' => $model,
                    'jobs' => $jobs,
                    'vendor_id' => old('vendor_id', $model->vendor_id),
                ])
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>

@push('js')
<script>
var prev_vendor_id = $('#form-vendor_id').val();
$('#form-vendor_id').change(function() {
    var selected = $("#form-vendor_id").select2('data').length > 0 ? $("#form-vendor_id").select2('data')[0] : null;
    if (prev_vendor_id != null) {
        Swal.fire({
            title: trans('project_vendor.message.sure'),
            text: trans('project_vendor.message.confirm_change_vendor'),
            icon: 'warning',
            showCancelButton: true,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                prev_vendor_id = $(this).val();
                if (Livewire != undefined && prev_vendor_id != null) {
                    Livewire.emit('changeVendor', prev_vendor_id);
                }
            }
        })
    } else {
        prev_vendor_id = $(this).val();
        if (Livewire != undefined && prev_vendor_id != null) {
            Livewire.emit('changeVendor', prev_vendor_id);
        }
    }
});
</script>
@endpush

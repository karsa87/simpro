@inject('util', 'App\Util\Helpers\Util')

<div class="d-flex">
    <!--begin: Info-->
    <div class="flex-grow-1">
        <!--begin: Title-->
        <div class="d-flex align-items-center justify-content-between flex-wrap">
            <div class="mr-3">
                <!--begin::Name-->
                <a href="#" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">{{ $model->projectname }}
                <i class="flaticon2-correct text-success icon-md ml-2"></i></a>
                <!--end::Name-->
                <!--begin::Contacts-->
                <div class="d-flex flex-wrap my-2">
                    <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon points="0 0 24 0 24 24 0 24"/>
                                <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>{{ $model->customer_name }}</a>
                    <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/General/Lock.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M11.914857,14.1427403 L14.1188827,11.9387145 C14.7276032,11.329994 14.8785122,10.4000511 14.4935235,9.63007378 L14.3686433,9.38031323 C13.9836546,8.61033591 14.1345636,7.680393 14.7432841,7.07167248 L17.4760882,4.33886839 C17.6713503,4.14360624 17.9879328,4.14360624 18.183195,4.33886839 C18.2211956,4.37686904 18.2528214,4.42074752 18.2768552,4.46881498 L19.3808309,6.67676638 C20.2253855,8.3658756 19.8943345,10.4059034 18.5589765,11.7412615 L12.560151,17.740087 C11.1066115,19.1936265 8.95659008,19.7011777 7.00646221,19.0511351 L4.5919826,18.2463085 C4.33001094,18.1589846 4.18843095,17.8758246 4.27575484,17.613853 C4.30030124,17.5402138 4.34165566,17.4733009 4.39654309,17.4184135 L7.04781491,14.7671417 C7.65653544,14.1584211 8.58647835,14.0075122 9.35645567,14.3925008 L9.60621621,14.5173811 C10.3761935,14.9023698 11.3061364,14.7514608 11.914857,14.1427403 Z" fill="#000000"/>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>{{ $model->customer_phone }}</a>
                    <a href="#" class="text-muted text-hover-primary font-weight-bold">
                    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Map/Marker2.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>{{ $model->customer_address }}</a>
                </div>
                <!--end::Contacts-->
            </div>
        </div>
        <!--end: Title-->
        <!--begin: Content-->
        <div class="d-flex align-items-center flex-wrap justify-content-between">
            <div class="flex-grow-1 font-weight-bold text-dark-50 py-5 py-lg-2 mr-5">{{ $model->information }}</div>
            <div class="d-flex flex-wrap align-items-center py-2">
                <div class="d-flex align-items-center mr-10">
                    <div class="mr-6">
                        <div class="font-weight-bold mb-2">@lang('project.label.no_project')</div>
                        <span class="btn btn-sm btn-text btn-light-success text-uppercase font-weight-bold">{{ $model->no_project }}</span>
                    </div>
                    <div class="">
                        <div class="font-weight-bold mb-2">@lang('project.label.no_spk')</div>
                        <span class="btn btn-sm btn-text btn-light-success text-uppercase font-weight-bold">{{ $model->no_spk }}</span>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-wrap align-items-center py-2">
                <div class="d-flex align-items-center mr-10">
                    <div class="mr-6">
                        <div class="font-weight-bold mb-2">@lang('project.label.start_date')</div>
                        <span class="btn btn-sm btn-text btn-light-primary text-uppercase font-weight-bold">{{ $util->formatDate($model->start_date) }}</span>
                    </div>
                    <div class="">
                        <div class="font-weight-bold mb-2">@lang('project.label.due_date')</div>
                        <span class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold">{{ $util->formatDate($model->due_date) }}</span>
                    </div>
                </div>
                <div class="flex-grow-1 flex-shrink-0 w-150px w-xl-300px mt-4 mt-sm-0">
                    <span class="font-weight-bold">@lang('project.label.progress')</span>
                    <div class="progress progress-xs mt-2 mb-2">
                        <div class="progress-bar bg-success" role="progressbar" style="width: {{$model->progress}}%;" aria-valuenow="{{ $model->progress }}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <span class="font-weight-bolder text-dark">{{$model->progress}}%</span>
                </div>
            </div>
        </div>
        <!--end: Content-->
    </div>
    <!--end: Info-->
</div>

@inject('util', 'App\Util\Helpers\Util')

<!--begin::Step 1-->
<div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
    <h3 class="mb-10 font-weight-bold text-dark">@lang('project.label.project_detail'):</h3>
    <div class="row">
        <div class="col-xl-12">
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.no_project')</label>
                <div class="col-lg-9 col-xl-9">
                    <input class="form-control form-control-lg form-control-solid @classInpError('no_project') type-review" name="no_project" type="text" value="{{ old('no_project', $model->no_project) }}" placeholder="@lang('project.placeholder.no_project')" readonly />

                    @inpSpanError(['column'=>'no_project'])
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.no_spk')</label>
                <div class="col-lg-9 col-xl-9">
                    <input class="form-control form-control-lg form-control-solid text-uppercase @classInpError('no_spk')" name="no_spk" type="text" value="{{ old('no_spk', $model->no_spk) }}" placeholder="@lang('project.placeholder.no_spk')" />

                    @inpSpanError(['column'=>'no_spk'])
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.projectname')</label>
                <div class="col-lg-9 col-xl-9">
                    <input class="form-control form-control-lg form-control-solid @classInpError('name')" name="projectname" type="text" value="{{ old('projectname', $model->projectname) }}" placeholder="@lang('project.placeholder.projectname')" oninput="this.value = this.value.toUpperCase()" />

                    @inpSpanError(['column'=>'projectname'])
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.customer_name')</label>
                <div class="col-lg-9 col-xl-9">
                    <input class="form-control form-control-lg form-control-solid @classInpError('customer_name')" name="customer_name" type="text" value="{{ old('customer_name', $model->customer_name) }}" placeholder="@lang('project.placeholder.customer_name')" oninput="this.value = this.value.toUpperCase()" />

                    <span class="form-text text-muted">@lang('project.message.desc_customer_name')</span>
                    @inpSpanError(['column'=>'customer_name'])
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.customer_company')</label>
                <div class="col-lg-9 col-xl-9">
                    <input class="form-control form-control-lg form-control-solid @classInpError('customer_company')" name="customer_company" type="text" value="{{ old('customer_company', $model->customer_company) }}" placeholder="@lang('project.placeholder.customer_company')" oninput="this.value = this.value.toUpperCase()" />

                    @inpSpanError(['column'=>'customer_company'])
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.customer_phone')</label>
                <div class="col-lg-9 col-xl-9">
                    <div class="input-group input-group-lg input-group-solid">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="la la-phone"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control form-control-lg form-control-solid @classInpError('customer_phone')" name="customer_phone" value="{{ old('customer_phone', $model->customer_phone) }}" placeholder="@lang('project.placeholder.customer_phone')" />

                        @inpSpanError(['column'=>'customer_phone'])
                    </div>
                    <span class="form-text text-muted">@lang('project.message.desc_customer_phone')</span>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.customer_address')</label>
                <div class="col-lg-9 col-xl-9">
                    <textarea class="form-control form-control-lg form-control-solid @classInpError('customer_address')" name="customer_address" rows="3" oninput="this.value = this.value.toUpperCase()">{{ old('customer_address', $model->customer_address) }}</textarea>

                    @inpSpanError(['column'=>'customer_address'])
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.project_type_id')</label>
                <div class="col-lg-9 col-xl-9">
                    <select name="project_type_id" id="form-project_type_id" class="form-control ajax-select2 @classInpError('project_type_id')" data-url="{{ route('ajax.project_type') }}" data-placeholder="Select" style="width: 100%;">
                        @if (old('project_type_id') || $model->project_type_id)
                            @php
                                $model->project_type_id = old('project_type_id', $model->project_type_id);
                                $text = sprintf('%s - %s', $model->projectType->name, $util::format_currency($model->projectType->price));
                            @endphp
                            <option value="{{ $model->projectType->id }}" selected>{{ $text }}</option>
                        @endif
                    </select>

                    @inpSpanError(['column'=>'project_type_id'])
                </div>
            </div>
            {{-- <div class="form-group row">
                <label class="col-lg-3 col-form-label text-lg-right">@lang('project_debt_payment.label.project_shop_id')</label>
                <div class="col-lg-7">
                    <select name="project_shop_id" id="form-project_shop_id" class="form-control ajax-select2 @classInpError('project_shop_id')" data-url="{{ route('ajax.project_shop.debt', ['pid'=>$model->project_id]) }}" data-placeholder="@lang('global.select')" style="width: 100%;">
                        @if (old('project_shop_id') || $model->project_shop_id)
                            @php
                                $model->project_shop_id = old('project_shop_id', $model->project_shop_id);
                            @endphp
                            <option value="{{ $model->projectShop->id }}" selected>{{ sprintf('%s - %s', $model->projectShop->project->no_project, $model->projectShop->store_name) }}</option>
                        @endif
                    </select>

                    @inpSpanError(['column'=>'project_shop_id'])
                </div>
            </div> --}}
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.information')</label>
                <div class="col-lg-9 col-xl-9">
                    <textarea class="form-control form-control-lg form-control-solid @classInpError('information')" name="information" rows="3">{{ old('information', $model->information) }}</textarea>

                    @inpSpanError(['column'=>'information'])
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.term_of_work')</label>
                <div class="col-lg-5 col-xl-5">
                    <div class="input-group date">
                        <input type="text" class="form-control form-control-lg form-control-solid datepicker @classInpError('start_date')" readonly="readonly" value="{{ date('Y-m-d', strtotime(old('start_date', $model->start_date ?? now()))) }}" name="start_date" />
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-calendar glyphicon-th"></i>
                            </span>
                        </div>

                        @inpSpanError(['column'=>'start_date'])
                    </div>
                    <span class="form-text text-muted">@lang('project.message.desc_start_date')</span>
                </div>
                <div class="col-lg-4 col-xl-4">
                    <input type="number" class="form-control form-control-lg form-control-solid @classInpError('term_of_work')" value="{{ old('term_of_work', $model->term_of_work) }}" name="term_of_work" />
                    <span class="form-text text-muted">@lang('project.message.desc_term_of_work')</span>

                    @inpSpanError(['column'=>'term_of_work'])
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.due_date')</label>
                <div class="col-lg-9 col-xl-9">
                    <div class="input-group date">
                        <input type="text" class="form-control form-control-lg form-control-solid @classInpError('due_date')" readonly="readonly" value="{{ date('Y-m-d', strtotime(old('due_date', $model->due_date ?? now()))) }}" name="due_date" />
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-calendar glyphicon-th"></i>
                            </span>
                        </div>

                        @inpSpanError(['column'=>'due_date'])
                    </div>
                    <span class="form-text text-muted">@lang('project.message.desc_due_date')</span>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">@lang('project.label.total_amount')</label>
                <div class="col-lg-9 col-xl-9">
                    <input class="form-control form-control-lg form-control-solid format-currency @classInpError('total_amount')" type="text" value="{{ old('total_amount', $model->total_amount) }}" name="total_amount" />
                    <span class="form-text text-muted">@lang('project.message.desc_total_amount')</span>

                    @inpSpanError(['column'=>'total_amount'])
                </div>
            </div>
        </div>
    </div>
</div>
<!--end::Step 1-->
<!--begin::Step 2-->
<div class="pb-5" data-wizard-type="step-content">
    <div class="row">
        <div class="col-lg-12">
            @livewire('file.file-form', [
                'files' => old('files', $files)
            ])
        </div>
    </div>
</div>
<!--end::Step 2-->
<!--begin::Step 3-->
<div class="pb-5" data-wizard-type="step-content">
    <h3 class="mb-10 font-weight-bold text-dark">@lang('project.label.detail_jobs')</h3>

    @livewire('project.job-detail-form', [
        'jobs' => old('jobs', $jobs)
    ])
</div>
<!--end::Step 3-->
<!--begin::Step 4-->
<div class="pb-5" data-wizard-type="step-content">
    <h4 class="mb-10 font-weight-bold">@lang('project.message.desc_review_submit')</h4>
    <h6 class="font-weight-bold mb-3">@lang('project.label.project_detail'):</h6>
    <table class="w-100">
        <tr>
            <td class="col-4 p-0 m-0 font-weight-bold text-muted">@lang('project.label.no_project')</td>
            <td class="col-8 p-0 m-0 font-weight-bold" id="review-no_project">{{ old('no_project', $model->no_project) }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.no_spk')</td>
            <td class="font-weight-bold" id="review-no_spk">{{ old('no_spk', $model->no_spk) }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.projectname')</td>
            <td class="font-weight-bold" id="review-projectname">{{ old('projectname', $model->projectname) }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.customer_name')</td>
            <td class="font-weight-bold" id="review-customer_name">{{ old('customer_name', $model->customer_name) }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.customer_company')</td>
            <td class="font-weight-bold" id="review-customer_company">{{ old('customer_company', $model->customer_company) }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.customer_address')</td>
            <td class="font-weight-bold" id="review-customer_address">{{ old('customer_address', $model->customer_address) }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.project_type_id')</td>
            <td class="font-weight-bold" id="review-project_type_id">
                @if (old('project_type_id', $model->project_type_id))
                    @php
                        $model->project_type_id = old('project_type_id', $model->project_type_id);
                        $text = sprintf('%s - %s', $model->projectType->name, $util::format_currency($model->projectType->price));
                    @endphp
                    {{ $text }}
                @endif
            </td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.customer_phone')</td>
            <td class="font-weight-bold" id="review-customer_phone">{{ old('customer_phone', $model->customer_phone) }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.information')</td>
            <td class="font-weight-bold" id="review-information">{{ old('information', $model->information) }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.term_of_work')</td>
            <td class="font-weight-bold">
                <span id="review-term_of_work">{{ old('term_of_work', $model->term_of_work) }}</span>
                @lang('project.message.desc_start_date')
            </td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.start_date')</td>
            <td class="font-weight-bold" id="review-start_date">{{ date('d-m-Y H:i', strtotime(old('start_date', $model->start_date ?? now()))) }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.due_date')</td>
            <td class="font-weight-bold" id="review-due_date">{{ date('d-m-Y H:i', strtotime(old('due_date', $model->due_date ?? now()))) }}</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-muted">@lang('project.label.total_amount')</td>
            <td class="font-weight-bold" id="review-total_amount">{{ $util->format_currency($util->remove_format_currency(old('total_amount', $model->total_amount ?? 0))) }}</td>
        </tr>
    </table>
</div>
<!--end::Step 4-->
<!--begin::Actions-->
<div class="d-flex justify-content-between border-top mt-5 pt-10">
    <div class="mr-2">
        <button type="button" class="btn btn-light-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-prev">@lang('global.prev')</button>
    </div>
    <div>
        <button type="submit" class="btn btn-success font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-submit">@lang('global.save')</button>
        <button type="button" class="btn btn-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-next">@lang('global.next')</button>
    </div>
</div>
<!--end::Actions-->

@push('js')
<script>
$(function() {
    $('input[name=term_of_work]').keyup(function() {change_due_date();});
    $('input[name=start_date]').change(function() {change_due_date();});

    var change_due_date = function() {
        var term_of_work = $('input[name=term_of_work]').val();
        term_of_work = (term_of_work != '') ? parseInt(term_of_work) : 1;
        var result = new Date($('input[name=start_date]').val());
        result.setDate(result.getDate() + term_of_work);
        var day = ("0" + result.getDate()).slice(-2);
        var month = ("0" + (result.getMonth() + 1)).slice(-2);
        var year = result.getFullYear();
        var date =  year + "-" + month + "-" + day;

        $('input[name=due_date]').val(date);
    };
})
</script>
@endpush

@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('project.project.change.worker.update', $model->id) }}">
    @csrf
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom card-sticky gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">Pekerja</h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('project.project.show', $model->id) }}" type="reset" class="btn btn-danger mr-2">Cancel</a>
                    <button type="submit" class="btn btn-success mr-2">Save</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                @livewire('project.worker-form', ['workers' => old('workers', $workers)])
            </div>
        </div>
    </div>
</div>
</form>
<!--end::Form-->
@endsection

@push('js')
<script>
    $(function(){
        // basic
        $('#kt_select2_1').select2({
            placeholder: "Select a state"
        });

        $('#kt_datetimepicker_3').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            pickerPosition: 'bottom-left',
            todayBtn: true,
            format: 'dd-mm-yyyy'
        });

        $("#kt_inputmask_7").inputmask({ alias : "currency", prefix: '', digits: 2 });
    });
</script>
@endpush

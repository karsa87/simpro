@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('project.label.project_detail')</span>
                </h3>
                <div class="card-toolbar">
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">
                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ki ki-bold-more-hor"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover py-5">
                                @hasPermission('project.project.create')
                                <li class="navi-item">
                                    <a href="{{ route('project.project.create') }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-add-1"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.new')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('project.project.edit')
                                <li class="navi-item">
                                    <a href="{{ route('project.project.edit', $model->id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-pen"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.edit')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('project.project.destroy')
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <a data-href="{{ route('project.project.destroy', $model->id) }}" data-redirect="{{ route('project.project.index') }}" class="navi-link btn-delete">
                                        <span class="navi-icon">
                                            <i class="flaticon2-trash"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.delete')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @if (!$model->isCancel() || !$model->isCLose())
                                @hasPermission('project.project.change.status.done')
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <a href="{{ route('project.project.change.status.done', $model->id) }}" class="navi-link">
                                        <span class="navi-text">@lang('project.list.status.2')</span>
                                    </a>
                                </li>
                                @endhasPermission
                                @endif

                                @if ($model->isDone())
                                @hasPermission('project.project.change.status.close')
                                <li class="navi-item">
                                    <a href="{{ route('project.project.change.status.close', $model->id) }}" class="navi-link">
                                        <span class="navi-text">@lang('project.list.status.3')</span>
                                    </a>
                                </li>
                                @endhasPermission
                                @endif

                                @if (!$model->isClose())
                                @hasPermission('project.project.change.status.cancel')
                                <li class="navi-item">
                                    <a href="{{ route('project.project.change.status.cancel', $model->id) }}" class="navi-link">
                                        <span class="navi-text">@lang('project.list.status.9')</span>
                                    </a>
                                </li>
                                @endhasPermission
                                @endif
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Header-->
            <div class="card-body">
                @include('project.project.section.project_detail')
                <div class="separator separator-solid my-7"></div>
                <!--begin: Items-->
                <div class="d-flex align-items-center flex-wrap">
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon-piggy-bank icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="font-weight-bolder font-size-sm">@lang('project.label.total_amount')</span>
                            <span class="font-weight-bolder font-size-h5">
                            <span class="text-dark-50 font-weight-bold">Rp. </span>{{ $util->format_currency($model->total_amount) }}</span>
                        </div>
                    </div>
                    <!--end: Item-->
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon-piggy-bank icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="font-weight-bolder font-size-sm">@lang('project_payment.label.pay')</span>
                            <span class="font-weight-bolder font-size-h5">
                            <span class="text-dark-50 font-weight-bold">Rp. </span>{{ $util->format_currency($model->payments->sum('amount')) }}</span>
                        </div>
                    </div>
                    <!--end: Item-->
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon-pie-chart icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="font-weight-bolder font-size-sm">@lang('project.label.shop')</span>
                            <span class="font-weight-bolder font-size-h5">
                            <span class="text-dark-50 font-weight-bold">Rp. </span>{{ $util->format_currency($model->shops->sum('total_amount')) }}</span>
                        </div>
                    </div>
                    <!--end: Item-->
                    <!--begin: Item-->
                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                        <span class="mr-4">
                            <i class="flaticon-confetti icon-2x text-muted font-weight-bold"></i>
                        </span>
                        <div class="d-flex flex-column text-dark-75">
                            <span class="font-weight-bolder font-size-sm">@lang('project.label.expend')</span>
                            <span class="font-weight-bolder font-size-h5">
                            <span class="text-dark-50 font-weight-bold">Rp. </span>{{ $util->format_currency($model->expends->sum('amount')) }}</span>
                        </div>
                    </div>
                    <!--end: Item-->
                </div>
                <!--begin: Items-->
                <div class="separator separator-solid my-7"></div>
                <!--begin: Items-->
                @include('master.file.component.list_file', [
                    'files' => $model->files ?? [],
                    'column_class' => 'col-xl-2 col-lg-3 col-sm-4 col-md-6 col-xs-12'
                ])
                <div class="d-flex align-items-center flex-wrap">
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('project.label.detail_jobs')</span>
                </h3>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-4">
                <table class="w-100 table table-head-custom table-head-bg table-borderless table-vertical-center">
                    <thead>
                        <tr>
                            <td class="font-weight-bold text-muted text-center col-1">@lang('global.no')</td>
                            <td class="font-weight-bold col-3">@lang('project.label.job')</td>
                            <td class="font-weight-bold col-4">@lang('project.label.detail_jobs')</td>
                            <td class="font-weight-bold col-2">@lang('project.label.progress')</td>
                            <td class="font-weight-bold text-center col-2">@lang('project.label.status')</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($model->jobs as $i => $job)
                        <tr>
                            <td class="align-top font-weight-bold text-muted text-center">{{ $i+1 }}</td>
                            <td class="align-top font-weight-bold">{{ $job->job }}</td>
                            <td class="align-top text-wrap">
                                <p style="white-space: pre-line">{!! htmlspecialchars_decode($job->detail) !!}</p>
                            </td>
                            <td class="align-top">
                                <div class="progress progress-xs mt-2 mb-2">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{$job->progress}}%;" aria-valuenow="{{ $job->progress }}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="font-weight-bolder text-dark">{{$job->progress}}%</span>
                            </td>
                            <td class="font-weight-bold text-center">
                                <span class="label font-weight-bold label-lg label-light-@lang("project.list.status_class.$job->status") label-inline">
                                    @lang("project.list.status.$job->status")
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!--begin::Row-->
<div class="row">
    <div class="col-lg-6">
        <!--begin::Advance Table Widget 3-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 py-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('project.label.workers')</span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">@lang('project.message.list_workers')</span>
                </h3>
                <div class="card-toolbar">
                    @hasPermission('project.project.change.worker')
                    <a href="{{ route('project.project.change.worker', $model->id) }}" class="btn btn-success font-weight-bolder font-size-sm">
                    <span class="svg-icon svg-icon-md svg-icon-white">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon points="0 0 24 0 24 24 0 24" />
                                <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>@lang('global.add') / @lang('global.edit') @lang('project.label.workers')</a>
                    @endhasPermission
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-0 pb-3">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                            <tr class="text-uppercase">
                                <th style="min-width: 250px" class="">
                                    <span class="text-dark-75">@lang('project.label.employee_name')</span>
                                </th>
                                <th style="min-width: 120px" class="text-center">@lang('project.label.employee_position')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($model->workers as $worker)
                            <tr>
                                <td class="py-2">
                                    <div class="d-flex align-items-center">
                                        <div>
                                            <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{ $worker->name }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    {{ $positions[$worker->pivot->position_id]->name }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end::Table-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Advance Table Widget 3-->
    </div>

    <div class="col-lg-6">
        <!--begin::Advance Table Widget 3-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 py-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('project_vendor.label.vendor_id')</span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">@lang('project_vendor.message.list_data')</span>
                </h3>
                <div class="card-toolbar">
                    @hasPermission('project.project_vendor.create')
                    <a href="{{ route('project.project_vendor.create', ['project_id' => $model->id]) }}" class="btn btn-success font-weight-bolder font-size-sm">
                    <span class="svg-icon svg-icon-md svg-icon-white">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon points="0 0 24 0 24 24 0 24" />
                                <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>@lang('global.add') @lang('project_vendor.label.vendor_id')</a>
                    @endhasPermission
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-0 pb-3">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                            <tr class="text-uppercase">
                                <th style="min-width: 250px" class="">
                                    <span class="text-dark-75">@lang('project_vendor.label.vendor_id')</span>
                                </th>
                                <th style="min-width: 120px">@lang('global.action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($model->vendors as $vendor)
                            <tr>
                                <td class="pl-0 py-2">
                                    <div class="d-flex align-items-center">
                                        <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{ $vendor->vendor->name }}</span>
                                    </div>
                                </td>
                                <td class="text-right pr-0">
                                    @hasPermission('project.project_vendor.edit')
                                    <a href="{{ route('project.project_vendor.edit', $vendor->id) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('project.project_vendor.show')
                                    <a href="{{ route('project.project_vendor.show', $vendor->id) }}"  class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon-eye"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('project.project_vendor.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('project.project_vendor.destroy', $vendor->id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </button>
                                    @endhasPermission
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end::Table-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Advance Table Widget 3-->
    </div>
</div>
<!--end::Row-->

<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('project_payment.title')</span>
                </h3>

                <div class="card-toolbar">
                    @hasPermission('project.project_payment.index')
                    <a href="{{ route('project.project_payment.create', $model->id) }}" class="btn btn-success font-weight-bolder font-size-sm">
                    <span class="svg-icon svg-icon-md svg-icon-white">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Add-user.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M15.9497475,3.80761184 L13.0246125,6.73274681 C12.2435639,7.51379539 12.2435639,8.78012535 13.0246125,9.56117394 L14.4388261,10.9753875 C15.2198746,11.7564361 16.4862046,11.7564361 17.2672532,10.9753875 L20.1923882,8.05025253 C20.7341101,10.0447871 20.2295941,12.2556873 18.674559,13.8107223 C16.8453326,15.6399488 14.1085592,16.0155296 11.8839934,14.9444337 L6.75735931,20.0710678 C5.97631073,20.8521164 4.70998077,20.8521164 3.92893219,20.0710678 C3.1478836,19.2900192 3.1478836,18.0236893 3.92893219,17.2426407 L9.05556629,12.1160066 C7.98447038,9.89144078 8.36005124,7.15466739 10.1892777,5.32544095 C11.7443127,3.77040588 13.9552129,3.26588995 15.9497475,3.80761184 Z" fill="#000000"/>
                                <path d="M16.6568542,5.92893219 L18.0710678,7.34314575 C18.4615921,7.73367004 18.4615921,8.36683502 18.0710678,8.75735931 L16.6913928,10.1370344 C16.3008685,10.5275587 15.6677035,10.5275587 15.2771792,10.1370344 L13.8629656,8.7228208 C13.4724413,8.33229651 13.4724413,7.69913153 13.8629656,7.30860724 L15.2426407,5.92893219 C15.633165,5.5384079 16.26633,5.5384079 16.6568542,5.92893219 Z" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>@lang('project_payment.label.pay')</a>
                    @endhasPermission
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-4">
                <table class="w-100 table table-head-custom table-head-bg table-borderless table-vertical-center">
                    <thead>
                        <tr>
                            <td class="font-weight-bold text-muted">@lang('project_payment.label.date')</td>
                            <td class="font-weight-bold">@lang('project_payment.label.title')</td>
                            <td class="font-weight-bold text-center">@lang('project_payment.label.employee_id')</td>
                            <td class="font-weight-bold text-right">@lang('project_payment.label.amount')</td>
                            <td class="font-weight-bold text-center">@lang('global.action')</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($model->payments as $payment)
                        <tr>
                            <td class="font-weight-bold text-muted">{{ $util->formatDate($payment->date) }}</td>
                            <td class="font-weight-bold">{{ $payment->title }}</td>
                            <td class="font-weight-bold text-center">
                                {{ $payment->employee->name }}
                            </td>
                            <td class="font-weight-bold text-right">
                                {{ $util->format_currency($payment->amount) }}
                            </td>
                            <td class="font-weight-bold text-center">
                                @hasPermission('project.project_payment.edit')
                                <a href="{{ route('project.project_payment.edit', $payment->id) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                    <i class="icon-md flaticon2-pen"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('project.project_payment.show')
                                <a title="@lang('global.show_detail')" href="{{ route('project.project_payment.show', $payment->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon-eye"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('project.project_payment.destroy')
                                <a data-href="{{ route('project.project_payment.destroy', $payment->id) }}" class="btn btn-icon btn-outline-secondary mr-2" title="@lang('global.delete')" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-trash"></i>
                                </a>
                                @endhasPermission
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Belanja</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="{{ route('project.project_shop.create', ['project_id' => $model->id]) }}" class="btn btn-primary font-weight-bolder">
                        Tambah Belanjaan
                    </a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <livewire:table.project-shop-table :projectId="$model->id" />
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>

<div class="row mt-5">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Pengeluaran Lain-lain</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="{{ route('finance.expend.create', ['_pid' => $model->id]) }}" class="btn btn-primary font-weight-bolder">
                        Tambah Pengeluaran
                    </a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <livewire:table.project-expend-table :projectId="$model->id" />
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

@push('js')
<script>
"use strict";
// Class definition

var KTDatatableHtmlTableDemo = function() {
    // Private functions

    // demo initializer
    var demo = function() {

		var datatable = $('#kt_datatable_belanja').KTDatatable({
			data: {
				saveState: {cookie: false},
			},
			search: {
				input: $('#kt_datatable_belanja_search_query'),
				key: 'generalSearch'
			},
			columns: [
                {
					field: 'Status',
					title: 'Status',
					autoHide: false,
					// callback function support for column rendering
					template: function(row) {
						var status = {
							0: {
                                'title': 'Belum Lunas',
                                'class': ' label-light-danger'
                            },
							1: {
                                'title': 'Lunas',
                                'class': ' label-light-success'
                            }
						};
						return '<span class="label font-weight-bold label-lg' + status[row.Status].class + ' label-inline">' + status[row.Status].title + '</span>';
					},
				}
            ],
		});

        $('#kt_datatable_belanja_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_belanja_search_status').selectpicker();
    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

var KTDatatablePengelauran = function() {
    // Private functions

    // demo initializer
    var demo = function() {

		var datatable = $('#kt_datatable_pengeluaran').KTDatatable({
			data: {
				saveState: {cookie: false},
			},
			search: {
				input: $('#kt_datatable_pengeluaran_search_query'),
				key: 'generalSearch'
			},
			columns: [
                {
					field: 'Status',
					title: 'Status',
					// autoHide: false,
					// callback function support for column rendering
					template: function(row) {
						var status = {
							0: {
                                'title': 'Belum Lunas',
                                'class': ' label-light-danger'
                            },
							1: {
                                'title': 'Lunas',
                                'class': ' label-light-success'
                            }
						};
						return '<span class="label font-weight-bold label-lg' + status[row.Status].class + ' label-inline">' + status[row.Status].title + '</span>';
					},
				}
            ],
		});

        $('#kt_datatable_pengeluaran_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_pengeluaran_search_status').selectpicker();
    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function() {
	KTDatatableHtmlTableDemo.init();
    KTDatatablePengelauran.init();
});
</script>
@endpush

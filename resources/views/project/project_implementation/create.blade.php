{{-- @include('project.project.form') --}}
@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('project.project_implementation.store') }}">
    @csrf

    @include('project.project_implementation._form')
</form>
<!--end::Form-->
@endsection

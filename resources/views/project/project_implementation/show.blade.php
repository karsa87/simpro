@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <div class="card-body">
                @include('project.project.section.project_detail', ['model' => $model])
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">@lang('project_implementation.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('project.project_implementation.create')
                    <!--begin::Button-->
                    <a href="{{ route('project.project_implementation.create') }}" class="btn btn-primary font-weight-bolder">
                        @lang('global.add')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin::Search Form-->
                <div class="mb-7">
                    <form id="form-search-project-implementation" role="form" action="{{ route('project.project_implementation.show', $model->id) }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." name="_k" value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->
                <!--begin: Datatable--><table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-2">
                                @sortablelink('project.no_project', trans('project_implementation.label.project_id'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-4">@lang('project_implementation.label.project_job_id')</th>
                            <th class="col-2">
                                @sortablelink('date', trans('project_implementation.label.date'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">@lang('project_implementation.label.date')</th>
                            <th class="col-2">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($projectImplementations as $projectImplementation)
                            <tr>
                                <td>{{ $projectImplementation->project->no_project_company }}</td>
                                <td>{{ $projectImplementation->projectJob->job }}</td>
                                <td>{{ $util->formatDate($projectImplementation->date, 'Y-m-d') }}</td>
                                <td>
                                    @php
                                        $progress = $projectImplementation->progress;
                                    @endphp
                                    <div class="progress progress-xs mt-2 mb-2">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: {{$progress}}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="font-weight-bolder text-dark">{{$progress}}%</span>
                                </td>
                                <td>
                                    @hasPermission('project.project_implementation.edit')
                                    <a title="@lang('global.edit')" href="{{ route('project.project_implementation.edit', $projectImplementation->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('project.project_implementation.show_implementation')
                                    <a title="@lang('global.show_detail')" href="{{ route('project.project_implementation.show_implementation', $projectImplementation->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                        <i class="icon-md flaticon-eye"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('project.project_implementation.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('project.project_implementation.destroy', $projectImplementation->id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </button>
                                    @endhasPermission
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($projectImplementations, 'form-search-project-implementation') !!}
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

@push('js')
@endpush

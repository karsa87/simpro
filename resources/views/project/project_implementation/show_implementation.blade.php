@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('global.detail') @lang('project_implementation.title')</span>
                </h3>
                <div class="card-toolbar">
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">
                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ki ki-bold-more-hor"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover py-5">
                                @hasPermission('project.project_implementation.create')
                                <li class="navi-item">
                                    <a href="{{ route('project.project_implementation.create', ['project_id'=>$model->project_id]) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-add-1"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.new')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('project.project_implementation.edit')
                                <li class="navi-item">
                                    <a href="{{ route('project.project_implementation.edit', $model->id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-pen"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.edit')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('project.project_implementation.destroy')
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <a data-href="{{ route('project.project_implementation.destroy', $model->id) }}" data-redirect="{{ route('project.project_implementation.index') }}" class="navi-link btn-delete">
                                        <span class="navi-icon">
                                            <i class="flaticon2-trash"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.delete')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('project.project_implementation.index')
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <a href="{{ route('project.project_implementation.index') }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-left-arrow-1"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.back')</span>
                                    </a>
                                </li>
                                @endhasPermission
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Header-->
            <div class="card-body">
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('project_implementation.label.project_id')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->project->no_project }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('project_implementation.label.project_job_id')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->projectJob->job }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('project_implementation.label.date')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $util->formatDate($model->date, 'Y-m-d') }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('project_implementation.label.progress')</label>
                    <div class="col-8">
                        @php
                            $progress = $model->progress;
                        @endphp
                        <div class="progress progress-xs mt-2 mb-2">
                            <div class="progress-bar bg-success" role="progressbar" style="width: {{$progress}}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span class="font-weight-bolder text-dark">{{$progress}}%</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('project_implementation.label.information')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->information }}</span>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    <div class="col-sm-12 col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('file.title')</span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">@lang('global.list')</span>
                </h3>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-4">
                @include('master.file.component.list_file', ['files' => $model->files ?? []])
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

@push('js')
@endpush

{{-- @include('project.project.form') --}}
@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('project.project_implementation.update', $model->id) }}">
    @csrf
    @method('put')

    @include('project.project_implementation._form')
</form>
<!--end::Form-->
@endsection

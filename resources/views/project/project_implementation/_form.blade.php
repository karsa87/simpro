<div class="row">
    <div class="col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.form') @lang('project_implementation.title')</h3>

                    <select name="project_id" id="form-project_id" class="form-control ajax-select2 @classInpError('project_id')" data-url="{{ route('ajax.project') }}" data-placeholder="@lang('global.select')" style="width: 100%" {{ $model->project_id ? 'disabled' : '' }}>
                        @if (old('project_id') || $model->project_id)
                        @php
                            $model->project_id = old('project_id') ?? $model->project_id;
                        @endphp
                        <option value="{{ $model->project_id }}" selected>
                            {{ $model->project->no_project_company }}
                        </option>
                        @endif
                    </select>

                    @inpSpanError(['column'=>'project_id'])

                    @if ($model->project_id)
                        <input type="hidden" name="project_id" value="{{ $model->project_id }}" />
                    @endif
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('project.project_implementation.index') }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_implementation.label.project_job_id')</label>
                    <div class="col-lg-7">
                        <select class="form-control w-100 ajax-select2 @classInpError('project_job_id')" id="form-project_job_id" name="project_job_id" data-url="{{ route('ajax.project.jobs') }}" data-placeholder="@lang('global.select')">
                            @if (old('project_job_id') || $model->project_job_id)
                            @php
                                $model->project_job_id = old('project_job_id') ?? $model->project_job_id;
                            @endphp
                            <option value="{{ $model->project_job_id }}" selected>
                                {{ $model->projectJob->job }}
                            </option>
                            @endif
                        </select>

                        @inpSpanError(['column'=>'project_job_id'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_implementation.label.date')</label>
                    <div class="col-lg-7">
                        <div class="input-group date">
                            <input type="text" class="form-control datepicker @classInpError('date')" readonly="readonly" value="{{ date('Y-m-d', strtotime(old('date', $model->date ?? now()))) }}" name="date" />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar glyphicon-th"></i>
                                </span>
                            </div>

                            @inpSpanError(['column'=>'date'])
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_implementation.label.progress')</label>
                    <div class="col-lg-7">
                        <input type="number" min="1" max="100" class="form-control @classInpError('progress')" value="{{ old('progress', $model->progress) }}" name="progress" />
                        <span class="form-text text-muted">@lang('project_implementation.message.desc_progress')</span>

                        @inpSpanError(['column'=>'progress'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('project_implementation.label.information')</label>
                    <div class="col-lg-7">
                        <textarea class="form-control" name="information">{{ old('information', $model->information) }}</textarea>

                        @inpSpanError(['column'=>'information'])
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    <div class="col-lg-6">
        @livewire('file.file-form', [
            'files' => old('files', $files)
        ])
    </div>
</div>

@push('js')
<script>
    $('#form-project_id').change(function() {
        var project_id = $(this).val();
        var url = `{{ route('ajax.project.jobs') }}?pid=${project_id}`;
        $('#form-project_job_id').attr('data-url', url);
    });
</script>
@endpush

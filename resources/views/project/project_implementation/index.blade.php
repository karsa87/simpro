@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">@lang('project_implementation.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('project.project_implementation.create')
                    <!--begin::Button-->
                    <a href="{{ route('project.project_implementation.create') }}" class="btn btn-primary font-weight-bolder">
                        @lang('global.add')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <!--begin::Search Form-->
                <div class="mb-7">
                    <form id="form-search-project-implementation" role="form" action="{{ route(request()->route()->getName()) }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." name="_k" value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->
                <!--end: Search Form-->
                <!--begin: Datatable-->
                <table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-2">
                                @sortablelink('no_project', trans('project.label.no_project'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @sortablelink('no_spk', trans('project.label.no_spk'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @sortablelink('projectname', trans('project.label.projectname'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @sortablelink('customer_name', trans('project.label.customer_name'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @lang('project.label.progress')
                            </th>
                            <th class="col-2">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($projects as $project)
                        <tr>
                            <td>{{ $project->no_project }}</td>
                            <td>{{ $project->no_spk }}</td>
                            <td>{{ $project->projectname }}</td>
                            <td>{{ $project->customer_name }}</td>
                            <td>

                                @php
                                    $progress = $project->progress;
                                @endphp
                                <div class="progress progress-xs mt-2 mb-2">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{$progress}}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="font-weight-bolder text-dark">{{$progress}}%</span>
                            </td>
                            <td>
                                @hasPermission('project.project_implementation.create')
                                <a title="@lang('global.add')" href="{{ route('project.project_implementation.create', ['_pid' => $project->id]) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-plus"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('project.project.show')
                                <a title="@lang('global.show_detail')" href="{{ route('project.project_implementation.show', $project->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon-eye"></i>
                                </a>
                                @endhasPermission
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($projects, 'form-search-project-implementation') !!}
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

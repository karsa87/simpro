@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">@lang('project.title') Simulasi</h3>
                </div>
            </div>
            <div class="card-body">
                @livewire('project.simulation.form')
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row" id="content-notification">
    <div class="col-xs-12 col-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('notification.title')</h3>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table projects" data-form="form-search-notification">
                    <thead>
                        <tr>
                            <th width="25%">@lang('notification.label.by')</th>
                            <th width="45%">@lang('notification.label.message')</th>
                            <th width="20%">@lang('notification.label.time')</th>
                            <th width="10%" class="text-center">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($notifications) && count($notifications) > 0 )
                        @foreach( $notifications as $i => $r)
                        @php
                            $is_read = $r->users->first()->pivot->is_read;
                        @endphp
                        <tr class="{{ $is_read ? '' : 'bg-lightblue disabled color-palette' }}">
                            <td class="">
                                {{ $r->createdBy ? $r->createdBy->name : '' }}
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    @switch($r->type)
                                        @case(1)
                                            <span class="badge badge-warning">
                                            @break
                                        @case(2)
                                            <span class="badge badge-danger">
                                            @break
                                        @default
                                            <span class="badge badge-success">
                                    @endswitch
                                        @lang("notification.list.type.$r->type")
                                    </span>
                                </h6>
                                <p class="m-0 text-sm">
                                    <span class="notifications-list-date">{{ $r->message }}</span>
                                </p>
                            </td>
                            <td class="text-wrap">
                                {{ $util->formatDate($r->created_at, session(config('system.session.active_company'))->format_date) }}
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a title="@lang('global.show')" href="{{ url($r->link) }}" class="btn btn-sm btn-primary btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-external-link-square-alt"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($notifications) && count($notifications) > 0 )
            {!! \App\Util\Base\Layout::paging($notifications) !!}
            @endif
        </div>
    </div>
</div>
@stop
@extends('layouts.main')

@section('body')
<div id="kt_header_mobile" class="header-mobile align-items-center header-mobile-fixed">
	<!--begin::Logo-->
	<a href="{{ route('dashboard') }}">
		<img alt="Logo" src="{{ asset('assets/images/logo.png') }}" class="h-25px app-sidebar-logo-default" />
	</a>
	<!--end::Logo-->
	<!--begin::Toolbar-->
	<div class="d-flex align-items-center">
		<!--begin::Aside Mobile Toggle-->
		<button class="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle">
			<span></span>
		</button>
		<!--end::Aside Mobile Toggle-->
		<!--begin::Topbar Mobile Toggle-->
		<button class="btn btn-hover-text-primary p-0 ml-2" id="kt_header_mobile_topbar_toggle">
			<span class="svg-icon svg-icon-xl">
				<!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<polygon points="0 0 24 0 24 24 0 24" />
						<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
						<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
					</g>
				</svg>
				<!--end::Svg Icon-->
			</span>
		</button>
		<!--end::Topbar Mobile Toggle-->
	</div>
	<!--end::Toolbar-->
</div>
<!--end::Header Mobile-->
<div class="d-flex flex-column flex-root">
	<!--begin::Page-->
	<div class="d-flex flex-row flex-column-fluid page">
		<!--begin::Aside-->
		<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
			<!--begin::Brand-->
			<div class="brand flex-column-auto" id="kt_brand">
				<!--begin::Logo-->
				<a href="{{ route('dashboard') }}" class="brand-logo">
					<img alt="Logo" src="{{ asset('assets/images/logo/logo_wikacell.png') }}" height="30" style="width: 25%;"/>
				</a>
				<!--end::Logo-->
				<!--begin::Toggle-->
				<button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
					<span class="svg-icon svg-icon svg-icon-xl">
						<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-left.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<polygon points="0 0 24 0 24 24 0 24" />
								<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
								<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
							</g>
						</svg>
						<!--end::Svg Icon-->
					</span>
				</button>
				<!--end::Toolbar-->
			</div>
			<!--end::Brand-->
			<!--begin::Aside Menu-->
			<div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
				<!--begin::Menu Container-->
				<div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
					<!--begin::Menu Nav-->
					<ul class="menu-nav">
						<li class="menu-item {{ (request()->is('/')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
							<a href="{{ route('dashboard') }}" class="menu-link">
								<span class="svg-icon menu-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Layers.svg-->
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<rect x="0" y="0" width="24" height="24"/>
											<path d="M3.95709826,8.41510662 L11.47855,3.81866389 C11.7986624,3.62303967 12.2013376,3.62303967 12.52145,3.81866389 L20.0429,8.41510557 C20.6374094,8.77841684 21,9.42493654 21,10.1216692 L21,19.0000642 C21,20.1046337 20.1045695,21.0000642 19,21.0000642 L4.99998155,21.0000673 C3.89541205,21.0000673 2.99998155,20.1046368 2.99998155,19.0000673 L2.99999828,10.1216672 C2.99999935,9.42493561 3.36258984,8.77841732 3.95709826,8.41510662 Z M10,13 C9.44771525,13 9,13.4477153 9,14 L9,17 C9,17.5522847 9.44771525,18 10,18 L14,18 C14.5522847,18 15,17.5522847 15,17 L15,14 C15,13.4477153 14.5522847,13 14,13 L10,13 Z" fill="#000000"/>
										</g>
									</svg>
									<!--end::Svg Icon-->
								</span>
								<span class="menu-text">Dashboard</span>
							</a>
						</li>

						<li class="menu-item menu-item-submenu {{ (request()->is('setting/*')) ? 'menu-item-open' : 'menu-item-close' }}" aria-haspopup="false" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Building.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24"/>
										<path d="M18.6225,9.75 L18.75,9.75 C19.9926407,9.75 21,10.7573593 21,12 C21,13.2426407 19.9926407,14.25 18.75,14.25 L18.6854912,14.249994 C18.4911876,14.250769 18.3158978,14.366855 18.2393549,14.5454486 C18.1556809,14.7351461 18.1942911,14.948087 18.3278301,15.0846699 L18.372535,15.129375 C18.7950334,15.5514036 19.03243,16.1240792 19.03243,16.72125 C19.03243,17.3184208 18.7950334,17.8910964 18.373125,18.312535 C17.9510964,18.7350334 17.3784208,18.97243 16.78125,18.97243 C16.1840792,18.97243 15.6114036,18.7350334 15.1896699,18.3128301 L15.1505513,18.2736469 C15.008087,18.1342911 14.7951461,18.0956809 14.6054486,18.1793549 C14.426855,18.2558978 14.310769,18.4311876 14.31,18.6225 L14.31,18.75 C14.31,19.9926407 13.3026407,21 12.06,21 C10.8173593,21 9.81,19.9926407 9.81,18.75 C9.80552409,18.4999185 9.67898539,18.3229986 9.44717599,18.2361469 C9.26485393,18.1556809 9.05191298,18.1942911 8.91533009,18.3278301 L8.870625,18.372535 C8.44859642,18.7950334 7.87592081,19.03243 7.27875,19.03243 C6.68157919,19.03243 6.10890358,18.7950334 5.68746499,18.373125 C5.26496665,17.9510964 5.02757002,17.3784208 5.02757002,16.78125 C5.02757002,16.1840792 5.26496665,15.6114036 5.68716991,15.1896699 L5.72635306,15.1505513 C5.86570889,15.008087 5.90431906,14.7951461 5.82064513,14.6054486 C5.74410223,14.426855 5.56881236,14.310769 5.3775,14.31 L5.25,14.31 C4.00735931,14.31 3,13.3026407 3,12.06 C3,10.8173593 4.00735931,9.81 5.25,9.81 C5.50008154,9.80552409 5.67700139,9.67898539 5.76385306,9.44717599 C5.84431906,9.26485393 5.80570889,9.05191298 5.67216991,8.91533009 L5.62746499,8.870625 C5.20496665,8.44859642 4.96757002,7.87592081 4.96757002,7.27875 C4.96757002,6.68157919 5.20496665,6.10890358 5.626875,5.68746499 C6.04890358,5.26496665 6.62157919,5.02757002 7.21875,5.02757002 C7.81592081,5.02757002 8.38859642,5.26496665 8.81033009,5.68716991 L8.84944872,5.72635306 C8.99191298,5.86570889 9.20485393,5.90431906 9.38717599,5.82385306 L9.49484664,5.80114977 C9.65041313,5.71688974 9.7492905,5.55401473 9.75,5.3775 L9.75,5.25 C9.75,4.00735931 10.7573593,3 12,3 C13.2426407,3 14.25,4.00735931 14.25,5.25 L14.249994,5.31450877 C14.250769,5.50881236 14.366855,5.68410223 14.552824,5.76385306 C14.7351461,5.84431906 14.948087,5.80570889 15.0846699,5.67216991 L15.129375,5.62746499 C15.5514036,5.20496665 16.1240792,4.96757002 16.72125,4.96757002 C17.3184208,4.96757002 17.8910964,5.20496665 18.312535,5.626875 C18.7350334,6.04890358 18.97243,6.62157919 18.97243,7.21875 C18.97243,7.81592081 18.7350334,8.38859642 18.3128301,8.81033009 L18.2736469,8.84944872 C18.1342911,8.99191298 18.0956809,9.20485393 18.1761469,9.38717599 L18.1988502,9.49484664 C18.2831103,9.65041313 18.4459853,9.7492905 18.6225,9.75 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
										<path d="M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>
									</g>
								</svg><!--end::Svg Icon--></span>
								<span class="menu-text">Setting</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu" kt-hidden-height="80" style="">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Setting</span>
										</span>
									</li>
                                    @if (auth()->user()->isDeveloper())
									<li class="menu-item {{ (request()->is('setting/menu*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('setting.menu.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Menu</span>
										</a>
									</li>
									<li class="menu-item {{ (request()->is('setting/permission*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('setting.permission.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Permission</span>
										</a>
									</li>
									<li class="menu-item {{ (request()->is('setting/lock-data*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('setting.lock_data.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Lock Data</span>
										</a>
									</li>
									<li class="menu-item {{ (request()->is('setting/format-number*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('setting.format_number.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Format Number</span>
										</a>
									</li>
                                    @endif
									<li class="menu-item {{ (request()->is('setting/role*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('setting.role.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Role</span>
										</a>
									</li>
									<li class="menu-item {{ (request()->is('setting/user*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('setting.user.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">User</span>
										</a>
									</li>
								</ul>
							</div>
						</li>

						<li class="menu-item menu-item-submenu {{ (request()->is('master/*')) ? 'menu-item-open' : 'menu-item-close' }}" aria-haspopup="false" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Building.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24"/>
										<path d="M4,7 L20,7 L20,19.5 C20,20.3284271 19.3284271,21 18.5,21 L5.5,21 C4.67157288,21 4,20.3284271 4,19.5 L4,7 Z M10,10 C9.44771525,10 9,10.4477153 9,11 C9,11.5522847 9.44771525,12 10,12 L14,12 C14.5522847,12 15,11.5522847 15,11 C15,10.4477153 14.5522847,10 14,10 L10,10 Z" fill="#000000"/>
										<rect fill="#000000" opacity="0.3" x="2" y="3" width="20" height="4" rx="1"/>
									</g>
								</svg><!--end::Svg Icon--></span>
								<span class="menu-text">Master</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu" kt-hidden-height="80" style="">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Master</span>
										</span>
									</li>
                                    @hasPermission('master.company.index')
									<li class="menu-item {{ (request()->is('master/company*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('master.company.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Company</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('master.employee.index')
									<li class="menu-item {{ (request()->is('master/employee*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('master.employee.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Employee</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('master.area.index')
									<li class="menu-item {{ (request()->is('master/area*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('master.area.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Area</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('master.position.index')
									<li class="menu-item {{ (request()->is('master/position*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('master.position.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Position</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('master.customer.index')
									<li class="menu-item {{ (request()->is('master/customer*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('master.customer.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Customer</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('master.vendor.index')
									<li class="menu-item {{ (request()->is('master/vendor*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('master.vendor.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Vendor</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('master.unit.index')
									<li class="menu-item {{ (request()->is('master/unit*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('master.unit.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Unit</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('master.material.index')
									<li class="menu-item {{ (request()->is('master/material*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('master.material.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Material</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('master.formula.index')
									<li class="menu-item {{ (request()->is('master/formula*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('master.formula.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Formula</span>
										</a>
									</li>
                                    @endhasPermission
								</ul>
							</div>
						</li>

						<li class="menu-item menu-item-submenu {{ (request()->is('project/*')) ? 'menu-item-open' : 'menu-item-close' }}" aria-haspopup="false" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Building.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24"/>
										<path d="M13.5,21 L13.5,18 C13.5,17.4477153 13.0522847,17 12.5,17 L11.5,17 C10.9477153,17 10.5,17.4477153 10.5,18 L10.5,21 L5,21 L5,4 C5,2.8954305 5.8954305,2 7,2 L17,2 C18.1045695,2 19,2.8954305 19,4 L19,21 L13.5,21 Z M9,4 C8.44771525,4 8,4.44771525 8,5 L8,6 C8,6.55228475 8.44771525,7 9,7 L10,7 C10.5522847,7 11,6.55228475 11,6 L11,5 C11,4.44771525 10.5522847,4 10,4 L9,4 Z M14,4 C13.4477153,4 13,4.44771525 13,5 L13,6 C13,6.55228475 13.4477153,7 14,7 L15,7 C15.5522847,7 16,6.55228475 16,6 L16,5 C16,4.44771525 15.5522847,4 15,4 L14,4 Z M9,8 C8.44771525,8 8,8.44771525 8,9 L8,10 C8,10.5522847 8.44771525,11 9,11 L10,11 C10.5522847,11 11,10.5522847 11,10 L11,9 C11,8.44771525 10.5522847,8 10,8 L9,8 Z M9,12 C8.44771525,12 8,12.4477153 8,13 L8,14 C8,14.5522847 8.44771525,15 9,15 L10,15 C10.5522847,15 11,14.5522847 11,14 L11,13 C11,12.4477153 10.5522847,12 10,12 L9,12 Z M14,12 C13.4477153,12 13,12.4477153 13,13 L13,14 C13,14.5522847 13.4477153,15 14,15 L15,15 C15.5522847,15 16,14.5522847 16,14 L16,13 C16,12.4477153 15.5522847,12 15,12 L14,12 Z" fill="#000000"/>
										<rect fill="#FFFFFF" x="13" y="8" width="3" height="3" rx="1"/>
										<path d="M4,21 L20,21 C20.5522847,21 21,21.4477153 21,22 L21,22.4 C21,22.7313708 20.7313708,23 20.4,23 L3.6,23 C3.26862915,23 3,22.7313708 3,22.4 L3,22 C3,21.4477153 3.44771525,21 4,21 Z" fill="#000000" opacity="0.3"/>
									</g>
								</svg><!--end::Svg Icon--></span>
								<span class="menu-text">PROYEK</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu" kt-hidden-height="80" style="">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Proyek</span>
										</span>
									</li>
									<li class="menu-item {{ (request()->is('project/simulation/*') || request()->is('project/simulation')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('project.simulation.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Simulation</span>
										</a>
									</li>

                                    @hasPermission('project.project_type.index')
									<li class="menu-item {{ (request()->is('project/project-type/*') || request()->is('project/project-type')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('project.project_type.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Proyek Tipe</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('project.project.create')
									<li class="menu-item {{ (request()->is('project/project/create*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('project.project.create') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Open / Create</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('project.project.index')
									<li class="menu-item {{ (request()->is('project/project*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('project.project.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Data Proyek Aktif</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('project.project_implementation.index')
									<li class="menu-item {{ (request()->is('project/project-implementation*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('project.project_implementation.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Pelaksanaan Proyek</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('project.project.index.done')
									<li class="menu-item {{ (request()->is('project/project/done')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('project.project.index.done') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Proyek Selesai</span>
										</a>
									</li>
                                    @endhasPermission
								</ul>
							</div>
						</li>

						<li class="menu-item menu-item-submenu {{ (request()->is('finance/*')) ? 'menu-item-open' : 'menu-item-close' }}" aria-haspopup="false" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Building.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24"/>
										<rect fill="#000000" opacity="0.3" x="2" y="5" width="20" height="14" rx="2"/>
										<rect fill="#000000" x="2" y="8" width="20" height="3"/>
										<rect fill="#000000" opacity="0.3" x="16" y="14" width="4" height="2" rx="1"/>
									</g>
								</svg><!--end::Svg Icon--></span>
								<span class="menu-text">FINANCE</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu" kt-hidden-height="80" style="">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Finance</span>
										</span>
									</li>
                                    @hasPermission('finance.category_expend.index')
									<li class="menu-item {{ (request()->is('finance/category-expend*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('finance.category_expend.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Kategori Pengeluaran</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('finance.expend.index')
									<li class="menu-item {{ (request()->is('finance/expend*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('finance.expend.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Pengeluaran</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('finance.incentive.index')
									<li class="menu-item {{ (request()->is('finance/incentive*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('finance.incentive.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Insentif</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('finance.debt_payment_po.index')
									<li class="menu-item {{ (request()->is('finance/debt-payment-po*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('finance.debt_payment_po.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Pelunasan Pembelian</span>
										</a>
									</li>
                                    @endhasPermission
								</ul>
							</div>
						</li>

						<li class="menu-item menu-item-submenu {{ (request()->is('arsip/*')) ? 'menu-item-open' : 'menu-item-close' }}" aria-haspopup="false" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Building.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24"/>
										<path d="M4.5,3 L19.5,3 C20.3284271,3 21,3.67157288 21,4.5 L21,19.5 C21,20.3284271 20.3284271,21 19.5,21 L4.5,21 C3.67157288,21 3,20.3284271 3,19.5 L3,4.5 C3,3.67157288 3.67157288,3 4.5,3 Z M8,5 C7.44771525,5 7,5.44771525 7,6 C7,6.55228475 7.44771525,7 8,7 L16,7 C16.5522847,7 17,6.55228475 17,6 C17,5.44771525 16.5522847,5 16,5 L8,5 Z" fill="#000000"/>
									</g>
								</svg><!--end::Svg Icon--></span>
								<span class="menu-text">ARSIP</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu" kt-hidden-height="80" style="">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Arsip</span>
										</span>
									</li>
                                    @hasPermission('arsip.arsip.create')
									<li class="menu-item {{ (request()->is('arsip/arsip/create*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('arsip.arsip.create') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Buat Arsip</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('arsip.arsip.index')
									<li class="menu-item {{ (request()->is('arsip/arsip*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('arsip.arsip.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Data Arsip</span>
										</a>
									</li>
                                    @endhasPermission
								</ul>
							</div>
						</li>

						<li class="menu-item menu-item-submenu {{ (request()->is('report/*')) ? 'menu-item-open' : 'menu-item-close' }}" aria-haspopup="false" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Building.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24"/>
										<path d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z" fill="#000000" fill-rule="nonzero"/>
										<path d="M8.7295372,14.6839411 C8.35180695,15.0868534 7.71897114,15.1072675 7.31605887,14.7295372 C6.9131466,14.3518069 6.89273254,13.7189711 7.2704628,13.3160589 L11.0204628,9.31605887 C11.3857725,8.92639521 11.9928179,8.89260288 12.3991193,9.23931335 L15.358855,11.7649545 L19.2151172,6.88035571 C19.5573373,6.44687693 20.1861655,6.37289714 20.6196443,6.71511723 C21.0531231,7.05733733 21.1271029,7.68616551 20.7848828,8.11964429 L16.2848828,13.8196443 C15.9333973,14.2648593 15.2823707,14.3288915 14.8508807,13.9606866 L11.8268294,11.3801628 L8.7295372,14.6839411 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
									</g>
								</svg><!--end::Svg Icon--></span>
								<span class="menu-text">LAPORAN</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu" kt-hidden-height="80" style="">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Laporan</span>
										</span>
									</li>
                                    @hasPermission('report.project.index')
									<li class="menu-item {{ (request()->is('report/project*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('report.project.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Laporan Proyek</span>
										</a>
									</li>
                                    @endhasPermission

                                    @hasPermission('report.project_expend.index')
									<li class="menu-item {{ (request()->is('report/project-expend*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('report.project_expend.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Laporan Belanja / Pengeluaran Proyek</span>
										</a>
									</li>
                                    @endhasPermission

									{{-- <li class="menu-item {{ (request()->is('report/expend*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('report.expend.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Laporan Pengeluaran</span>
										</a>
									</li> --}}

                                    @hasPermission('report.loss_profit.index')
									<li class="menu-item {{ (request()->is('report/loss-profit*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('report.loss_profit.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Laporan Buku Besar</span>
										</a>
									</li>
                                    @endhasPermission
								</ul>
							</div>
						</li>

						<li class="menu-item menu-item-submenu {{ (request()->is('log/*')) ? 'menu-item-open' : 'menu-item-close' }}" aria-haspopup="false" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Building.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24"/>
										<rect fill="#000000" x="4" y="5" width="16" height="3" rx="1.5"/>
										<path d="M5.5,15 L18.5,15 C19.3284271,15 20,15.6715729 20,16.5 C20,17.3284271 19.3284271,18 18.5,18 L5.5,18 C4.67157288,18 4,17.3284271 4,16.5 C4,15.6715729 4.67157288,15 5.5,15 Z M5.5,10 L12.5,10 C13.3284271,10 14,10.6715729 14,11.5 C14,12.3284271 13.3284271,13 12.5,13 L5.5,13 C4.67157288,13 4,12.3284271 4,11.5 C4,10.6715729 4.67157288,10 5.5,10 Z" fill="#000000" opacity="0.3"/>
									</g>
								</svg><!--end::Svg Icon--></span>
								<span class="menu-text">LOG</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu" kt-hidden-height="80" style="">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Log</span>
										</span>
									</li>
									<li class="menu-item {{ (request()->is('log/project*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('log.project.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Log Proyek</span>
										</a>
									</li>
								</ul>
							</div>
						</li>

                        @hasPermission('saldo.employee.index')
						<li class="menu-item menu-item-submenu {{ (request()->is('saldo/*')) ? 'menu-item-open' : 'menu-item-close' }}" aria-haspopup="false" data-menu-toggle="hover">
							<a href="javascript:;" class="menu-link menu-toggle">
								<span class="svg-icon menu-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Home\Building.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24"/>
										<path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z" fill="#000000" opacity="0.3" transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) "/>
										<path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z" fill="#000000"/>
									</g>
								</svg><!--end::Svg Icon--></span>
								<span class="menu-text">SALDO</span>
								<i class="menu-arrow"></i>
							</a>
							<div class="menu-submenu" kt-hidden-height="80" style="">
								<i class="menu-arrow"></i>
								<ul class="menu-subnav">
									<li class="menu-item menu-item-parent" aria-haspopup="true">
										<span class="menu-link">
											<span class="menu-text">Saldo</span>
										</span>
									</li>
									<li class="menu-item {{ (request()->is('saldo/employee*')) ? 'menu-item-active' : '' }}" aria-haspopup="true">
										<a href="{{ route('saldo.employee.index') }}" class="menu-link">
											<i class="menu-bullet menu-bullet-dot">
												<span></span>
											</i>
											<span class="menu-text">Saldo Karyawan</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
                        @endhasPermission
					</ul>
					<!--end::Menu Nav-->
				</div>
				<!--end::Menu Container-->
			</div>
			<!--end::Aside Menu-->
		</div>
		<!--end::Aside-->
		<!--begin::Wrapper-->
		<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
			<!--begin::Header-->
			<div id="kt_header" class="header header-fixed">
				<!--begin::Container-->
				<div class="container-fluid d-flex align-items-stretch justify-content-between">
					<div></div>
					<!--begin::Topbar-->
					<div class="topbar">
						<div class="dropdown">
							<!--begin::Toggle-->
							<div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="true">
								<div class="btn btn-icon btn-clean btn-dropdown btn-lg mr-1">
									<img class="h-20px w-20px rounded-sm" src="{{ asset('assets/images/svg/Building.svg') }}" alt="" />
								</div>
							</div>
							<!--end::Toggle-->
							<!--begin::Dropdown-->
							<div
								class="dropdown-menu p-0 m-0 dropdown-menu-anim-up dropdown-menu-sm dropdown-menu-right"
								x-placement="bottom-end"
								style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-117px, 65px, 0px);">
								<!--begin::Nav-->
								<ul class="navi navi-hover py-4">
									@foreach (session(config('system.session.list_company')) as $company)
									<li class="navi-item {{ session(config('system.session.active_company'))->id == $company->id ? 'active' : '' }}">
										<a href="{{ route('master.company.change.active', $company->id) }}" class="navi-link">
											<span class="symbol symbol-20 mr-3">
												<img src="{{ $company->logoUrl }}" alt="{{ $company->name }}" />
											</span>
											<span class="navi-text">{{ $company->name }}</span>
										</a>
									</li>
									@endforeach
								</ul>
								<!--end::Nav-->
							</div>
							<!--end::Dropdown-->
						</div>
						<div class="dropdown">
							<!--begin::Toggle-->
							<div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="true">
								<div class="btn btn-icon btn-clean btn-dropdown btn-lg mr-1">
									<img class="h-20px w-20px rounded-sm" src="{{ asset($locale['asset'] ?? 'assets/images/flags/226-united-states.svg') }}" alt="" />
								</div>
							</div>
							<!--end::Toggle-->
							<!--begin::Dropdown-->
							<div
								class="dropdown-menu p-0 m-0 dropdown-menu-anim-up dropdown-menu-sm dropdown-menu-right"
								x-placement="bottom-end"
								style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-117px, 65px, 0px);">
								<!--begin::Nav-->
								<ul class="navi navi-hover py-4">
									@foreach (trans('global.array.locale') as $slug => $language)
									<li class="navi-item {{ $language['slug'] == ($locale['slug'] ?? 'en') ? 'active' : '' }}">
										<a href="{{ route('change.language', $slug) }}" class="navi-link">
											<span class="symbol symbol-20 mr-3">
												<img src="{{ asset($language['asset']) }}" alt="{{ $language['label'] }}" />
											</span>
											<span class="navi-text">{{ $language['label'] }}</span>
										</a>
									</li>
									@endforeach
								</ul>
								<!--end::Nav-->
							</div>
							<!--end::Dropdown-->
						</div>
						<!--begin::User-->
						<div class="topbar-item">
							<div class="btn btn-icon w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
								<span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
								<span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{ \Auth::user() ? \Auth::user()->name : 'Guest' }}</span>
								<span class="symbol symbol-35 symbol-light-success">
									@if (\Auth::user())
										<div class="symbol-label" style="background-image:url('{{ \Auth::user() ? \Auth::user()->profilePhotoUrl : url('assets/media/users/300_21.jpg') }}')"></div>
									@else
										<span class="symbol-label font-size-h5 font-weight-bold">{{ \Auth::user() ? substr(\Auth::user()->name, 0, 1) : 'G' }}</span>
									@endif
								</span>
							</div>
						</div>
						<!--end::User-->
					</div>
					<!--end::Topbar-->
				</div>
				<!--end::Container-->
			</div>
			<!--end::Header-->
			<!--begin::Content-->
			<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
				<div class="d-flex flex-column-fluid">
					<!--begin::Container-->
					<div class="container">
						@yield('content')
					</div>
				</div>
			</div>
			<!--end::Content-->
		</div>
		<!--end::Wrapper-->
	</div>
	<!--end::Page-->
</div>
<!--end::Main-->
<!-- begin::User Panel-->
<div id="kt_quick_user" class="offcanvas offcanvas-right p-10">
	<!--begin::Header-->
	<div class="offcanvas-header d-flex align-items-center justify-content-between pb-5">
		<h3 class="font-weight-bold m-0">User Profile
		<a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_user_close">
			<i class="ki ki-close icon-xs text-muted"></i>
		</a>
	</div>
	<!--end::Header-->
	<!--begin::Content-->
	<div class="offcanvas-content pr-5 mr-n5">
		<!--begin::Header-->
		<div class="d-flex align-items-center mt-5">
			<div class="symbol symbol-100 mr-5">
				<div class="symbol-label" style="background-image:url('{{ \Auth::user() ? \Auth::user()->profilePhotoUrl : url('assets/media/users/300_21.jpg') }}')"></div>
				<i class="symbol-badge bg-success"></i>
			</div>
			<div class="d-flex flex-column">
				<a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">{{ \Auth::user() ? \Auth::user()->name : 'Guest' }}</a>
				<!-- <div class="text-muted mt-1">Application Developer</div> -->
				<div class="navi mt-2">
					<a href="#" class="navi-item">
						<span class="navi-link p-0 pb-2">
							<span class="navi-icon mr-1">
								<span class="svg-icon svg-icon-lg svg-icon-primary">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<rect x="0" y="0" width="24" height="24" />
											<path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
											<circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
										</g>
									</svg>
									<!--end::Svg Icon-->
								</span>
							</span>
							<span class="navi-text text-muted text-hover-primary">{{ \Auth::user() ? \Auth::user()->email : 'Guest' }}</span>
						</span>
					</a>
					<form method="POST" action="{{ route('logout') }}">
						@csrf

						<a href="{{ route('logout') }}" class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5" onclick="event.preventDefault(); this.closest('form').submit();">Sign Out</a>
					</form>
				</div>
			</div>
		</div>
		<!--end::Header-->
		<!--begin::Separator-->
		<div class="separator separator-dashed mt-8 mb-5"></div>
		<!--end::Separator-->
		<!--begin::Nav-->
		<div class="navi navi-spacer-x-0 p-0">
			<!--begin::Item-->
			<a href="{{ route('setting.user.profile') }}" class="navi-item">
				<div class="navi-link">
					<div class="symbol symbol-40 bg-light mr-3">
						<div class="symbol-label">
							<span class="svg-icon svg-icon-md svg-icon-success">
								<!--begin::Svg Icon | path:assets/media/svg/icons/General/Notification2.svg-->
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24" />
										<path d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z" fill="#000000" />
										<circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
									</g>
								</svg>
								<!--end::Svg Icon-->
							</span>
						</div>
					</div>
					<div class="navi-text">
						<div class="font-weight-bold">My Profile</div>
						<div class="text-muted">Account settings and more
						<span class="label label-light-danger label-inline font-weight-bold">update</span></div>
					</div>
				</div>
			</a>
			<!--end:Item-->
			{{-- <!--begin::Item-->
			<a href="custom/apps/user/profile-3.html" class="navi-item">
				<div class="navi-link">
					<div class="symbol symbol-40 bg-light mr-3">
						<div class="symbol-label">
							<span class="svg-icon svg-icon-md svg-icon-warning">
								<!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Chart-bar1.svg-->
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24" />
										<rect fill="#000000" opacity="0.3" x="12" y="4" width="3" height="13" rx="1.5" />
										<rect fill="#000000" opacity="0.3" x="7" y="9" width="3" height="8" rx="1.5" />
										<path d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z" fill="#000000" fill-rule="nonzero" />
										<rect fill="#000000" opacity="0.3" x="17" y="11" width="3" height="6" rx="1.5" />
									</g>
								</svg>
								<!--end::Svg Icon-->
							</span>
						</div>
					</div>
					<div class="navi-text">
						<div class="font-weight-bold">My Messages</div>
						<div class="text-muted">Inbox and tasks</div>
					</div>
				</div>
			</a>
			<!--end:Item-->
			<!--begin::Item-->
			<a href="custom/apps/user/profile-2.html" class="navi-item">
				<div class="navi-link">
					<div class="symbol symbol-40 bg-light mr-3">
						<div class="symbol-label">
							<span class="svg-icon svg-icon-md svg-icon-danger">
								<!--begin::Svg Icon | path:assets/media/svg/icons/Files/Selected-file.svg-->
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<polygon points="0 0 24 0 24 24 0 24" />
										<path d="M4.85714286,1 L11.7364114,1 C12.0910962,1 12.4343066,1.12568431 12.7051108,1.35473959 L17.4686994,5.3839416 C17.8056532,5.66894833 18,6.08787823 18,6.52920201 L18,19.0833333 C18,20.8738751 17.9795521,21 16.1428571,21 L4.85714286,21 C3.02044787,21 3,20.8738751 3,19.0833333 L3,2.91666667 C3,1.12612489 3.02044787,1 4.85714286,1 Z M8,12 C7.44771525,12 7,12.4477153 7,13 C7,13.5522847 7.44771525,14 8,14 L15,14 C15.5522847,14 16,13.5522847 16,13 C16,12.4477153 15.5522847,12 15,12 L8,12 Z M8,16 C7.44771525,16 7,16.4477153 7,17 C7,17.5522847 7.44771525,18 8,18 L11,18 C11.5522847,18 12,17.5522847 12,17 C12,16.4477153 11.5522847,16 11,16 L8,16 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
										<path d="M6.85714286,3 L14.7364114,3 C15.0910962,3 15.4343066,3.12568431 15.7051108,3.35473959 L20.4686994,7.3839416 C20.8056532,7.66894833 21,8.08787823 21,8.52920201 L21,21.0833333 C21,22.8738751 20.9795521,23 19.1428571,23 L6.85714286,23 C5.02044787,23 5,22.8738751 5,21.0833333 L5,4.91666667 C5,3.12612489 5.02044787,3 6.85714286,3 Z M8,12 C7.44771525,12 7,12.4477153 7,13 C7,13.5522847 7.44771525,14 8,14 L15,14 C15.5522847,14 16,13.5522847 16,13 C16,12.4477153 15.5522847,12 15,12 L8,12 Z M8,16 C7.44771525,16 7,16.4477153 7,17 C7,17.5522847 7.44771525,18 8,18 L11,18 C11.5522847,18 12,17.5522847 12,17 C12,16.4477153 11.5522847,16 11,16 L8,16 Z" fill="#000000" fill-rule="nonzero" />
									</g>
								</svg>
								<!--end::Svg Icon-->
							</span>
						</div>
					</div>
					<div class="navi-text">
						<div class="font-weight-bold">My Activities</div>
						<div class="text-muted">Logs and notifications</div>
					</div>
				</div>
			</a>
			<!--end:Item-->
			<!--begin::Item-->
			<a href="custom/apps/userprofile-1/overview.html" class="navi-item">
				<div class="navi-link">
					<div class="symbol symbol-40 bg-light mr-3">
						<div class="symbol-label">
							<span class="svg-icon svg-icon-md svg-icon-primary">
								<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-opened.svg-->
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24" />
										<path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z" fill="#000000" opacity="0.3" />
										<path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z" fill="#000000" />
									</g>
								</svg>
								<!--end::Svg Icon-->
							</span>
						</div>
					</div>
					<div class="navi-text">
						<div class="font-weight-bold">My Tasks</div>
						<div class="text-muted">latest tasks and projects</div>
					</div>
				</div>
			</a>
			<!--end:Item--> --}}
		</div>
		<!--end::Nav-->
	</div>
	<!--end::Content-->
</div>
<!-- end::User Panel-->
@endsection

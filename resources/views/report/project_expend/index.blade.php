@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

{{-- -	Data Proyek berjalan (Nama Proyek, PIC, Periode, Total Kontrak, Kontrak Yang telah Terbayar) --}}
@section('content')
<div class="row">
    <div class="col-lg-6 col-xxl-4">
        <!--begin::Mixed Widget 13-->
        <div class="card card-custom gutter-b">
            <!--begin::Beader-->
            <div class="card-header border-0 py-5">
                <h3 class="card-title font-weight-bolder">@lang('project_shop.title')</h3>
                <div class="card-toolbar"></div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body p-0 d-flex flex-column">
                <!--begin::Chart-->
                <div id="widget_shop_chart" style="height: 200px"></div>
                <!--end::Chart-->
                <!--begin::Stats-->
                <div class="card-spacer pt-5 bg-white flex-grow-1 card-rounded">
                    <!--begin::Row-->
                    <div class="row row-paddingless">
                        <div class="col mr-8">
                            <div class="font-size-sm text-muted font-weight-bold">@lang('project_shop.label.total_amount')</div>
                            <div class="font-size-sm text-muted font-weight-bold">@lang('project_shop.label.last_month')</div>
                            <div class="font-size-h4 font-weight-bolder">{{ $util->format_currency($projectShops['last_month']) }}</div>
                        </div>
                        <div class="col">
                            <div class="font-size-sm text-muted font-weight-bold">@lang('project_shop.label.total_amount')</div>
                            <div class="font-size-sm text-muted font-weight-bold">@lang('project_shop.label.this_month')</div>
                            <div class="font-size-h4 font-weight-bolder">{{ $util->format_currency($projectShops['this_month']) }}</div>
                        </div>
                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Stats-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Mixed Widget 13-->
    </div>

    <div class="col-lg-6 col-xxl-4">
        <!--begin::Mixed Widget 13-->
        <div class="card card-custom gutter-b">
            <!--begin::Beader-->
            <div class="card-header border-0 py-5">
                <h3 class="card-title font-weight-bolder">@lang('project.title') @lang('project.label.expend')</h3>
                <div class="card-toolbar"></div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body p-0 d-flex flex-column">
                <!--begin::Chart-->
                <div id="widget_expend_chart" style="height: 200px"></div>
                <!--end::Chart-->
                <!--begin::Stats-->
                <div class="card-spacer pt-5 bg-white flex-grow-1 card-rounded">
                    <!--begin::Row-->
                    <div class="row row-paddingless">
                        <div class="col mr-8">
                            <div class="font-size-sm text-muted font-weight-bold">@lang('expend.label.amount')</div>
                            <div class="font-size-sm text-muted font-weight-bold">@lang('project_shop.label.last_month')</div>
                            <div class="font-size-h4 font-weight-bolder">{{ $util->format_currency($expends['last_month']) }}</div>
                        </div>
                        <div class="col">
                            <div class="font-size-sm text-muted font-weight-bold">@lang('expend.label.amount')</div>
                            <div class="font-size-sm text-muted font-weight-bold">@lang('project_shop.label.this_month')</div>
                            <div class="font-size-h4 font-weight-bolder">{{ $util->format_currency($expends['this_month']) }}</div>
                        </div>
                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Stats-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Mixed Widget 13-->
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-5 pb-5 bg-primary">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.report') @lang('expend.title') / @lang('project_shop.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('report.project_expend.export')
                    <!--begin::Button-->
                    <a href="javascript:void(0)" data-href="{{ route('report.project_expend.export') }}" class="btn btn-transparent-white btn-sm font-weight-bolder px-5 btn-export">
                        @lang('global.export')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <!--begin::Search Form-->
                <div class="mb-7">
                    <form id="form-search-project" role="form" action="{{ route(request()->route()->getName()) }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" name="_k" value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="d-flex align-items-center">
                                            <label class="mr-3 mb-0 d-none d-md-block">@lang('project.label.status'):</label>
                                            <select class="form-control" id="kt_datatable_search_status" name="_sts">
                                                <option value="">All</option>
                                                @foreach (trans('project.list.status') as $id => $name)
                                                <option value="{{ $id }}" {{ is_numeric(request('_sts')) && $id == request('_sts') ? 'selected' : '' }}>{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="d-flex align-items-center">
                                            <label class="mr-3 mb-0 d-none d-md-block">@lang('project.label.customer_name'):</label>
                                            <select class="form-control" id="kt_datatable_search_cs" name="_c">
                                                <option value="">All</option>
                                                @foreach ($customers as $name)
                                                <option value="{{ $name }}" {{ $name == request('_c') ? 'selected' : '' }}>{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->
                <!--end: Search Form-->
                <!--begin: Datatable-->
                <table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-2">
                                @sortablelink('projectname', trans('project.label.projectname'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @sortablelink('customer_name', trans('project.label.customer_name'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @sortablelink('date', trans('project.label.start_date'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">@lang('global.total') @lang('project_shop.title')</th>
                            <th class="col-2">@lang('global.total') @lang('expend.title')</th>
                            <th class="col-2">
                                @sortablelink('status', trans('project.label.status'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($projects as $project)
                        <tr>
                            <td>
                                {{ $project->projectname }}
                                <p class="p-0 text-inverse-light text-xs">
                                    {{ $project->no_project }}
                                </p>
                            </td>
                            <td>{{ $project->customer_name }}</td>
                            <td>{{ sprintf('%s - %s', $util->formatDate($project->start_date, $util::FORMAT_DATE_ID_LONG), $util->formatDate($project->due_date, $util::FORMAT_DATE_ID_LONG)) }}</td>
                            <td>{{ $util->format_currency($project->shops->sum('total_amount')) }}</td>
                            <td>{{ $util->format_currency($project->expends->sum('amount')) }}</td>
                            <td>
                                <span class="label font-weight-bold label-lg label-light-@lang("project.list.status_class.$project->status") label-inline">
                                    @lang("project.list.status.$project->status")
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($projects, 'form-search-project') !!}
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

@push('js')
<script>
    $(function(){
        $(".btn-export").click(function() {
            window.open(`${$(this).data('href')}?${$.param($("#form-search-project").serializeArray())}`);
        });
    });
</script>
<script>
    var _initWidgetShop = function() {
        var element = document.getElementById("widget_shop_chart");
        var height = parseInt(KTUtil.css(element, 'height'));

        if (!element) {
            return;
        }

        var options = {
            series: [{
                name: 'Total Belanja',
                // data: [30, 25, 45, 30, 55, 55, 30, 25, 45, 30, 55, 55]
                data: JSON.parse('{{ json_encode(array_values($projectShops["chart"])) }}')
            }],
            chart: {
                type: 'area',
                height: height,
                toolbar: {
                    show: false
                },
                zoom: {
                    enabled: false
                },
                sparkline: {
                    enabled: true
                }
            },
            plotOptions: {},
            legend: {
                show: false
            },
            dataLabels: {
                enabled: false
            },
            fill: {
                type: 'solid',
                opacity: 1
            },
            stroke: {
                curve: 'smooth',
                show: true,
                width: 3,
                colors: [KTApp.getSettings()['colors']['theme']['base']['danger']]
            },
            xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false
                },
                labels: {
                    show: false,
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                },
                crosshairs: {
                    show: false,
                    position: 'front',
                    stroke: {
                        color: KTApp.getSettings()['colors']['gray']['gray-300'],
                        width: 1,
                        dashArray: 3
                    }
                },
                tooltip: {
                    enabled: true,
                    formatter: undefined,
                    offsetY: 0,
                    style: {
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            yaxis: {
                min: 0,
                // max: 60,
                labels: {
                    show: false,
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                hover: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0
                    }
                }
            },
            tooltip: {
                style: {
                    fontSize: '12px',
                    fontFamily: KTApp.getSettings()['font-family']
                },
                y: {
                    formatter: function(val) {
                        return "Rp. " + val + " million"
                    }
                }
            },
            colors: [KTApp.getSettings()['colors']['theme']['light']['danger']],
            markers: {
                colors: [KTApp.getSettings()['colors']['theme']['light']['danger']],
                strokeColor: [KTApp.getSettings()['colors']['theme']['base']['danger']],
                strokeWidth: 3
            }
        };

        var chart = new ApexCharts(element, options);
        chart.render();
    }

    var _initWidgetExpend = function() {
        var element = document.getElementById("widget_expend_chart");
        var height = parseInt(KTUtil.css(element, 'height'));

        if (!element) {
            return;
        }

        var options = {
            series: [{
                name: 'Total Pengeluaran',
                // data: [30, 25, 30, 55, 26, 30, 55, 45, 25, 45, 30, 55]
                data: JSON.parse('{{ json_encode(array_values($expends["chart"])) }}')
            }],
            chart: {
                type: 'area',
                height: height,
                toolbar: {
                    show: false
                },
                zoom: {
                    enabled: false
                },
                sparkline: {
                    enabled: true
                }
            },
            plotOptions: {},
            legend: {
                show: false
            },
            dataLabels: {
                enabled: false
            },
            fill: {
                type: 'solid',
                opacity: 1
            },
            stroke: {
                curve: 'smooth',
                show: true,
                width: 3,
                colors: [KTApp.getSettings()['colors']['theme']['base']['primary']]
            },
            xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false
                },
                labels: {
                    show: false,
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                },
                crosshairs: {
                    show: false,
                    position: 'front',
                    stroke: {
                        color: KTApp.getSettings()['colors']['gray']['gray-300'],
                        width: 1,
                        dashArray: 3
                    }
                },
                tooltip: {
                    enabled: true,
                    formatter: undefined,
                    offsetY: 0,
                    style: {
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            yaxis: {
                min: 0,
                // max: 60,
                labels: {
                    show: false,
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                hover: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0
                    }
                }
            },
            tooltip: {
                style: {
                    fontSize: '12px',
                    fontFamily: KTApp.getSettings()['font-family']
                },
                y: {
                    formatter: function(val) {
                        return "Rp. " + val + " million"
                    }
                }
            },
            colors: [KTApp.getSettings()['colors']['theme']['light']['primary']],
            markers: {
                colors: [KTApp.getSettings()['colors']['theme']['light']['primary']],
                strokeColor: [KTApp.getSettings()['colors']['theme']['base']['primary']],
                strokeWidth: 3
            }
        };

        var chart = new ApexCharts(element, options);
        chart.render();
    }
    $(document).ready(function() {
        _initWidgetShop();
        _initWidgetExpend();
    });
</script>
@endpush

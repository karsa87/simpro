@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

{{-- -	Data Proyek berjalan (Nama Proyek, PIC, Periode, Total Kontrak, Kontrak Yang telah Terbayar) --}}
@section('content')
<div class="row">
    <div class="col-lg-12 col-xxl-4">
        <!--begin::Mixed Widget 4-->
        <div class="card card-custom bg-radial-gradient-danger gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 py-5">
                <h3 class="card-title font-weight-bolder text-white">@lang('project.title')</h3>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body d-flex flex-column p-0">
                <!--begin::Chart-->
                <div id="widget_project_chart" style="height: 200px"></div>
                <!--end::Chart-->
                <!--begin::Stats-->
                <div class="card-spacer pt-5 bg-white flex-grow-1 card-rounded">
                    <!--begin::Row-->
                    <div class="row m-0">
                        <div class="col mr-8">
                            <div class="font-size-sm text-muted font-weight-bold">@lang('project.label.project_done')</div>
                            <div class="font-size-h4 font-weight-bolder">{{ $util->format_currency($project['done']) }}</div>
                        </div>
                        <div class="col">
                            <div class="font-size-sm text-muted font-weight-bold">@lang('project.label.project_run')</div>
                            <div class="font-size-h4 font-weight-bolder">{{ $util->format_currency($project['run']) }}</div>
                        </div>
                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Stats-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Mixed Widget 4-->
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-5 pb-5 bg-primary">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.report') @lang('project.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('report.project_expend.export')
                    <!--begin::Button-->
                    <a href="javascript:void(0)" data-href="{{ route('report.project.export') }}" class="btn btn-transparent-white btn-sm font-weight-bolder px-5 btn-export">
                        @lang('global.export')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <!--begin::Search Form-->
                <div class="mb-7">
                    <form id="form-search-project" role="form" action="{{ route(request()->route()->getName()) }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" name="_k" value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="d-flex align-items-center">
                                            <label class="mr-3 mb-0 d-none d-md-block">@lang('project.label.status'):</label>
                                            <select class="form-control" id="kt_datatable_search_status" name="_sts">
                                                <option value="">All</option>
                                                @foreach (trans('project.list.status') as $id => $name)
                                                <option value="{{ $id }}" {{ is_numeric(request('_sts')) && $id == request('_sts') ? 'selected' : '' }}>{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="d-flex align-items-center">
                                            <label class="mr-3 mb-0 d-none d-md-block">@lang('project.label.customer_name'):</label>
                                            <select class="form-control" id="kt_datatable_search_cs" name="_c">
                                                <option value="">All</option>
                                                @foreach ($customers as $name)
                                                <option value="{{ $name }}" {{ $name == request('_c') ? 'selected' : '' }}>{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->
                <!--end: Search Form-->
                <!--begin: Datatable-->
                <table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-2">
                                @sortablelink('projectname', trans('project.label.projectname'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @sortablelink('customer_name', trans('project.label.customer_name'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @sortablelink('date', trans('project.label.start_date'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">
                                @sortablelink('total_amount', trans('project.label.total_amount'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-2">@lang('project_shop.label.paid')</th>
                            <th class="col-2">
                                @sortablelink('status', trans('project.label.status'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($projects as $_project)
                        <tr>
                            <td>
                                {{ $_project->projectname }}
                                <p class="p-0 text-inverse-light text-xs">
                                    {{ $_project->no_project }}
                                </p>
                            </td>
                            <td>{{ $_project->customer_name }}</td>
                            <td>{{ sprintf('%s - %s', $util->formatDate($_project->start_date, $util::FORMAT_DATE_ID_LONG), $util->formatDate($_project->due_date, $util::FORMAT_DATE_ID_LONG)) }}</td>
                            <td>{{ $util->format_currency($_project->total_amount) }}</td>
                            <td>{{ $util->format_currency($_project->payments->sum('amount')) }}</td>
                            <td>
                                <span class="label font-weight-bold label-lg label-light-@lang("project.list.status_class.$_project->status") label-inline">
                                    @lang("project.list.status.$_project->status")
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($projects, 'form-search-project') !!}
        </div>
        <!--end::Card-->

    </div>
</div>
@endsection

@push('js')
<script>
    $(function(){
        $(".btn-export").click(function() {
            window.open(`${$(this).data('href')}?${$.param($("#form-search-project").serializeArray())}`);
        });
    });
</script>
<script>
    $('#kt_datatable_search_status').change(function() {
        $('#form-search-project').submit();
    });
    $('#kt_datatable_search_cs').change(function() {
        $('#form-search-project').submit();
    });

    var _initWidgetProject = function() {
        var element = document.getElementById("widget_project_chart");
        var height = parseInt(KTUtil.css(element, 'height'));

        if (!element) {
            return;
        }

        var options = {
            series: [{
                name: 'Done',
                // data: [35, 65, 75, 55, 45, 60, 55, 35, 65, 75, 55, 45, 60, 55]
                data: ['{{ $project["done"] }}']
            }, {
                name: 'Run',
                // data: [50, 65, 60, 40, 70, 40, 70, 80, 50, 65, 60, 60, 80, 60]
                data: ['{{ $project["run"] }}']
            }],
            chart: {
                type: 'bar',
                height: height,
                toolbar: {
                    show: false
                },
                sparkline: {
                    enabled: true
                },
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: ['50%'],
                    endingShape: 'rounded'
                },
            },
            legend: {
                show: false
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 1,
                colors: ['transparent']
            },
            xaxis: {
                // categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                categories: ['Project'],
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false
                },
                labels: {
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            yaxis: {
                min: 0,
                // max: 100,
                labels: {
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            fill: {
                type: ['solid', 'solid'],
                opacity: [1, 1]
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                hover: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0
                    }
                }
            },
            tooltip: {
                style: {
                    fontSize: '12px',
                    fontFamily: KTApp.getSettings()['font-family']
                },
                y: {
                    formatter: function(val) {
                        return val
                    }
                },
                marker: {
                    show: false
                }
            },
            colors: ['#1BC5BD', '#FFF4DE'],
            grid: {
                borderColor: KTApp.getSettings()['colors']['gray']['gray-200'],
                strokeDashArray: 4,
                yaxis: {
                    lines: {
                        show: true
                    }
                },
                padding: {
                    left: 20,
                    right: 20
                }
            }
        };

        var chart = new ApexCharts(element, options);
        chart.render();
    }
    $(document).ready(function() {
        _initWidgetProject();
    });
</script>
@endpush

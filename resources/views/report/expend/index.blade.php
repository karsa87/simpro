@extends('layouts.admin')
{{-- -	Data Proyek berjalan (Nama Proyek, PIC, Periode, Total Kontrak, Kontrak Yang telah Terbayar) --}}
@section('content')
<div class="row">
    <div class="col-lg-12 col-xxl-4">
        <!--begin::Mixed Widget 4-->
        <div class="card card-custom bg-radial-gradient-danger gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 py-5">
                <h3 class="card-title font-weight-bolder text-white">Proyek</h3>
                <div class="card-toolbar">
                    <div class="dropdown dropdown-inline">
                        <a href="#" class="btn btn-text-white btn-hover-white btn-sm btn-icon border-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ki ki-bold-more-hor"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover">
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-writing"></i>
                                        </span>
                                        <span class="navi-text">Add New</span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-calendar-8"></i>
                                        </span>
                                        <span class="navi-text">List</span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-graph-1"></i>
                                        </span>
                                        <span class="navi-text">Report</span>
                                    </a>
                                </li>
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body d-flex flex-column p-0">
                <!--begin::Chart-->
                <div id="widget_project_chart" style="height: 200px"></div>
                <!--end::Chart-->
                <!--begin::Stats-->
                <div class="card-spacer pt-5 bg-white flex-grow-1 card-rounded">
                    <!--begin::Row-->
                    <div class="row m-0">
                        <div class="col mr-8">
                            <div class="font-size-sm text-muted font-weight-bold">Project Done</div>
                            <div class="font-size-h4 font-weight-bolder">10</div>
                        </div>
                        <div class="col">
                            <div class="font-size-sm text-muted font-weight-bold">Project Run</div>
                            <div class="font-size-h4 font-weight-bolder">15</div>
                        </div>
                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Stats-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Mixed Widget 4-->
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-5 pb-5 bg-primary">
                <div class="card-title">
                    <h3 class="card-label">Laporan Proyek</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="{{ route('report.project.export') }}" class="btn btn-transparent-white btn-sm font-weight-bolder px-5">
                        Export
                    </a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <!--begin::Search Form-->
                <div class="mb-7">
                    <div class="row align-items-center">
                        <div class="col-lg-12">
                            <div class="row align-items-center">
                                <div class="col-md-4 my-2 my-md-0">
                                    <div class="input-icon">
                                        <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" name="_k" value="{{ request('_k') }}" />
                                        <span>
                                            <i class="flaticon2-search-1 text-muted"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-4 my-2 my-md-0">
                                    <div class="d-flex align-items-center">
                                        <label class="mr-3 mb-0 d-none d-md-block">Status:</label>
                                        <select class="form-control" id="kt_datatable_search_status">
                                            <option value="">All</option>
                                            <option value="0">Run</option>
                                            <option value="1">Done</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 my-2 my-md-0">
                                    <div class="d-flex align-items-center">
                                        <label class="mr-3 mb-0 d-none d-md-block">PIC:</label>
                                        <select class="form-control" id="kt_datatable_search_type">
                                            <option value="">All</option>
                                            <option value="Wasis">Wasis</option>
                                            <option value="Indra">Indra</option>
                                            <option value="Kipli">Kipli</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Search Form-->
                <!--end: Search Form-->
                <!--begin: Datatable-->
                <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                    <thead>
                        <tr>
                            <th title="Field #1">Nama Proyek</th>
                            <th title="Field #2">PIC</th>
                            <th title="Field #3">Periode</th>
                            <th title="Field #4">Total</th>
                            <th title="Field #5">Terbayarkan</th>
                            <th title="Field #6">Status</th>
                            <th title="Field #7">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i < 20; $i++)
                            <tr>
                                <td>Proyek Pembanguan Apartemen {{ $i }}</td>
                                <td>Wasis</td>
                                <td>{{ date('d M Y', strtotime('-' . (random_int(1, 10)) . 'weeks')) }} - {{ date('d M Y', strtotime('+' . (random_int(1, 10)) . 'weeks')) }}</td>
                                <td>{{ number_format(random_int(1000000, 100000000000)) }}</td>
                                <td>{{ number_format(random_int(1000000, 100000000)) }}</td>
                                <td>{{ random_int(0, 1) }}</td>
                                <td>
                                    <a href="{{ route('report.project.show', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon-eye"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Proyek Pembanguan Perumahan Griyashanta {{ $i }}</td>
                                <td>Indra</td>
                                <td>{{ date('d M Y', strtotime('-' . (random_int(1, 10)) . 'weeks')) }} - {{ date('d M Y', strtotime('+' . (random_int(1, 10)) . 'weeks')) }}</td>
                                <td>{{ number_format(random_int(1000000, 100000000000)) }}</td>
                                <td>{{ number_format(random_int(1000000, 100000000)) }}</td>
                                <td>{{ random_int(0, 1) }}</td>
                                <td>
                                    <a href="{{ route('report.project.show', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon-eye"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Proyek Pembanguan Gorong - Gorong {{ $i }}</td>
                                <td>Kipli</td>
                                <td>{{ date('d M Y', strtotime('-' . (random_int(1, 10)) . 'weeks')) }} - {{ date('d M Y', strtotime('+' . (random_int(1, 10)) . 'weeks')) }}</td>
                                <td>{{ number_format(random_int(1000000, 100000000000)) }}</td>
                                <td>{{ number_format(random_int(1000000, 100000000)) }}</td>
                                <td>{{ random_int(0, 1) }}</td>
                                <td>
                                    <a href="{{ route('report.project.show', 1) }}" class="btn btn-icon btn-outline-secondary mr-2">
                                        <i class="icon-md flaticon-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endfor
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->

    </div>
</div>
@endsection

@push('js')
<script>
"use strict";
// Class definition

var KTDatatableHtmlTableDemo = function() {
    // Private functions

    // demo initializer
    var demo = function() {

		var datatable = $('#kt_datatable').KTDatatable({
			data: {
				saveState: {cookie: false},
			},
			search: {
				input: $('#kt_datatable_search_query'),
				key: 'generalSearch'
			},
			columns: [
				{
					field: 'DepositPaid',
					type: 'text',
				},
				{
					field: 'OrderDate',
					type: 'date',
					format: 'YYYY-MM-DD',
				},
                {
					field: 'Status',
					title: 'Status',
					autoHide: false,
					// callback function support for column rendering
					template: function(row) {
						var status = {
							0: {
                                'title': 'Run',
                                'class': ' label-light-warning'
                            },
							1: {
                                'title': 'Done',
                                'class': ' label-light-success'
                            }
						};
						return '<span class="label font-weight-bold label-lg' + status[row.Status].class + ' label-inline">' + status[row.Status].title + '</span>';
					},
				},
                {
					field: 'PIC',
					title: 'PIC',
				},
			],
		});

        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'PIC');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();

    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function() {
	KTDatatableHtmlTableDemo.init();
});
</script>


<script>
    var _initWidgetProject = function() {
        var element = document.getElementById("widget_project_chart");
        var height = parseInt(KTUtil.css(element, 'height'));

        if (!element) {
            return;
        }

        var options = {
            series: [{
                name: 'Done',
                data: [35, 65, 75, 55, 45, 60, 55, 35, 65, 75, 55, 45, 60, 55]
            }, {
                name: 'Run',
                data: [50, 65, 60, 40, 70, 40, 70, 80, 50, 65, 60, 60, 80, 60]
            }],
            chart: {
                type: 'bar',
                height: height,
                toolbar: {
                    show: false
                },
                sparkline: {
                    enabled: true
                },
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: ['50%'],
                    endingShape: 'rounded'
                },
            },
            legend: {
                show: false
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 1,
                colors: ['transparent']
            },
            xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false
                },
                labels: {
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            yaxis: {
                min: 0,
                max: 100,
                labels: {
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            fill: {
                type: ['solid', 'solid'],
                opacity: [1, 1]
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                hover: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0
                    }
                }
            },
            tooltip: {
                style: {
                    fontSize: '12px',
                    fontFamily: KTApp.getSettings()['font-family']
                },
                y: {
                    formatter: function(val) {
                        return val
                    }
                },
                marker: {
                    show: false
                }
            },
            colors: ['#1BC5BD', '#FFF4DE'],
            grid: {
                borderColor: KTApp.getSettings()['colors']['gray']['gray-200'],
                strokeDashArray: 4,
                yaxis: {
                    lines: {
                        show: true
                    }
                },
                padding: {
                    left: 20,
                    right: 20
                }
            }
        };

        var chart = new ApexCharts(element, options);
        chart.render();
    }
    $(document).ready(function() {
        _initWidgetProject();
    });
</script>
@endpush

@extends('layouts.admin')
{{-- -	Data Proyek berjalan (Nama Proyek, PIC, Periode, Total Kontrak, Kontrak Yang telah Terbayar) --}}
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header">
                <h3 class="card-title">
                    @lang('global.report') @lang('project.label.loss_profit')
                </h3>
            </div>
            <!--begin::Form-->
            <form id="form-search-loss-profit" class="form" action="{{ route('report.loss_profit.index') }}">
                <div class="card-body">
                    <div class="form-group form-group-last">
                        <div class="alert alert-custom alert-default" role="alert">
                            <div class="alert-icon"><i class="flaticon-warning text-primary"></i></div>
                            <div class="alert-text">@lang('project.message.desc_loss_profit')</div>
                        </div>
                    </div>
                    <div data-repeater-item="" class="form-group row align-items-center">
                        <div class="col-md-6">
                            <label>@lang('global.month')</label>
                            <select class="form-control form-control-solid" name="_m">
                                @foreach (trans('global.array.months') as $id => $name)
                                    <option value="{{ $id }}" {{ $id == request('_m', date('n')) ? 'selected' : '' }}>
                                        {{ $name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>@lang('global.year')</label>
                            <select class="form-control form-control-solid" name="_y">
                                @for ($i = 0; $i <= 20; $i++)
                                    @php
                                        $year = date('Y', strtotime('-' . $i . 'years'));
                                    @endphp
                                    <option value="{{ $year }}" {{ $year == request('_y', date('Y')) ? 'selected' : '' }}>{{ $year }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>

                    <div class="row mt-5">
                        <div class="col-lg-8">
                            <!--begin::Card-->
                            <div class="card card-custom gutter-b">
                                <!--begin::Header-->
                                <div class="card-header h-auto">
                                    <!--begin::Title-->
                                    <div class="card-title py-5">
                                        <h3 class="card-label">@lang('project.label.omzet')</h3>
                                    </div>
                                    <!--end::Title-->
                                </div>
                                <!--end::Header-->
                                <div class="card-body">
                                    <!--begin::Chart-->
                                    <div id="chart_omzet"></div>
                                    <!--end::Chart-->
                                </div>
                            </div>
                            <!--end::Card-->
                        </div>
                        <div class="col-lg-4">
                            <!--begin::Mixed Widget 4-->
                            <div class="card card-custom gutter-b">
                                <!--begin::Header-->
                                <div class="card-header border-0 py-5">
                                    <h3 class="card-title font-weight-bolder">@lang('project.label.income') @lang('global.and') @lang('project.label.expend')</h3>
                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <div class="card-body d-flex flex-column p-0">
                                    <!--begin::Chart-->
                                    <div id="chart_pie" class="d-flex justify-content-center"></div>
                                    <!--end::Chart-->
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::Mixed Widget 4-->
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-6">
                            <button type="reset" class="btn btn-secondary">@lang('global.cancel')</button>
                        </div>
                        <div class="col-sm-6 text-right">
                            <button type="submit" class="btn btn-success mr-2">@lang('global.search')</button>

                            {{-- @hasPermission('report.loss_profit.export')
                            <a href="javascript:void(0)" data-href="{{ route('report.loss_profit.export') }}" class="btn btn-primary mr-2 btn-export">
                                @lang('global.export')
                            </a>
                            @endhasPermission --}}
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(function(){
        $(".btn-export").click(function() {
            window.open(`${$(this).data('href')}?${$.param($("#form-search-loss-profit").serializeArray())}`);
        });
    });
</script>
<script>
// Shared Colors Definition
const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';
    var _pieChart = function () {
		const apexChart = "#chart_pie";
		var options = {
			// series: [44, 55, 13],
			series: [parseFloat('{{ $project_shops }}'), parseFloat('{{ $project_expends }}'), parseFloat('{{ array_sum($incomes) }}')],
			chart: {
				height: 350,
				width: 350,
				type: 'pie',
			},
			labels: ['Belanja', 'Pengeluaran Lain-Lain', 'Pemasukkan'],
            legend: {
                position: 'bottom'
            },
			responsive: [{
				breakpoint: 480,
				options: {
					chart: {
				        height: 350,
						width: 200
					},
                    legend: {
                        position: 'bottom'
                    },
				}
			}],
			colors: [primary, warning, success]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

    var _initWidgetProject = function() {
		const apexChart = "#chart_omzet";
		var options = {
			series: [{
				name: "Day",
				// data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
                data: JSON.parse('{{ json_encode(array_values($omzet)) }}'),
			}],
			chart: {
				height: 250,
				type: 'line',
				zoom: {
					enabled: false
				}
			},
			dataLabels: {
				enabled: false
			},
			stroke: {
				curve: 'straight'
			},
            legend: {
                position: 'bottom'
            },
			grid: {
				row: {
					colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
					opacity: 0.5
				},
			},
			xaxis: {
				// categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                categories: JSON.parse('{{ json_encode(array_keys($omzet)) }}'),
                title: {
                    text: 'Day'
                }
			},
			colors: [primary]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
    }
    $(document).ready(function() {
        _pieChart();
        _initWidgetProject();
    });
</script>
@endpush

<!-- form start -->
<form id="form-area" role="form" method="POST" action="{{ route('master.area.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="code">@lang('area.label.code')</label>
            <input type="text" name="code" class="form-control @classInpError('code')" placeholder="@lang('area.placeholder.code')" value="{{ old('code') }}">

            @inpSpanError(['column'=>'code'])
        </div>
        <div class="form-group">
            <label for="name">@lang('area.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('area.placeholder.name')" value="{{ old('name') }}">

            @inpSpanError(['column'=>'name'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.area.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>

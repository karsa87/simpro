<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.form') @lang('customer.title')</h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('master.customer.index') }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('customer.label.name')</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control @classInpError('name')" value="{{ old('name', $model->name) }}" name="name" placeholder="@lang('customer.placeholder.name')" />

                        @inpSpanError(['column'=>'name'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('customer.label.email')</label>
                    <div class="col-lg-7">
                        <input type="email" class="form-control @classInpError('email')" value="{{ old('email', $model->email) }}" name="email" placeholder="@lang('customer.placeholder.email')" />

                        @inpSpanError(['column'=>'email'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('customer.label.phone')</label>
                    <div class="col-lg-7">
                        <div class="input-group input-group-lg input-group-solid">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="la la-phone"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control form-control-lg form-control-solid @classInpError('phone')" name="phone" value="{{ old('phone', $model->phone) }}" placeholder="@lang('customer.placeholder.phone')" />

                            @inpSpanError(['column'=>'phone'])
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('customer.label.fax')</label>
                    <div class="col-lg-7">
                        <div class="input-group input-group-lg input-group-solid">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="la la-phone"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control form-control-lg form-control-solid @classInpError('fax')" name="fax" value="{{ old('fax', $model->fax) }}" placeholder="@lang('customer.placeholder.fax')" />

                            @inpSpanError(['column'=>'fax'])
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('customer.label.address')</label>
                    <div class="col-lg-7">
                        <textarea class="form-control @classInpError('address')" name="address" rows="3">{{ old('address', $model->address) }}</textarea>

                        @inpSpanError(['column'=>'address'])
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>

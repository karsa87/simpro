@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('master.customer.store') }}">
    @csrf

    @include('master.customer._form')
</form>
<!--end::Form-->
@endsection

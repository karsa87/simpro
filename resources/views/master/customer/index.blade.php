@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">@lang('customer.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('master.customer.create')
                    <!--begin::Button-->
                    <a href="{{ route('master.customer.create') }}" class="btn btn-primary font-weight-bolder">
                        @lang('global.add')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <div class="mb-7">
                    <form id="form-search-customer" role="form" action="{{ route('master.customer.index') }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." name="_k" value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->

                <!--begin: Datatable-->
                <table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-3 text-center">
                                @sortablelink('name', trans('customer.label.name'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-3">
                                @sortablelink('email', trans('customer.label.email'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-4">@lang('customer.label.address')</th>
                            <th class="col-2 text-center">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customers as $customer)
                        <tr>
                            <td>{{ $customer->name }}</td>
                            <td>{{ $customer->email }}</td>
                            <td class="text-wrap">
                                {{ $customer->address }}
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('customer.label.phone'):{{ $customer->phone }}</strong>
                                    </small>
                                </p>
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('customer.label.fax'):{{ $customer->fax }}</strong>
                                    </small>
                                </p>
                            </td>
                            <td class="text-center">
                                @hasPermission('master.customer.edit')
                                <a title="@lang('global.edit')" href="{{ route('master.customer.edit', $customer->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-pen"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('master.customer.show')
                                <a title="@lang('global.show_detail')" href="{{ route('master.customer.show', $customer->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon-eye"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('master.customer.show')
                                <button title="@lang('global.delete')" data-href="{{ route('master.customer.destroy', $customer->id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-trash"></i>
                                </button>
                                @endhasPermission
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($customers, 'form-search-customer') !!}
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

<!-- form start -->
<form id="form-unit" role="form" method="POST" action="{{ route('master.unit.store') }}">
    @csrf
    <input type="hidden" name="id" value="{{ old('id') }}">
    <div class="card-body">
        <div class="form-group">
            <label for="name">@lang('unit.label.name')</label>
            <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('unit.placeholder.name')" value="{{ old('name') }}">

            @inpSpanError(['column'=>'name'])
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <a href="{{ route('master.unit.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
        <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
    </div>
</form>

@extends('layouts.admin')

@section('content')
<div class="row" id="content-unit">
    <div class="col-xs-12 col-sm-12 col-lg-8">
        <div class="card card-primary card-custom card-outline">
            <div class="card-header flex-wrap pt-6 pb-0">
                <h3 class="card-title"> @lang('global.list') @lang('unit.title')</h3>
            </div>
            <div class="card-body table-responsive">
                <div class="mb-7">
                    <form id="form-search-unit" role="form" action="{{ route('master.unit.index') }}">
                        <div class="row align-items-center">
                            <div class="form-group col-lg-4">
                                <div class="input-group">
                                    <input type="text" name="_k" placeholder="@lang('global.keyword')" value="{{ request('_k') }}" class="form-control float-right" />
                                    <div class="input-group-append">
                                        <button type="submit" id="btn-search" class="btn btn-default"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th style="width: 5%;" class="text-center">@lang('global.no')</th>
                            <th style="width: 35%;" class="sorting-form" data-column="name">@lang('unit.label.name')</th>
                            <th style="width: 15%;">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($units) && count($units) > 0 )
                        @foreach( $units as $i => $r)
                        <tr>
                            <td class="text-center">{{ $units->firstItem() + $i }}</td>
                            <td class="text-wrap">{{ $r->name }}</td>
                            <td class="">
                                <div class="btn-group">
                                    @hasPermission('master.unit.edit')
                                    <a title="@lang('global.edit')" href="javascript:void(0)" data-href="{{ route('master.unit.edit', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-edit-unit btn-loader" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-pen"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.unit.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('master.unit.destroy', $r['id']) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                        <i class="icon-md flaticon2-trash"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($units) && count($units) > 0 )
            {!! \App\Util\Base\Layout::paging($units) !!}
            @endif
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-lg-4">
        <div class="card card-primary">
            <div class="card-header flex-wrap pt-6 pb-0">
                <h3 class="card-title">@lang('global.form') @lang('unit.title')</h3>
            </div>
            <!-- /.card-header -->
            @include( $template . 'master.unit.form')
        </div>
    </div>
</div>
@stop

@push('js')
    <script type="text/javascript" src="{{ asset('js/admin/master/unit.js') }}?{{ config('app.version') }}"></script>
@endpush

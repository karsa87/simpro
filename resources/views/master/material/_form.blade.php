<div class="row">
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.form') @lang('material.title')</h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('master.material.index') }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('material.label.name')</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control @classInpError('name')" value="{{ old('name', $model->name) }}" name="name" placeholder="@lang('material.placeholder.name')" />

                        @inpSpanError(['column'=>'name'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('material.label.description')</label>
                    <div class="col-lg-7">
                        <textarea class="form-control @classInpError('description')" name="description" rows="3">{{ old('description', $model->description) }}</textarea>

                        @inpSpanError(['column'=>'description'])
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                @livewire('material.select-unit', [
                    'units' => old('units', $units)
                ])
            </div>
        </div>
    </div>
</div>

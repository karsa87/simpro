@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('master.material.update', $model->id) }}">
    @csrf
    @method('put')
    <input type="hidden" name="id" value="{{ old('id', $model->id) }}" />

    @include('master.material._form')
</form>
<!--end::Form-->
@endsection

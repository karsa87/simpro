@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('master.material.store') }}">
    @csrf

    @include('master.material._form')
</form>
<!--end::Form-->
@endsection

@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">@lang('material.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('master.material.create')
                    <!--begin::Button-->
                    <a href="{{ route('master.material.create') }}" class="btn btn-primary font-weight-bolder">
                        @lang('global.add')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <div class="mb-7">
                    <form id="form-search-material" role="form" action="{{ route('master.material.index') }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." name="_k" value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->

                <!--begin: Datatable-->
                <table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-3 text-center">
                                @sortablelink('name', trans('material.label.name'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-3">@lang('material.label.description')</th>
                            <th class="col-4">@lang('material.label.unit')</th>
                            <th class="col-2 text-center">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($materials as $material)
                        <tr>
                            <td>{{ $material->name }}</td>
                            <td>{{ $material->description }}</td>
                            <td class="text-wrap">
                                <ul>
                                    @foreach ($material->units->take(5) as $unit)
                                        <li>{{ $util->format_currency($unit->pivot->price) }} / {{ $unit->pivot->qty }} {{ $unit->name }}</li>
                                    @endforeach
                                </ul>
                            </td>
                            <td class="text-center">
                                @hasPermission('master.material.edit')
                                <a title="@lang('global.edit')" href="{{ route('master.material.edit', $material->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-pen"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('master.material.show')
                                <a title="@lang('global.show_detail')" href="{{ route('master.material.show', $material->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon-eye"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('master.material.show')
                                <button title="@lang('global.delete')" data-href="{{ route('master.material.destroy', $material->id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-trash"></i>
                                </button>
                                @endhasPermission
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($materials, 'form-search-material') !!}
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

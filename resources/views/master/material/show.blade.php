@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-sm-12 col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('material.title')</span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">@lang('global.detail')</span>
                </h3>
                <div class="card-toolbar">
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">
                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ki ki-bold-more-hor"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover py-5">
                                @hasPermission('master.material.create')
                                <li class="navi-item">
                                    <a href="{{ route('master.material.create') }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-add-1"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.new')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('master.material.edit')
                                <li class="navi-item">
                                    <a href="{{ route('master.material.edit', $model->id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-pen"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.edit')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('master.material.destroy')
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <a data-href="{{ route('master.material.destroy', $model->id) }}" data-redirect="{{ route('master.material.index') }}" class="navi-link btn-delete">
                                        <span class="navi-icon">
                                            <i class="flaticon2-trash"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.delete')</span>
                                    </a>
                                </li>
                                @endhasPermission
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-4">
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('material.label.name')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->name }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('material.label.description')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->description }}</span>
                    </div>
                </div>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
    <div class="col-sm-12 col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('material.label.unit')</span>
                </h3>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-4">
                <table class="w-100 table table-head-custom table-head-bg table-borderless table-vertical-center">
                    <thead>
                        <tr>
                            <td class="font-weight-bold col-3 text-center">@lang('material.label.unit')</td>
                            <td class="font-weight-bold col-3 text-center">@lang('material.label.qty')</td>
                            <td class="font-weight-bold col-4 text-right">@lang('material.label.price')</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($model->units as $unit)
                        <tr>
                            <td class="align-top font-weight-bold text-center">{{ $unit->name }}</td>
                            <td class="align-top text-wrap text-center">{{ $unit->pivot->qty }}</td>
                            <td class="align-top text-wrap text-right">{{ $util->format_currency($unit->pivot->price) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

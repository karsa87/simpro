<div class="row">
    @foreach ($files as $file)
        <!--begin::Item-->
        <div class="mr-5 my-1 text-center {{ isset($column_class) ? $column_class : 'col-lg-3 col-sm-4 col-md-6 col-xs-12' }}">
            <!--begin::Symbol-->
            <div class="symbol mr-5 pt-1">
                <div class="symbol-label min-w-50px min-h-50px" style="background-image: url('{{ asset('assets/images/file-manager.jpg') }}');"></div>
            </div>
            <!--end::Symbol-->
            <!--begin::Info-->
            <div class="d-flex flex-column text-center">
                <!--begin::Action-->
                <span class="text-wrap">{{ $file->name }}</span>
                <div>
                    @hasPermission('master.file.download')
                    <a href="{{ $file->canDownload() ? route('master.file.download', $file->id) : $file->url }}" class="btn btn-light font-weight-bolder font-size-sm py-2" target="_blank">
                        @if ($file->canDownload())
                            @lang('global.download')
                        @else
                            @lang('global.preview')
                        @endif
                    </a>
                    @endhasPermission
                </div>
                <!--end::Action-->
            </div>
            <!--end::Info-->
        </div>
        <!--end::Item-->
    @endforeach
</div>

@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">@lang('formula.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('master.formula.create')
                    <!--begin::Button-->
                    <a href="{{ route('master.formula.create') }}" class="btn btn-primary font-weight-bolder">
                        @lang('global.add')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <div class="mb-7">
                    <form id="form-search-formula" role="form" action="{{ route('master.formula.index') }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." name="_k" value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->

                <!--begin: Datatable-->
                <table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-3 text-center">
                                @sortablelink('code', trans('formula.label.code'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-3 text-center">
                                @sortablelink('name', trans('formula.label.name'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-3">@lang('formula.label.total_amount')</th>
                            <th class="col-2 text-center">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($formulas as $formula)
                        <tr>
                            <td>{{ $formula->code }}</td>
                            <td>{{ $formula->name }}</td>
                            <td>{{ $util->format_currency($util->roundedThousand($formula->total_amount)) }}</td>
                            <td class="text-center">
                                @hasPermission('master.formula.edit')
                                <a title="@lang('global.edit')" href="{{ route('master.formula.edit', $formula->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-pen"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('master.formula.show')
                                <a title="@lang('global.show_detail')" href="{{ route('master.formula.show', $formula->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon-eye"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('master.formula.show')
                                <button title="@lang('global.delete')" data-href="{{ route('master.formula.destroy', $formula->id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-trash"></i>
                                </button>
                                @endhasPermission
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($formulas, 'form-search-formula') !!}
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

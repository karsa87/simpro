@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('master.formula.store') }}" onkeydown="return event.key != 'Enter';">
    @csrf

    @include('master.formula._form')
</form>
<!--end::Form-->
@endsection

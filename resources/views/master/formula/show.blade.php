@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-sm-12 col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('formula.title')</span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">@lang('global.detail')</span>
                </h3>
                <div class="card-toolbar">
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">
                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ki ki-bold-more-hor"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover py-5">
                                @hasPermission('master.formula.create')
                                <li class="navi-item">
                                    <a href="{{ route('master.formula.create') }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-add-1"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.new')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('master.formula.edit')
                                <li class="navi-item">
                                    <a href="{{ route('master.formula.edit', $model->id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-pen"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.edit')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('master.formula.destroy')
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <a data-href="{{ route('master.formula.destroy', $model->id) }}" data-redirect="{{ route('master.formula.index') }}" class="navi-link btn-delete">
                                        <span class="navi-icon">
                                            <i class="flaticon2-trash"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.delete')</span>
                                    </a>
                                </li>
                                @endhasPermission
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-4">
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('formula.label.code')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->code }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('formula.label.name')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->name }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('formula.label.description')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->description }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('formula.label.rounding')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $util->format_currency($util->roundedThousand($model->total_amount)) }}</span>
                    </div>
                </div>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
    <div class="col-sm-12 col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('formula.label.unit_id')</span>
                </h3>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-4">
                <table class="w-100 table table-head-custom table-head-bg table-borderless table-vertical-center">
                    <thead>
                        <tr>
                            <td class="font-weight-bold col-2 text-center">@lang('formula.label.coefficient')</td>
                            <td class="font-weight-bold col-2 text-center">@lang('formula.label.unit_id')</td>
                            <td class="font-weight-bold col-3 text-center">@lang('formula.label.material_id')</td>
                            <td class="font-weight-bold col-2 text-right">@lang('formula.label.amount')</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($model->formula_items as $item)
                        <tr>
                            <td class="align-top font-weight-bold text-center">{{ $item->coefficient }}</td>
                            <td class="align-top text-wrap text-center">{{ optional($item->unit)->name }}</td>
                            <td class="align-top text-wrap text-center">{{ optional($item->material)->name }}</td>
                            <td class="align-top text-wrap text-right">{{ $util->format_currency($item->amount) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-right" colspan="3">@lang('formula.label.subtotal')</th>
                            <th class="text-right">{{ $util->format_currency($model->subtotal) }}</th>
                        </tr>
                        <tr>
                            <th class="text-right" colspan="3">@lang('formula.label.tax')</th>
                            <th class="text-right">{{ $util->format_currency($model->tax_amount) }}</th>
                        </tr>
                        <tr>
                            <th class="text-right" colspan="3">@lang('formula.label.total')</th>
                            <th class="text-right">{{ $util->format_currency($model->total_amount) }}</th>
                        </tr>
                        <tr>
                            <th class="text-right" colspan="3">@lang('formula.label.rounding')</th>
                            <th class="text-right">{{ $util->format_currency($util->roundedThousand($model->total_amount)) }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

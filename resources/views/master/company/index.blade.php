@extends('layouts.admin')

@section('content')
<div class="row" id="content-company">
    <div class="col-xs-12 col-sm-12 col-lg-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"> @lang('global.list') @lang('company.title')</h3>

                <div class="card-tools">
                    <form id="form-search-company" role="form" action="{{ route('master.company.index') }}" class="form-inline">
                        <div class="form-group col-lg-8">
                            <div class="input-group">
                                <input type="text" name="_k" placeholder="@lang('global.keyword')" value="{{ request('_k') }}" class="form-control float-right" />
                                <div class="input-group-append">
                                    <button type="submit" id="btn-search" class="btn btn-default"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        @hasPermission('master.company.create')
                        <div class="col-lg-4">
                            <a href="{{ route('master.company.create') }}" class="btn btn-success float-right">@lang('global.add')</a>
                        </div>
                        @endhasPermission
                    </form>
                </div>
            </div>
            <div class="card-body table-responsive p-0">
                <table class="table table-striped projects sorting-table" data-form="form-search-company">
                    <thead>
                        <tr>
                            <th class="text-center">@lang('global.no')</th>
                            <th class="sorting-form" data-column="name">@lang('company.label.name')</th>
                            <th class="sorting-form" data-column="address">@lang('company.label.address')</th>
                            <th class="sorting-form" data-column="npwp">@lang('company.label.npwp')</th>
                            <th>@lang('company.label.director_id')</th>
                            <th>@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if( isset($companies) && count($companies) > 0 )
                        @foreach( $companies as $i => $r)
                        <tr>
                            <td class="text-center">{{ $companies->firstItem() + $i }}</td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->name }}
                                </h6>
                                @if (!auth()->user()->underAdministrator())
                                <p class="m-0">
                                    <small>
                                        <strong>@lang('global.id'):{{ $r->id }}</strong>
                                    </small>
                                </p>
                                @endif
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->address }}
                                </h6>
                                <p class="m-0">
                                    <span class="users-list-date">{{ $r->city }}, {{ $r->postal_code }}</span>
                                </p>
                                <p class="m-0">
                                    <span class="users-list-date">{{ $r->phone }}</span>
                                </p>
                            </td>
                            <td class="text-wrap">
                                <h6 class="mb-1">
                                    {{ $r->npwp }}
                                </h6>
                                <p class="m-0">
                                    <span class="users-list-date">{{ $r->license_number }}</span>
                                </p>
                            </td>
                            <td class="text-wrap">
                                {{ $r->director ? $r->director->name : '-' }}
                            </td>
                            <td class="">
                                <div class="btn-group">

                                    @hasPermission('master.company.show')
                                    <a title="@lang('global.show_detail')" href="{{ route('master.company.show', $r['id']) }}" class="btn btn-sm btn-info btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-eye m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.company.edit')
                                    <a title="@lang('global.edit')" href="{{ route('master.company.edit', $r['id']) }}" class="btn btn-sm btn-warning btn-loader" data-toggle="tooltip">
                                        <i class="fas fa-edit m-0"></i>
                                    </a>
                                    @endhasPermission

                                    @hasPermission('master.company.destroy')
                                    <button title="@lang('global.delete')" data-href="{{ route('master.company.destroy', $r['id']) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip">
                                        <i class="fa fa-trash m-0"></i>
                                    </button>
                                    @endhasPermission
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5" class="text-center">@lang('global.data_not_available')</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if( isset($companies) && count($companies) > 0 )
            {!! \App\Util\Base\Layout::paging($companies) !!}
            @endif
        </div>
    </div>
</div>
@stop

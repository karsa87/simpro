@extends('layouts.admin')

@inject('util', 'App\Util\Helpers\Util')

@section('content')
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-3">
        <!-- Profile Image -->
        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
                <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle" src="{{ $model->logoUrl }}" alt="{{ $model->name }}" />
                </div>

                <h3 class="profile-username text-center">{{ $model->name }}</h3>

                <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item"><b>@lang('company.label.email')</b> <a class="float-right">{{ $model->email }}</a></li>
                    <li class="list-group-item"><b>@lang('company.label.fax')</b> <a class="float-right">{{ $model->fax }}</a></li>
                    <li class="list-group-item"><b>@lang('company.label.phone')</b> <a class="float-right">{{ $model->phone }}</a></li>
                </ul>
                @hasPermission('master.company.edit')
                <a href="{{ route('master.company.edit', $model->id) }}" class="btn btn-warning btn-block"><b>@lang('global.edit')</b></a>
                @endhasPermission
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    <div class="col-sm-12 col-md-6 col-lg-9">
        <div class="card">
            <div class="card-header p-2">
                @lang('global.detail')
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-4">@lang('company.label.address')</dt>
                    <dd class="col-sm-8">{{ $model->address }}</dd>
                    <dt class="col-sm-4">@lang('company.label.city')</dt>
                    <dd class="col-sm-8">{{ $model->city }}</dd>
                    <dt class="col-sm-4">@lang('company.label.postal_code')</dt>
                    <dd class="col-sm-8">{{ $model->postal_code }}</dd>
                    <dt class="col-sm-4">@lang('company.label.npwp')</dt>
                    <dd class="col-sm-8">{{ $model->npwp }}</dd>
                    <dt class="col-sm-4">@lang('company.label.license_number')</dt>
                    <dd class="col-sm-8">{{ $model->license_number }}</dd>
                    <dt class="col-sm-4">@lang('company.label.director_id')</dt>
                    <dd class="col-sm-8">{{ $model->director ? $model->director->name : '-' }}</dd>
                    <dt class="col-sm-4">@lang('company.label.pic_payment')</dt>
                    <dd class="col-sm-8">{{ $model->picPayment ? $model->picPayment->name : '-' }}</dd>
                    <dt class="col-sm-4">@lang('company.label.pic_purchase')</dt>
                    <dd class="col-sm-8">{{ $model->picPurchase ? $model->picPurchase->name : '-' }}</dd>
                    <dt class="col-sm-4">@lang('company.label.pic_warehouse')</dt>
                    <dd class="col-sm-8">{{ $model->picWarehouse ? $model->picWarehouse->name : '-' }}</dd>
                    <dt class="col-sm-4">@lang('company.label.format_date')</dt>
                    <dd class="col-sm-8">@lang("company.list.format_date.$model->format_date")</dd>
                </dl>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="{{ route('master.company.index') }}" class="btn btn-danger btn-loader"><b>@lang('global.back')</b></a>

                @hasPermission('master.company.edit')
                <a href="{{ route('master.company.edit', $model->id) }}" class="btn btn-warning float-right btn-loader"><b>@lang('global.edit')</b></a>
                @endhasPermission
            </div>
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
@stop
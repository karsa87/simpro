@extends('layouts.admin')

@inject('userModel', '\App\Models\User')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form id="form-user" role="form" method="POST" action="{{ route('master.company.store') }}">
            @csrf
            <input type="hidden" name="id" value="{{ old('id', $model->id) }}">
            <div class="card card-warning">
                <div class="card-header">
                    <h3 class="card-title">@lang('global.form') @lang('company.title')</h3>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <input type="hidden" name="logo" id="dir-upload-logo" value="{{ old('logo', $model->logo) }}">
                                <div class="dropzone files-upload" id="files-upload" data-inp-id="dir-upload-logo" data-href="{{ route('upload.company.logo') }}" data-accept-file="image/jpeg,image/png,.png,.jpg,.jpeg"></div>
                            </div>
                            <div class="form-group">
                                <label for="name">@lang('company.label.name')</label>
                                <input type="text" name="name" class="form-control @classInpError('name')" placeholder="@lang('company.placeholder.name')" value="{{ old('name', $model->name) }}">
                                
                                @inpSpanError(['column'=>'name'])
                            </div>
                            <div class="form-group">
                                <label for="email">@lang('company.label.email')</label>
                                <input type="email" name="email" class="form-control @classInpError('email')" placeholder="@lang('company.placeholder.email')" value="{{ old('email', $model->email) }}">
                                
                                @inpSpanError(['column'=>'email'])
                            </div>

                            <div class="row">
                                <div class="form-group col-lg-6 col-md-12">
                                    <label for="fax">@lang('company.label.fax')</label>
                                    <input type="text" name="fax" class="form-control @classInpError('fax')" placeholder="@lang('company.placeholder.fax')" value="{{ old('fax', $model->fax) }}">
                                    
                                    @inpSpanError(['column'=>'fax'])
                                </div>
                                <div class="form-group col-lg-6 col-md-12">
                                    <label for="phone">@lang('company.label.phone')</label>
                                    <input type="text" name="phone" class="form-control @classInpError('phone')" placeholder="@lang('company.placeholder.phone')" value="{{ old('phone', $model->phone) }}">
                                    
                                    @inpSpanError(['column'=>'phone'])
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="address">@lang('company.label.address')</label>
                                <textarea name="address" class="form-control @classInpError('address')" rows="2" placeholder="@lang('company.placeholder.address')" >{{ old('address', $model->address) }}</textarea>
                                
                                @inpSpanError(['column'=>'address'])
                            </div>

                            <div class="row">
                                <div class="form-group col-lg-6 col-md-12">
                                    <label for="city">@lang('company.label.city')</label>
                                    <input type="text" name="city" class="form-control @classInpError('city')" placeholder="@lang('company.placeholder.city')" value="{{ old('city', $model->city) }}">
                                    
                                    @inpSpanError(['column'=>'city'])
                                </div>
                                <div class="form-group col-lg-6 col-md-12">
                                    <label for="postal_code">@lang('company.label.postal_code')</label>
                                    <input type="text" name="postal_code" class="form-control @classInpError('postal_code')" placeholder="@lang('company.placeholder.postal_code')" value="{{ old('postal_code', $model->postal_code) }}">
                                    
                                    @inpSpanError(['column'=>'postal_code'])
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="row">
                                <div class="form-group col-lg-6 col-md-12">
                                    <label for="npwp">@lang('company.label.npwp')</label>
    
                                    <input type="text" class="form-control @classInpError('npwp')" name="npwp" value="{{ old('npwp', $model->npwp) }}" data-inputmask='"mask": "99.999.999.9-999.999"' data-mask>
                                    
                                    @inpSpanError(['column'=>'npwp'])
                                </div>
                                <div class="form-group col-lg-6 col-md-12">
                                    <label for="license_number">@lang('company.label.license_number')</label>
                                    <input type="text" name="license_number" class="form-control @classInpError('license_number')" placeholder="@lang('company.placeholder.license_number')" value="{{ old('license_number', $model->license_number) }}">
                                    
                                    @inpSpanError(['column'=>'license_number'])
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="form-director_id" class="control-label">
                                    @lang('company.label.director_id')
                                </label>
                                <select name="director_id" id="form-director_id" class="form-control ajax-select2 @classInpError('director_id')" data-url="{{ route('ajax.user') }}" data-placeholder="">
                                    @if (old('director_id') || $model->director_id)
                                    @php
                                        $model->director_id = old('director_id') ?? $model->director_id;
                                    @endphp
                                    <option value="{{ $model->director_id }}" selected>
                                        {{ $model->director->name }}
                                    </option>
                                    @endif
                                </select>

                                @inpSpanError(['column'=>'director_id'])
                            </div>

                            <div class="form-group">
                                <label for="form-pic_payment" class="control-label">
                                    @lang('company.label.pic_payment')
                                </label>
                                <select name="pic_payment" id="form-pic_payment" class="form-control ajax-select2 @classInpError('pic_payment')" data-url="{{ route('ajax.user') }}" data-placeholder="">
                                    @if (old('pic_payment') || $model->pic_payment)
                                    @php
                                        $model->pic_payment = old('pic_payment') ?? $model->pic_payment;
                                    @endphp
                                    <option value="{{ $model->pic_payment }}" selected>
                                        {{ $model->picPayment->name }}
                                    </option>
                                    @endif
                                </select>

                                @inpSpanError(['column'=>'pic_payment'])
                            </div>

                            <div class="form-group">
                                <label for="form-pic_purchase" class="control-label">
                                    @lang('company.label.pic_purchase')
                                </label>
                                <select name="pic_purchase" id="form-pic_purchase" class="form-control ajax-select2 @classInpError('pic_purchase')" data-url="{{ route('ajax.user') }}" data-placeholder="">
                                    @if (old('pic_purchase') || $model->pic_purchase)
                                    @php
                                        $model->pic_purchase = old('pic_purchase') ?? $model->pic_purchase;
                                    @endphp
                                    <option value="{{ $model->pic_purchase }}" selected>
                                        {{ $model->picPurchase->name }}
                                    </option>
                                    @endif
                                </select>

                                @inpSpanError(['column'=>'pic_purchase'])
                            </div>

                            <div class="form-group">
                                <label for="form-pic_warehouse" class="control-label">
                                    @lang('company.label.pic_warehouse')
                                </label>
                                <select name="pic_warehouse" id="form-pic_warehouse" class="form-control ajax-select2 @classInpError('pic_warehouse')" data-url="{{ route('ajax.user') }}" data-placeholder="">
                                    @if (old('pic_warehouse') || $model->pic_warehouse)
                                    @php
                                        $model->pic_warehouse = old('pic_warehouse') ?? $model->pic_warehouse;
                                    @endphp
                                    <option value="{{ $model->pic_warehouse }}" selected>
                                        {{ $model->picWarehouse->name }}
                                    </option>
                                    @endif
                                </select>

                                @inpSpanError(['column'=>'pic_warehouse'])
                            </div>

                            <div class="form-group">
                                <label for="form-format_date" class="control-label">
                                    @lang('company.label.format_date')
                                </label>
                                <select name="format_date" id="form-format_date" class="form-control @classInpError('format_date')" data-url="{{ route('ajax.user') }}" data-placeholder="">
                                    @php
                                        $format_date = old('format_date', $model->format_date);
                                    @endphp
                                    @foreach (trans('company.list.format_date') as $id => $name)
                                        <option value="{{ $id }}" {{ $format_date == $id ? 'selected' : '' }}>
                                            {{ $name }}
                                        </option>
                                    @endforeach
                                </select>

                                @inpSpanError(['column'=>'format_date'])
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <a href="{{ route('master.company.index') }}" class="btn btn-danger btn-loader">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success float-right btn-loader">@lang('global.save')</button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
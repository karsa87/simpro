@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('master.vendor.store') }}">
    @csrf

    @include('master.vendor._form')
</form>
<!--end::Form-->
@endsection

@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('master.vendor.update', $model->id) }}">
    @csrf
    @method('put')
    <input type="hidden" name="id" value="{{ old('id', $model->id) }}" />

    @include('master.vendor._form')
</form>
<!--end::Form-->
@endsection

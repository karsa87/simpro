<div class="row">
    <div class="col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom card-sticky gutter-b" id="kt_page_sticky_card">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">@lang('global.form') @lang('arsip.title')</h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ route('arsip.arsip.index') }}" type="reset" class="btn btn-danger mr-2">@lang('global.cancel')</a>
                    <button type="submit" class="btn btn-success mr-2">@lang('global.save')</button>
                </div>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('arsip.label.name')</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control @classInpError('name')" value="{{ old('name', $model->name) }}" name="name" placeholder="@lang('arsip.placeholder.name')" />

                        @inpSpanError(['column'=>'name'])
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">@lang('arsip.label.description')</label>
                    <div class="col-lg-7">
                        <textarea class="form-control @classInpError('description')" name="description" placeholder="@lang('arsip.placeholder.description')" rows="3">{{ old('description', $model->description) }}</textarea>

                        @inpSpanError(['column'=>'description'])
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    <div class="col-lg-6">
        @livewire('file.file-form', [
            'files' => old('files', $files)
        ])
    </div>
</div>

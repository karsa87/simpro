@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('arsip.arsip.update', $model->id) }}">
    @csrf
    @method('put')
    <input type="hidden" name="id" value="{{ old('id', $model->id) }}" />

    @include('arsip.arsip._form')
</form>
<!--end::Form-->
@endsection

@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('global.detail') @lang('arsip.title')</span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">{{ $model->files->count() }} @lang('arsip.label.total_file')</span>
                </h3>
                <div class="card-toolbar">
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions">
                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ki ki-bold-more-hor"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover py-5">
                                @hasPermission('arsip.arsip.create')
                                <li class="navi-item">
                                    <a href="{{ route('arsip.arsip.create') }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-add-1"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.new')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('arsip.arsip.edit')
                                <li class="navi-item">
                                    <a href="{{ route('arsip.arsip.edit', $model->id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-pen"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.edit')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('arsip.arsip.destroy')
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <a data-href="{{ route('arsip.arsip.destroy', $model->id) }}" data-redirect="{{ route('arsip.arsip.index') }}" class="navi-link btn-delete">
                                        <span class="navi-icon">
                                            <i class="flaticon2-trash"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.delete')</span>
                                    </a>
                                </li>
                                @endhasPermission

                                @hasPermission('arsip.arsip.index')
                                <li class="navi-separator my-3"></li>
                                <li class="navi-item">
                                    <a href="{{ route('arsip.arsip.index', $model->project_id) }}" class="navi-link">
                                        <span class="navi-icon">
                                            <i class="flaticon2-left-arrow-1"></i>
                                        </span>
                                        <span class="navi-text">@lang('global.back')</span>
                                    </a>
                                </li>
                                @endhasPermission
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-4">
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('arsip.label.name')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->name }}</span>
                    </div>
                </div>
                <div class="form-group row my-2">
                    <label class="col-4 col-form-label">@lang('arsip.label.description')</label>
                    <div class="col-8">
                        <span class="form-control-plaintext font-weight-bolder">{{ $model->description }}</span>
                    </div>
                </div>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
    <div class="col-sm-12 col-lg-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">@lang('file.title')</span>
                    <span class="text-muted mt-3 font-weight-bold font-size-sm">@lang('global.list')</span>
                </h3>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-4">
                @include('master.file.component.list_file', ['files' => $model->files ?? []])
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

@push('js')
@endpush

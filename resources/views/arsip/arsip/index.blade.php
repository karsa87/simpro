@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">@lang('arsip.title')</h3>
                </div>
                <div class="card-toolbar">
                    @hasPermission('arsip.arsip.create')
                    <!--begin::Button-->
                    <a href="{{ route('arsip.arsip.create') }}" class="btn btn-primary font-weight-bolder">
                        @lang('global.add')
                    </a>
                    <!--end::Button-->
                    @endhasPermission
                </div>
            </div>
            <div class="card-body">
                <!--begin: Search Form-->
                <!--begin::Search Form-->
                <div class="mb-7">
                    <form id="form-search-arsip" role="form" action="{{ route(request()->route()->getName()) }}">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" name="_k" value="{{ request('_k') }}" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end::Search Form-->
                <!--end: Search Form-->
                <!--begin: Datatable-->
                <table class="table table-hover table-stripped">
                    <thead>
                        <tr>
                            <th class="col-4">
                                @sortablelink('name', trans('arsip.label.name'), request()->except(["token"]),  ['rel' => 'nofollow'])
                            </th>
                            <th class="col-4 text-center">@lang('arsip.label.description')</th>
                            <th class="col-2 text-center">@lang('arsip.label.total_file')</th>
                            <th class="col-2 text-center">@lang('global.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($arsips as $arsip)
                        <tr>
                            <td>{{ $arsip->name }}</td>
                            <td>{{ $arsip->description }}</td>
                            <td class="text-center">{{ $arsip->files->count() }}</td>
                            <td class="text-center">
                                @hasPermission('arsip.arsip.edit')
                                <a title="@lang('global.edit')" href="{{ route('arsip.arsip.edit', $arsip->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-pen"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('arsip.arsip.show')
                                <a title="@lang('global.show_detail')" href="{{ route('arsip.arsip.show', $arsip->id) }}" class="btn btn-icon btn-outline-secondary mr-2" data-toggle="tooltip">
                                    <i class="icon-md flaticon-eye"></i>
                                </a>
                                @endhasPermission

                                @hasPermission('arsip.arsip.destroy')
                                <button title="@lang('global.delete')" data-href="{{ route('arsip.arsip.destroy', $arsip->id) }}" class="btn btn-icon btn-outline-secondary mr-2 btn-delete" data-toggle="tooltip">
                                    <i class="icon-md flaticon2-trash"></i>
                                </button>
                                @endhasPermission
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
            {!! \App\Util\Base\Layout::paging($arsips, 'form-search-arsip') !!}
        </div>
        <!--end::Card-->
    </div>
</div>
@endsection

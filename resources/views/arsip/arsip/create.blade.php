@extends('layouts.admin')

@section('content')
<form method="POST" action="{{ route('arsip.arsip.store') }}">
    @csrf

    @include('arsip.arsip._form')
</form>
<!--end::Form-->
@endsection

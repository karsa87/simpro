require('./bootstrap');

window.CONFIG = {
    path: $('meta[name=path]').attr("content"),
    full_url: $('meta[name=full]').attr("content"),
    token: $('meta[name=csrf-token]').attr("content"),
    base_url: $('meta[name=base]').attr("content"),
    referrer: $('meta[name=referrer]').attr("content"),
    storage_url: $('meta[name=storage_url]').attr("content"),
};

//sweetalert
window.showProgress = function()
{
    let timerInterval
    Swal.fire({
        title: 'Wait!!!',
        // timer: 2000,
        timerProgressBar: true,
        onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
                const content = Swal.getContent()
                if (content) {
                    const b = content.querySelector('b')
                    if (b) {
                        b.textContent = Swal.getTimerLeft()
                    }
                }
            }, 100)
        },
        onClose: () => {
            clearInterval(timerInterval)
        }
    }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
            // console.log('I was closed by the timer')
        }
    })
}

$('[data-mask]').inputmask()

$('.btn-delete').click(function(){
    showSweetAlert($(this));
})

$('.btn-show-alert').click(function(){
    showSweetAlert($(this));
})

function showSweetAlert(el)
{
    var url = el.attr('href');
    if(url == undefined || url == null || url == ""){
        url = el.attr('data-href');
    }

    var redirect_url = el.attr('redirect');
    if(redirect_url == undefined || redirect_url == null || redirect_url == ""){
        redirect_url = el.attr('data-redirect');
    }

    var title = el.attr('data-title');
    if(title == undefined || title == null || title == ""){
        title = "Are you sure?";
    }

    var description = el.attr('data-description');
    if(description == undefined || description == null || description == ""){
        description = "";
    }

    var icon = el.attr('data-icon');
    if(icon == undefined || icon == null || icon == ""){
        icon = "warning";
    }

    var msg_success = el.attr('data-message-success');
    if(msg_success == undefined || msg_success == null || msg_success == ""){
        msg_success = "Success delete";
    }

    var msg_failed = el.attr('data-message-failed');
    if(msg_failed == undefined || msg_failed == null || msg_failed == ""){
        msg_failed = "Failed delete";
    }

    var csrf_token = el.attr('data-csrf-token');
    if(csrf_token == undefined || csrf_token == null || csrf_token == ""){
        csrf_token = "";
    }

    var method = el.attr('data-method');
    if(method == undefined || method == null || method == ""){
        method = "DELETE";
    }

    var param = el.attr('data-param');
    if(param == undefined || param == null || param == ""){
        param = "{}";
    }

    var refresh = el.attr('data-refresh');
    if(refresh == undefined || refresh == null || refresh == ""){
        refresh = 1;
    }

    const swal_confirm = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success ml-1',
          cancelButton: 'btn btn-danger mr-1'
        },
        buttonsStyling: false
    })

    swal_confirm.fire({
        title: title,
        text: description,
        type: icon,
        icon: icon,
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        reverseButtons: true,
        showLoaderOnConfirm: true,
    }).then((result) => {
        if (result.value) {
            if (method == 'GET') {
                window.location.replace(url);
            } else {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': CONFIG.token
                    },
                    url: url,
                    type: method,
                    data: JSON.parse(param),
                    success: function(result){
                        if(result.status == "1" || result.status == 1){
                            swal_confirm.fire({
                                title: 'Deleted!',
                                type: 'success',
                                onClose: () => {
                                    if (refresh == 1 || refresh == '1') {
                                        if (redirect_url != '' && redirect_url != null && redirect_url != undefined) {
                                            window.location.replace(redirect_url);
                                        } else {
                                            location.reload();
                                        }
                                    }
                                }
                            })
                        } else {
                            if (refresh == 1 || refresh == '1') {
                                location.reload();
                            }
                        }
                    },
                    beforeSend: function( xhr ) {
                        showProgress();
                    }
                })
                .always(function(result) {
                    Swal.close();
                })
                .fail(function() {
                    swal_confirm.fire(
                        "Can't delete",
                        msg_failed,
                        'error'
                    )
                });
            }
        }
    })
}

window.Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

window.showAlert = function(message, type)
{
    Toast.fire({
        icon: type,
        title: message
    })
}

//Initialize Select2 Elements
$('.select2').select2({
    allowClear: true,
});

window.load_all_ajax_select2 = function() {
    $(".ajax-select2").each(function(i) {
        $(this).select2({
            placeholder: '',
            allowClear: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': CONFIG.token
                },
                url: $(this).data('url'),
                processResults: function(data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data.items
                    };
                }
            },
            escapeMarkup: function(markup) {
                return markup;
            },
            templateResult: function(data) {
                if (data.html != undefined) {
                    return data.html;
                }
                return data.text;
            },
            templateSelection: function(data) {
              return data.text;
            }
        });
    });
};
load_all_ajax_select2();

Dropzone.autoDiscover = false;
if($(".files-upload").length > 0) {
    $(".files-upload").each(function( index ) {
        var image_uploaded = [];
        var dropfile = $(this);
        var href = dropfile.attr("data-href");
        var thumbnail = dropfile.attr("data-thumbnail");

        var max_file_size = dropfile.attr("data-max-file-size");
        max_file_size = max_file_size == undefined ? 2 : max_file_size;

        var max_file = dropfile.attr("data-max-file");
        max_file = max_file == undefined ? 1 : max_file;

        var accept_file = dropfile.attr("data-accept-file");
        var inp_upload_id = "#" + dropfile.attr("data-inp-id");
        dropfile.removeAttr("data-max-file");
        dropfile.removeAttr("data-max-file-size");
        dropfile.removeAttr("data-accept-file");

        dropfile.dropzone({
            addRemoveLinks: true,
            url: href,
            acceptedFiles: accept_file,
            maxFilesize: max_file_size,
            maxFiles: max_file,
            headers: {
                'X-CSRF-TOKEN': window.CONFIG.token
            },
            init: function() { /* event listeners for removed files from dropzone*/
                var keyword_input = $(inp_upload_id).val();
                if(keyword_input !== "" && keyword_input !== undefined && keyword_input !== null){
                    datum = JSON.parse(keyword_input);
                    var dropzone = this;
                    datum.forEach(function(img_path, index){
                        var mockFile = { name: CONFIG.storage_url + img_path, size: 32000, src:CONFIG.storage_url + img_path };
                        dropzone.options.addedfile.call(dropzone, mockFile);
                        if(thumbnail !== undefined) {
                            dropzone.options.thumbnail.call(dropzone, mockFile, thumbnail);
                        } else {
                            dropzone.options.thumbnail.call(dropzone, mockFile, mockFile.src);
                        }
                        mockFile.previewElement.classList.add('dz-success');
                        mockFile.previewElement.classList.add('dz-complete');
                        image_uploaded.push(img_path);
                    });

                    dropzone.options.maxFiles = dropzone.options.maxFiles - image_uploaded.length;
                }
                this.on('processing', function(file){
                    showAlert("Processing upload file", "info");
                    return true;
                });
                this.on('queuecomplete', function(){
                    return true;
                });
                this.on('success',function(file, result){
                    // changing src of preview element
                    if(thumbnail !== undefined) {
                        file.previewElement.querySelector("img").src = thumbnail;
                    }

                    Swal.fire({
                        title: "Upload",
                        text: "Complete upload file",
                        icon: 'success',
                    });

                    image_uploaded.push(result.path);
                    $(inp_upload_id).val( JSON.stringify(image_uploaded) );

                    return true;
                });


                this.on("error", function(file, message){
                    Swal.fire({
                        title: "Upload",
                        text: message,
                        icon: 'error',
                    });
                });

                this.on("removedfile", function (file) {
                    var max = this.options.maxFiles + 1;
                    if (max <= max_file) {
                        this.options.maxFiles = max;
                    }

                    var index = image_uploaded.indexOf(file.src.replace(CONFIG.storage_url + '/', ''));
                    image_uploaded = image_uploaded.splice(index, 0);
                    $(inp_upload_id).val( JSON.stringify(image_uploaded) );
                });

                this.on('reloadFiles', function() {
                    image_uploaded = [];
                    var keyword_input = $(inp_upload_id).val();
                    if(keyword_input !== "" && keyword_input !== undefined && keyword_input !== null){
                        datum = JSON.parse(keyword_input);
                        var dropzone = this;

                        dropzone.files.forEach(function(file) {
                            file.previewElement.remove();
                        });
                        dropzone.removeAllFiles();
                        $('div.dz-success').remove();

                        datum.forEach(function(img_path, index){
                            var mockFile = { name: CONFIG.storage_url + img_path, size: 32000, src:CONFIG.storage_url + img_path };
                            dropzone.options.addedfile.call(dropzone, mockFile);
                            if(thumbnail !== undefined) {
                                dropzone.options.thumbnail.call(dropzone, mockFile, thumbnail);
                            } else {
                                dropzone.options.thumbnail.call(dropzone, mockFile, mockFile.src);
                            }
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');
                            image_uploaded.push(img_path);
                        });
                        dropzone.options.maxFiles = dropzone.options.maxFiles - image_uploaded.length;
                    }
                    $(inp_upload_id).val( JSON.stringify(image_uploaded) );
                });
            }
        });
    });
}

$('.datetimepicker').datetimepicker({
    todayHighlight: true,
    autoclose: true,
    pickerPosition: 'bottom-left',
    todayBtn: true,
    format: 'yyyy-mm-dd hh:ii'
});

$('.datepicker').datepicker({
    todayHighlight: true,
    autoclose: true,
    pickerPosition: 'bottom-left',
    todayBtn: true,
    format: 'yyyy-mm-dd'
});

$('.date-rangepicker').daterangepicker({
    autoUpdateInput: false,
    locale: {
        format: 'YYYY-MM-DD',
        cancelLabel: 'Clear'
    }
});

$('.date-rangepicker').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
    $(this).change();
});

$('.date-rangepicker').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
    $(this).change();
});

$('.datetime-rangepicker').daterangepicker({
    autoUpdateInput: false,
    timePicker24Hour: true,
    timePicker: true,
    locale: {
        format: 'YYYY-MM-DD HH:mm',
        cancelLabel: 'Clear'
    }
});

$('.datetime-rangepicker').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm') + ' - ' + picker.endDate.format('YYYY-MM-DD HH:mm'));
    $(this).change();
});

$('.datetime-rangepicker').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
    $(this).change();
});

$('[data-toggle="tooltip"]').tooltip()
$(".format-currency").inputmask({ alias : "currency", prefix: '', digits: 2 });
